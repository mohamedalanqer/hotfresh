package com.app;

import android.app.Application;
import android.content.Context;
import android.graphics.fonts.FontFamily;


import androidx.annotation.NonNull;

import com.app.hotfresh.Manager.FontManager;
import com.google.android.material.resources.CancelableFontCallback;

import es.dmoral.toasty.Toasty;


public class MyApplication extends Application {
    private static MyApplication sInstance;
    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

    }
    public static MyApplication getInstance() {
        Toasty.Config.getInstance().setToastTypeface(FontManager.getTypeface(sInstance.getApplicationContext())).setTextSize(11).apply();

        return MyApplication.sInstance;
    }
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }



}
