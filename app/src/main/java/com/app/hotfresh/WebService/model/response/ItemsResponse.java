package com.app.hotfresh.WebService.model.response;


import com.app.hotfresh.Medoles.Category;
import com.app.hotfresh.Medoles.Item;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class ItemsResponse extends RootResponse {
    @SerializedName("Item")
    @Expose
    public List<Item> items ;

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
