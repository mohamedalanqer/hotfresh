package com.app.hotfresh.WebService.model.response;


import com.app.hotfresh.Medoles.OnlineOrder;
import com.app.hotfresh.Medoles.Order;
import com.app.hotfresh.Medoles.OrderDetials;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class OrderResponse extends RootResponse {
    @SerializedName("OnlineOrder")
    @Expose
    public List<OnlineOrder> orders ;


    private String   BillDate ;
    private String   BillTime ;
    private String   BillCode ;
    private String   CustName ;
    private String   CustCode ;
    private String   CustMobile ;
    private String   Total ;
    private String   Tax ;
    private String   CustAddress ;
    private String   OrderStatus ;
    private String   DeliveryType ;
    private String   Lat ;
    private String   Long ;
    private String   PaymentType ;
    private String   ReceivedDate ;
    private String   ReceivedTime ;
    private String   Notes ;
    private String   CharitiesName ;
    private String   CheckID ;
    private String   Depositamount ;
    private String   Company ;
    private String   DeliveryID ;
    private String   DeliveryStatus ;
    private String PayStatus ;
    private String DeliveryAmount ="" ;



    public OrderResponse() {
    }

    public OrderResponse(List<OnlineOrder> orders) {
        this.orders = orders;
    }

    public List<OnlineOrder> getOrders() {
        return orders;
    }

    public void setOrders(List<OnlineOrder> orders) {
        this.orders = orders;
    }

    public String getBillDate() {
        return BillDate;
    }

    public void setBillDate(String billDate) {
        BillDate = billDate;
    }

    public String getBillTime() {
        return BillTime;
    }

    public void setBillTime(String billTime) {
        BillTime = billTime;
    }

    public String getBillCode() {
        return BillCode;
    }

    public void setBillCode(String billCode) {
        BillCode = billCode;
    }

    public String getCustName() {
        return CustName;
    }

    public void setCustName(String custName) {
        CustName = custName;
    }

    public String getCustCode() {
        return CustCode;
    }

    public void setCustCode(String custCode) {
        CustCode = custCode;
    }

    public String getCustMobile() {
        return CustMobile;
    }

    public void setCustMobile(String custMobile) {
        CustMobile = custMobile;
    }

    public String getTotal() {
        return Total;
    }

    public void setTotal(String total) {
        Total = total;
    }

    public String getTax() {
        return Tax;
    }

    public void setTax(String tax) {
        Tax = tax;
    }

    public String getCustAddress() {
        return CustAddress;
    }

    public void setCustAddress(String custAddress) {
        CustAddress = custAddress;
    }

    public String getOrderStatus() {
        return OrderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        OrderStatus = orderStatus;
    }

    public String getDeliveryType() {
        return DeliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        DeliveryType = deliveryType;
    }

    public String getLat() {
        return Lat;
    }

    public void setLat(String lat) {
        Lat = lat;
    }

    public String getLong() {
        return Long;
    }

    public void setLong(String aLong) {
        Long = aLong;
    }

    public String getPaymentType() {
        return PaymentType;
    }

    public void setPaymentType(String paymentType) {
        PaymentType = paymentType;
    }

    public String getReceivedDate() {
        return ReceivedDate;
    }

    public void setReceivedDate(String receivedDate) {
        ReceivedDate = receivedDate;
    }

    public String getReceivedTime() {
        return ReceivedTime;
    }

    public void setReceivedTime(String receivedTime) {
        ReceivedTime = receivedTime;
    }

    public String getNotes() {
        return Notes;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }

    public String getCharitiesName() {
        return CharitiesName;
    }

    public void setCharitiesName(String charitiesName) {
        CharitiesName = charitiesName;
    }

    public String getCheckID() {
        return CheckID;
    }

    public void setCheckID(String checkID) {
        CheckID = checkID;
    }

    public String getDepositamount() {
        return Depositamount;
    }

    public void setDepositamount(String depositamount) {
        Depositamount = depositamount;
    }

    public String getCompany() {
        return Company;
    }

    public void setCompany(String company) {
        Company = company;
    }

    public String getDeliveryID() {
        return DeliveryID;
    }

    public void setDeliveryID(String deliveryID) {
        DeliveryID = deliveryID;
    }

    public String getDeliveryStatus() {
        return DeliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        DeliveryStatus = deliveryStatus;
    }

    public String getPayStatus() {
        return PayStatus;
    }

    public void setPayStatus(String payStatus) {
        PayStatus = payStatus;
    }

    public String getDeliveryAmount() {
        return DeliveryAmount;
    }

    public void setDeliveryAmount(String deliveryAmount) {
        DeliveryAmount = deliveryAmount;
    }
}
