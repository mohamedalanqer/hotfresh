package com.app.hotfresh.WebService.model.response;


import com.google.gson.annotations.SerializedName;

public class DResponse {
    @SerializedName("d")
    private String status;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "DResponse{" +
                "status='" + status + '\'' +
                '}';
    }
}
