package com.app.hotfresh.WebService.model.response;


import com.app.hotfresh.Medoles.Item;
import com.app.hotfresh.Medoles.ItemService;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class ItemsServiceResponse extends RootResponse {
    @SerializedName("ItemsService")
    @Expose
    public List<ItemService> itemServices ;

    public ItemsServiceResponse() {
    }

    public List<ItemService> getItemServices() {
        return itemServices;
    }

    public void setItemServices(List<ItemService> itemServices) {
        this.itemServices = itemServices;
    }
}
