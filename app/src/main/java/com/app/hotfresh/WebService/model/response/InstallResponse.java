package com.app.hotfresh.WebService.model.response;


import com.app.hotfresh.Medoles.Install.Branch;
import com.app.hotfresh.Medoles.Install.Definition;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class InstallResponse extends RootResponse {
    @SerializedName("Branchs")
    @Expose
    public List<Branch> branches ;



    @SerializedName("Definitions")
    @Expose
    public Definition Definition ;

    public InstallResponse() {
    }

    public List<Branch> getBranches() {
        return branches;
    }

    public void setBranches(List<Branch> branches) {
        this.branches = branches;
    }

    public com.app.hotfresh.Medoles.Install.Definition getDefinition() {
        return Definition;
    }

    public void setDefinition(com.app.hotfresh.Medoles.Install.Definition definition) {
        Definition = definition;
    }
}
