package com.app.hotfresh.WebService.model.response;


import com.app.hotfresh.Medoles.Charities.Charitie;
import com.app.hotfresh.Medoles.Order;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class OrdersResponse extends RootResponse {
    @SerializedName("Orders")
    @Expose
    public List<Order> orders ;

    public OrdersResponse() {
    }

    public OrdersResponse(List<Order> orders) {
        this.orders = orders;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
}
