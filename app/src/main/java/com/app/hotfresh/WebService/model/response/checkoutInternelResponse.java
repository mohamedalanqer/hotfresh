package com.app.hotfresh.WebService.model.response;


import com.app.hotfresh.Medoles.Checkouts.resultCheckOut;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class checkoutInternelResponse  extends RootResponse{
    @SerializedName("Resultcode")
    @Expose
    public String Resultcode ;

    @SerializedName("buildNumber")
    @Expose
    public String buildNumber ;

    @SerializedName("timestamp")
    @Expose
    public String timestamp ;

    @SerializedName("ndc")
    @Expose
    public String ndc ;

    @SerializedName("id")
    @Expose
    public String id ;

    public checkoutInternelResponse() {
    }

    public String getResultcode() {
        return Resultcode;
    }

    public void setResultcode(String resultcode) {
        Resultcode = resultcode;
    }

    public String getBuildNumber() {
        return buildNumber;
    }

    public void setBuildNumber(String buildNumber) {
        this.buildNumber = buildNumber;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getNdc() {
        return ndc;
    }

    public void setNdc(String ndc) {
        this.ndc = ndc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
