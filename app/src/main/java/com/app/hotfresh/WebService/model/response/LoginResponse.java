package com.app.hotfresh.WebService.model.response;


import com.app.hotfresh.Medoles.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;



public class LoginResponse  extends RootResponse {
    @SerializedName("user")
    @Expose
    public User user ;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
