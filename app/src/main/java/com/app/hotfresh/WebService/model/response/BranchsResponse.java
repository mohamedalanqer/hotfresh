package com.app.hotfresh.WebService.model.response;


import com.app.hotfresh.Medoles.Install.Branch;
import com.app.hotfresh.Medoles.Item;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class BranchsResponse extends RootResponse {
    @SerializedName("Branchs")
    @Expose
    public List<Branch> branches ;

    public List<Branch> getBranches() {
        return branches;
    }

    public void setBranches(List<Branch> branches) {
        this.branches = branches;
    }
}
