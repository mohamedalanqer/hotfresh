package com.app.hotfresh.WebService.model.response;


import com.app.hotfresh.Medoles.Item;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class TaxValueResponse extends RootResponse {
    @SerializedName("TaxValue")
    @Expose
    public String TaxValue ;

    public String getTaxValue() {
        return TaxValue;
    }

    public void setTaxValue(String taxValue) {
        TaxValue = taxValue;
    }
}
