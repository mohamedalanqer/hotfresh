package com.app.hotfresh.WebService.model.response;


import com.app.hotfresh.Medoles.OnlineOrder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class DistanceResponse  {


    @SerializedName("Distance")
    private String Distance ;
    @SerializedName("AmountPeerKilo")
    private String AmountPeerKilo ;
    @SerializedName("FinalAmount")
    private String FinalAmount ;
    @SerializedName("Status")
    private String Status ="";
    @SerializedName("Message")
    private String Message = "" ;

    public DistanceResponse() {
    }

    public String getDistance() {
        return Distance;
    }

    public void setDistance(String distance) {
        Distance = distance;
    }

    public String getAmountPeerKilo() {
        return AmountPeerKilo;
    }

    public void setAmountPeerKilo(String amountPeerKilo) {
        AmountPeerKilo = amountPeerKilo;
    }

    public String getFinalAmount() {
        return FinalAmount;
    }

    public void setFinalAmount(String finalAmount) {
        FinalAmount = finalAmount;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }
}
