package com.app.hotfresh.WebService.model.response;


import com.app.hotfresh.Medoles.Charities.Charitie;
import com.app.hotfresh.Medoles.Charities.CharitiesItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class CharitiesItemsResponse extends RootResponse {
    @SerializedName("CharitiesItems")
    @Expose
    public List<CharitiesItem> charitiesItems ;

    public CharitiesItemsResponse() {
    }

    public List<CharitiesItem> getCharitiesItems() {
        return charitiesItems;
    }

    public void setCharitiesItems(List<CharitiesItem> charitiesItems) {
        this.charitiesItems = charitiesItems;
    }
}
