package com.app.hotfresh.WebService.model.response;


import com.app.hotfresh.Medoles.Item;
import com.app.hotfresh.Medoles.Nofitication;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class NofiticationsResponse extends RootResponse {
    @SerializedName("Nofitications")
    @Expose
    public List<Nofitication> nofitications ;

    public List<Nofitication> getNofitications() {
        return nofitications;
    }

    public void setNofitications(List<Nofitication> nofitications) {
        this.nofitications = nofitications;
    }
}
