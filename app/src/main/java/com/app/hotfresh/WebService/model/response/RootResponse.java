package com.app.hotfresh.WebService.model.response;


import com.google.gson.annotations.SerializedName;

public class RootResponse     {
    @SerializedName("Status")
    private String status;
    @SerializedName("Message")
    private String Message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }
}
