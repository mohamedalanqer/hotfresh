package com.app.hotfresh.WebService.model.response;


import com.google.gson.annotations.SerializedName;

public class DObjectResponse {
    @SerializedName("d")
    private RootResponse status;

    public RootResponse getStatus() {
        return status;
    }

    public void setStatus(RootResponse status) {
        this.status = status;
    }
}
