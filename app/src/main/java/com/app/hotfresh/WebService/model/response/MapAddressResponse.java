package com.app.hotfresh.WebService.model.response;

import com.app.hotfresh.Medoles.MapAddress.Address;
import com.google.gson.annotations.SerializedName;



public class MapAddressResponse extends RootResponse{
    @SerializedName("display_name")
    String display_name  ;
    @SerializedName("address")
    Address address  ;

    public MapAddressResponse() {
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public  Address getAddress() {
        return address;
    }

    public void setAddress( Address address) {
        this.address = address;
    }
}

