package com.app.hotfresh.WebService;


import com.app.hotfresh.Medoles.CreateOrder.CreateOrder;
import com.app.hotfresh.Medoles.PointsGroups.PointsGroups;
import com.app.hotfresh.Medoles.User;
import com.app.hotfresh.WebService.model.response.BranchsResponse;
import com.app.hotfresh.WebService.model.response.CategoryResponse;
import com.app.hotfresh.WebService.model.response.CharitiesItemsResponse;
import com.app.hotfresh.WebService.model.response.CharitiesResponse;
import com.app.hotfresh.WebService.model.response.DObjectResponse;
import com.app.hotfresh.WebService.model.response.DResponse;
import com.app.hotfresh.WebService.model.response.DistanceResponse;
import com.app.hotfresh.WebService.model.response.HatUserWalletResponse;
import com.app.hotfresh.WebService.model.response.InstallResponse;
import com.app.hotfresh.WebService.model.response.ItemsOffersResponse;
import com.app.hotfresh.WebService.model.response.ItemsResponse;
import com.app.hotfresh.WebService.model.response.ItemsServiceResponse;
import com.app.hotfresh.WebService.model.response.LoginResponse;
import com.app.hotfresh.WebService.model.response.MapAddressResponse;
import com.app.hotfresh.WebService.model.response.NofiticationsResponse;
import com.app.hotfresh.WebService.model.response.OrderResponse;
import com.app.hotfresh.WebService.model.response.OrdersCountsResponse;
import com.app.hotfresh.WebService.model.response.OrdersResponse;
import com.app.hotfresh.WebService.model.response.PackagesResponse;
import com.app.hotfresh.WebService.model.response.RootResponse;
import com.app.hotfresh.WebService.model.response.TaxValueResponse;
import com.app.hotfresh.WebService.model.response.WalletResponse;
import com.app.hotfresh.WebService.model.response.checkoutInternelResponse;
import com.app.hotfresh.WebService.model.response.checkoutsResponse;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;

public interface RetrofitService {


    @FormUrlEncoded
    @POST("GetData.asmx/Login")
    Call<LoginResponse> createLogin(@Field("Mobile") String Mobile, @Field("PassWord") String PassWord,
                                    @Field("DeviceToken") String DeviceToken, @Field("TypeToken") String TypeToken);

    @FormUrlEncoded
    @POST("SendData.asmx/Registration")
    Call<LoginResponse> createRegister(@Field("Mobile") String Mobile, @Field("PassWord") String PassWord,
                                       @Field("CustomerName") String CustomerName,
                                       @Field("DeviceToken") String DeviceToken, @Field("TypeToken") String TypeToken);


    @GET("GetData.asmx/ItemsCategory")
    Call<CategoryResponse> GetCategory();


    @GET("GetData.asmx/GetItemByCategory")
    Call<ItemsResponse> GetItemByCategory(@Query("Category") String Category);

    @FormUrlEncoded
    @POST("GetData.asmx/GetItemByCategory")
    Call<ItemsResponse> GetItemByCategory(@Field("CategoryID") String Category,
                                          @Field("AccessToken") String DeviceToken);

    @GET("GetData.asmx/GetItemService")
    Call<ItemsServiceResponse> GetItemServiceByCodeItem(@Query("ItemCode") String ItemCode); // 152463


    @GET("GetData.asmx/ItemsOffers")
    Call<ItemsOffersResponse> GetItemsOffersByCategory(@Query("CategID") String Categ);


    @FormUrlEncoded
    @POST("GetData.asmx/ItemsOffers")
    Call<ItemsOffersResponse> GetItemsOffersByCategory(@Field("CategID") String Categ, @Field("AccessToken") String TypeToken);


    @GET("GetData.asmx/Last5ItemsOffers")
    Call<ItemsOffersResponse> GetLast5ItemsOffers();


    @GET("GetData.asmx/Charities")
    Call<CharitiesResponse> GetCharities();

    @GET("GetData.asmx/CharitiesItems")
    Call<CharitiesItemsResponse> GetItemsCharitiesItems(@Query("CategID") String Categ, @Query("CharitiesID") String Charities);


    @FormUrlEncoded
    @POST("GetData.asmx/CharitiesItems")
    Call<CharitiesItemsResponse> GetItemsCharitiesItems(@Field("CategID") String Categ, @Field("CharitiesID") String Charities,
                                                        @Field("AccessToken") String DeviceToken);

    // @POST("https://test.oppwa.com/v1/checkouts")
    @POST("https://test.oppwa.com/v1/checkouts")
    Call<checkoutsResponse> requestCheckoutIdxxx(@Query("entityId") String entityId,
                                                 @Query("amount") String amount,
                                                 @Query("currency") String currency,
                                                 @Query("paymentType") String paymentType);


    @FormUrlEncoded
    @POST("Generate_CheckOut.asmx/NewTrn")
    Call<checkoutInternelResponse> requestCheckoutId(@Field("accesstoken") String accesstoken,
                                                     @Field("trnamount") String amount,
                                                     @Field("billcode") String billcode);

    @FormUrlEncoded
    @POST("Generate_CheckOut.asmx/NewTrn")
    Call<checkoutInternelResponse> requestCheckoutId(@Field("accesstoken") String accesstoken,
                                                     @Field("trnamount") String amount,
                                                     @Field("billcode") String billcode,
                                                     @Field("trntype") String trntype);

    @FormUrlEncoded
    @POST("Generate_CheckOut.asmx/CheckOutTrn")
    Call<RootResponse> checkoutsPayment(@Field("trnid") String trnid);

    @FormUrlEncoded
    @POST("Generate_CheckOut.asmx/CheckOutTrn")
    Call<RootResponse> checkoutsPayment(@Field("trnid") String trnid,
                                        @Field("trntype") String trntype);

    //https://test.oppwa.com/v1/checkouts/{id}/payment
    @Headers("Content-Type: application/json")
    @POST("StringService.asmx/NewOrder")
    Call<DResponse> CreateNewOrder(@Body CreateOrder createOrder);


    @Headers("Content-Type: application/json")
    @POST("StringService.asmx/NewCharitiesOrder")
    Call<DResponse> CreateNewCharitiesOrder(@Body CreateOrder createOrder);


    @GET("GetData.asmx/ClientOrders")
    Call<OrdersResponse> GetClientOrderByStatus(@Query("AccessToken") String AccessToken, @Query("orderstatus") String orderstatus);


    @GET("GetData.asmx/OrderDetials")
    Call<OrderResponse> GetOrderDetials(@Query("BillCode") String BillCode);


    @GET("https://nominatim.openstreetmap.org/reverse?format=json")
    Call<MapAddressResponse> GetNameStreet(@Query("lat") Double lat,
                                           @Query("lon") Double lon,
                                           @Query("zoom") int zoom,
                                           @Query("addressdetails") int addressdetails,
                                           @Query("accept-language") String lang);


    @FormUrlEncoded
    @POST("SendData.asmx/RetriveOrder")
    Call<RootResponse> SetRetriveOrder(@Field("BillCode") String BillCode, @Field("RetriveType") String RetriveType,
                                       @Field("Company") String Company);


    @GET("GetData.asmx/GetWalletBalance")
    Call<WalletResponse> GetWalletBalance(@Query("AccessToken") String AccessToken);


    @FormUrlEncoded
    @POST("SendData.asmx/SaveTransaction")
    Call<RootResponse> SaveTransaction(@Field("Mobile") String Mobile,
                                       @Field("PaymentType") String PaymentType,
                                       @Field("Company") String Company,
                                       @Field("Amount") String Amount,
                                       @Field("CheckID") String CheckID);


    @GET("SendData.asmx/ResetMyPassWord")
    Call<RootResponse> ResetMyPassWord(@Query("Mobile") String Mobile);


    @FormUrlEncoded
    @POST("SendData.asmx/SendMobileCodeAndPassWord")
    Call<RootResponse> SendMobileCodeAndPassWord(@Field("ClientMobile") String Mobile);








    @FormUrlEncoded
    @POST("SendData.asmx/UpdatePassWord")
    Call<RootResponse> UpdateUserPassword(@Field("AccessToken") String AccessToken,
                                          @Field("OldPass") String OldPass,
                                          @Field("NewPass") String NewPass);


    @FormUrlEncoded
    @POST("GetData.asmx/CheckMobileActivateCode")
    Call<RootResponse> CheckMobileActivateCode(@Field("AccessToken") String AccessToken,
                                               @Field("Code") String Code);


    @FormUrlEncoded
    @POST("SendData.asmx/SendMobileActivateCode")
    Call<RootResponse> SendMobileActivateCode(@Field("AccessToken") String AccessToken);


    @FormUrlEncoded
    @POST("SendData.asmx/UpdateCustInfo")
    Call<RootResponse> UpdateProfile(@Field("AccessToken") String AccessToken,
                                     @Field("CustomerName") String CustomerName,
                                     @Field("Address") String Address);


    @FormUrlEncoded
    @POST("SendData.asmx/UpdateCustInfo")
    Call<RootResponse> UpdateProfile(@Field("AccessToken") String AccessToken,
                                     @Field("CustomerName") String CustomerName,
                                     @Field("Mobile") String Mobile,
                                     @Field("Address") String Address);

    @FormUrlEncoded
    @POST("GetData.asmx/GetUserInfoByAccessToken")
    Call<LoginResponse> GetMyProfile(@Field("AccessToken") String AccessToken);


    @GET("GetData.asmx/Notifications")
    Call<NofiticationsResponse> GetNofitications(@Query("AccessToken") String AccessToken);


    @GET("GetData.asmx/Settings")
    Call<InstallResponse> GetInstall();


    @GET("GetData.asmx/UserStatment")
    Call<HatUserWalletResponse> GetUserStatmentPayment(@Query("AccessToken") String AccessToken);


    @GET("GetData.asmx/OrdersCounts")
    Call<OrdersCountsResponse> OrdersCounts(@Query("AccessToken") String AccessToken);


    @FormUrlEncoded
    @POST("SendData.asmx/UpdateCheckID")
    Call<RootResponse> UpdateCheckID(@Field("BillCode") String BillCode,
                                     @Field("CheckID") String CheckID);


    @GET("GetData.asmx/PointsGroups")
    Call<PackagesResponse> GetPackagesList();


    @FormUrlEncoded
    @POST("SendData.asmx/ConvertPointToMoney")
    Call<RootResponse> ConvertPointToMoney(@Field("AccessToken") String AccessToken);

    @FormUrlEncoded
    @POST("SendData.asmx/GetUserDistance")
    Call<DistanceResponse> GetUserDistanceMap(@Field("UserLat") String UserLat ,
                                               @Field("UserLong") String UserLong ,
                                               @Field("BranchID") String BranchID );

    @FormUrlEncoded
    @POST("SendData.asmx/CheckSmsCodeForUpdatePassWord")
    Call<RootResponse> ResetPassword(@Field("Mobile") String Mobile,
                                     @Field("NewPassWord") String NewPassWord,
                                     @Field("ConfirmPassWord") String ConfirmPassWord,
                                     @Field("Code") String Code);

}
