package com.app.hotfresh.WebService.model.response;


import com.app.hotfresh.Medoles.Category;
import com.app.hotfresh.Medoles.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class CategoryResponse extends RootResponse {
    @SerializedName("Category")
    @Expose
    public List<Category> category ;

    public List<Category> getCategory() {
        return category;
    }

    public void setCategory(List<Category> category) {
        this.category = category;
    }
}
