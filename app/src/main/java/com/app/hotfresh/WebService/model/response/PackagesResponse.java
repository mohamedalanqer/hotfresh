package com.app.hotfresh.WebService.model.response;


import com.app.hotfresh.Medoles.Nofitication;
import com.app.hotfresh.Medoles.PointsGroups.PointsGroups;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class PackagesResponse extends RootResponse {
    @SerializedName("PointsGroups")
    @Expose
    public List<PointsGroups> pointsGroups ;

    public List<PointsGroups> getPointsGroups() {
        return pointsGroups;
    }

    public void setPointsGroups(List<PointsGroups> pointsGroups) {
        this.pointsGroups = pointsGroups;
    }
}
