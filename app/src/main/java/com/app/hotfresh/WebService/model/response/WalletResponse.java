package com.app.hotfresh.WebService.model.response;


import com.app.hotfresh.Medoles.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class WalletResponse extends RootResponse {
    @SerializedName("Balance")
    @Expose
    public String Balance ;

    public String getBalance() {
        return Balance;
    }

    public void setBalance(String balance) {
        Balance = balance;
    }
}
