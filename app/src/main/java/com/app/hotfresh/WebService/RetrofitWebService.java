package com.app.hotfresh.WebService;

import android.content.Context;
import android.util.Log;

import com.app.hotfresh.CallBack.InternetAvailableCallback;
import com.app.hotfresh.Manager.AppLanguage;
import com.app.hotfresh.Manager.RootManager;
import com.app.hotfresh.Manager.getUserDetials;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitWebService {
    private static final String TAG = RetrofitWebService.class.getSimpleName();
    private static final Map<String, RetrofitService> mServices = new HashMap<>();
    private Context context;


    private RetrofitWebService(String url, final Context context) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        this.context = context;
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .writeTimeout(3, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES)
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request request = null;
                        request = chain.request().newBuilder()
                                .addHeader("Accept", "application/json")
                                .addHeader("Content-Type", "multipart/form-data")
                                .addHeader("X-localization", AppLanguage.getLanguage(context) + "")
                                .build();

                        //}
                        return chain.proceed(request);
                    }
                })
                .build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        mServices.put(url, retrofit.create(RetrofitService.class));
    }

    private RetrofitWebService(String url) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        this.context = context;
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .writeTimeout(3, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES)
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {

                        Request request = null;
                        request = chain.request().newBuilder()
                                // .addHeader("Content-Type", "application/json")
                                .addHeader("Authorization", "Bearer OGFjN2E0Yzk3MWFhYmYwZTAxNzFiM2QyZTQxOTE0MTl8UFFTZlk4NjN0Tg==")
                                .build();


                        return chain.proceed(request);
                    }
                })
                .build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(url)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        mServices.put(url, retrofit.create(RetrofitService.class));
    }

    public static RetrofitService getService(Context context) {
        // TODO change url
        String url = RootManager.URL;

        if (null == mServices.get(url)) {
            new RetrofitWebService(url, context);
        }
        return mServices.get(url);
    }

    public static RetrofitService getServiceOut() {
        // TODO change url
        String url = RootManager.URL;
        new RetrofitWebService(url);
        return mServices.get(url);
    }

    public static <T> T getBody(Callback<?> callback, Call call, Response<?> response) {
        //TODO change response
        /*GasResponse r = (GasResponse) response.body();
        if (null != r) {
            return (T) r;
        }*/
        Log.d(TAG, response.toString());
        callback.onFailure(call, new Throwable("Body is null or statusToasts not \"done\""));
        return null;
    }

    public static void log(Throwable t) {
        Log.e(TAG, null != t.getMessage() ? t.getMessage() : t.toString());
    }
}
