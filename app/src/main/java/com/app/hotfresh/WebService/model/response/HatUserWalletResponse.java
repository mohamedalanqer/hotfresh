package com.app.hotfresh.WebService.model.response;


import com.app.hotfresh.Medoles.Checkouts.HatUserWallet;
import com.app.hotfresh.Medoles.Install.Branch;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class HatUserWalletResponse extends RootResponse {
    @SerializedName("HatUserWallet")
    @Expose
    public List<HatUserWallet> hatUserWallets ;

    public List<HatUserWallet> getHatUserWallets() {
        return hatUserWallets;
    }

    public void setHatUserWallets(List<HatUserWallet> hatUserWallets) {
        this.hatUserWallets = hatUserWallets;
    }
}
