package com.app.hotfresh.WebService.model.response;


import com.app.hotfresh.Medoles.Category;
import com.app.hotfresh.Medoles.Charities.Charitie;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class CharitiesResponse extends RootResponse {
    @SerializedName("Charities")
    @Expose
    public List<Charitie> charities ;

    public List<Charitie> getCharities() {
        return charities;
    }

    public void setCharities(List<Charitie> charities) {
        this.charities = charities;
    }
}
