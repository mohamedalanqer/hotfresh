package com.app.hotfresh.WebService.model.response;


import com.app.hotfresh.Medoles.Category;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class OrdersCountsResponse extends RootResponse {

    public String Certain ;
    public String Uncertain ;
    public String Ready ;
    public String Recevied ;
    public String Updated ;

    public String getCertain() {
        return Certain;
    }

    public String getUncertain() {
        return Uncertain;
    }

    public String getReady() {
        return Ready;
    }

    public String getRecevied() {
        return Recevied;
    }

    public String getUpdated() {
        return Updated;
    }
}
