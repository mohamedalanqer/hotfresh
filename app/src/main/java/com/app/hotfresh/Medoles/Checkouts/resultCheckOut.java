package com.app.hotfresh.Medoles.Checkouts;

public class resultCheckOut {
    private String code ;
    private String description ;

    public resultCheckOut() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
