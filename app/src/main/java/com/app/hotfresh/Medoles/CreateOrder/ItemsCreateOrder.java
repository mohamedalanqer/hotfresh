package com.app.hotfresh.Medoles.CreateOrder;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ItemsCreateOrder {
    @SerializedName("ItemCode")
    private String ItemCode;
    @SerializedName("Qty")
    private String Qty;
    @SerializedName("ItemsService")
   private List<ItemsServiceCreateOrder> ItemsService;

    public ItemsCreateOrder() {
    }

    public ItemsCreateOrder(String itemCode, String qty, List<ItemsServiceCreateOrder> itemsService) {
        ItemCode = itemCode;
        Qty = qty;
        ItemsService = itemsService;
    }

    public ItemsCreateOrder(String itemCode, String qty) {
        ItemCode = itemCode;
        Qty = qty;
       // ItemsService = itemsService;
    }
}
