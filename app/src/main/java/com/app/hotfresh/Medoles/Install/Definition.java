package com.app.hotfresh.Medoles.Install;

public class Definition {

    private String FaceBook;
    private String Instgram;
    private String Twitter;
    private String CloseAppStatus;
    private String CloseMsg;
    private String Privacy;
    private String AboutUs;
    private String Tax;
    private String PointsToRyial;


    private String PaymentTypeVisa = "True";
    private String PaymentTypeCash = "True";
    private String PaymentTypeWallet = "True";
    private String DeliveryTypeOnShop = "True";
    private String DeliveryTypeDelivery = "True";


    public Definition() {
    }

    public String getFaceBook() {
        return FaceBook;
    }

    public void setFaceBook(String faceBook) {
        FaceBook = faceBook;
    }

    public String getInstgram() {
        return Instgram;
    }

    public void setInstgram(String instgram) {
        Instgram = instgram;
    }

    public String getTwitter() {
        return Twitter;
    }

    public void setTwitter(String twitter) {
        Twitter = twitter;
    }

    public String getCloseAppStatus() {
        return CloseAppStatus;
    }

    public void setCloseAppStatus(String closeAppStatus) {
        CloseAppStatus = closeAppStatus;
    }

    public String getCloseMsg() {
        return CloseMsg;
    }

    public void setCloseMsg(String closeMsg) {
        CloseMsg = closeMsg;
    }

    public String getPrivacy() {
        return Privacy;
    }

    public void setPrivacy(String privacy) {
        Privacy = privacy;
    }

    public String getAboutUs() {
        return AboutUs;
    }

    public void setAboutUs(String aboutUs) {
        AboutUs = aboutUs;
    }

    public String getTax() {
        return Tax;
    }

    public void setTax(String tax) {
        Tax = tax;
    }


    public String getPaymentTypeVisa() {
        return PaymentTypeVisa;
    }

    public void setPaymentTypeVisa(String paymentTypeVisa) {
        PaymentTypeVisa = paymentTypeVisa;
    }

    public String getPaymentTypeCash() {
        return PaymentTypeCash;
    }

    public void setPaymentTypeCash(String paymentTypeCash) {
        PaymentTypeCash = paymentTypeCash;
    }

    public String getPaymentTypeWallet() {
        return PaymentTypeWallet;
    }

    public void setPaymentTypeWallet(String paymentTypeWallet) {
        PaymentTypeWallet = paymentTypeWallet;
    }

    public String getDeliveryTypeOnShop() {
        return DeliveryTypeOnShop;
    }

    public void setDeliveryTypeOnShop(String deliveryTypeOnShop) {
        DeliveryTypeOnShop = deliveryTypeOnShop;
    }

    public String getDeliveryTypeDelivery() {
        return DeliveryTypeDelivery;
    }

    public void setDeliveryTypeDelivery(String deliveryTypeDelivery) {
        DeliveryTypeDelivery = deliveryTypeDelivery;
    }

    public String getPointsToRyial() {
        return PointsToRyial;
    }

    public void setPointsToRyial(String pointsToRyial) {
        PointsToRyial = pointsToRyial;
    }
}
