package com.app.hotfresh.Medoles;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {
    @SerializedName("Name")
    @Expose
    private String Name;

    @SerializedName("Code")
    @Expose
    private String Code;

    @SerializedName("Mobile")
    @Expose
    private String Mobile;

    @SerializedName("Address")
    @Expose
    private String Address;

    @SerializedName("AccessToken")
    @Expose
    private String AccessToken;


    @SerializedName("Balance")
    @Expose
    private String Balance;

    @SerializedName("MobileActivate")
    @Expose
    private String MobileActivate;


    @SerializedName("ActivatedCode")
    @Expose
    private String ActivatedCode;

    @SerializedName("PointsDeposits")
    @Expose
    private String PointsDeposits;

    @SerializedName("PointsWithdrawals")
    @Expose
    private String PointsWithdrawals;

    @SerializedName("PointBalance")
    @Expose
    private String PointBalance;

    @SerializedName("PointGroupID")
    @Expose
    private String PointGroupID;

    @SerializedName("PointGroupsName")
    @Expose
    private String PointGroupsName;

    @SerializedName("NextGroupLimit")
    @Expose
    private String NextGroupLimit ="";


    @SerializedName("GroupEndDate")
    @Expose
    private String GroupEndDate ="";



    public User() {
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getMobile() {
        return Mobile;
    }
    public String getMobileString() {


        if (!TextUtils.isEmpty(Mobile)) {

            if (Mobile.startsWith("00966")) {
                Mobile = Mobile.substring(5);
                if(Mobile.length() == 9){
                    if(Mobile.startsWith("5")){
                        Mobile="0"+Mobile;
                    }
                }

            }
            if (Mobile.startsWith("966")) {
                Mobile = Mobile.substring(3);
                if(Mobile.length() == 9){
                    if(Mobile.startsWith("5")){
                        Mobile="0"+Mobile;
                    }
                }
            }

        }
        return Mobile;
    }
    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getAccessToken() {
        return AccessToken;
    }

    public void setAccessToken(String accessToken) {
        AccessToken = accessToken;
    }

    public String getBalance() {
        return Balance;
    }

    public void setBalance(String balance) {
        Balance = balance;
    }

    public String getMobileActivate() {
        return MobileActivate;
    }

    public void setMobileActivate(String mobileActivate) {
        MobileActivate = mobileActivate;
    }

    public String getActivatedCode() {
        return ActivatedCode;
    }

    public void setActivatedCode(String activatedCode) {
        ActivatedCode = activatedCode;
    }

    public String getPointsDeposits() {
        return PointsDeposits;
    }

    public void setPointsDeposits(String pointsDeposits) {
        PointsDeposits = pointsDeposits;
    }

    public String getPointsWithdrawals() {
        return PointsWithdrawals;
    }

    public void setPointsWithdrawals(String pointsWithdrawals) {
        PointsWithdrawals = pointsWithdrawals;
    }

    public String getPointBalance() {
        return PointBalance;
    }

    public void setPointBalance(String pointBalance) {
        PointBalance = pointBalance;
    }

    public String getPointGroupID() {
        return PointGroupID;
    }

    public void setPointGroupID(String pointGroupID) {
        PointGroupID = pointGroupID;
    }

    public String getPointGroupsName() {
        return PointGroupsName;
    }

    public void setPointGroupsName(String pointGroupsName) {
        PointGroupsName = pointGroupsName;
    }

    @Override
    public String toString() {
        return "User{" +
                "Name='" + Name + '\'' +
                ", Code='" + Code + '\'' +
                ", Mobile='" + Mobile + '\'' +
                ", Address='" + Address + '\'' +
                ", AccessToken='" + AccessToken + '\'' +
                ", Balance='" + Balance + '\'' +
                ", MobileActivate='" + MobileActivate + '\'' +
                ", ActivatedCode='" + ActivatedCode + '\'' +
                ", PointsDeposits='" + PointsDeposits + '\'' +
                ", PointsWithdrawals='" + PointsWithdrawals + '\'' +
                ", PointBalance='" + PointBalance + '\'' +
                ", PointGroupID='" + PointGroupID + '\'' +
                ", PointGroupsName='" + PointGroupsName + '\'' +
                '}';
    }

    public String getNextGroupLimit() {
        return NextGroupLimit;
    }

    public void setNextGroupLimit(String nextGroupLimit) {
        NextGroupLimit = nextGroupLimit;
    }

    public String getGroupEndDate() {
        return GroupEndDate;
    }

    public void setGroupEndDate(String groupEndDate) {
        GroupEndDate = groupEndDate;
    }
}

