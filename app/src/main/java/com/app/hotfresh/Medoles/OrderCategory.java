package com.app.hotfresh.Medoles;

public class OrderCategory {
    private int CategoryID ;
    private String TagName ;
    private String CategoryName ;
    private boolean IsSelect = false  ;
    private String Count = "0"  ;
    private boolean IsAnimationTrue = false  ;

    public OrderCategory() {
    }

    public String getTagName() {
        return TagName;
    }

    public OrderCategory(int categoryID, String tagName, String categoryName) {
        CategoryID = categoryID;
        TagName = tagName;
        CategoryName = categoryName;
    }

    public OrderCategory(int categoryID, String tagName, String categoryName, String count) {
        CategoryID = categoryID;
        TagName = tagName;
        CategoryName = categoryName;
        Count = count;
    }

    public void setTagName(String tagName) {
        TagName = tagName;
    }

    public OrderCategory(int categoryID, String categoryName) {
        CategoryID = categoryID;
        CategoryName = categoryName;
    }

    public int getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(int categoryID) {
        CategoryID = categoryID;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public boolean isSelect() {
        return IsSelect;
    }

    public void setSelect(boolean select) {
        IsSelect = select;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String count) {
        Count = count;
    }

    public boolean isAnimationTrue() {
        return IsAnimationTrue;
    }

    public void setAnimationTrue(boolean animationTrue) {
        IsAnimationTrue = animationTrue;
    }
}
