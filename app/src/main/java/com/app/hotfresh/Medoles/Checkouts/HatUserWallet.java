package com.app.hotfresh.Medoles.Checkouts;

public class HatUserWallet {
    private String TrnType ;
    private String Amount ;
    private String CheckID ;
    private String Mobile ;
    private String Company ;
    private String BillCode ;
    private String PaymentType ;
    private String Date ;
    private String Time ;

    public HatUserWallet() {
    }

    public String getTrnType() {
        return TrnType;
    }

    public void setTrnType(String trnType) {
        TrnType = trnType;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getCheckID() {
        return CheckID;
    }

    public void setCheckID(String checkID) {
        CheckID = checkID;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getCompany() {
        return Company;
    }

    public void setCompany(String company) {
        Company = company;
    }

    public String getBillCode() {
        return BillCode;
    }

    public void setBillCode(String billCode) {
        BillCode = billCode;
    }

    public String getPaymentType() {
        return PaymentType;
    }

    public void setPaymentType(String paymentType) {
        PaymentType = paymentType;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }
}
