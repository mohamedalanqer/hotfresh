package com.app.hotfresh.Medoles.CreateOrder;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CreateOrder {
    @SerializedName("AccessToken")
    @Expose
    private String AccessToken;

    @SerializedName("DeliveryType")
    @Expose
    private String DeliveryType;

    @SerializedName("Lat")
    @Expose
    private String Lat;

    @SerializedName("Long")
    @Expose
    private String aLong;



    @SerializedName("PaymentType")
    @Expose
    private String PaymentType;


    @SerializedName("ReceivedDate")
    @Expose
    private String ReceivedDate;


    @SerializedName("ReceivedTime")
    @Expose
    private String ReceivedTime;


    @SerializedName("Notes")
    @Expose
    private String Notes;


    @SerializedName("CheckID")
    @Expose
    private String CheckID;


    @SerializedName("Company")
    @Expose
    private String Company;


    @SerializedName("ClientAddress")
    @Expose
    private String ClientAddress;



    @SerializedName("depositamount")
    @Expose
    private String depositamount;



    @SerializedName("CharitiesName")
    @Expose
    private String CharitiesName;









    @SerializedName("Items")
    @Expose
    private List<ItemsCreateOrder> Items;



    @SerializedName("DeliveryAmount")
    @Expose
    private String DeliveryAmount;


    public CreateOrder() {
    }

    public CreateOrder(String accessToken,   List<ItemsCreateOrder> items) {
        AccessToken = accessToken;
        Items = items;
    }

    @Override
    public String toString() {
        return "CreateOrder{" +
                "AccessToken='" + AccessToken + '\'' +
                ", Items=" + Items +
                '}';
    }

    public String getAccessToken() {
        return AccessToken;
    }

    public void setAccessToken(String accessToken) {
        AccessToken = accessToken;
    }

    public String getDeliveryType() {
        return DeliveryType;
    }

    public void setDeliveryType(String deliveryType) {
        DeliveryType = deliveryType;
    }

    public String getLat() {
        return Lat;
    }

    public void setLat(String lat) {
        Lat = lat;
    }

    public String getaLong() {
        return aLong;
    }

    public void setaLong(String aLong) {
        this.aLong = aLong;
    }

    public String getPaymentType() {
        return PaymentType;
    }

    public void setPaymentType(String paymentType) {
        PaymentType = paymentType;
    }

    public String getReceivedDate() {
        return ReceivedDate;
    }

    public void setReceivedDate(String receivedDate) {
        ReceivedDate = receivedDate;
    }

    public String getReceivedTime() {
        return ReceivedTime;
    }

    public void setReceivedTime(String receivedTime) {
        ReceivedTime = receivedTime;
    }

    public String getNotes() {
        return Notes;
    }

    public void setNotes(String notes) {
        Notes = notes;
    }

    public String getCheckID() {
        return CheckID;
    }

    public void setCheckID(String checkID) {
        CheckID = checkID;
    }

    public String getCompany() {
        return Company;
    }

    public void setCompany(String company) {
        Company = company;
    }

    public String getClientAddress() {
        return ClientAddress;
    }

    public void setClientAddress(String clientAddress) {
        ClientAddress = clientAddress;
    }

    public List<ItemsCreateOrder> getItems() {
        return Items;
    }

    public void setItems(List<ItemsCreateOrder> items) {
        Items = items;
    }

    public String getDepositamount() {
        return depositamount;
    }

    public void setDepositamount(String depositamount) {
        this.depositamount = depositamount;
    }

    public String getCharitiesName() {
        return CharitiesName;
    }

    public void setCharitiesName(String charitiesName) {
        CharitiesName = charitiesName;
    }

    public String getDeliveryAmount() {
        return DeliveryAmount;
    }

    public void setDeliveryAmount(String deliveryAmount) {
        DeliveryAmount = deliveryAmount;
    }
}
