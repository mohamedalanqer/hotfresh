package com.app.hotfresh.Medoles.PointsGroups;

public class PointsGroups {
    private String GroupID ;
    private String GroupName ;
    private String ConvertPointsRate ;
    private String GroupImage ;
    private boolean SelectGroup  =false;
    private String Period ="" ;



    public PointsGroups() {
    }

    public String getGroupID() {
        return GroupID;
    }

    public void setGroupID(String groupID) {
        GroupID = groupID;
    }

    public String getGroupName() {
        return GroupName;
    }

    public void setGroupName(String groupName) {
        GroupName = groupName;
    }

    public String getConvertPointsRate() {
        return ConvertPointsRate;
    }

    public void setConvertPointsRate(String convertPointsRate) {
        ConvertPointsRate = convertPointsRate;
    }

    public String getGroupImage() {
        return GroupImage;
    }

    public void setGroupImage(String groupImage) {
        GroupImage = groupImage;
    }


    public boolean isSelectGroup() {
        return SelectGroup;
    }

    public void setSelectGroup(boolean selectGroup) {
        SelectGroup = selectGroup;
    }

    public String getPeriod() {
        return Period;
    }

    public void setPeriod(String period) {
        Period = period;
    }
}
