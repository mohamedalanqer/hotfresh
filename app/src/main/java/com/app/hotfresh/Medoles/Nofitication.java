package com.app.hotfresh.Medoles;

public class Nofitication {
    private String   Mobile ;
    private String   Title ;
    private String   Message ;
    private String   NotifDate ;
    private String   NotifTime ;
    private String   BillCode ;

    public Nofitication() {
    }

    public Nofitication(String mobile, String title, String message, String notifDate, String notifTime, String billCode) {
        Mobile = mobile;
        Title = title;
        Message = message;
        NotifDate = notifDate;
        NotifTime = notifTime;
        BillCode = billCode;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getNotifDate() {
        return NotifDate;
    }

    public void setNotifDate(String notifDate) {
        NotifDate = notifDate;
    }

    public String getNotifTime() {
        return NotifTime;
    }

    public void setNotifTime(String notifTime) {
        NotifTime = notifTime;
    }

    public String getBillCode() {
        return BillCode;
    }

    public void setBillCode(String billCode) {
        BillCode = billCode;
    }
}
