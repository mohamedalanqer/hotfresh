package com.app.hotfresh.Medoles;

public class SettingItem {
    private int id ;
    private String name ;
    private int ResImg ;

    public SettingItem(int id, String name, int resImg) {
        this.id = id;
        this.name = name;
        ResImg = resImg;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getResImg() {
        return ResImg;
    }

    public void setResImg(int resImg) {
        ResImg = resImg;
    }
}
