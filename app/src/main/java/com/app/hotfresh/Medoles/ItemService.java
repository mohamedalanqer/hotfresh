package com.app.hotfresh.Medoles;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Random;

@Entity
public class ItemService {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "IdItem")

    private int IdItem;

    @ColumnInfo(name = "ServiceNameAra")
    private String ServiceNameAra;
    @ColumnInfo(name = "ServiceNameEng")
    private String ServiceNameEng;
    @ColumnInfo(name = "ServicePrice")
    private String ServicePrice;
    @ColumnInfo(name = "ServiceCode")
    private String ServiceCode;


    @ColumnInfo(name = "ItemCode")
    private String ItemCode;

    private boolean isIs_selected;

    public ItemService() {
    }

    public String getServiceNameAra() {
        return ServiceNameAra;
    }

    public void setServiceNameAra(String serviceNameAra) {
        ServiceNameAra = serviceNameAra;
    }

    public String getServiceNameEng() {
        return ServiceNameEng;
    }

    public void setServiceNameEng(String serviceNameEng) {
        ServiceNameEng = serviceNameEng;
    }

    public String getServicePrice() {
        return ServicePrice;
    }

    public void setServicePrice(String servicePrice) {
        ServicePrice = servicePrice;
    }

    public String getServiceCode() {
        return ServiceCode;
    }

    public void setServiceCode(String serviceCode) {
        ServiceCode = serviceCode;
    }

    public boolean isIs_selected() {
        return isIs_selected;
    }

    public void setIs_selected(boolean is_selected) {
        isIs_selected = is_selected;
    }

    public int getIdItem() {
        return IdItem;
    }

    public void setIdItem(int idItem) {
        IdItem = idItem;
    }

    public String getItemCode() {
        return ItemCode;
    }

    public void setItemCode(String itemCode) {
        ItemCode = itemCode;
    }

    @Override
    public String toString() {
        return "ItemService{" +
                "IdItem=" + IdItem +
                ", ServiceNameAra='" + ServiceNameAra + '\'' +
                ", ServiceNameEng='" + ServiceNameEng + '\'' +
                ", ServicePrice='" + ServicePrice + '\'' +
                ", ServiceCode='" + ServiceCode + '\'' +
                ", ItemCode='" + ItemCode + '\'' +
                ", isIs_selected=" + isIs_selected +
                '}';
    }
}
