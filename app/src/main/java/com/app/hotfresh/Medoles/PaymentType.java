package com.app.hotfresh.Medoles;

public class PaymentType {
    private int CategoryID ;
    private String TagName ;

    private String CategoryName ;
    private int ResImg ;
    private boolean ItemSelect = false ;

    public PaymentType() {
    }

    public String getTagName() {
        return TagName;
    }

    public PaymentType(int categoryID, String tagName, String categoryName, int resImg, boolean itemSelect) {
        CategoryID = categoryID;
        TagName = tagName;
        CategoryName = categoryName;
        ResImg = resImg;
        ItemSelect = itemSelect;
    }

    public PaymentType(int categoryID, String tagName, String categoryName, int resImg) {
        CategoryID = categoryID;
        TagName = tagName;
        CategoryName = categoryName;
        ResImg = resImg;
    }

    public PaymentType(int categoryID, String tagName, String categoryName) {
        CategoryID = categoryID;
        TagName = tagName;
        CategoryName = categoryName;
    }

    public void setTagName(String tagName) {
        TagName = tagName;
    }

    public PaymentType(int categoryID, String categoryName) {
        CategoryID = categoryID;
        CategoryName = categoryName;
    }

    public int getCategoryID() {
        return CategoryID;
    }

    public void setCategoryID(int categoryID) {
        CategoryID = categoryID;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public int getResImg() {
        return ResImg;
    }

    public void setResImg(int resImg) {
        ResImg = resImg;
    }

    public boolean isItemSelect() {
        return ItemSelect;
    }

    public void setItemSelect(boolean itemSelect) {
        ItemSelect = itemSelect;
    }
}
