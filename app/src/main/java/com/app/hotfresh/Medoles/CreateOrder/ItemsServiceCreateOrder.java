package com.app.hotfresh.Medoles.CreateOrder;

import com.google.gson.annotations.SerializedName;

public class ItemsServiceCreateOrder {
    @SerializedName("ItemCode")
    private String ItemCode ;
    @SerializedName("ServiceName")
    private String ServiceName ;
    @SerializedName("Qty")
    private String Qty ;

    public ItemsServiceCreateOrder() {
    }

    public ItemsServiceCreateOrder(String itemCode, String serviceName, String qty) {
        ItemCode = itemCode;
        ServiceName = serviceName;
        Qty = qty;
    }
}
