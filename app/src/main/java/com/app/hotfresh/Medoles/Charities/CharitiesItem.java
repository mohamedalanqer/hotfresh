package com.app.hotfresh.Medoles.Charities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class CharitiesItem {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "IdItem")
    private int IdItem;

    @ColumnInfo(name = "Code")
    private String Code;
    @ColumnInfo(name = "Name")
    private String Name;
    @ColumnInfo(name = "Charities")
    private String Charities;
    @ColumnInfo(name = "CharitiesPrice")
    private String CharitiesPrice;
    @ColumnInfo(name = "ImgPath")
    private String ImgPath;
    @ColumnInfo(name = "Quantity")
    private int Quantity;

    @ColumnInfo(name = "Points")
    private String Points = "0";


    @ColumnInfo(name = "OfferPoints")
    private String OfferPoints = "0";



    @ColumnInfo(name = "TotalPoints")
    private String TotalPoints  = "0";

    public CharitiesItem() {
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getCharities() {
        return Charities;
    }

    public void setCharities(String charities) {
        Charities = charities;
    }

    public String getCharitiesPrice() {
        return CharitiesPrice;
    }

    public void setCharitiesPrice(String charitiesPrice) {
        CharitiesPrice = charitiesPrice;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int quantity) {
        Quantity = quantity;
    }

    public String getImgPath() {
        return ImgPath;
    }

    public void setImgPath(String imgPath) {
        ImgPath = imgPath;
    }

    public int getIdItem() {
        return IdItem;
    }

    public void setIdItem(int idItem) {
        IdItem = idItem;
    }

    public String getPoints() {
        return Points;
    }

    public void setPoints(String points) {
        Points = points;
    }

    public String getOfferPoints() {
        return OfferPoints;
    }

    public void setOfferPoints(String offerPoints) {
        OfferPoints = offerPoints;
    }

    public String getTotalPoints() {
        return TotalPoints;
    }

    public void setTotalPoints(String totalPoints) {
        TotalPoints = totalPoints;
    }
}
