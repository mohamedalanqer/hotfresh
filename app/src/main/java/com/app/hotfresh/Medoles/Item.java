package com.app.hotfresh.Medoles;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.Relation;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Entity
public class Item {
    @PrimaryKey
    @ColumnInfo(name = "Id")
    private int Id;
    @ColumnInfo(name = "Code")
    private String Code;
    @ColumnInfo(name = "Name")
    private String Name;
    @ColumnInfo(name = "NameEng")
    private String NameEng;
    @ColumnInfo(name = "Categ")
    private String Categ;
    @ColumnInfo(name = "Total")
    private String Total;
    @ColumnInfo(name = "Quan")
    private String Quan;
    @ColumnInfo(name = "ImgPath")
    private String ImgPath;

    @ColumnInfo(name = "Quantity")
    private int Quantity;
    @Ignore
    private boolean IsAddToCart;

    @Ignore
    private List<ItemService> itemServices = new ArrayList<>();


    @Ignore
    private String OfferDiscount;
    @Ignore
    private String OfferEndDate;

    @ColumnInfo(name = "Points")
    private String Points = "0";

    @ColumnInfo(name = "OfferPoints")
    private String OfferPoints  = "0";

    @ColumnInfo(name = "TotalPoints")
    private String TotalPoints  = "0";


    public Item() {
        this.Quantity = 0;
        this.IsAddToCart = false;
        this.itemServices = new ArrayList<>();
        if (Code != null)
            this.Id = Integer.parseInt(Code);
        else
            this.Id = new Random().nextInt((1000 - 1) + 1) + 1;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getNameEng() {
        return NameEng;
    }

    public void setNameEng(String nameEng) {
        NameEng = nameEng;
    }

    public String getCateg() {
        return Categ;
    }

    public void setCateg(String categ) {
        Categ = categ;
    }

    public String getTotal() {
        return Total;
    }

    public void setTotal(String total) {
        Total = total;
    }

    public String getQuan() {
        return Quan;
    }

    public void setQuan(String quan) {
        Quan = quan;
    }

    public String getImgPath() {
        return ImgPath;
    }

    public void setImgPath(String imgPath) {
        ImgPath = imgPath;
    }

    public int getQuantity() {
        return Quantity;
    }

    public void setQuantity(int quantity) {
        Quantity = quantity;
    }

    public boolean isAddToCart() {
        return IsAddToCart;
    }

    public void setAddToCart(boolean addToCart) {
        IsAddToCart = addToCart;
    }

    public List<ItemService> getItemServices() {
        return itemServices;
    }

    public void setItemServices(List<ItemService> itemServices) {
        this.itemServices = itemServices;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getOfferDiscount() {
        return OfferDiscount;
    }

    public void setOfferDiscount(String offerDiscount) {
        OfferDiscount = offerDiscount;
    }

    public String getOfferEndDate() {
        return OfferEndDate;
    }

    public void setOfferEndDate(String offerEndDate) {
        OfferEndDate = offerEndDate;
    }

    @Override
    public String toString() {
        return "Item{" +
                "Id=" + Id +
                ", Code='" + Code + '\'' +
                ", Name='" + Name + '\'' +
                ", NameEng='" + NameEng + '\'' +
                ", Categ='" + Categ + '\'' +
                ", Total='" + Total + '\'' +
                ", Quan='" + Quan + '\'' +
                ", ImgPath='" + ImgPath + '\'' +
                ", Quantity=" + Quantity +
                ", IsAddToCart=" + IsAddToCart +
                ", itemServices=" + itemServices +
                ", OfferDiscount='" + OfferDiscount + '\'' +
                ", OfferEndDate='" + OfferEndDate + '\'' +
                '}';
    }


    public String getPoints() {
        return Points;
    }

    public void setPoints(String points) {
        Points = points;
    }

    public String getOfferPoints() {
        return OfferPoints;
    }

    public void setOfferPoints(String offerPoints) {
        OfferPoints = offerPoints;
    }

    public String getTotalPoints() {
        return TotalPoints;
    }

    public void setTotalPoints(String totalPoints) {
        TotalPoints = totalPoints;
    }
}
