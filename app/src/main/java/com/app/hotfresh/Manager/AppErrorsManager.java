package com.app.hotfresh.Manager;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.hotfresh.CallBack.InstallCallback;
import com.app.hotfresh.CallBack.InstallTwoStringCallback;
import com.app.hotfresh.CallBack.InstallTwoValueCallback;
import com.app.hotfresh.R;
import com.app.hotfresh.Ui.Adapters.RecyclerBranch;
import com.app.hotfresh.Ui.Adapters.RecyclerPayamentDialog;
import com.app.hotfresh.Ui.Adapters.RecyclerServiceDialog;
import com.bumptech.glide.Glide;


public class AppErrorsManager {


    public static void showErrorDialog(Context context, String error) {
        if (context != null) {
            AlertDialog alertDialogBuilder = new AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.error))
                    .setMessage(error)
                    .setPositiveButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    }).show();

            TextView textView = alertDialogBuilder.findViewById(android.R.id.message);
            TextView alertTitle = alertDialogBuilder.findViewById(R.id.alertTitle);
            Button button1 = alertDialogBuilder.findViewById(android.R.id.button1);
            Button button2 = alertDialogBuilder.findViewById(android.R.id.button2);
            if (textView != null) {
                textView.setLineSpacing(0, 1.5f);
            }

        }
    }


    public static void showErrorDialog(Activity context, String error, DialogInterface.OnClickListener okClickListener) {
        if (context != null && !context.isFinishing()) {
            AlertDialog alertDialogBuilder = new AlertDialog.Builder(context)
                    .setTitle(R.string.error).setMessage(error).setPositiveButton(R.string.ok, okClickListener).show();

            TextView textView = alertDialogBuilder.findViewById(android.R.id.message);
            TextView alertTitle = alertDialogBuilder.findViewById(R.id.alertTitle);
            Button button1 = alertDialogBuilder.findViewById(android.R.id.button1);
            Button button2 = alertDialogBuilder.findViewById(android.R.id.button2);
            if (textView != null) {
                textView.setLineSpacing(0, 1.5f);
            }

        }
    }


    public static void showSuccessDialog(Activity context, String title, String error,
                                         DialogInterface.OnClickListener okClickListener,
                                         DialogInterface.OnClickListener cancelClickListener) {
        if (context != null && !context.isFinishing()) {
            AlertDialog alertDialogBuilder = new AlertDialog.Builder(context)
                    .setTitle(title)
                    .setMessage(error)
                    .setPositiveButton(context.getString(R.string.confirm), okClickListener).setNegativeButton(context.getString(R.string.cancel), cancelClickListener)
                    .show();

            TextView textView = alertDialogBuilder.findViewById(android.R.id.message);
            TextView alertTitle = alertDialogBuilder.findViewById(R.id.alertTitle);
            Button button1 = alertDialogBuilder.findViewById(android.R.id.button1);
            Button button2 = alertDialogBuilder.findViewById(android.R.id.button2);
            if (textView != null) {
                textView.setLineSpacing(0, 1.5f);
            }

        }
    }

    public static void showDescriptionDialog(Activity context, String title, String error,
                                             DialogInterface.OnClickListener okClickListener) {
        if (context != null && !context.isFinishing()) {
            AlertDialog alertDialogBuilder = new AlertDialog.Builder(context)
                    .setTitle(title)
                    .setMessage(error)
                    .setPositiveButton(context.getString(R.string.close), okClickListener)
                    .show();

            TextView textView = alertDialogBuilder.findViewById(android.R.id.message);
            TextView alertTitle = alertDialogBuilder.findViewById(R.id.alertTitle);
            Button button1 = alertDialogBuilder.findViewById(android.R.id.button1);
            Button button2 = alertDialogBuilder.findViewById(android.R.id.button2);
            if (textView != null) {
                textView.setLineSpacing(0, 1.5f);
            }

        }
    }


    public static void showSuccessDialog(Activity context, String error,
                                         DialogInterface.OnClickListener okClickListener) {
        if (context != null && !context.isFinishing()) {
            AlertDialog alertDialogBuilder = new AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.info))
                    .setMessage(error)
                    .setPositiveButton(context.getString(R.string.ok), okClickListener)
                    .show();

            TextView textView = alertDialogBuilder.findViewById(android.R.id.message);
            TextView alertTitle = alertDialogBuilder.findViewById(R.id.alertTitle);
            Button button1 = alertDialogBuilder.findViewById(android.R.id.button1);
            Button button2 = alertDialogBuilder.findViewById(android.R.id.button2);
            if (textView != null) {
                textView.setLineSpacing(0, 1.5f);
            }

        }
    }


    public static void showCustomErrorDialog(Activity context, String title, String error) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_custom_error_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        TextView text_title_error_dialog = dialoglayout.findViewById(R.id.text_title_error_dialog);
        TextView text_body_error_dialog = dialoglayout.findViewById(R.id.text_body_error_dialog);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);
        FontManager.applyFont(context, dialoglayout);

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
            }
        });

        if (!title.isEmpty())
            text_title_error_dialog.setText(title);
        text_body_error_dialog.setText(error);
        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    public static void InternetUnAvailableDialog(Activity context, String title, String error, InstallCallback installCallback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_no_internet_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        TextView text_title_error_dialog = dialoglayout.findViewById(R.id.text_title_error_dialog);
        TextView text_body_error_dialog = dialoglayout.findViewById(R.id.text_body_error_dialog);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);
        TextView retry_dialog = dialoglayout.findViewById(R.id.retry_dialog);
        FontManager.applyFont(context, dialoglayout);

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
                installCallback.onStatusDone("close");
            }
        });

        retry_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
                installCallback.onStatusDone("retry");
            }
        });

        if (!title.isEmpty())
            text_title_error_dialog.setText(title);
        text_body_error_dialog.setText(error);
        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    public static void showAddBalanceDialog(Activity context, InstallTwoStringCallback installCallback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_add_balance_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        EditText edit_dialog_amount = dialoglayout.findViewById(R.id.edit_dialog_amount);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);
        ImageView img_visa = dialoglayout.findViewById(R.id.img_visa);
        ImageView img_mada = dialoglayout.findViewById(R.id.img_mada);
        ImageView img_master = dialoglayout.findViewById(R.id.img_master);
        TextView text_hint_select_type = dialoglayout.findViewById(R.id.text_hint_select_type);


        final String[] TypeSelect = {RootManager.TypeSelectNotSelect};
        img_visa.setOnClickListener(view -> {
            TypeSelect[0] = RootManager.TypeSelectVisa;
            img_visa.setSelected(true);
            img_master.setSelected(false);
            img_mada.setSelected(false);
        });
        img_master.setOnClickListener(view -> {
            TypeSelect[0] = RootManager.TypeSelectVisa;
            img_visa.setSelected(false);
            img_master.setSelected(true);
            img_mada.setSelected(false);
        });
        img_mada.setOnClickListener(view -> {
            TypeSelect[0] = RootManager.TypeSelectMada;
            img_visa.setSelected(false);
            img_master.setSelected(false);
            img_mada.setSelected(true);
        });
        FontManager.applyFont(context, dialoglayout);

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edit_dialog_amount.getText().toString().trim())) {
                    edit_dialog_amount.setError(context.getResources().getString(R.string.field_required));
                    edit_dialog_amount.requestFocus();
                } else if (TextUtils.equals(TypeSelect[0], RootManager.TypeSelectNotSelect)) {
                    text_hint_select_type.setError(context.getResources().getString(R.string.field_required));
                    text_hint_select_type.requestFocus();
                } else {
                    String amount = edit_dialog_amount.getText().toString().trim();
                    installCallback.onStatusDone(amount, TypeSelect[0]);
                    alertDialogBuilder.dismiss();
                }
            }
        });


        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    public static void showSelectTypePayDialog(Activity context, InstallTwoStringCallback installCallback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_type_pay_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);

        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);
        ImageView img_visa = dialoglayout.findViewById(R.id.img_visa);
        ImageView img_mada = dialoglayout.findViewById(R.id.img_mada);
        ImageView img_master = dialoglayout.findViewById(R.id.img_master);
        TextView text_hint_select_type = dialoglayout.findViewById(R.id.text_hint_select_type);


        final String[] TypeSelect = {RootManager.TypeSelectNotSelect};
        img_visa.setOnClickListener(view -> {
            TypeSelect[0] = RootManager.TypeSelectVisa;
            img_visa.setSelected(true);
            img_master.setSelected(false);
            img_mada.setSelected(false);
        });
        img_master.setOnClickListener(view -> {
            TypeSelect[0] = RootManager.TypeSelectVisa;
            img_visa.setSelected(false);
            img_master.setSelected(true);
            img_mada.setSelected(false);
        });
        img_mada.setOnClickListener(view -> {
            TypeSelect[0] = RootManager.TypeSelectMada;
            img_visa.setSelected(false);
            img_master.setSelected(false);
            img_mada.setSelected(true);
        });
        FontManager.applyFont(context, dialoglayout);

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.equals(TypeSelect[0], RootManager.TypeSelectNotSelect)) {
                    text_hint_select_type.setError(context.getResources().getString(R.string.field_required));
                    text_hint_select_type.requestFocus();
                } else {
                    String amount = "0";
                    installCallback.onStatusDone(amount, TypeSelect[0]);
                    alertDialogBuilder.dismiss();
                }
            }
        });


        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    public static void showResetPasswordDialog(Activity context, InstallCallback installCallback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_reset_pass_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        EditText edit_dialog_amount = dialoglayout.findViewById(R.id.edit_dialog_amount);
        TextView text_title_error_dialog = dialoglayout.findViewById(R.id.text_title_error_dialog);
        TextView text_message_edittext = dialoglayout.findViewById(R.id.text_message_edittext);
        text_title_error_dialog.setText("" + context.getResources().getString(R.string.ForgetPassword));
        edit_dialog_amount.setHint("" + context.getResources().getString(R.string.phone));
        edit_dialog_amount.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(context, R.drawable.ic_mobile_start), null, null, null);

        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);
        FontManager.applyFont(context, dialoglayout);

        Drawable top = context.getResources().getDrawable(R.drawable.ic_password_reset);
        text_title_error_dialog.setCompoundDrawablesWithIntrinsicBounds(null, top, null, null);

        text_message_edittext.setVisibility(View.VISIBLE);
        close_dialog.setText("" + context.getResources().getString(R.string.send));
        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edit_dialog_amount.getText().toString().trim())) {
                    edit_dialog_amount.setError(context.getResources().getString(R.string.field_required));
                    edit_dialog_amount.requestFocus();
                } else {
                    String mobile = edit_dialog_amount.getText().toString().trim();
                    installCallback.onStatusDone(mobile);
                    alertDialogBuilder.dismiss();
                }
            }
        });


        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    public static void showActiveMobileDialog(Activity context, String key, String mobile, InstallCallback installCallback, final AlertDialog alertDialogBuilder) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_avtive_mobile_layout, null);
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        EditText edit_dialog_amount = dialoglayout.findViewById(R.id.edit_dialog_amount);
        TextView text_title_error_dialog = dialoglayout.findViewById(R.id.text_title_error_dialog);
        TextView text_message_edittext = dialoglayout.findViewById(R.id.text_message_edittext);
        text_title_error_dialog.setText("" + context.getResources().getString(R.string.active_mobile));
        edit_dialog_amount.setHint("" + context.getResources().getString(R.string.Enter_verificationcode));
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);
        TextView newCode_text = dialoglayout.findViewById(R.id.newCode_text);
        TextView text_my_mobile = dialoglayout.findViewById(R.id.text_my_mobile);
        text_my_mobile.setVisibility(View.VISIBLE);

        String msg = context.getResources().getString(R.string.msg_mobile_my);
        msg = msg.replace("$", mobile);
        text_my_mobile.setText("" + msg);

        newCode_text.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
        FontManager.applyFont(context, dialoglayout);
        if (!TextUtils.isEmpty(key)) {
            edit_dialog_amount.setText(key);
        }

        text_message_edittext.setText("" + context.getResources().getString(R.string.Enter_verificationcode_message));

        text_message_edittext.setVisibility(View.GONE);
        close_dialog.setText("" + context.getResources().getString(R.string.Check));
        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edit_dialog_amount.getText().toString().trim())) {
                    edit_dialog_amount.setError(context.getResources().getString(R.string.field_required));
                    edit_dialog_amount.requestFocus();
                } else {
                    String mobile = edit_dialog_amount.getText().toString().trim();
                    installCallback.onStatusDone(mobile);
                    alertDialogBuilder.dismiss();
                }
            }
        });


        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }
        newCode_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.equals(newCode_text.getText().toString(), context.getResources().getString(R.string.Requestnewcode))) {
                    startTimer(context, 60000, newCode_text);
                    installCallback.onStatusDone("NewCode");

                }

            }
        });
    }

    public static void startTimer(final Context context, long time, final TextView textView) {
        textView.setEnabled(false);
        CountDownTimer counter = new CountDownTimer(time, 1) {
            public void onTick(long millisUntilDone) {

                Log.d("counter_label", "Counter text should be changed");
                long Secand = millisUntilDone / 1000;
                long minutes = (Secand % 3600) / 60;
                long seconds = Secand % 60;

                String timeString = String.format("%02d:%02d", minutes, seconds);
                textView.setText("00:" + timeString + "");
            }

            public void onFinish() {
                textView.setText(context.getResources().getString(R.string.Requestnewcode));
                textView.setEnabled(true);

            }
        }.start();
    }

    public static void showCustomErrorDialogNotCancel(Activity context, String title, String error, final InstallCallback installCallback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_custom_error_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        TextView text_title_error_dialog = dialoglayout.findViewById(R.id.text_title_error_dialog);
        TextView text_body_error_dialog = dialoglayout.findViewById(R.id.text_body_error_dialog);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);

        FontManager.applyFont(context, dialoglayout);

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
                installCallback.onStatusDone("Close");
            }
        });


        if (!title.isEmpty())
            text_title_error_dialog.setText(title);
        text_body_error_dialog.setText(error);
        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.setCancelable(false);
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    public static void showCustomErrorDialogTwoAction(Activity context, String title, String error, final InstallCallback installCallback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_custom_error_two_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        TextView text_title_error_dialog = dialoglayout.findViewById(R.id.text_title_error_dialog);
        TextView text_body_error_dialog = dialoglayout.findViewById(R.id.text_body_error_dialog);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);
        TextView text_accept = dialoglayout.findViewById(R.id.text_accept);

        FontManager.applyFont(context, dialoglayout);

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
                installCallback.onStatusDone("Close");
            }
        });
        text_accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
                installCallback.onStatusDone("yes");
            }
        });

        if (!title.isEmpty())
            text_title_error_dialog.setText(title);
        text_body_error_dialog.setText(error);
        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.setCancelable(false);
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    public static void showCreateSuccessDialogNotCancel(Activity context, String title, String error, final InstallCallback installCallback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_create_success_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        TextView text_title_error_dialog = dialoglayout.findViewById(R.id.text_title_error_dialog);
        TextView text_body_error_dialog = dialoglayout.findViewById(R.id.text_body_error_dialog);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);

        FontManager.applyFont(context, dialoglayout);

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
                installCallback.onStatusDone("Close");
            }
        });


        if (!title.isEmpty())
            text_title_error_dialog.setText(title);
        text_body_error_dialog.setText(error);
        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.setCancelable(false);
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    public static void showLogOutsDialogNotCancel(Activity context, String title, String error, final InstallCallback installCallback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_logout_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        TextView text_title_error_dialog = dialoglayout.findViewById(R.id.text_title_error_dialog);
        TextView text_body_error_dialog = dialoglayout.findViewById(R.id.text_body_error_dialog);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);
        TextView close_logout = dialoglayout.findViewById(R.id.close_logout);

        FontManager.applyFont(context, dialoglayout);

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
                installCallback.onStatusDone("Close");
            }
        });
        close_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
                installCallback.onStatusDone("Exit");
            }
        });

        if (!title.isEmpty())
            text_title_error_dialog.setText(title);
        text_body_error_dialog.setText(error);
        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.setCancelable(true);
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    public static void showWalletSuccessDialogNotCancel(Activity context, String title, String error, final InstallCallback installCallback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_wallet_success_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        TextView text_title_error_dialog = dialoglayout.findViewById(R.id.text_title_error_dialog);
        TextView text_body_error_dialog = dialoglayout.findViewById(R.id.text_body_error_dialog);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);

        FontManager.applyFont(context, dialoglayout);

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
                installCallback.onStatusDone("Close");
            }
        });


        if (!title.isEmpty())
            text_title_error_dialog.setText(title);
        text_body_error_dialog.setText(error);
        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.setCancelable(false);
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    public static void showAddressListDialog(Activity context, RecyclerServiceDialog adapter, final InstallCallback callback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_filter_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        RecyclerView recycler_view = dialoglayout.findViewById(R.id.recycler_view);

        TextView text_yes = dialoglayout.findViewById(R.id.text_yes);
        ImageView img_close = dialoglayout.findViewById(R.id.img_close);
        TextView text_body_title_dialog = dialoglayout.findViewById(R.id.text_body_title_dialog);

        FontManager.applyFont(context, dialoglayout);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setAdapter(adapter);

        text_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onStatusDone("yes");
                alertDialogBuilder.dismiss();
            }
        });


        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onStatusDone("no");
                alertDialogBuilder.dismiss();

            }
        });

        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    public static void showServiceListDialog(Activity context, RecyclerServiceDialog adapter, String btn_str, final InstallCallback callback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_filter_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        RecyclerView recycler_view = dialoglayout.findViewById(R.id.recycler_view);

        TextView text_yes = dialoglayout.findViewById(R.id.text_yes);
        ImageView img_close = dialoglayout.findViewById(R.id.img_close);
        TextView text_body_title_dialog = dialoglayout.findViewById(R.id.text_body_title_dialog);

        FontManager.applyFont(context, dialoglayout);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setAdapter(adapter);

        text_yes.setText("" + btn_str);
        text_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onStatusDone("yes");
                alertDialogBuilder.dismiss();
            }
        });


        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onStatusDone("no");
                alertDialogBuilder.dismiss();

            }
        });

        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    public static void showPayamentListDialog(Activity context, RecyclerPayamentDialog adapter, final InstallCallback callback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_filter_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        RecyclerView recycler_view = dialoglayout.findViewById(R.id.recycler_view);

        TextView text_yes = dialoglayout.findViewById(R.id.text_yes);
        ImageView img_close = dialoglayout.findViewById(R.id.img_close);
        TextView text_body_title_dialog = dialoglayout.findViewById(R.id.text_body_title_dialog);
        text_body_title_dialog.setText("" + context.getString(R.string.Paymentmethod));
        FontManager.applyFont(context, dialoglayout);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setAdapter(adapter);
        text_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onStatusDone("yes");
                alertDialogBuilder.dismiss();
            }
        });


        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onStatusDone("no");
                alertDialogBuilder.dismiss();

            }
        });

        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    public static void showBranchListDialog(Activity context, RecyclerBranch adapter, final InstallCallback callback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_filter_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        RecyclerView recycler_view = dialoglayout.findViewById(R.id.recycler_view);

        TextView text_yes = dialoglayout.findViewById(R.id.text_yes);
        ImageView img_close = dialoglayout.findViewById(R.id.img_close);
        TextView text_body_title_dialog = dialoglayout.findViewById(R.id.text_body_title_dialog);
        text_body_title_dialog.setText("" + context.getString(R.string.selectBranchs));
        FontManager.applyFont(context, dialoglayout);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setAdapter(adapter);
        text_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onStatusDone("yes");
                alertDialogBuilder.dismiss();
            }
        });


        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onStatusDone("no");
                alertDialogBuilder.dismiss();

            }
        });

        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    public static void showRemoveItemDialog(Activity context, String title, String body, final InstallCallback callback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_remove_item_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);

        TextView text_yes = dialoglayout.findViewById(R.id.text_yes);
        TextView text_no = dialoglayout.findViewById(R.id.text_no);
        ImageView img_close = dialoglayout.findViewById(R.id.img_close);
        TextView text_body_title_dialog = dialoglayout.findViewById(R.id.text_body_title_dialog);
        TextView text_body_error_dialog = dialoglayout.findViewById(R.id.text_body_error_dialog);

        FontManager.applyFont(context, dialoglayout);

        text_body_title_dialog.setText("" + title);
        text_body_error_dialog.setText("" + body);


        text_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onStatusDone("yes");
                alertDialogBuilder.dismiss();
            }
        });

        text_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onStatusDone("no");
                alertDialogBuilder.dismiss();

            }
        });

        img_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onStatusDone("no");
                alertDialogBuilder.dismiss();

            }
        });

        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    public static void showCustomErrorDialogTop(Activity context, String title, String error, final InstallCallback installCallback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_notfication_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        TextView text_title_error_dialog = dialoglayout.findViewById(R.id.text_title_error_dialog);
        TextView text_body_error_dialog = dialoglayout.findViewById(R.id.text_body_error_dialog);
        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);

        FontManager.applyFont(context, dialoglayout);

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
                installCallback.onStatusDone("Close");
            }
        });


        if (!title.isEmpty())
            text_title_error_dialog.setText(title);
        text_body_error_dialog.setText(error);
        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.setCancelable(false);
                    WindowManager.LayoutParams wmlp = alertDialogBuilder.getWindow().getAttributes();

                    wmlp.gravity = Gravity.TOP;
                    wmlp.x = 0;   //x position
                    wmlp.y = 0;   //y position
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    public static void showLoginAfterDialogNotCancel(Activity context, String title, String error, final InstallCallback installCallback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_login_after_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        TextView text_title_error_dialog = dialoglayout.findViewById(R.id.text_title_error_dialog);
        TextView text_body_error_dialog = dialoglayout.findViewById(R.id.text_body_error_dialog);


        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);
        TextView login_dialog = dialoglayout.findViewById(R.id.login_dialog);

        FontManager.applyFont(context, dialoglayout);

        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
                installCallback.onStatusDone("Close");
            }
        });
        login_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
                installCallback.onStatusDone("login");
            }
        });

        if (!title.isEmpty())
            text_title_error_dialog.setText(title);
        text_body_error_dialog.setText(error);
        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.setCancelable(false);
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

    public static void showMobileNotActiveDialog(Activity context, String title, String error, final InstallCallback installCallback) {
        LayoutInflater inflater = context.getLayoutInflater();
        View dialoglayout = inflater.inflate(R.layout.dialog_login_after_layout, null);
        final AlertDialog alertDialogBuilder = new AlertDialog.Builder(context).create();
        alertDialogBuilder.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        alertDialogBuilder.setView(dialoglayout);
        TextView text_title_error_dialog = dialoglayout.findViewById(R.id.text_title_error_dialog);
        TextView text_body_error_dialog = dialoglayout.findViewById(R.id.text_body_error_dialog);
        ImageView img_dialog = dialoglayout.findViewById(R.id.img_dialog);
        img_dialog.setImageResource(R.drawable.ic_actice_mobile_msg);


        TextView close_dialog = dialoglayout.findViewById(R.id.close_dialog);
        TextView login_dialog = dialoglayout.findViewById(R.id.login_dialog);

        FontManager.applyFont(context, dialoglayout);
        login_dialog.setText("" + context.getResources().getString(R.string.active_mobile));
        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
                installCallback.onStatusDone("Close");
            }
        });
        login_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogBuilder.dismiss();
                installCallback.onStatusDone("active");
            }
        });

        if (!title.isEmpty())
            text_title_error_dialog.setText(title);
        text_body_error_dialog.setText(error);
        if (!((Activity) context).isFinishing()) {
            if (context != null)
                try {
                    alertDialogBuilder.setCancelable(false);
                    alertDialogBuilder.show();
                } catch (WindowManager.BadTokenException e) {
                    //use a log message
                }
        }

    }

}
