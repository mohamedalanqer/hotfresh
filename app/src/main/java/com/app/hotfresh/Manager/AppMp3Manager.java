package com.app.hotfresh.Manager;

import android.content.Context;
import android.media.MediaPlayer;

public class AppMp3Manager {
    MediaPlayer mediaPlayer;
    Context context;

    public AppMp3Manager(Context context) {
        this.context = context;
    }

    public void stopPlaying() {
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.release();
            mediaPlayer = null;
        }
    }

    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    public void setMediaPlayer(MediaPlayer mediaPlayer) {
        this.mediaPlayer = mediaPlayer;
    }

    public void palyNotificationMusic(int winning) {
        if (mediaPlayer != null) {
            if (!mediaPlayer.isPlaying()) {
                mediaPlayer = MediaPlayer.create(context, winning);
                mediaPlayer.setLooping(false);
                mediaPlayer.start();
            }
        }else{
            mediaPlayer = MediaPlayer.create(context, winning);
            mediaPlayer.setLooping(false);
            mediaPlayer.start();
        }
    }
}