package com.app.hotfresh.Manager;


import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;

import com.app.hotfresh.R;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RootManager {

    public static final String URL = "http://cn.sa/HatService/New/";
    public static final String URL_INSTALL = "http://cn.sa/HatService/";
    public static final String IMAGE_URL = "http://cn.sa/HatService/";

    public static final String DataBaseName = "database-hotfresh-v3";

    public static final String Stats_uncertain = "Uncertain";
    public static final String Stats_certain = "Certain";
    public static final String Stats_ready = "Ready";
    public static final String Stats_updated = "Updated";
    public static final String Stats_recevied = "Recevied";
    public static final String Stats_OnWay = "OnWay";

    public static final String TypeSelectVisa = "visa";
    public static final String TypeSelectMada = "mada";
    public static final String TypeSelectNotSelect = "NotSelect";


    public static final String Transaction_Deposit = "Deposit";
    public static final String Transaction_Withdraw = "Withdraw";
    public static final String Transaction_Archives = "Archives";


    public static final String PaymentType_cash = "Cash";
    public static final String PaymentType_visa = "Visa";
    public static final String PaymentType_wallet = "Wallet";
    public static final String PaymentType_PayCash = "PayCash";
    public static final String PaymentType_Global = "Global";


    public static final String DeliveryType_Cash = "Cash";
    public static final String DeliveryType_OnHand = "OnHand";
    public static final String Company_Select = "company_select";
    public static final String SMS_PROVIDER_NAME = "HAT"; // "056-7194-607";
    public static final int SMS_PATTERN_NUMBER = 4;


    public static final String Info_Tag_Privacy = "privacy";
    public static final String Info_Tag_About = "about";
    public static final String Info_Tag_Branch = "branch";
    public static final String Info_Tag_Contact = "contact";

    private static final String Result_codes_successfully = "/^(000\\.000\\.|000\\.100\\.1|000\\.[36])/";
    private static final String Result_codes_successfully_manually = "/^(000\\.400\\.0[^3]|000\\.400\\.100)/";


    public static final boolean IsResultCodesSuccessfullyPay(String code) {
        List<String> stringList = new ArrayList<>();
        stringList.add("000.000.000");
        stringList.add("000.000.100");
        stringList.add("000.100.110");
        stringList.add("000.100.111");
        stringList.add("000.100.112");
        stringList.add("000.300.000");
        stringList.add("000.300.100");
        stringList.add("000.300.101");
        stringList.add("000.300.102");
        stringList.add("000.310.100");
        stringList.add("000.310.101");
        stringList.add("000.310.110");
        stringList.add("000.600.000");


        stringList.add("000.400.000");
        stringList.add("000.400.010");
        stringList.add("000.400.020");
        stringList.add("000.400.040");
        stringList.add("000.400.050");
        stringList.add("000.400.060");
        stringList.add("000.400.070");
        stringList.add("000.400.080");
        stringList.add("000.400.090");
        stringList.add("000.400.100");

        for (int i = 0; i < stringList.size(); i++) {
            if (TextUtils.equals(code, stringList.get(i))) {
                return true;
            }
        }

        return false;

    }

    public static final String GetMessageStatusByTagName(String tagName, Context context, ImageView img_status) {
        Log.e("tagtag", tagName + "");
        String message = "";
        if (TextUtils.equals(tagName, Stats_certain)) {
            message = context.getResources().getString(R.string.m_certain);
            img_status.setImageResource(R.drawable.img_status_1);
        } else if (TextUtils.equals(tagName, Stats_uncertain)) {
            message = context.getResources().getString(R.string.m_uncertain);
            img_status.setImageResource(R.drawable.img_status_2);
        } else if (TextUtils.equals(tagName, Stats_ready)) {
            message = context.getResources().getString(R.string.m_ready);
            img_status.setImageResource(R.drawable.img_status_3);
        } else if (TextUtils.equals(tagName, Stats_updated)) {
            message = context.getResources().getString(R.string.m_updated);
            img_status.setImageResource(R.drawable.img_status_4);
        } else if (TextUtils.equals(tagName, Stats_OnWay)) {
            message = context.getResources().getString(R.string.m_OnWay);
            img_status.setImageResource(R.drawable.ic_onway);
        } else if (TextUtils.equals(tagName, Stats_recevied)) {
            message = context.getResources().getString(R.string.m_recevied);
            img_status.setImageResource(R.drawable.img_status_5);
        }
        return message;
    }

    public static final String IsShowSercivceM = "IsShowSercivceM";
    public static final String IsShowSubscriptionM = "IsShowSubscriptionM";

    public static final String KeyGuide = "key_guide";
    public static final String ActionEditProfile = "edit_profile";
    public static final String ActionChangePassword = "change_password";
    public static final String ActionPrivacyPolicy = "privacy_policy";
    public static final String ActionAboutUs = "about_us";

    public static final String ActionMyPoints = "my_points";
    public static final String ActionMyExpectations = "my_expectations";
    public static final String ActionMysubscriptions = "my_subscriptions";
    public static final String ActionKeyLocalBroadcastManager = "com.app.hotfresh_FCM-MESSAGE";

    public final static int RESPONSE_CODE_OK = 200;
    public final static String RESPONSE_STATUS_OK = "OK";
    public final static int RESPONSE_CODE_CREATED = 201;
    public final static String RESPONSE_STATUS_CREATED = "Created";
    public final static int RESPONSE_CODE_NO_CONTENT = 204;
    public final static String RESPONSE_STATUS_NO_CONTENT = "No Content";
    public final static int RESPONSE_CODE_BAD_REQUEST = 400;
    public final static String RESPONSE_STATUS_BAD_REQUEST = "Bad Request";
    public final static int RESPONSE_CODE_UNAUTHORIZED = 401;
    public final static String RESPONSE_STATUS_UNAUTHORIZED = "Unauthorized";
    public final static int RESPONSE_CODE_FORBIDDEN = 403;
    public final static String RESPONSE_STATUS_FORBIDDEN = "Forbidden";
    public final static int RESPONSE_CODE_NOT_FOUND = 404;
    public final static String RESPONSE_STATUS_NOT_FOUND = "Not Found";
    public final static int RESPONSE_CODE_CONFLICT = 409;
    public final static String RESPONSE_STATUS_CONFLICT = "Conflict";
    public final static int RESPONSE_CODE_UNPROCESSABLE_ENTITY = 422;
    public final static String RESPONSE_STATUS_UNPROCESSABLE_ENTITY = "Unprocessable Entity";

    public final static int RESPONSE_ORDER_STATUS_PENDING = 0;
    public final static int RESPONSE_ORDER_STATUS_ACCEPTED = 1;
    public final static int RESPONSE_ORDER_STATUS_START_TRIP = 2;
    public final static int RESPONSE_ORDER_STATUS_END_TRIP = 3;
    public final static int RESPONSE_ORDER_STATUS_DECLINED = 4;


}
