package com.app.hotfresh.Manager;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Build;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.google.android.material.navigation.NavigationView;


public class FontManager {

    private static final String ROOT = "fonts/",
            FONTAWESOME = ROOT + "cairo_regular.ttf";


    public static Typeface getTypeface(Context context) {
        return Typeface.createFromAsset(context.getAssets(), FONTAWESOME);

    }

    public static Typeface getTypefaceTextInputRegular(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/cairo_regular.ttf");
    }

    public static Typeface getTypefaceTextInputBold(Context context) {
        return Typeface.createFromAsset(context.getAssets(), "fonts/cairo_regular.ttf");
    }


    public static void strikeThroughText(TextView price) {
        price.setPaintFlags(price.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    public static void applyFont(final Context context, final View v) {
        String Lang = AppLanguage.getLanguage(context);
        //if (TextUtils.equals("ar", Lang)) {


        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/cairo_regular.ttf");
        try {
            if (v instanceof ViewGroup) {
                ViewGroup vg = (ViewGroup) v;
                for (int i = 0; i < vg.getChildCount(); i++) {
                    View child = vg.getChildAt(i);
                    applyFont(context, child);
                }
            } else if (v instanceof TextView) {
                ((TextView) v).setTypeface(font);
            } else if (v instanceof EditText) {
                ((EditText) v).setTypeface(font);
            } else if (v instanceof Button) {
                ((Button) v).setTypeface(font);
            } else if (v instanceof RadioButton) {
                ((RadioButton) v).setTypeface(font);
            }
        } catch (Exception e) {
            e.printStackTrace();
            // ignore
        }
        //  }
    }
    public static void applyFontToMenuItem(final Context context,MenuItem mi) {
        Typeface font = Typeface.createFromAsset(context.getAssets(), "fonts/cairo_regular.ttf");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }
    public static boolean checkPermission(Context context, String permission) {
        if (context == null) return false;
        int res;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            res = context.checkSelfPermission(permission);
            return (res == PackageManager.PERMISSION_GRANTED);
        } else {
            return true;
        }
    }
    public static void ChangeFontMenu(final Context context, NavigationView navigationView) {
        Menu m = navigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
            MenuItem mi = m.getItem(i);

            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                     applyFontToMenuItem(context, subMenuItem);
                }
            }
            FontManager.applyFontToMenuItem(context, mi);

        }
        FontManager.applyFont(context, navigationView);
    }

}