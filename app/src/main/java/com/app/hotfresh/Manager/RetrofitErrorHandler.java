package com.app.hotfresh.Manager;

import android.util.Log;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import java.net.SocketTimeoutException;

public class RetrofitErrorHandler implements ErrorHandler {
    @Override
    public void warning(SAXParseException e) throws SAXException {
        e.getCause();
        Log.e("getCause",  e.getCause() + "F" );

    }

    @Override
    public void error(SAXParseException e) throws SAXException {
        Log.e("getCause",  e.getCause() + "b" );

    }

    @Override
    public void fatalError(SAXParseException e) throws SAXException {
        Log.e("getCause",  e.getCause() + "m" );

    }
}