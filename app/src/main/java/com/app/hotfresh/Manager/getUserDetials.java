package com.app.hotfresh.Manager;

import android.content.Context;
import android.text.TextUtils;

import com.app.hotfresh.Medoles.User;
import com.google.gson.Gson;

public class getUserDetials {


    public static  User User(Context context) {
        User user = new User();
        Gson gson = new Gson();
        String json = AppPreferences.getString(context, "userJson");
        if (json != null) {
            if (!TextUtils.equals("0", json)) {
                return gson.fromJson(json, User.class);
            }

        }
        return null;
    }
}
