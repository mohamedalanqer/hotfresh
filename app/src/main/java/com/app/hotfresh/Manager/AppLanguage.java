package com.app.hotfresh.Manager;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.util.Log;

import com.app.MyApplication;

import java.util.Locale;

public class AppLanguage {

    public final static String ARABIC = "ar";
    public final static String PERSIAN = "en";


    public static String getLanguage(Context context) {
        return AppPreferences.getString(context, "language", ARABIC); // Locale.getDefault().getLanguage()
    }

    public static void saveLanguage(Context context, String language) {
        AppPreferences.saveString(context, "language", language);
    }

    private static void changeLang(Context context, String lang) {
        if (lang.equalsIgnoreCase(""))
            return;
        Locale myLocale = new Locale(lang);
        Locale.setDefault(myLocale);
        Configuration config = new Configuration();
        config.locale = myLocale;
        ((Activity) context).getBaseContext().getResources().updateConfiguration(config,
                ((Activity) context).getBaseContext().getResources().getDisplayMetrics());
//        updateTexts();
    }


    public static  void setContentLang(Activity context){
        String languageToLoad = AppLanguage.getLanguage(context); // your language
        Locale locale;
        switch (languageToLoad) {
            case "العربية":
                locale = new Locale("ar");
                break;
            case "English":
                locale = new Locale("en");
                break;
            case "ar":
                locale = new Locale("ar");
                break;
            case "en":
                locale = new Locale("en");
                break;
            default:
                locale = new Locale(languageToLoad);
                break;
        }
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        Log.e("locale",locale.getLanguage().toString());
        context.getBaseContext().getResources().updateConfiguration(config,
                context.getBaseContext().getResources().getDisplayMetrics());
        setLocale(config.locale);


    }
    public static void setLocale(Locale locale){
        Context context = MyApplication.getInstance();
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        Locale.setDefault(locale);
        configuration.setLocale(locale);

        if (Build.VERSION.SDK_INT >= 25) {
            context = context.getApplicationContext().createConfigurationContext(configuration);
            context = context.createConfigurationContext(configuration);
        }

        context.getResources().updateConfiguration(configuration,
                resources.getDisplayMetrics());
    }
   /** @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static void setResources(Context context) {
        Locale locale;
        String languageToLoad = AppLanguage.getLanguage(context); // your language
        switch (languageToLoad) {
            case "العربية":
                locale = new Locale("ar");
                break;
            case "English":
                locale = new Locale("en");
                break;
            case "ar":
                locale = new Locale("ar");
                break;
            case "en":
                locale = new Locale("en");
                break;
            default:
                locale = new Locale(languageToLoad);
                break;
        }

        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        android.content.res.Configuration configuration = res.getConfiguration();
        configuration.setLocale(locale);
        res.updateConfiguration(configuration, dm);

    }*/
}
