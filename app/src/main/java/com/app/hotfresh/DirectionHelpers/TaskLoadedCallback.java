package com.app.hotfresh.DirectionHelpers;

public interface  TaskLoadedCallback {
    void onTaskDone(Object... values);
}
