package com.app.hotfresh.Ui.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.hotfresh.CallBack.OnItemClickTagListener;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Medoles.Category;
import com.app.hotfresh.Medoles.Charities.Charitie;
import com.app.hotfresh.R;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.List;


public class RecyclerCharitie extends RecyclerView.Adapter<RecyclerCharitie.CustomView> {
    Context context;
    List<Charitie> mylist;
    private OnItemClickTagListener listener;
    int Layout;
    public static int ItemSelect = -1 ;


    public class CustomView extends RecyclerView.ViewHolder {
        TextView row_name;
        CardView card_view ;

        public CustomView(View v) {
            super(v);
            FontManager.applyFont(context, v);
            row_name = v.findViewById(R.id.row_name);
            card_view = v.findViewById(R.id.card_view);
        }

    }

    public RecyclerCharitie() {
    }

    public RecyclerCharitie(Context context, List<Charitie> mylist, int ItemSelect , int Layout, OnItemClickTagListener listener) {
        this.context = context;
        this.mylist = mylist;
        this.listener = listener;
        this.Layout = Layout;
        this.ItemSelect  = ItemSelect ;
    }

    @Override
    public CustomView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(Layout, parent, false);
        CustomView viewholder = new CustomView(view);
        return viewholder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final CustomView holder, final int position) {
        holder.row_name.setText(mylist.get(position).getName() + "");

            if(ItemSelect == position){
                holder.card_view.setCardBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                holder.row_name.setTextColor(context.getResources().getColor(R.color.colorWhite));
            }else{
                holder.card_view.setCardBackgroundColor(context.getResources().getColor(R.color.colorWhite));
                holder.row_name.setTextColor(context.getResources().getColor(R.color.colorBlack));
            }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    listener.onItemClick(view, position, "view");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });


    }


    @Override
    public int getItemCount() {
        return mylist.size();
    }


}

