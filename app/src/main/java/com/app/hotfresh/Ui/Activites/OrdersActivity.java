package com.app.hotfresh.Ui.Activites;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.hotfresh.CallBack.InstallCallback;
import com.app.hotfresh.CallBack.InternetAvailableCallback;
import com.app.hotfresh.CallBack.OnItemClickTagListener;
import com.app.hotfresh.DataBase.AppDatabase;
import com.app.hotfresh.DataBase.InsertUpdateDeleteItem;
import com.app.hotfresh.Manager.AppErrorsManager;
import com.app.hotfresh.Manager.AppLanguage;
import com.app.hotfresh.Manager.AppMp3Manager;
import com.app.hotfresh.Manager.DrawableRound;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Manager.InternetConnectionUtils;
import com.app.hotfresh.Manager.RootManager;
import com.app.hotfresh.Manager.getUserDetials;
import com.app.hotfresh.Medoles.Category;
import com.app.hotfresh.Medoles.Charities.CharitiesItem;
import com.app.hotfresh.Medoles.ItemService;
import com.app.hotfresh.Medoles.Order;
import com.app.hotfresh.Medoles.OrderCategory;
import com.app.hotfresh.Medoles.User;
import com.app.hotfresh.R;
import com.app.hotfresh.Ui.Adapters.RecyclerCategory;
import com.app.hotfresh.Ui.Adapters.RecyclerCategoryOrders;
import com.app.hotfresh.Ui.Adapters.RecyclerCharitiesItems;
import com.app.hotfresh.Ui.Adapters.RecyclerOrder;
import com.app.hotfresh.WebService.RetrofitWebService;
import com.app.hotfresh.WebService.model.response.CategoryResponse;
import com.app.hotfresh.WebService.model.response.CharitiesItemsResponse;
import com.app.hotfresh.WebService.model.response.OrdersCountsResponse;
import com.app.hotfresh.WebService.model.response.OrdersResponse;
import com.google.gson.Gson;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.hotfresh.Manager.RootManager.ActionKeyLocalBroadcastManager;
import static com.app.hotfresh.Manager.RootManager.DataBaseName;
import static com.app.hotfresh.Manager.RootManager.Stats_certain;
import static com.app.hotfresh.Manager.RootManager.Stats_ready;
import static com.app.hotfresh.Manager.RootManager.Stats_recevied;
import static com.app.hotfresh.Manager.RootManager.Stats_uncertain;
import static com.app.hotfresh.Manager.RootManager.Stats_updated;

public class OrdersActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    private FrameLayout layout_loading;

    RecyclerView recycler_view;
    RecyclerOrder adapter;
    List<Order> items_list = new ArrayList<>();
    private String CategoryName = "";
    private TextView toolbarNameTxt;
    AppDatabase db;
    List<CharitiesItem> itemListDataBase = new ArrayList<>();
    SwipeRefreshLayout swipeRefreshLayout;


    RecyclerView recycler_view_category;
    RecyclerCategoryOrders adapter_category;
    List<OrderCategory> categories_list = new ArrayList<>();
    private LinearLayout layout_empty;
    private TextView text_empty;
    private Handler handler;
    private String CharitiesName = "";
    private RelativeLayout end_main;
    private RelativeLayout Layout_notification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppLanguage.setContentLang(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_list_orders);
        FontManager.applyFont(this, findViewById(R.id.layout));
        handler = new Handler(Looper.getMainLooper());


        initSetup();
        setUpDataBase();
    }

    private void setUpDataBase() {
        db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, DataBaseName).allowMainThreadQueries().build();

        ImageView count_cart_img = findViewById(R.id.image_count_cart);
        TextView text_count_cart = findViewById(R.id.text_count_cart);


        db.itemDao().getCount().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                DrawableRound.TextToDrawableRound(getApplicationContext(), count_cart_img, String.valueOf(integer), text_count_cart);
            }
        });
    }

    User user = new User();

    private void initSetup() {
        recycler_view = findViewById(R.id.recycler_view);
        recycler_view_category = findViewById(R.id.recycler_view_category);
        layout_loading = findViewById(R.id.layout_loading);
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        layout_empty = findViewById(R.id.layout_empty);
        text_empty = findViewById(R.id.text_empty);
        end_main = findViewById(R.id.end_main);
        Layout_notification = findViewById(R.id.Layout_notification);
        Layout_notification.setVisibility(View.GONE);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this::onRefresh);
        end_main.setOnClickListener(view -> {
            onBackPressed();
        });
        layout_loading.setOnClickListener(view -> {
            return;
        });
        CharitiesName = getIntent().getStringExtra("CharitiesName");
        toolbarNameTxt.setText("" + getResources().getString(R.string.TheOrders));

        user = getUserDetials.User(this);
        if (user != null) {
            GetListCategories();
        }

    }


    private void GetOrdersByStatus(String token, String orderstatus) {
        layout_empty.setVisibility(View.GONE);
        recycler_view.setVisibility(View.VISIBLE);
        layout_loading.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(this).GetClientOrderByStatus(token, orderstatus).enqueue(new Callback<OrdersResponse>() {
            @Override
            public void onResponse(Call<OrdersResponse> call, Response<OrdersResponse> response) {
                Log.e("GetCertainOrders", response.toString());
                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (response.body() != null) {
                        if (TextUtils.equals("True", response.body().getStatus())) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    items_list = response.body().getOrders();
                                    if (items_list.size() > 0) {
                                        layout_empty.setVisibility(View.GONE);
                                        recycler_view.setVisibility(View.VISIBLE);
                                        setupRecycler();
                                    } else {
                                        recycler_view.setVisibility(View.GONE);
                                        layout_empty.setVisibility(View.VISIBLE);
                                        text_empty.setText("" + response.body().getMessage());
                                        for (int i = 0; i < categories_list.size(); i++) {
                                            if (TextUtils.equals(orderstatus + "".trim(), categories_list.get(i).getTagName()))
                                                text_empty.setText(Html.fromHtml("<font color='red'>" + categories_list.get(i).getCategoryName() + "</font>" + "<br/>" + response.body().getMessage()));
                                        }
                                    }
                                }
                            });

                        } else if (TextUtils.equals("False", response.body().getStatus())) {
                            recycler_view.setVisibility(View.GONE);
                            layout_empty.setVisibility(View.VISIBLE);
                            text_empty.setText("" + response.body().getMessage());
                            for (int i = 0; i < categories_list.size(); i++) {
                                if (TextUtils.equals(orderstatus + "".trim(), categories_list.get(i).getTagName()))
                                    text_empty.setText(Html.fromHtml("<font color='red'>" + categories_list.get(i).getCategoryName() + "</font>" + "<br/>" + response.body().getMessage()));
                            }
                        }
                    }
                } else {
                    AppErrorsManager.showCustomErrorDialog(OrdersActivity.this, getResources().getString(R.string.Failure),
                            getResources().getString(R.string.InternalServerError));
                }


                layout_loading.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<OrdersResponse> call, Throwable t) {
                Log.e("GetCertainOrders", t.getMessage() + "");

                layout_loading.setVisibility(View.GONE);
                InternetConnectionUtils.isInternetAvailable(getApplicationContext(), new InternetAvailableCallback() {
                    @Override
                    public void onInternetAvailable(boolean isAvailable) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (isAvailable) {

                                    AppErrorsManager.showCustomErrorDialog(OrdersActivity.this, getResources().getString(R.string.Failure),
                                            t.getMessage() + "");
                                } else {
                                    AppErrorsManager.InternetUnAvailableDialog(OrdersActivity.this, getResources().getString(R.string.Failure),
                                            getResources().getString(R.string.nointernetconnection), new InstallCallback() {
                                                @Override
                                                public void onStatusDone(String status) {
                                                    if (TextUtils.equals("retry", status)) {
                                                        GetOrdersByStatus(token, orderstatus);
                                                    }
                                                }
                                            });
                                }
                            }
                        });
                    }
                });
            }
        });


    }

    private void GetOrdersCountsWebService(String token) {
        layout_loading.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(this).OrdersCounts(token).enqueue(new Callback<OrdersCountsResponse>() {
            @Override
            public void onResponse(Call<OrdersCountsResponse> call, Response<OrdersCountsResponse> response) {
                Log.e("GetCertainOrders", response.toString());
                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (response.body() != null) {
                        if (TextUtils.equals("True", response.body().getStatus())) {
                            setContToTableTop(response);
                        } else if (TextUtils.equals("False", response.body().getStatus())) {

                        }
                    }
                } else {
                    AppErrorsManager.showCustomErrorDialog(OrdersActivity.this, getResources().getString(R.string.Failure),
                            getResources().getString(R.string.InternalServerError));
                }


                layout_loading.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<OrdersCountsResponse> call, Throwable t) {
                Log.e("GetCertainOrders", t.getMessage() + "");

                layout_loading.setVisibility(View.GONE);
                InternetConnectionUtils.isInternetAvailable(getApplicationContext(), new InternetAvailableCallback() {
                    @Override
                    public void onInternetAvailable(boolean isAvailable) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (isAvailable) {

                                    AppErrorsManager.showCustomErrorDialog(OrdersActivity.this, getResources().getString(R.string.Failure),
                                            t.getMessage() + "");
                                } else {
                                    AppErrorsManager.InternetUnAvailableDialog(OrdersActivity.this, getResources().getString(R.string.Failure),
                                            getResources().getString(R.string.nointernetconnection), new InstallCallback() {
                                                @Override
                                                public void onStatusDone(String status) {
                                                    if (TextUtils.equals("retry", status)) {
                                                        GetOrdersCountsWebService(token);
                                                    }
                                                }
                                            });
                                }
                            }
                        });
                    }
                });
            }
        });


    }

    private void setContToTableTop(Response<OrdersCountsResponse> response) {
        if (response.body() != null) {
            if (categories_list.size() > 4) {
                if (!TextUtils.equals(response.body().getUncertain(), "0")) {
                    categories_list.get(0).setCategoryName(categories_list.get(0).getCategoryName());
                    categories_list.get(0).setCount(response.body().getUncertain());
                }
                if (!TextUtils.equals(response.body().getCertain(), "0")) {
                    categories_list.get(1).setCategoryName(categories_list.get(1).getCategoryName());
                    categories_list.get(1).setCount(response.body().getCertain());
                }
                if (!TextUtils.equals(response.body().getReady(), "0")) {
                    categories_list.get(2).setCategoryName(categories_list.get(2).getCategoryName());
                    categories_list.get(2).setCount(response.body().getReady());
                }
                if (!TextUtils.equals(response.body().getUpdated(), "0")) {
                    categories_list.get(3).setCategoryName(categories_list.get(3).getCategoryName());
                    categories_list.get(3).setCount(response.body().getUpdated());
                }
                if (!TextUtils.equals(response.body().getRecevied(), "0")) {
                    categories_list.get(4).setCategoryName(categories_list.get(4).getCategoryName());
                    categories_list.get(4).setCount(response.body().getRecevied());
                }
                adapter_category.notifyDataSetChanged();
            }
        }
    }


    private void GetListCategories() {
        categories_list.add(new OrderCategory(1, Stats_uncertain, getResources().getString(R.string.s_uncertain)));
        categories_list.add(new OrderCategory(2, Stats_certain, getResources().getString(R.string.s_certain)));
        categories_list.add(new OrderCategory(3, Stats_ready, getResources().getString(R.string.s_ready)));
        categories_list.add(new OrderCategory(4, Stats_updated, getResources().getString(R.string.s_updated)));
        categories_list.add(new OrderCategory(5, Stats_recevied, getResources().getString(R.string.s_recevied)));
        if (categories_list.size() > 0) {
            setupRecyclerCategory();
            if (user != null) {
                OrderCategory orderCategory = categories_list.get(0);
                categories_list.get(0).setSelect(true);
                SetDataListByTagName(orderCategory.getTagName());
                GetOrdersCountsWebService(user.getAccessToken());

            }
        }


    }


    public void setupRecyclerCategory() {
        adapter_category = new RecyclerCategoryOrders(getApplicationContext(), categories_list, 0, R.layout.row_item_category_order, new OnItemClickTagListener() {
            @Override
            public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                adapter_category.ItemSelect = position;
                for (int i = 0; i < categories_list.size(); i++) {
                    categories_list.get(i).setSelect(false);
                }
                categories_list.get(position).setSelect(true);
                adapter_category.notifyDataSetChanged();
                SetDataListByTagName(categories_list.get(position).getTagName());
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.HORIZONTAL, false);
        recycler_view_category.setLayoutManager(linearLayoutManager);
        recycler_view_category.setAdapter(adapter_category);

    }

    private void SetDataListByTagName(String tagName) {
        if (user != null) {
            GetOrdersByStatus(user.getAccessToken(), tagName);

        }
    }


    public void setupRecycler() {
       // items_list.get(0).setPayStatus("False");
        adapter = new RecyclerOrder(getApplicationContext(), items_list, -1, R.layout.row_item_order, new OnItemClickTagListener() {
            @Override
            public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                Order item = items_list.get(position);

                if (TextUtils.equals("view", tag)) {
                    GoToDetailsOrderActivity(item);
                    adapter.notifyDataSetChanged();
                } else if (TextUtils.equals("onway", tag)) {
                    categories_list.get(2).setAnimationTrue(true);
                    adapter_category.notifyDataSetChanged();
                } else {
                    if (adapter.ItemSelect != position)
                        adapter.ItemSelect = position;
                    else
                        adapter.ItemSelect = -1;
                    adapter.notifyDataSetChanged();
                }

            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setAdapter(adapter);

    }

    private void GoToDetailsOrderActivity(Order order) {
        String order_str = new Gson().toJson(order);
        Intent intent = new Intent(getApplicationContext(), DetailsOrderActivity.class);
        intent.putExtra("BillCode", order.getBillCode());
        intent.putExtra("order_str", order_str);
        startActivityForResult(intent, 300);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 300 && resultCode == 300) {
            for (int i = 0; i < categories_list.size(); i++) {
                if (categories_list.get(i).isSelect()) {
                    SetDataListByTagName(categories_list.get(i).getTagName());
                    return;
                }
            }
        }

    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);

                user = getUserDetials.User(OrdersActivity.this);
                if (user != null) {
                    if (user != null) {
                        for (int i = 0; i < categories_list.size(); i++) {
                            if (categories_list.get(i).isSelect()) {
                                SetDataListByTagName(categories_list.get(i).getTagName());
                                return;
                            }
                        }
                    }
                }
            }
        }, 2000);
    }

    @Override
    public void onBackPressed() {
        Intent mainIntent = new Intent(OrdersActivity.this, HomeActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
        finish();
    }

    private AppMp3Manager appMp3Manager;

    private BroadcastReceiver mHander = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String title = intent.getStringExtra("title");
            String body = intent.getStringExtra("body");
            appMp3Manager = new AppMp3Manager(OrdersActivity.this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    AppErrorsManager.showCustomErrorDialogTop(OrdersActivity.this, title, body, new InstallCallback() {
                        @Override
                        public void onStatusDone(String status) {

                        }
                    });
                    appMp3Manager.palyNotificationMusic(R.raw.bell_sound);
                }
            }, 1000);
        }
    };


    @Override
    protected void onRestart() {
        setUpDataBase();
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        super.onRestart();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHander);
        super.onPause();
    }
}
