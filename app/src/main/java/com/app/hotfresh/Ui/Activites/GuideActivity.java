package com.app.hotfresh.Ui.Activites;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.app.hotfresh.Manager.AppLanguage;
import com.app.hotfresh.Manager.AppPreferences;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Manager.getUserDetials;
import com.app.hotfresh.Medoles.User;
import com.app.hotfresh.R;
import com.app.hotfresh.Ui.Fragments.LoginFragment;
import com.app.hotfresh.Ui.Fragments.RegisterFragment;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.gson.Gson;

public class GuideActivity extends AppCompatActivity implements View.OnClickListener {

    User user = new User();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, Window.FEATURE_NO_TITLE);
        // getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        //  getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        AppLanguage.setContentLang(this);
        setContentView(R.layout.activity_guide);
        FontManager.applyFont(this, findViewById(R.id.layout));
        user = getUserDetials.User(this);
        initSetUp();

    }

    private void initSetUp() {
        Button btn_action = findViewById(R.id.btn_action);
        btn_action.setOnClickListener(this::onClick);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_action:
                AppPreferences.saveOrEditBoolean(this, "isGuideShow", true);
                if (user != null) {
                    Intent mainIntent = new Intent(GuideActivity.this, HomeActivity.class);
                    mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(mainIntent);
                    finish();

                } else {
                    Intent mainIntent = new Intent(GuideActivity.this, UserActionsActivity.class);
                    mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(mainIntent);
                    finish();
                }
                break;


        }
    }


}
