package com.app.hotfresh.Ui.Activites;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.PointerIcon;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.hotfresh.CallBack.InstallCallback;
import com.app.hotfresh.CallBack.InternetAvailableCallback;
import com.app.hotfresh.CallBack.OnItemClickListener;
import com.app.hotfresh.Manager.AppErrorsManager;
import com.app.hotfresh.Manager.AppLanguage;
import com.app.hotfresh.Manager.AppMp3Manager;
import com.app.hotfresh.Manager.AppPreferences;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Manager.InternetConnectionUtils;
import com.app.hotfresh.Manager.RootManager;
import com.app.hotfresh.Manager.getUserDetials;
import com.app.hotfresh.Medoles.Install.Branch;
import com.app.hotfresh.Medoles.Install.Definition;
import com.app.hotfresh.Medoles.PointsGroups.PointsGroups;
import com.app.hotfresh.Medoles.User;
import com.app.hotfresh.R;
import com.app.hotfresh.Ui.Adapters.AdapterPackages;
import com.app.hotfresh.Ui.Adapters.KKViewPager;
import com.app.hotfresh.WebService.RetrofitWebService;
import com.app.hotfresh.WebService.model.response.LoginResponse;
import com.app.hotfresh.WebService.model.response.NofiticationsResponse;
import com.app.hotfresh.WebService.model.response.PackagesResponse;
import com.app.hotfresh.WebService.model.response.RootResponse;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.hotfresh.Manager.RootManager.ActionKeyLocalBroadcastManager;

public class PackagesActivity extends AppCompatActivity {
    private Button btn_action;
    private FrameLayout layout_loading;
    private TextView toolbarNameTxt;
    private KKViewPager view_pager;
    private DotsIndicator dotsIndicator;
    private User user = null;
    List<PointsGroups> storyList = new ArrayList<>();
    private AdapterPackages adapter;
    private Handler handler;

    private ImageView img_item;
    private TextView row_name, text_count, text_my_package, text_btn_points, text_btn_package;
    private LinearLayout layout_points;
    private RelativeLayout layout_packages;

    private TextView TextPointsDeposits, TextPointsWithdrawals, TextPointBalance, TextPointGroupsName, TextPointsToRyial;
    private RelativeLayout end_main;


    private ProgressBar progress_bar;
    private TextView text_progress, text_limit_date;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppLanguage.setContentLang(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_packages);
        FontManager.applyFont(this, findViewById(R.id.layout));
        handler = new Handler(Looper.getMainLooper());
        initSetUp();
    }

    private String PointsToRyial = "0";

    private void initSetUp() {
        user = getUserDetials.User(this);


        end_main = findViewById(R.id.end_main);
        end_main.setOnClickListener(view -> {
            onBackPressed();
        });

        text_btn_points = findViewById(R.id.text_btn_points);
        text_btn_package = findViewById(R.id.text_btn_package);
        layout_points = findViewById(R.id.layout_points);
        layout_packages = findViewById(R.id.layout_packages);
        layout_points.setVisibility(View.GONE);
        layout_packages.setVisibility(View.VISIBLE);
        TextPointsToRyial = findViewById(R.id.TextPointsToRyial);
        String definitionsJson = AppPreferences.getString(PackagesActivity.this, "definitionsJson");


        progress_bar = findViewById(R.id.progress_bar);
        text_progress = findViewById(R.id.text_progress);
        text_limit_date = findViewById(R.id.text_limit_date);
        text_progress.setVisibility(View.GONE);
        text_limit_date.setVisibility(View.GONE);
        progress_bar.setVisibility(View.GONE);

        if (definitionsJson != null) {
            Definition definition = new Gson().fromJson(definitionsJson, Definition.class);
            if (definition != null) {
                PointsToRyial = definition.getPointsToRyial();
            }
        }

        TextPointsDeposits = findViewById(R.id.TextPointsDeposits);
        TextPointsWithdrawals = findViewById(R.id.TextPointsWithdrawals);
        TextPointBalance = findViewById(R.id.TextPointBalance);
        TextPointGroupsName = findViewById(R.id.TextPointGroupsName);


        text_btn_points.setOnClickListener(view -> {
            ToggeSetPointsView();
        });
        text_btn_package.setOnClickListener(view -> {
            ToggeSetPackagesView();
        });
        ToggeSetPackagesView();


        img_item = findViewById(R.id.img_item);
        row_name = findViewById(R.id.row_name);
        text_count = findViewById(R.id.text_count);
        text_my_package = findViewById(R.id.text_my_package);
        //  user.setPointBalance("256");

        SetValueToUser();


        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        layout_loading = findViewById(R.id.layout_loading);
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        toolbarNameTxt.setVisibility(View.VISIBLE);
        toolbarNameTxt.setTextColor(getResources().getColor(R.color.colorBlack));
        btn_action = findViewById(R.id.btn_action);
        btn_action.setVisibility(View.INVISIBLE);
        layout_loading.setOnClickListener(view1 -> {
            return;
        });
        TextView text_count_cart = findViewById(R.id.text_count_cart);
        ImageView image_count_cart = findViewById(R.id.image_count_cart);
        ImageView img_bar_cart = findViewById(R.id.img_bar_cart);
        text_count_cart.setVisibility(View.GONE);
        image_count_cart.setVisibility(View.GONE);
        img_bar_cart.setVisibility(View.GONE);
        toolbarNameTxt.setText("" + getResources().getString(R.string.Packages));
        view_pager = findViewById(R.id.kk_pager);
        dotsIndicator = findViewById(R.id.worm_dots_indicator);
        GetPackagesListWebService();


    }

    private void SetValueToUser() {
        user = getUserDetials.User(this);
        //   user.setPointBalance("1240");
        if (user != null) {
            row_name.setText("" + user.getName());
            if (!TextUtils.isEmpty(user.getPointBalance() + ""))
                text_count.setText("" + user.getPointBalance() + "");
            else
                text_count.setText("0");
            text_my_package.setOnClickListener(view -> {
                AppErrorsManager.showCustomErrorDialogTwoAction(PackagesActivity.this,
                        getResources().getString(R.string.Redeem_my_points),
                        getResources().getString(R.string.AreYouSureRedeemPoints), new InstallCallback() {
                            @Override
                            public void onStatusDone(String status) {
                                if (TextUtils.equals("yes", status)) {
                                    ConvertPointToMoneyWebService();
                                }
                            }
                        });
            });

            if (!TextUtils.isEmpty(user.getPointsDeposits()))
                TextPointsDeposits.setText("" + user.getPointsDeposits() + " " + getResources().getString(R.string.Point));
            if (!TextUtils.isEmpty(user.getPointsWithdrawals()))
                TextPointsWithdrawals.setText("" + user.getPointsWithdrawals() + " " + getResources().getString(R.string.Point));
            if (!TextUtils.isEmpty(user.getPointBalance())) {
                TextPointBalance.setText("" + user.getPointBalance() + " " + getResources().getString(R.string.Point));

                if (!TextUtils.isEmpty(user.getPointBalance())) {
                    if (!TextUtils.equals(user.getPointBalance(), "0")) {

                        if (PointsToRyial != null) {
                            if (!TextUtils.isEmpty(PointsToRyial)) {
                                if (!TextUtils.equals(PointsToRyial, "0")) {
                                    Float point_r = Float.parseFloat(PointsToRyial);
                                    Float points = Float.parseFloat(user.getPointBalance());
                                    if ((points > 0.0)) {
                                        //if(point_r < points){
                                        TextPointsToRyial.setText(getResources().getString(R.string.YourPointsEqual) + " " + points / point_r + " " + getResources().getString(R.string.SAR));
                                        //  }
                                    }

                                }
                            }

                        }

                    }
                }
            }
            if (!TextUtils.isEmpty(user.getPointGroupsName()))
                TextPointGroupsName.setText("" + user.getPointGroupsName() + "");


            String string_progress = getResources().getString(R.string.msg_progress);
            string_progress = string_progress.replace("@", user.getGroupEndDate());
            String needPoints = "0";
            //  user.setPointsDeposits("400");
            if (user.getPointsDeposits() != null) {
                if (!TextUtils.isEmpty(user.getPointsDeposits())) {
                    int pointDeposits = Integer.parseInt(user.getPointsDeposits());
                    int pointNextGroupLimit = Integer.parseInt(user.getNextGroupLimit());
                    needPoints = pointNextGroupLimit - pointDeposits + "";
                    progress_bar.setMax(pointNextGroupLimit);
                    if (pointDeposits == 0) {
                        progress_bar.setProgress(30);
                    } else
                        progress_bar.setProgress(pointDeposits);

                    progress_bar.setVisibility(View.VISIBLE);
                    text_progress.setVisibility(View.VISIBLE);
                    text_limit_date.setVisibility(View.VISIBLE);
                    fadeOutProgressLayout(this, progress_bar);

                }
            }
            string_progress = string_progress.replace("$", needPoints);
            String stringlimetDate = getResources().getString(R.string.MsgLimitPoints).replace("$", user.getGroupEndDate());
            text_progress.setText("" + string_progress);
            text_limit_date.setText("" + stringlimetDate);

        }
    }

    public static void fadeOutProgressLayout(Activity act, final ProgressBar progressLayout) {
        act.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Animation fadeOut = new AlphaAnimation(1, 0);
                fadeOut.setInterpolator(new AccelerateInterpolator());
                fadeOut.setDuration(300);

                fadeOut.setAnimationListener(new Animation.AnimationListener() {
                    public void onAnimationEnd(Animation animation) {
                        progressLayout.setVisibility(View.VISIBLE);
                    }

                    public void onAnimationRepeat(Animation animation) {
                    }

                    public void onAnimationStart(Animation animation) {
                    }
                });

                progressLayout.startAnimation(fadeOut);
            }
        });
    }

    private void ConvertPointToMoneyWebService() {
        if (user != null) {
            layout_loading.setVisibility(View.VISIBLE);
            RetrofitWebService.getService(this).ConvertPointToMoney(user.getAccessToken()).enqueue(new Callback<RootResponse>() {
                @Override
                public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {


                    layout_loading.setVisibility(View.GONE);
                    if (TextUtils.equals("True", response.body().getStatus())) {
                        GetMyProfile(user);
                    } else if (TextUtils.equals("False", response.body().getStatus())) {
                        AppErrorsManager.showCustomErrorDialog(PackagesActivity.this,
                                getResources().getString(R.string.error),
                                response.body().getMessage() + "");
                    }
                }

                @Override
                public void onFailure(Call<RootResponse> call, Throwable t) {

                    layout_loading.setVisibility(View.GONE);
                }
            });
        }
    }


    private void ToggeSetPointsView() {
        layout_points.setVisibility(View.VISIBLE);
        layout_packages.setVisibility(View.GONE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            text_btn_points.setBackgroundTintList(getResources().getColorStateList(R.color.colortest));
            text_btn_points.setTextColor(getResources().getColor(R.color.colorWhite));

            text_btn_package.setBackgroundTintList(getResources().getColorStateList(R.color.colorGreyLight));
            text_btn_package.setTextColor(getResources().getColor(R.color.colorBlack));
        }
    }

    private void ToggeSetPackagesView() {
        layout_points.setVisibility(View.GONE);
        layout_packages.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            text_btn_package.setBackgroundTintList(getResources().getColorStateList(R.color.colortest));
            text_btn_package.setTextColor(getResources().getColor(R.color.colorWhite));

            text_btn_points.setBackgroundTintList(getResources().getColorStateList(R.color.colorGreyLight));
            text_btn_points.setTextColor(getResources().getColor(R.color.colorBlack));
        }
    }

    private String idSelect = "-1";

    private void SetValueToViewPager(final List<PointsGroups> storyList) {
        if (storyList.size() > 0) {
            idSelect = storyList.get(0).getGroupID();
        }
        // user.setPointGroupID("2");
        for (int i = 0; i < storyList.size(); i++) {
            if (TextUtils.equals(storyList.get(i).getGroupID(), user.getPointGroupID())) {
                storyList.get(i).setSelectGroup(true);
            }
        }
        adapter = new AdapterPackages(PackagesActivity.this, storyList, new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) throws JSONException, FileNotFoundException {

            }
        });
        view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                idSelect = storyList.get(position).getGroupID();

            }

            @Override
            public void onPageSelected(int position) {
                idSelect = storyList.get(position).getGroupID();

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        view_pager.setAnimationEnabled(true);
        view_pager.setFadeEnabled(true);
        view_pager.setFadeFactor(0.4f);
        view_pager.setAdapter(adapter);
        dotsIndicator.setViewPager(view_pager);

        if (user != null) {
            GetMyProfile(user);
        }
    }


    private void GetPackagesListWebService() {
        if (user != null) {
            layout_loading.setVisibility(View.VISIBLE);
            RetrofitWebService.getService(this).GetPackagesList().enqueue(new Callback<PackagesResponse>() {
                @Override
                public void onResponse(Call<PackagesResponse> call, Response<PackagesResponse> response) {
                    if (RootManager.RESPONSE_CODE_OK == response.code()) {
                        if (response.body() != null) {
                            if (TextUtils.equals("True", response.body().getStatus())) {
                                storyList = response.body().getPointsGroups();
                                if (storyList.size() > 0) {
                                    SetValueToViewPager(storyList);
                                }
                            } else if (TextUtils.equals("False", response.body().getStatus())) {
                                AppErrorsManager.showCustomErrorDialog(PackagesActivity.this, getResources().getString(R.string.error),
                                        response.body().getMessage() + "");
                            }
                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(PackagesActivity.this, getResources().getString(R.string.Failure),
                                getResources().getString(R.string.InternalServerError));
                    }

                    layout_loading.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<PackagesResponse> call, Throwable t) {
                    AppErrorsManager.showCustomErrorDialog(PackagesActivity.this, getResources().getString(R.string.Failure),
                            t.getMessage() + "");
                }
            });

        }


    }

    private AppMp3Manager appMp3Manager;
    private BroadcastReceiver mHander = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String title = intent.getStringExtra("title");
            String body = intent.getStringExtra("body");
            appMp3Manager = new AppMp3Manager(PackagesActivity.this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    AppErrorsManager.showCustomErrorDialogTop(PackagesActivity.this, title, body, new InstallCallback() {
                        @Override
                        public void onStatusDone(String status) {
                            if (user != null)
                                GetMyProfile(user);
                        }
                    });
                    appMp3Manager.palyNotificationMusic(R.raw.bell_sound);
                }
            }, 1000);
        }
    };

    @Override
    protected void onRestart() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        super.onRestart();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHander);
        super.onPause();
    }


    private void GetMyProfile(User user) {
        if (user != null) {
            String old_token = user.getAccessToken();
            RetrofitWebService.getService(this).GetMyProfile(user.getAccessToken()).enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    if (RootManager.RESPONSE_CODE_OK == response.code()) {
                        if (response.body() != null) {
                            if (TextUtils.equals("True", response.body().getStatus())) {

                                User user = response.body().getUser();
                                user.setAccessToken(old_token);
                                if (user != null) {
                                    Gson json = new Gson();
                                    String userJson = json.toJson(user);
                                    AppPreferences.saveString(PackagesActivity.this, "userJson", userJson);

                                    SetValueToUser();
                                }
                            }
                        } else if (TextUtils.equals("False", response.body().getStatus())) {

                        }
                    }

                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Log.e("GetMyProfile", t.toString());
                    AppErrorsManager.showCustomErrorDialog(PackagesActivity.this, getResources().getString(R.string.Failure),
                            t.getMessage() + "");
                }
            });
        }
    }
}