package com.app.hotfresh.Ui.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.app.hotfresh.CallBack.OnItemClickTagListener;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Medoles.Category;
import com.app.hotfresh.Medoles.SettingItem;
import com.app.hotfresh.R;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.List;


public class RecyclerSetting extends RecyclerView.Adapter<RecyclerSetting.CustomView> {
    Context context;
    List<SettingItem> mylist;
    private OnItemClickTagListener listener;
    int Layout;
    public static int ItemSelect = -1 ;


    public class CustomView extends RecyclerView.ViewHolder {
        TextView row_name;
        ImageView row_img ;
        CardView card_view ;

        public CustomView(View v) {
            super(v);
            FontManager.applyFont(context, v);
            row_name = v.findViewById(R.id.row_name);
            row_img = v.findViewById(R.id.row_img);
            card_view = v.findViewById(R.id.card_view);
        }

    }

    public RecyclerSetting() {
    }

    public RecyclerSetting(Context context, List<SettingItem> mylist, int ItemSelect , int Layout, OnItemClickTagListener listener) {
        this.context = context;
        this.mylist = mylist;
        this.listener = listener;
        this.Layout = Layout;
        this.ItemSelect  = ItemSelect ;
    }

    @Override
    public CustomView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(Layout, parent, false);
        CustomView viewholder = new CustomView(view);
        return viewholder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final CustomView holder, final int position) {
        holder.row_name.setText(mylist.get(position).getName() + "");
        holder.row_img.setImageResource(mylist.get(position).getResImg());
            if(ItemSelect == position){
                holder.row_img.setBackgroundResource(R.drawable.shape_cercle_notfication);
                holder.row_img.setColorFilter(ContextCompat.getColor(context, R.color.colorWhite), android.graphics.PorterDuff.Mode.SRC_IN);
                holder.row_name.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
            }else{
                holder.row_img.setBackgroundResource(R.drawable.shape_cercle_white);
                holder.row_img.setColorFilter(ContextCompat.getColor(context, R.color.colorBlack), android.graphics.PorterDuff.Mode.SRC_IN);
                holder.row_name.setTextColor(ContextCompat.getColor(context, R.color.colorBlack));

            }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    listener.onItemClick(view, position, "view");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return mylist.size();
    }


}

