package com.app.hotfresh.Ui.Activites;

import android.content.BroadcastReceiver;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.text.Html;
import android.text.TextUtils;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.hotfresh.CallBack.InstallCallback;
import com.app.hotfresh.CallBack.InternetAvailableCallback;
import com.app.hotfresh.CallBack.OnItemClickTagListener;
import com.app.hotfresh.Manager.AppErrorsManager;
import com.app.hotfresh.Manager.AppLanguage;
import com.app.hotfresh.Manager.AppMp3Manager;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Manager.InternetConnectionUtils;
import com.app.hotfresh.Manager.RootManager;
import com.app.hotfresh.Manager.getUserDetials;
import com.app.hotfresh.Medoles.Checkouts.HatUserWallet;
import com.app.hotfresh.Medoles.Nofitication;
import com.app.hotfresh.Medoles.User;
import com.app.hotfresh.R;
import com.app.hotfresh.Ui.Adapters.RecyclerNofitication;
import com.app.hotfresh.Ui.Adapters.RecyclerPayment;
import com.app.hotfresh.WebService.RetrofitWebService;
import com.app.hotfresh.WebService.model.response.HatUserWalletResponse;
import com.app.hotfresh.WebService.model.response.NofiticationsResponse;
import com.app.hotfresh.WebService.model.response.checkoutsResponse;
import com.oppwa.mobile.connect.checkout.dialog.CheckoutActivity;
import com.oppwa.mobile.connect.checkout.meta.CheckoutSettings;
import com.oppwa.mobile.connect.exception.PaymentError;
import com.oppwa.mobile.connect.provider.Connect;
import com.oppwa.mobile.connect.provider.Transaction;
import com.oppwa.mobile.connect.provider.TransactionType;

import org.json.JSONException;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;

import io.card.payment.CardIOActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.hotfresh.Manager.RootManager.ActionKeyLocalBroadcastManager;

public class PayametActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    private FrameLayout layout_loading;

    RecyclerView recycler_view;
    RecyclerPayment adapter;
    List<HatUserWallet> hatUserWalletList = new ArrayList<>();
    private TextView toolbarNameTxt;
    private Handler handler;
    private RelativeLayout end_main;
    SwipeRefreshLayout swipeRefreshLayout;
    private RelativeLayout Layout_notification;
    private LinearLayout layout_empty;
    private TextView text_empty;
    User user = new User();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_payment);
        FontManager.applyFont(this, findViewById(R.id.layout));
        handler = new Handler(Looper.getMainLooper());


        initSetup();
    }


    private void initSetup() {
        recycler_view = findViewById(R.id.recycler_view);
        layout_loading = findViewById(R.id.layout_loading);
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        end_main = findViewById(R.id.end_main);
        layout_empty = findViewById(R.id.layout_empty);
        text_empty = findViewById(R.id.text_empty);
        layout_empty.setVisibility(View.GONE);
        recycler_view.setVisibility(View.VISIBLE);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this::onRefresh);
        Layout_notification = findViewById(R.id.Layout_notification);
        Layout_notification.setVisibility(View.GONE);

        end_main.setOnClickListener(view -> {
            onBackPressed();
        });
        layout_loading.setOnClickListener(view -> {
            return;
        });
        user = getUserDetials.User(this);
        if (user != null) {
            GetUserStatmentPaymentWebService();
        }
        toolbarNameTxt.setText("" + getResources().getString(R.string.Payments));


    }


    private void GetUserStatmentPaymentWebService() {
        if (user != null) {
            layout_empty.setVisibility(View.GONE);
            recycler_view.setVisibility(View.VISIBLE);
            layout_loading.setVisibility(View.VISIBLE);
            RetrofitWebService.getService(this).GetUserStatmentPayment(user.getAccessToken() + "").enqueue(new Callback<HatUserWalletResponse>() {
                @Override
                public void onResponse(Call<HatUserWalletResponse> call, Response<HatUserWalletResponse> response) {
                    if (RootManager.RESPONSE_CODE_OK == response.code()) {
                        if (response.body() != null) {
                            if (TextUtils.equals("True", response.body().getStatus())) {
                                hatUserWalletList = response.body().getHatUserWallets();
                                if (hatUserWalletList.size() > 0) {
                                    layout_empty.setVisibility(View.GONE);
                                    recycler_view.setVisibility(View.VISIBLE);
                                    setupRecycler();
                                } else {
                                    recycler_view.setVisibility(View.GONE);
                                    layout_empty.setVisibility(View.VISIBLE);
                                    text_empty.setText(Html.fromHtml("<font color='#FDC723'>" + getResources().getString(R.string.Payments) + "</font>" + "<br/>" + response.body().getMessage()));
                                }
                            } else if (TextUtils.equals("False", response.body().getStatus())) {
                                recycler_view.setVisibility(View.GONE);
                                layout_empty.setVisibility(View.VISIBLE);
                                text_empty.setText(Html.fromHtml("<font color='#FDC723'>" + getResources().getString(R.string.Payments) + "</font>" + "<br/>" + response.body().getMessage()));
                            }
                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(PayametActivity.this, getResources().getString(R.string.Failure),
                                getResources().getString(R.string.InternalServerError));
                    }

                    layout_loading.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<HatUserWalletResponse> call, Throwable t) {


                    layout_loading.setVisibility(View.GONE);
                    InternetConnectionUtils.isInternetAvailable(getApplicationContext(), new InternetAvailableCallback() {
                        @Override
                        public void onInternetAvailable(boolean isAvailable) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (isAvailable) {

                                        AppErrorsManager.showCustomErrorDialog(PayametActivity.this, getResources().getString(R.string.Failure),
                                                t.getMessage() + "");
                                    } else {
                                        AppErrorsManager.InternetUnAvailableDialog(PayametActivity.this, getResources().getString(R.string.Failure),
                                                getResources().getString(R.string.nointernetconnection), new InstallCallback() {
                                                    @Override
                                                    public void onStatusDone(String status) {
                                                        if (TextUtils.equals("retry", status)) {
                                                            GetUserStatmentPaymentWebService();
                                                        }
                                                    }
                                                });
                                    }
                                }
                            });
                        }
                    });
                }
            });
        }


    }


    public void setupRecycler() {
        adapter = new RecyclerPayment(getApplicationContext(), hatUserWalletList, -1, R.layout.row_item_payment, new OnItemClickTagListener() {
            @Override
            public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                HatUserWallet hatUserWallet = hatUserWalletList.get(position);
                if (TextUtils.equals("view", tag)) {
                    adapter.ItemSelect = position;
                    ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("copy", hatUserWallet.getCheckID() + "");
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(PayametActivity.this, "" + getResources().getString(R.string.Copied), Toast.LENGTH_SHORT).show();
                }
                adapter.notifyDataSetChanged();
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setAdapter(adapter);

    }

    private void GoToItemActivity(Nofitication nofitication) {
        Intent intent = new Intent(getApplicationContext(), CharitiesItemsActivity.class);
        intent.putExtra("BillCode", nofitication.getBillCode());
        startActivity(intent);
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                GetUserStatmentPaymentWebService();
            }
        }, 2000);
    }


    private AppMp3Manager appMp3Manager;
    private BroadcastReceiver mHander = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String title = intent.getStringExtra("title");
            String body = intent.getStringExtra("body");
            appMp3Manager = new AppMp3Manager(PayametActivity.this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    AppErrorsManager.showCustomErrorDialogTop(PayametActivity.this, title, body, new InstallCallback() {
                        @Override
                        public void onStatusDone(String status) {
                            GetUserStatmentPaymentWebService();
                        }
                    });
                    appMp3Manager.palyNotificationMusic(R.raw.bell_sound);
                }
            }, 1000);
        }
    };


    @Override
    protected void onRestart() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        super.onRestart();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHander);
        super.onPause();
    }
}

