package com.app.hotfresh.Ui.Fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Build;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.TextView;
import android.widget.TimePicker;


import androidx.fragment.app.DialogFragment;

import com.app.hotfresh.Manager.AppErrorsManager;
import com.app.hotfresh.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

public class TimePickerFragment extends DialogFragment
        implements TimePickerDialog.OnTimeSetListener {

    private int hours, min1;
    private int MAx_time = 0;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(),
                AlertDialog.THEME_DEVICE_DEFAULT_LIGHT, this, hour, minute, DateFormat.is24HourFormat(getActivity()));
    }

    public TimePickerFragment() {
    }

    @SuppressLint("ValidFragment")
    public TimePickerFragment(int MAx_time) {
        this.MAx_time = MAx_time;
    }

    @SuppressLint("SetTextI18n")
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        Log.e("dfdf0", hourOfDay + "");
        Calendar datetime = Calendar.getInstance();
        TextView textView = requireActivity().findViewById(R.id.deliveryTimeTxt);
        TextView textView1 = requireActivity().findViewById(R.id.deliveryDateTxt);
        String dateTxt = textView1.getText().toString();

        Locale loc = new Locale("en", "US");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", loc);
        try {
            Date date = format.parse(dateTxt);
            Date date1 = new Date();
            int diff = date1.compareTo(date);
            if (diff == 1) {
                System.out.println("The date is future day");
                if (hourOfDay < datetime.get(Calendar.HOUR_OF_DAY)) {
                    AppErrorsManager.showErrorDialog(getActivity(), getString(R.string.correct_time));
                    textView.setText("");
                } else if (hourOfDay == datetime.get(Calendar.HOUR_OF_DAY)) {
                    if (minute < datetime.get(Calendar.MINUTE)) {
                        AppErrorsManager.showErrorDialog(getActivity(), getString(R.string.correct_time));
                        textView.setText("");
                    } else {

                        String min;
                        if (minute < 10)
                            min = "0" + minute;
                        else
                            min = String.valueOf(minute);

                        String timeing = hourOfDay + ":" + min;
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm", loc);
                        java.text.DateFormat df = new SimpleDateFormat("hh:mm", loc);
                        Date startDate;
                        try {
                            Date today = Calendar.getInstance().getTime();
                            String reportDate = df.format(today);
                            Log.e("repotdate", reportDate);
                            startDate = simpleDateFormat.parse(reportDate);

                            Date endDate = simpleDateFormat.parse(timeing);
                            Log.e("repotdate", "endDate " + endDate);

                            long difference = endDate.getTime() - startDate.getTime();
                            Log.e("repotdate", "difference " + difference);

                            if (difference < 0) {
                                Date dateMax = simpleDateFormat.parse("12:00");
                                Date dateMin = simpleDateFormat.parse("00:00");
                                difference = (dateMax.getTime() - startDate.getTime()) + (endDate.getTime() - dateMin.getTime());
                            }
                            Log.e("repotdate", "difference 2 " + difference);

                            int days = (int) (difference / (1000 * 60 * 60 * 12));
                            hours = (int) ((difference - (1000 * 60 * 60 * 12 * days)) / (1000 * 60 * 60));
                            min1 = (int) (difference - (1000 * 60 * 60 * 12 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);
                            Log.e("log_tag", "Hours: " + hours + ", Mins: " + min1);

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }


                        Log.e("AllTimeMin", hours + "hours ");
                        Log.e("AllTimeMin", MAx_time + "MAx_time ");

                        int minHours = 0;
                        int AllTimeMin = 1;
                        if (MAx_time > 0) {
                            if (hours > 0) {
                                AllTimeMin += hours * 60;
                            }
                            AllTimeMin += min1;
                        }

                        Log.e("AllTimeMin", AllTimeMin + " < " + MAx_time);
                        if (MAx_time > 0) {
                            MAx_time += 5;
                        }
                        if (AllTimeMin < MAx_time) {
                            // AppErrorsManager.showErrorDialog(getActivity(), getString(R.string.after_three_current)+""+MAx_time);
                            AppErrorsManager.showErrorDialog(getActivity(), getString(R.string.havemealproductiveneedstime) + " " + MAx_time + " " + getString(R.string.min));
                        } else if (hours < 0 || min1 < 0) {
                            AppErrorsManager.showErrorDialog(getActivity(), getString(R.string.error_time));

                        } else {
                            textView.setText(timeing);
                            SetTimeHelf(timeing, textView);
                            textView.setError(null);
                        }
                    }
                } else {
                    String min;
                    if (minute < 10)
                        min = "0" + minute;
                    else
                        min = String.valueOf(minute);

                    String timeing = hourOfDay + ":" + min;
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm", loc);
                    java.text.DateFormat df = new SimpleDateFormat("hh:mm", loc);
                    Date startDate;
                    try {
                        Date today = Calendar.getInstance().getTime();
                        String reportDate = df.format(today);
                        startDate = simpleDateFormat.parse(reportDate);

                        Date endDate = simpleDateFormat.parse(timeing);

                        long difference = endDate.getTime() - startDate.getTime();

                        if (difference < 0) {
                            Date dateMax = simpleDateFormat.parse("12:00");
                            Date dateMin = simpleDateFormat.parse("00:00");
                            difference = (dateMax.getTime() - startDate.getTime()) + (endDate.getTime() - dateMin.getTime());
                        }
                        int days = (int) (difference / (1000 * 60 * 60 * 12));
                        hours = (int) ((difference - (1000 * 60 * 60 * 12 * days)) / (1000 * 60 * 60));
                        min1 = (int) (difference - (1000 * 60 * 60 * 12 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    int AllTimeMin = 1;
                    if (MAx_time > 0) {
                        if (hours > 0) {
                            AllTimeMin += hours * 60;
                        }
                        AllTimeMin += min1;
                    }
                    if (MAx_time > 0) {
                        MAx_time += 5;
                    }
                    if (AllTimeMin < MAx_time) {
                        AppErrorsManager.showErrorDialog(getActivity(), getString(R.string.havemealproductiveneedstime) + " " + MAx_time + " " + getString(R.string.min));
                    } else if (hours < 0 || min1 < 0) {
                        AppErrorsManager.showErrorDialog(getActivity(), getString(R.string.error_time));


                    } else {
                        textView.setText(timeing);
                        SetTimeHelf(timeing, textView);
                        textView.setError(null);
                    }
                }
            } else {
                String min;
                if (minute < 10)
                    min = "0" + minute;
                else
                    min = String.valueOf(minute);

                String timeing = hourOfDay + ":" + min; //+ ":" + "00"

                textView.setText(timeing);
                SetTimeHelf(timeing, textView);
                textView.setError(null);

            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }


    public void SetTimeHelf(String time, TextView textView) {
        Locale loc = new Locale("en", "US");
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm", loc);
            final Date dateObj = sdf.parse(time);
            System.out.println(dateObj);
            Log.e("SimpleDateFormat", " " + new SimpleDateFormat("K:mm").format(dateObj));
            String newTime = new SimpleDateFormat("hh:mm a").format(dateObj);
            String newTimeEn = new SimpleDateFormat("hh:mm a",loc).format(dateObj);
            textView.setTag(newTimeEn);
            Log.e("datatime",newTimeEn+"");
            textView.setText(newTime);
        } catch (final ParseException e) {
            e.printStackTrace();
        }
    }


}