package com.app.hotfresh.Ui.Activites;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.hotfresh.CallBack.InstallCallback;
import com.app.hotfresh.CallBack.OnItemClickTagListener;
import com.app.hotfresh.Manager.AppErrorsManager;
import com.app.hotfresh.Manager.AppLanguage;
import com.app.hotfresh.Manager.AppMp3Manager;
import com.app.hotfresh.Manager.AppPreferences;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Manager.RootManager;
import com.app.hotfresh.Manager.getUserDetials;
import com.app.hotfresh.Medoles.Install.Branch;
import com.app.hotfresh.Medoles.Install.Definition;
import com.app.hotfresh.Medoles.User;
import com.app.hotfresh.R;
import com.app.hotfresh.Ui.Adapters.RecyclerBranch;
import com.app.hotfresh.WebService.RetrofitWebService;
import com.app.hotfresh.WebService.model.response.LoginResponse;
import com.app.hotfresh.WebService.model.response.RootResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.hotfresh.Manager.RootManager.ActionKeyLocalBroadcastManager;
import static com.app.hotfresh.Manager.RootManager.Info_Tag_About;
import static com.app.hotfresh.Manager.RootManager.Info_Tag_Branch;
import static com.app.hotfresh.Manager.RootManager.Info_Tag_Privacy;

public class InfoActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    private FrameLayout layout_loading;


    private TextView toolbarNameTxt;
    private RelativeLayout Layout_notification;
    private RelativeLayout end_main;
    private TextView info_text;
    SwipeRefreshLayout swipeRefreshLayout;


    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppLanguage.setContentLang(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_info);
        FontManager.applyFont(this, findViewById(R.id.layout));
        handler = new Handler(Looper.getMainLooper());


        initSetup();

    }


    User user = new User();
    List<Branch> branchList = new ArrayList<>();
    Definition definition = new Definition();
    private String TagAction = "";
    RecyclerBranch adapter_branch;
    RecyclerView recycler_view;

    private void initSetup() {
        layout_loading = findViewById(R.id.layout_loading);
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        Layout_notification = findViewById(R.id.Layout_notification);
        info_text = findViewById(R.id.info_text);
        recycler_view = findViewById(R.id.recycler_view);

        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);

        end_main = findViewById(R.id.end_main);
        end_main.setOnClickListener(view -> {
            onBackPressed();
        });
        user = getUserDetials.User(this);


        Layout_notification.setVisibility(View.GONE);
        layout_loading.setOnClickListener(view -> {
            return;
        });
        String branchesJson = AppPreferences.getString(InfoActivity.this, "branchesJson");
        String definitionsJson = AppPreferences.getString(InfoActivity.this, "definitionsJson");
        if (branchList.size() < 1) {
            if (branchesJson != null) {
                branchList = new Gson().fromJson(branchesJson, new TypeToken<List<Branch>>() {
                }.getType());
            }
        }
        if (definitionsJson != null) {
            definition = new Gson().fromJson(definitionsJson, new Definition().getClass());
        }

        layout_loading.setVisibility(View.GONE);
        TagAction = getIntent().getStringExtra("TagAction");
        if (TextUtils.equals(Info_Tag_Privacy, TagAction)) {
            recycler_view.setVisibility(View.GONE);
            info_text.setText("" + definition.getPrivacy());
            toolbarNameTxt.setText("" + getResources().getString(R.string.Privacy));
        } else if (TextUtils.equals(Info_Tag_About, TagAction)) {
            recycler_view.setVisibility(View.GONE);
            info_text.setText("" + definition.getAboutUs());
            toolbarNameTxt.setText("" + getResources().getString(R.string.aboutus));
        } else if (TextUtils.equals(Info_Tag_Branch, TagAction)) {
            toolbarNameTxt.setText("" + getResources().getString(R.string.Branchs));
            recycler_view.setVisibility(View.VISIBLE);
            info_text.setVisibility(View.GONE);
            adapter_branch = new RecyclerBranch(this, branchList, 10000, R.layout.row_item_branch_info, new OnItemClickTagListener() {
                @Override
                public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                    if (TextUtils.equals("map", tag)) {
                        GoToMapBranchActivity(branchList.get(position).getLat(), branchList.get(position).getLong());
                    }

                }
            });
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            recycler_view.setLayoutManager(linearLayoutManager);
            recycler_view.setAdapter(adapter_branch);
            adapter_branch.notifyDataSetChanged();
        }


    }

    private void GoToMapBranchActivity(String lat, String aLong) {
        Uri gmmIntentUri = Uri.parse("google.streetview:cbll=" + lat + "," + aLong);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }


    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                initSetup();
            }
        }, 2000);
    }

    private AppMp3Manager appMp3Manager;
    private BroadcastReceiver mHander = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String title = intent.getStringExtra("title");
            String body = intent.getStringExtra("body");
            appMp3Manager = new AppMp3Manager(InfoActivity.this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    AppErrorsManager.showCustomErrorDialogTop(InfoActivity.this, title, body, new InstallCallback() {
                        @Override
                        public void onStatusDone(String status) {

                        }
                    });
                    appMp3Manager.palyNotificationMusic(R.raw.bell_sound);
                }
            }, 1000);
        }
    };


    @Override
    protected void onRestart() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        super.onRestart();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHander);
        super.onPause();
    }
}
