package com.app.hotfresh.Ui.Activites;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.hotfresh.CallBack.InstallCallback;
import com.app.hotfresh.CallBack.InstallTwoStringCallback;
import com.app.hotfresh.CallBack.InternetAvailableCallback;
import com.app.hotfresh.CallBack.OnItemClickTagListener;
import com.app.hotfresh.DataBase.InsertUpdateDeleteItem;
import com.app.hotfresh.Manager.AppErrorsManager;
import com.app.hotfresh.Manager.AppLanguage;
import com.app.hotfresh.Manager.AppMp3Manager;
import com.app.hotfresh.Manager.AppPreferences;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Manager.InternetConnectionUtils;
import com.app.hotfresh.Manager.RootManager;
import com.app.hotfresh.Manager.getUserDetials;
import com.app.hotfresh.Medoles.Install.Branch;
import com.app.hotfresh.Medoles.Item;
import com.app.hotfresh.Medoles.ItemService;
import com.app.hotfresh.Medoles.OnlineOrder;
import com.app.hotfresh.Medoles.Order;
import com.app.hotfresh.Medoles.OrderDetials;
import com.app.hotfresh.Medoles.User;
import com.app.hotfresh.R;
import com.app.hotfresh.Ui.Adapters.RecyclerItemOrders;
import com.app.hotfresh.Ui.Adapters.RecyclerOrder;
import com.app.hotfresh.Ui.Adapters.RecyclerServiceDialog;
import com.app.hotfresh.WebService.RetrofitWebService;
import com.app.hotfresh.WebService.model.response.DObjectResponse;
import com.app.hotfresh.WebService.model.response.DResponse;
import com.app.hotfresh.WebService.model.response.LoginResponse;
import com.app.hotfresh.WebService.model.response.OrderResponse;
import com.app.hotfresh.WebService.model.response.RootResponse;
import com.app.hotfresh.WebService.model.response.checkoutInternelResponse;
import com.app.hotfresh.WebService.model.response.checkoutsResponse;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.oppwa.mobile.connect.checkout.dialog.CheckoutActivity;
import com.oppwa.mobile.connect.checkout.meta.CheckoutSettings;
import com.oppwa.mobile.connect.exception.PaymentError;
import com.oppwa.mobile.connect.provider.Connect;
import com.oppwa.mobile.connect.provider.Transaction;
import com.oppwa.mobile.connect.provider.TransactionType;

import org.json.JSONException;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;

import io.card.payment.CardIOActivity;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.hotfresh.Manager.RootManager.ActionKeyLocalBroadcastManager;
import static com.app.hotfresh.Manager.RootManager.Company_Select;

public class DetailsOrderActivity extends AppCompatActivity implements View.OnClickListener {


    private FrameLayout layout_loading;
    RecyclerView recycler_view;
    RecyclerItemOrders adapter;
    private List<OnlineOrder> onlineOrders = new ArrayList<>();

    private TextView toolbarNameTxt;
    private RelativeLayout Layout_notification;
    private RelativeLayout end_main;

    private Handler handler;
    private String checkoutId = "";
    private String BillCode = "";
    private Order order = null;
    private OrderResponse orderDetials = new OrderResponse();

    TextView row_date, text_tax, row_text_price, text_status, row_date_order;
    LinearLayout layout_status, layout_sheet_pay;
    ImageView img_status;
    RelativeLayout layout_data;
    TextView text_invoicen_umber;
    Button btn_sheet_cancel, btn_sheet_confirm, btn_sheet_pay;
    User user = new User();
    private TextView text_type_delivery, text_payment, text_charitie_name, text_address, text_notes, text_Branch;
    private LinearLayout layout_charities, layout_address, layout_notes;

    TextView trackingMapBtn, text_staus_pay;
    LinearLayout layout_traking;
    ImageView img_tracking_State;

    private boolean BackToHome = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppLanguage.setContentLang(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_order_details);
        FontManager.applyFont(this, findViewById(R.id.layout));
        user = getUserDetials.User(this);
        BillCode = getIntent().getStringExtra("BillCode");

        Log.e("BillCode", BillCode + "");
        String order_str = getIntent().getStringExtra("order_str");
        if (order_str != null) {
            order = new Gson().fromJson(order_str, Order.class);
        } else if (getIntent().getStringExtra("BillCode") != null) {
            if (TextUtils.isEmpty(getIntent().getStringExtra("BillCode"))) {
                BackToHome = true;
            }
        }
        handler = new Handler(Looper.getMainLooper());


        initSetup();

    }


    private void initSetup() {
        layout_loading = findViewById(R.id.layout_loading);
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        Layout_notification = findViewById(R.id.Layout_notification);
        recycler_view = findViewById(R.id.recycler_view);
        btn_sheet_cancel = findViewById(R.id.btn_sheet_cancel);
        btn_sheet_confirm = findViewById(R.id.btn_sheet_confirm);
        btn_sheet_pay = findViewById(R.id.btn_sheet_pay);
        layout_sheet_pay = findViewById(R.id.layout_sheet_pay);
        btn_sheet_pay.setVisibility(View.GONE);
        layout_sheet_pay.setVisibility(View.GONE);
        btn_sheet_cancel.setVisibility(View.GONE);
        btn_sheet_confirm.setVisibility(View.GONE);
        btn_sheet_cancel.setOnClickListener(this);
        btn_sheet_confirm.setOnClickListener(this);
        row_date_order = findViewById(R.id.row_date_order);
        row_date_order.setText("--");
        trackingMapBtn = findViewById(R.id.trackingMapBtn);
        trackingMapBtn.setOnClickListener(this::onClick);
        btn_sheet_pay.setOnClickListener(this);
        layout_traking = findViewById(R.id.layout_traking);
        layout_traking.setVisibility(View.GONE);
        img_tracking_State = findViewById(R.id.img_tracking_State);

        end_main = findViewById(R.id.end_main);
        text_Branch = findViewById(R.id.text_Branch);


        text_type_delivery = findViewById(R.id.text_type_delivery);
        text_payment = findViewById(R.id.text_payment);
        text_charitie_name = findViewById(R.id.text_charitie_name);
        layout_charities = findViewById(R.id.layout_charities);
        layout_charities.setVisibility(View.GONE);


        layout_address = findViewById(R.id.layout_address);
        layout_address.setVisibility(View.GONE);
        text_address = findViewById(R.id.text_address);

        layout_notes = findViewById(R.id.layout_notes);
        layout_notes.setVisibility(View.GONE);
        text_notes = findViewById(R.id.text_notes);


        end_main.setOnClickListener(view -> {
            onBackPressed();
        });
        User user = new User();
        user = getUserDetials.User(this);


        Layout_notification.setVisibility(View.GONE);
        layout_loading.setOnClickListener(view -> {
            return;
        });

        toolbarNameTxt.setText("" + getResources().getString(R.string.DetailsOrder));
        if (!TextUtils.isEmpty(BillCode)) {
            GetOrderDetailsWebService(BillCode);
        }
        text_tax = findViewById(R.id.text_tax);
        row_date = findViewById(R.id.row_date);
        row_text_price = findViewById(R.id.row_text_price);
        layout_status = findViewById(R.id.layout_status);
        img_status = findViewById(R.id.img_status);
        text_status = findViewById(R.id.text_status);
        text_invoicen_umber = findViewById(R.id.text_invoicen_umber);
        text_staus_pay = findViewById(R.id.text_staus_pay);

        if (order != null) {
            text_tax.setText(order.getTax() + "" + " " + getResources().getString(R.string.SAR));
            row_date.setText(order.getBillDate() + "");
            row_date_order.setText(order.getBillDate() + "");
            row_text_price.setText(order.getTotal() + " " + getResources().getString(R.string.SAR));
            text_invoicen_umber.setText("" + order.getBillCode());
            text_staus_pay.setText("--");

            if (!TextUtils.isEmpty(order.getStatus())) {
                Log.e("getStatus", order.getStatus() + " F");
                String message = RootManager.GetMessageStatusByTagName(order.getStatus(), getApplicationContext(), img_status);
                text_status.setText("" + message);
                if (TextUtils.equals(order.getStatus(), RootManager.Stats_uncertain)) {
                    btn_sheet_cancel.setVisibility(View.VISIBLE);
                    btn_sheet_confirm.setVisibility(View.GONE);
                } else if (TextUtils.equals(order.getStatus(), RootManager.Stats_updated)) {
                    btn_sheet_cancel.setVisibility(View.VISIBLE);
                    btn_sheet_confirm.setVisibility(View.VISIBLE);
                } else {
                    btn_sheet_cancel.setVisibility(View.GONE);
                    btn_sheet_confirm.setVisibility(View.GONE);
                }
            }

        }
        SetUpBottomSheet();
    }

    private void instalBranch(String company) {
        List<Branch> branchList = new ArrayList<>();
        String branchesJson = AppPreferences.getString(DetailsOrderActivity.this, "branchesJson");

        if (branchesJson != null) {
            branchList = new Gson().fromJson(branchesJson, new TypeToken<List<Branch>>() {
            }.getType());
        }

        for (int i = 0; i < branchList.size(); i++) {
            if (TextUtils.equals(company, branchList.get(i).getCompany())) {
                text_Branch.setText(branchList.get(i).getName());
            }
        }

    }

    private String TotalOrderPay = "";
    private void SetValueToView(OrderResponse order) {
        if (order != null) {
            company = order.getCompany();
            instalBranch(company);
            text_tax.setText(order.getTax() + "" + " " + getResources().getString(R.string.SAR));
            row_date.setText(order.getReceivedDate() + " " + order.getReceivedTime());
            row_date_order.setText(order.getBillDate() + " " + order.getBillTime());
            row_text_price.setText(order.getTotal() + " " + getResources().getString(R.string.SAR));
            TotalOrderPay = order.getTotal() ;
            text_invoicen_umber.setText("" + order.getBillCode());

            if (!TextUtils.isEmpty(order.getPayStatus())) {
                text_staus_pay.setText("" + order.getPayStatus());
                if (TextUtils.equals("True", order.getPayStatus())) {
                    text_staus_pay.setText("" + getResources().getString(R.string.paid));
                } else {
                    text_staus_pay.setText("" + getResources().getString(R.string.unpaid));
                }
            }
            if (!TextUtils.isEmpty(order.getOrderStatus())) {
                String message = RootManager.GetMessageStatusByTagName(order.getOrderStatus(), getApplicationContext(), img_status);
                text_status.setText("" + message);
                if (TextUtils.equals(order.getOrderStatus(), RootManager.Stats_uncertain)) {
                    btn_sheet_cancel.setVisibility(View.VISIBLE);
                    btn_sheet_confirm.setVisibility(View.GONE);
                } else if (TextUtils.equals(order.getOrderStatus(), RootManager.Stats_updated)) {
                    btn_sheet_cancel.setVisibility(View.VISIBLE);
                    btn_sheet_confirm.setVisibility(View.VISIBLE);
                } else {
                    btn_sheet_cancel.setVisibility(View.GONE);
                    btn_sheet_confirm.setVisibility(View.GONE);
                }
            }

            text_type_delivery.setText("" + order.getDeliveryType());
            if (!TextUtils.isEmpty(order.getDeliveryType() + "")) {
                Log.e("getDeliveryType", order.getDeliveryType() + "|");
                if (TextUtils.equals(order.getDeliveryType().trim() + "", RootManager.DeliveryType_Cash)) {
                    text_type_delivery.setText("" + getResources().getString(R.string.Receipt_shop));
                } else if (TextUtils.equals(order.getDeliveryType().trim() + "", RootManager.DeliveryType_OnHand)) {
                    if (!TextUtils.isEmpty(order.getCharitiesName() + "")) {
                        text_type_delivery.setText("" + getResources().getString(R.string.delivere_to_home));
                        layout_address.setVisibility(View.GONE);
                        text_address.setText("");
                    } else {
                        text_type_delivery.setText("" + getResources().getString(R.string.delivere_to_home));
                        layout_address.setVisibility(View.VISIBLE);
                        text_address.setText("" + order.getCustAddress());
                    }
                    TextView TextDeliveryAmount = findViewById(R.id.TextDeliveryAmount);
                    LinearLayout LayoutDeliveryAmount = findViewById(R.id.LayoutDeliveryAmount);

                    LayoutDeliveryAmount.setVisibility(View.GONE);
                    if (order.getDeliveryAmount() != null) {
                        if (!TextUtils.isEmpty(order.getDeliveryAmount())) {
                            LayoutDeliveryAmount.setVisibility(View.VISIBLE);
                            TextDeliveryAmount.setText(String.format(Locale.ENGLISH, "%.2f", Double.parseDouble(order.getDeliveryAmount())) + " " + getResources().getString(R.string.SAR));

                        }
                    }
                }
            }
            if (!TextUtils.isEmpty(order.getCharitiesName() + "")) {
                text_charitie_name.setText("" + order.getCharitiesName());
                layout_charities.setVisibility(View.VISIBLE);
                text_charitie_name.setTextColor(getResources().getColor(R.color.colorGreen));
            } else {
                layout_charities.setVisibility(View.GONE);
            }


            if (TextUtils.isEmpty(order.getNotes() + "")) {
                layout_notes.setVisibility(View.VISIBLE);
                text_notes.setText("" + getResources().getString(R.string.dont_have_notes));
            } else {
                if (TextUtils.equals("Nothing", order.getNotes() + "")) {
                    layout_notes.setVisibility(View.VISIBLE);
                    text_notes.setText("" + getResources().getString(R.string.dont_have_notes));
                } else {
                    layout_notes.setVisibility(View.VISIBLE);
                    text_notes.setText("" + order.getNotes());
                }
            }

            text_payment.setText("" + order.getPaymentType());
            if (TextUtils.equals(order.getPaymentType() + "", RootManager.PaymentType_cash)) {
                text_payment.setText("" + getResources().getString(R.string.payment_cash));
            } else if (TextUtils.equals(order.getPaymentType() + "", RootManager.PaymentType_visa)) {
                Log.e("ordderchickidd", order.getCheckID() + " f");
                text_payment.setText("" + getResources().getString(R.string.payment_visa));
                if (TextUtils.equals(order.getOrderStatus(), RootManager.Stats_certain)) {
                    if (TextUtils.isEmpty(order.getCheckID())) {
                        btn_sheet_pay.setVisibility(View.VISIBLE);
                        layout_sheet_pay.setVisibility(View.VISIBLE);
                    } else {
                        btn_sheet_pay.setVisibility(View.GONE);
                        layout_sheet_pay.setVisibility(View.GONE);
                    }
                } else {
                    btn_sheet_pay.setVisibility(View.GONE);
                    layout_sheet_pay.setVisibility(View.GONE);
                }
            } else if (TextUtils.equals(order.getPaymentType() + "", RootManager.PaymentType_wallet)) {
                text_payment.setText("" + getResources().getString(R.string.payment_wallet));
            } else if (TextUtils.equals(order.getPaymentType() + "", RootManager.PaymentType_PayCash)) {
                text_payment.setText("" + getResources().getString(R.string.payment_PayCash));
            } else if (TextUtils.equals(order.getPaymentType() + "", RootManager.PaymentType_Global)) {
                text_payment.setText("" + getResources().getString(R.string.payment_Global));

            }


            //Data Traking
            //chick Is Order  DeliveryType ==> ToHome
            if (!TextUtils.isEmpty(order.getOrderStatus())) {
                if (TextUtils.equals(order.getOrderStatus(), RootManager.Stats_ready) ||
                        TextUtils.equals(order.getOrderStatus(), RootManager.Stats_recevied) ||
                        TextUtils.equals(order.getOrderStatus(), RootManager.Stats_OnWay)) {


                    if (TextUtils.equals(order.getDeliveryType().trim() + "", RootManager.DeliveryType_OnHand)) {


                        trackingMapBtn.setText("" + getResources().getString(R.string.TrakengMyOrder));
                        if (!TextUtils.isEmpty(order.getDeliveryID())) {
                            layout_traking.setVisibility(View.VISIBLE);
                            if (TextUtils.equals("Received", order.getDeliveryStatus())) {
                                trackingMapBtn.setText("" + getResources().getString(R.string.not_available));
                                trackingMapBtn.setEnabled(false);
                                trackingMapBtn.getBackground().setColorFilter(getResources().getColor(R.color.colorRed), PorterDuff.Mode.MULTIPLY);
                                img_tracking_State.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorRed), android.graphics.PorterDuff.Mode.SRC_IN);
                            } else if (TextUtils.equals("OnWay", order.getDeliveryStatus())) {
                                trackingMapBtn.setEnabled(true);
                                trackingMapBtn.setText("" + getResources().getString(R.string.available));
                                trackingMapBtn.getBackground().setColorFilter(getResources().getColor(R.color.colorGreen), PorterDuff.Mode.MULTIPLY);
                                img_tracking_State.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorGreen), android.graphics.PorterDuff.Mode.SRC_IN);
                            } else if (TextUtils.equals("Customer Recevied", order.getDeliveryStatus())) {
                                trackingMapBtn.setEnabled(true);
                                trackingMapBtn.setText("" + getResources().getString(R.string.available));
                                trackingMapBtn.getBackground().setColorFilter(getResources().getColor(R.color.colorGreen), PorterDuff.Mode.MULTIPLY);
                                img_tracking_State.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorGreen), android.graphics.PorterDuff.Mode.SRC_IN);

                            } else if (TextUtils.equals("CustomerRecevied", order.getDeliveryStatus())) {
                                trackingMapBtn.setEnabled(true);
                                trackingMapBtn.setText("" + getResources().getString(R.string.available));
                                trackingMapBtn.getBackground().setColorFilter(getResources().getColor(R.color.colorGreen), PorterDuff.Mode.MULTIPLY);
                                img_tracking_State.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorGreen), android.graphics.PorterDuff.Mode.SRC_IN);
                            } else {
                                trackingMapBtn.setText("" + getResources().getString(R.string.not_available));
                                trackingMapBtn.setEnabled(false);
                                trackingMapBtn.getBackground().setColorFilter(getResources().getColor(R.color.colorRed), PorterDuff.Mode.MULTIPLY);
                                img_tracking_State.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorRed), android.graphics.PorterDuff.Mode.SRC_IN);
                            }

                        } else {
                            layout_traking.setVisibility(View.GONE);
                        }


                    }


                    ChickFireBaseHaveOrderData(order);
                }
            }
        }

        // GetMyProfile();
    }

    FirebaseFirestore db;
    private boolean IsHaveFBData = false;

    private void ChickFireBaseHaveOrderData(OrderResponse order) {
        if (order != null) {
            db = FirebaseFirestore.getInstance();
            if (db != null) {
                db.collection("Delevery::").document(order.getBillCode()).collection(order.getBillCode())
                        .whereEqualTo("OrderId", order.getBillCode()).get().addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        if (queryDocumentSnapshots.size() == 0) {
                            IsHaveFBData = false;
                            trackingMapBtn.setText("" + getResources().getString(R.string.not_available));
                            trackingMapBtn.setEnabled(false);
                            trackingMapBtn.getBackground().setColorFilter(getResources().getColor(R.color.colorRed), PorterDuff.Mode.MULTIPLY);
                            img_tracking_State.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorRed), android.graphics.PorterDuff.Mode.SRC_IN);

                        } else {
                            IsHaveFBData = true;
                            trackingMapBtn.setEnabled(true);
                            trackingMapBtn.setText("" + getResources().getString(R.string.available));
                            trackingMapBtn.getBackground().setColorFilter(getResources().getColor(R.color.colorGreen), PorterDuff.Mode.MULTIPLY);
                            img_tracking_State.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorGreen), android.graphics.PorterDuff.Mode.SRC_IN);

                        }


                    }
                });
            }
        }
    }

    BottomSheetBehavior bottomSheetBehavior;

    private void SetUpBottomSheet() {
        LinearLayout llBottomSheet = findViewById(R.id.bottom_sheet);
        bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        //  bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        bottomSheetBehavior.setPeekHeight(120);
        bottomSheetBehavior.setHideable(true);
        bottomSheetBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                Log.e("addBottomSheetCallback", newState + " ");
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                Log.e("addBottomSheetCallback", slideOffset + " ");

            }
        });
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

    }


    private void GetOrderDetailsWebService(String billCode) {
        layout_loading.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(this).GetOrderDetials(billCode).enqueue(new Callback<OrderResponse>() {
            @Override
            public void onResponse(Call<OrderResponse> call, Response<OrderResponse> response) {
                Log.e("GetOrderDetials", response.toString());
                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (TextUtils.equals("True", response.body().getStatus())) {
                        if (response.body() != null) {
                            onlineOrders = response.body().getOrders();
                            if (onlineOrders != null) {
                                setupRecycler();
                            }
                            orderDetials = response.body();
                            if (orderDetials != null) {
                                 ;
                                SetValueToView(orderDetials);
                            }
                        } else {
                            AppErrorsManager.showCustomErrorDialog(DetailsOrderActivity.this, getResources().getString(R.string.error),
                                    response.body().getMessage());
                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(DetailsOrderActivity.this, getResources().getString(R.string.error),
                                response.body().getMessage());
                    }
                } else if (TextUtils.equals("False", response.body().getStatus())) {
                    AppErrorsManager.showCustomErrorDialog(DetailsOrderActivity.this, getResources().getString(R.string.error),
                            response.body().getMessage());
                } else {
                    AppErrorsManager.showCustomErrorDialog(DetailsOrderActivity.this, getResources().getString(R.string.Failure),
                            getResources().getString(R.string.InternalServerError));
                }
                layout_loading.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<OrderResponse> call, Throwable t) {
                layout_loading.setVisibility(View.GONE);
                InternetConnectionUtils.isInternetAvailable(getApplicationContext(), new InternetAvailableCallback() {
                    @Override
                    public void onInternetAvailable(boolean isAvailable) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {

                                if (isAvailable) {
                                    AppErrorsManager.showCustomErrorDialog(DetailsOrderActivity.this, getResources().getString(R.string.Failure),
                                            t.getMessage() + "");

                                } else {
                                    AppErrorsManager.InternetUnAvailableDialog(DetailsOrderActivity.this, getResources().getString(R.string.Failure),
                                            getResources().getString(R.string.nointernetconnection), new InstallCallback() {
                                                @Override
                                                public void onStatusDone(String status) {
                                                    if (TextUtils.equals("retry", status)) {
                                                        GetOrderDetailsWebService(billCode);
                                                    }
                                                }
                                            });
                                }
                            }
                        });
                    }
                });
            }
        });
    }

    public void setupRecycler() {
        adapter = new RecyclerItemOrders(getApplicationContext(), onlineOrders, R.layout.row_item_online_order, new OnItemClickTagListener() {
            @Override
            public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                OnlineOrder item = onlineOrders.get(position);
                if (TextUtils.equals("service", tag)) {
                    ShowDialogServiceItem(item.getItemServices());
                }
                adapter.notifyDataSetChanged();

            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setAdapter(adapter);

    }

    private String company = "hatt14";

    private void SetRetriveOrderWebService(String BillCode, String RetriveType) {
        layout_loading.setVisibility(View.VISIBLE);

        RetrofitWebService.getService(this).SetRetriveOrder(BillCode, RetriveType, company).enqueue(new Callback<RootResponse>() {
            @Override
            public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                Log.e("SetRetriveOrder", response.toString());
                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (response.body() != null) {
                        //   if (response.body().getStatus().contains("{")) {
                        Log.e("SetRetriveOrder", response.body().toString());
                        RootResponse rootResponse = response.body();
                        //new Gson().fromJson(response.body().getStatus(), RootResponse.class);
                        if (TextUtils.equals("True", rootResponse.getStatus())) {
                            AppErrorsManager.showCustomErrorDialogNotCancel(DetailsOrderActivity.this, getResources().getString(R.string.info),
                                    rootResponse.getMessage(), new InstallCallback() {
                                        @Override
                                        public void onStatusDone(String status) {
                                            Intent intent = getIntent();
                                            setResult(300, intent);
                                            finish();
                                        }
                                    });

                        } else {
                            AppErrorsManager.showCustomErrorDialog(DetailsOrderActivity.this, getResources().getString(R.string.error),
                                    rootResponse.getMessage());
                        }
                        // }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(DetailsOrderActivity.this, getResources().getString(R.string.Failure),
                                getResources().getString(R.string.InternalServerError));
                    }
                } else {
                    AppErrorsManager.showCustomErrorDialog(DetailsOrderActivity.this, getResources().getString(R.string.Failure),
                            getResources().getString(R.string.InternalServerError));
                }
                layout_loading.setVisibility(View.GONE);

                if (response.errorBody() != null) {
                    try {
                        Log.e("SetRetriveOrder", " error code: " + response.code() + " error content: " + response.errorBody().string().toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<RootResponse> call, Throwable t) {
                layout_loading.setVisibility(View.GONE);
                InternetConnectionUtils.isInternetAvailable(getApplicationContext(), new InternetAvailableCallback() {
                    @Override
                    public void onInternetAvailable(boolean isAvailable) {

                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (isAvailable) {
                                    AppErrorsManager.showCustomErrorDialog(DetailsOrderActivity.this, getResources().getString(R.string.Failure),
                                            t.getMessage() + "");

                                } else {
                                    AppErrorsManager.InternetUnAvailableDialog(DetailsOrderActivity.this, getResources().getString(R.string.Failure),
                                            getResources().getString(R.string.nointernetconnection), new InstallCallback() {
                                                @Override
                                                public void onStatusDone(String status) {
                                                    if (TextUtils.equals("retry", status)) {
                                                        SetRetriveOrderWebService(BillCode, RetriveType);
                                                    }
                                                }
                                            });
                                }
                            }
                        });
                    }
                });

            }
        });

    }

    private void UpdateCheckIDWebService(String BillCode, String checkoutId) {
        layout_loading.setVisibility(View.VISIBLE);

        RetrofitWebService.getService(this).UpdateCheckID(BillCode, checkoutId).enqueue(new Callback<RootResponse>() {
            @Override
            public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                Log.e("UpdateCheckID", response.toString());
                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (response.body() != null) {
                        //   if (response.body().getStatus().contains("{")) {
                        Log.e("SetRetriveOrder", response.body().toString());
                        RootResponse rootResponse = response.body();
                        //new Gson().fromJson(response.body().getStatus(), RootResponse.class);
                        if (TextUtils.equals("True", rootResponse.getStatus())) {
                            GetOrderDetailsWebService(BillCode);
                            AppErrorsManager.showCustomErrorDialogNotCancel(DetailsOrderActivity.this, getResources().getString(R.string.info),
                                    rootResponse.getMessage(), new InstallCallback() {
                                        @Override
                                        public void onStatusDone(String status) {
                                            Intent intent = getIntent();
                                            setResult(300, intent);
                                            finish();
                                        }
                                    });

                        } else {
                            AppErrorsManager.showCustomErrorDialog(DetailsOrderActivity.this, getResources().getString(R.string.error),
                                    rootResponse.getMessage());
                        }
                        // }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(DetailsOrderActivity.this, getResources().getString(R.string.Failure),
                                getResources().getString(R.string.InternalServerError));
                    }
                } else {
                    AppErrorsManager.showCustomErrorDialog(DetailsOrderActivity.this, getResources().getString(R.string.Failure),
                            getResources().getString(R.string.InternalServerError));
                }
                layout_loading.setVisibility(View.GONE);

                if (response.errorBody() != null) {
                    try {
                        Log.e("UpdateCheckID", " error code: " + response.code() + " error content: " + response.errorBody().string().toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<RootResponse> call, Throwable t) {
                layout_loading.setVisibility(View.GONE);
                InternetConnectionUtils.isInternetAvailable(getApplicationContext(), new InternetAvailableCallback() {
                    @Override
                    public void onInternetAvailable(boolean isAvailable) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (isAvailable) {
                                    AppErrorsManager.showCustomErrorDialog(DetailsOrderActivity.this, getResources().getString(R.string.Failure),
                                            t.getMessage() + "");

                                } else {
                                    AppErrorsManager.InternetUnAvailableDialog(DetailsOrderActivity.this, getResources().getString(R.string.Failure),
                                            getResources().getString(R.string.nointernetconnection), new InstallCallback() {
                                                @Override
                                                public void onStatusDone(String status) {
                                                    if (TextUtils.equals("retry", status)) {
                                                        UpdateCheckIDWebService(BillCode, checkoutId);
                                                    }
                                                }
                                            });
                                }
                            }
                        });
                    }
                });

            }
        });

    }

    private void ShowDialogServiceItem(List<ItemService> itemServices) {

        for (int i = 0; i < itemServices.size(); i++) {
            itemServices.get(i).setIs_selected(true);
        }
        RecyclerServiceDialog adapter_service = new RecyclerServiceDialog(this, itemServices, R.layout.row_item_service, new OnItemClickTagListener() {
            @Override
            public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
            }
        });

        AppErrorsManager.showServiceListDialog(DetailsOrderActivity.this, adapter_service, getResources().getString(R.string.close), new InstallCallback() {
            @Override
            public void onStatusDone(String status) {
                if (TextUtils.equals("yes", status)) {
                }

            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_sheet_cancel:
                SetRetriveOrderWebService(BillCode, "Cancel");
                break;
            case R.id.btn_sheet_confirm:
                SetRetriveOrderWebService(BillCode, "Confirm");
                break;
            case R.id.btn_sheet_pay:
                GetMyProfile();
                break;

            case R.id.trackingMapBtn:
                if (orderDetials != null)
                    GoToMapTracking(orderDetials);
                break;


        }
    }


    private AppMp3Manager appMp3Manager;
    private BroadcastReceiver mHander = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String title = intent.getStringExtra("title");
            String body = intent.getStringExtra("body");
            appMp3Manager = new AppMp3Manager(DetailsOrderActivity.this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    AppErrorsManager.showCustomErrorDialogTop(DetailsOrderActivity.this, title, body, new InstallCallback() {
                        @Override
                        public void onStatusDone(String status) {

                        }
                    });
                    appMp3Manager.palyNotificationMusic(R.raw.bell_sound);
                }
            }, 1000);
        }
    };


    @Override
    protected void onRestart() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        super.onRestart();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHander);
        super.onPause();
    }


    private String DepositAmount = "";

    private void GetMyProfile() {
        if (user != null) {
            layout_loading.setVisibility(View.VISIBLE);
            String old_token = user.getAccessToken();
            RetrofitWebService.getService(this).GetMyProfile(user.getAccessToken()).enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    Log.e("GetMyProfile", response.toString());
                    if (RootManager.RESPONSE_CODE_OK == response.code()) {
                        if (TextUtils.equals("True", response.body().getStatus())) {
                            if (response.body() != null) {
                                User user = (User) response.body().getUser();
                                user.setAccessToken(old_token);


                                if (user != null) {
                                    Gson json = new Gson();
                                    String userJson = json.toJson(user);
                                    AppPreferences.saveString(DetailsOrderActivity.this, "userJson", userJson);
                                    if (order != null) {
                                        if (order.getTotal() != null) {
                                            Log.e("temp", order.getTotal() + " temp");
                                            AppErrorsManager.showSelectTypePayDialog(DetailsOrderActivity.this, new InstallTwoStringCallback() {
                                                @Override
                                                public void onStatusDone(String amount, String typePay) {
                                                    SelectTypePay = typePay;
                                                    GetCheckOutID(order.getTotal() + "", typePay);
                                                }
                                            });
                                        }else {
                                            Log.e("temp", TotalOrderPay + " TotalOrderPay");

                                            AppErrorsManager.showSelectTypePayDialog(DetailsOrderActivity.this, new InstallTwoStringCallback() {
                                                @Override
                                                public void onStatusDone(String amount, String typePay) {
                                                    SelectTypePay = typePay;
                                                    GetCheckOutID(TotalOrderPay + "", typePay);
                                                }
                                            });
                                        }
                                    }else {
                                        Log.e("temp", TotalOrderPay + " TotalOrderPay");

                                        AppErrorsManager.showSelectTypePayDialog(DetailsOrderActivity.this, new InstallTwoStringCallback() {
                                            @Override
                                            public void onStatusDone(String amount, String typePay) {
                                                SelectTypePay = typePay;
                                                GetCheckOutID(TotalOrderPay + "", typePay);
                                            }
                                        });
                                    }
                                } else {

                                    AppErrorsManager.showCustomErrorDialog(DetailsOrderActivity.this, getResources().getString(R.string.error),
                                            response.body().getMessage());
                                }
                            } else {
                                AppErrorsManager.showCustomErrorDialog(DetailsOrderActivity.this, getResources().getString(R.string.error),
                                        response.body().getMessage());
                            }
                        } else if (TextUtils.equals("False", response.body().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(DetailsOrderActivity.this, getResources().getString(R.string.error),
                                    response.body().getMessage());
                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(DetailsOrderActivity.this, getResources().getString(R.string.Failure),
                                getResources().getString(R.string.InternalServerError));
                    }
                    layout_loading.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Log.e("GetMyProfile", t.toString());
                    layout_loading.setVisibility(View.GONE);
                    AppErrorsManager.showCustomErrorDialog(DetailsOrderActivity.this, getResources().getString(R.string.Failure),
                            t.getMessage() + "");
                }
            });
        }
    }

    /**
     * private void GetCheckOutID(String depositAmount) {
     * DepositAmount = depositAmount;
     * if (depositAmount.length() > 1) {
     * Double d = Double.parseDouble(depositAmount);
     * depositAmount = String.format(Locale.ENGLISH, "%.2f", d);
     * }
     * layout_loading.setVisibility(View.VISIBLE);
     * RetrofitWebService.getServiceOut().requestCheckoutId(
     * "8ac7a4ca71aac89b0171b3d5027d1695"
     * , depositAmount
     * , "SAR"
     * , "DB").enqueue(new Callback<checkoutsResponse>() {
     *
     * @Override public void onResponse(Call<checkoutsResponse> call, Response<checkoutsResponse> response) {
     * Log.e("requestCheckoutId", response.toString());
     * if (RootManager.RESPONSE_CODE_OK == response.code()) {
     * if (response.body() != null) {
     * checkoutId = response.body().getId();
     * Log.e("requestCheckoutId", response.body().getResult().getDescription() + "    " + checkoutId);
     * <p>
     * configCheckout(checkoutId);
     * }
     * } else {
     * AppErrorsManager.showCustomErrorDialog(DetailsOrderActivity.this, getResources().getString(R.string.Failure),
     * getResources().getString(R.string.InternalServerError));
     * }
     * <p>
     * <p>
     * if (response.errorBody() != null) {
     * try {
     * Log.e("requestCheckoutId", " error code: " + response.code() + " error content: " + response.errorBody().string().toString());
     * } catch (IOException e) {
     * e.printStackTrace();
     * }
     * }
     * <p>
     * layout_loading.setVisibility(View.GONE);
     * }
     * @Override public void onFailure(Call<checkoutsResponse> call, Throwable t) {
     * AppErrorsManager.showCustomErrorDialog(DetailsOrderActivity.this, getResources().getString(R.string.Failure),
     * t.getMessage() + "");
     * layout_loading.setVisibility(View.GONE);
     * }
     * });
     * }
     */


    private String SelectTypePay = RootManager.TypeSelectVisa;

    private void GetCheckOutID(String depositAmount, String typePay) {
        DepositAmount = depositAmount;
        if (depositAmount.length() > 1) {
            Double d = Double.parseDouble(depositAmount);
            depositAmount = String.format(Locale.ENGLISH, "%.2f", d);
        }
        layout_loading.setVisibility(View.VISIBLE);
        String finalDepositAmount = depositAmount;
        RetrofitWebService.getServiceOut().requestCheckoutId(
                user.getAccessToken()
                , depositAmount
                , "", typePay).enqueue(new Callback<checkoutInternelResponse>() {
            @Override
            public void onResponse(Call<checkoutInternelResponse> call, Response<checkoutInternelResponse> response) {
                Log.e("requestCheckoutId", response.toString() + "    " + checkoutId);
                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (response.body() != null) {
                        if (TextUtils.equals("True", response.body().getStatus())) {
                            checkoutId = response.body().getId();
                            Log.e("requestCheckoutId", response.body().getId() + "    " + checkoutId);
                            configCheckout(checkoutId, typePay);
                        } else if (TextUtils.equals("False", response.body().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(DetailsOrderActivity.this, getResources().getString(R.string.error),
                                    response.body().getMessage() + "");
                        }
                    }
                } else {
                    AppErrorsManager.showCustomErrorDialog(DetailsOrderActivity.this, getResources().getString(R.string.Failure),
                            getResources().getString(R.string.InternalServerError));
                }
                layout_loading.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<checkoutInternelResponse> call, Throwable t) {


                layout_loading.setVisibility(View.GONE);
                InternetConnectionUtils.isInternetAvailable(getApplicationContext(), new InternetAvailableCallback() {
                    @Override
                    public void onInternetAvailable(boolean isAvailable) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (isAvailable) {
                                    AppErrorsManager.showCustomErrorDialog(DetailsOrderActivity.this, getResources().getString(R.string.Failure),
                                            t.getMessage() + "");

                                } else {
                                    AppErrorsManager.InternetUnAvailableDialog(DetailsOrderActivity.this, getResources().getString(R.string.Failure),
                                            getResources().getString(R.string.nointernetconnection), new InstallCallback() {
                                                @Override
                                                public void onStatusDone(String status) {
                                                    if (TextUtils.equals("retry", status)) {
                                                        GetCheckOutID(finalDepositAmount, typePay);
                                                    }
                                                }
                                            });
                                }
                            }
                        });
                    }
                });
            }
        });
    }


    private void checkoutsPayment(String trnid) {
        layout_loading.setVisibility(View.VISIBLE);
        IsSendRequestCheckOut = true;
        RetrofitWebService.getServiceOut().checkoutsPayment(trnid, SelectTypePay).enqueue(new Callback<RootResponse>() {
            @Override
            public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                Log.e("checkoutsPayment", response.toString() + "    " + checkoutId);
                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (response.body() != null) {
                        if (TextUtils.equals("True", response.body().getStatus())) {
                            String msg = response.body().getMessage();
                            checkoutsResponse result = new Gson().fromJson(msg, checkoutsResponse.class);
                            if (RootManager.IsResultCodesSuccessfullyPay(result.getResult().getCode() + "")) {
                                SaveTransactionWebService(DepositAmount, checkoutId);
                            } else {
                                AppErrorsManager.showCustomErrorDialog(DetailsOrderActivity.this, getResources().getString(R.string.error),
                                        result.getResult().getDescription() + " \n  " + result.getResult().getCode() + " \n");
                            }
                            //    Toast.makeText(MyWalletActivity.this, "result" + result.getResult().getDescription()+ " || "+result.getResult().getCode(), Toast.LENGTH_SHORT).show();
                            Log.e("checkoutsPayment", "result" + result.getResult().getDescription() + " || " + result.getResult().getCode());

                        } else if (TextUtils.equals("False", response.body().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(DetailsOrderActivity.this, getResources().getString(R.string.error),
                                    response.body().getMessage() + "");
                        }
                    }
                } else {
                    AppErrorsManager.showCustomErrorDialog(DetailsOrderActivity.this, getResources().getString(R.string.Failure),
                            getResources().getString(R.string.InternalServerError));
                }
                layout_loading.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<RootResponse> call, Throwable t) {
                layout_loading.setVisibility(View.GONE);
                AppErrorsManager.showCustomErrorDialog(DetailsOrderActivity.this, getResources().getString(R.string.Failure),
                        t.getMessage() + "");
            }
        });
    }

    private void configCheckout(String checkoutId, String typePay) {
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HALF_EXPANDED);
        IsSendRequestCheckOut = false;
        Set<String> paymentBrands = new LinkedHashSet<String>();
        if (TextUtils.equals(RootManager.TypeSelectMada, typePay)) {
            paymentBrands.add("MADA");
        } else {
            paymentBrands.add("VISA");
            paymentBrands.add("MASTER");
        }
        CheckoutSettings checkoutSettings = new CheckoutSettings(checkoutId, paymentBrands, Connect.ProviderMode.LIVE);
        // Set shopper result URL
        checkoutSettings.setShopperResultUrl("myorder://result");
        checkoutSettings.setLocale(AppLanguage.getLanguage(this));
        Intent intent = checkoutSettings.createCheckoutActivityIntent(this);
        startActivityForResult(intent, CheckoutActivity.REQUEST_CODE_CHECKOUT);
    }

    private void SaveTransactionWebService(String amount, String checkoutId) {
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HALF_EXPANDED);
        layout_loading.setVisibility(View.VISIBLE);
        if (user != null) {
            RetrofitWebService.getService(this).SaveTransaction(user.getMobile(), RootManager.Transaction_Archives,
                    company, amount, checkoutId).enqueue(new Callback<RootResponse>() {
                @Override
                public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                    UpdateCheckIDWebService(order.getBillCode(), checkoutId);
                    layout_loading.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<RootResponse> call, Throwable t) {
                    Log.e("SaveTransaction", t.toString());
                    UpdateCheckIDWebService(order.getBillCode(), checkoutId);
                    layout_loading.setVisibility(View.GONE);
                }
            });
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode) {
            case CheckoutActivity.RESULT_OK:
                Transaction transaction = data.getParcelableExtra(CheckoutActivity.CHECKOUT_RESULT_TRANSACTION);
                Log.e("transaction", "RESULT_OK " + transaction.toString());
                String resourcePath = data.getStringExtra(CheckoutActivity.CHECKOUT_RESULT_RESOURCE_PATH);
                Log.e("transaction", "resourcePath " + transaction.toString());

                //  checkoutsPayment(checkoutId);
                // SaveTransactionWebService(DepositAmount, checkoutId);

                if (transaction.getTransactionType() == TransactionType.SYNC) {
                    Log.e("transaction", "RESULT_OK " + transaction.getTransactionType() + " ==" + TransactionType.SYNC);
                    if (!IsSendRequestCheckOut)
                        checkoutsPayment(checkoutId);
                } else {
                    Log.e("transaction", "else " + transaction.getTransactionType() + " ==" + TransactionType.SYNC);
                }

                break;
            case CheckoutActivity.RESULT_CANCELED:
                Log.e("transaction", "RESULT_CANCELED ");
                break;
            case CheckoutActivity.RESULT_ERROR:
                PaymentError error = data.getParcelableExtra(CheckoutActivity.CHECKOUT_RESULT_ERROR);
                Log.e("transaction", error.getErrorInfo() + " RESULT_ERROR");
        }
    }

    private boolean IsSendRequestCheckOut = false;

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.getScheme().equals("myorder")) {
            String checkoutId = intent.getData().getQueryParameter("id");
            if (!IsSendRequestCheckOut)
                checkoutsPayment(checkoutId);
        }


    }

    private void GoToMapTracking(OrderResponse order) {
        if (order != null) {
            String order_str = new Gson().toJson(order, OrderResponse.class);
            Intent intent = new Intent(getApplicationContext(), MapsTrackingActivity.class);
            intent.putExtra("order_str", order_str);
            startActivity(intent);
        }

    }

    @Override
    public void onBackPressed() {
        if (BackToHome) {
            Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        } else
            super.onBackPressed();

    }
}
