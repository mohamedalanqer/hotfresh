package com.app.hotfresh.Ui.Adapters;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.hotfresh.CallBack.OnItemClickTagListener;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Medoles.Item;
import com.app.hotfresh.Medoles.OnlineOrder;
import com.app.hotfresh.R;
import com.bumptech.glide.Glide;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Locale;


public class RecyclerItemOrders extends RecyclerView.Adapter<RecyclerItemOrders.CustomView> {
    Context context;
    List<OnlineOrder> mylist;
    private OnItemClickTagListener listener;
    int Layout;


    public class CustomView extends RecyclerView.ViewHolder {
        TextView row_name, row_text_price, row_text_service, row_text_tax, text_count_cart, row_text_total;
        ImageView row_img_item;

        public CustomView(View v) {
            super(v);
            FontManager.applyFont(context, v);
            row_name = v.findViewById(R.id.row_name);
            row_text_price = v.findViewById(R.id.row_text_price);
            row_text_service = v.findViewById(R.id.row_text_service);
            text_count_cart = v.findViewById(R.id.text_count_cart);
            row_img_item = v.findViewById(R.id.row_img_item);
            row_text_total = v.findViewById(R.id.row_text_total);
            row_text_tax = v.findViewById(R.id.row_text_tax);

        }

    }

    public RecyclerItemOrders() {
    }

    public RecyclerItemOrders(Context context, List<OnlineOrder> mylist, int Layout, OnItemClickTagListener listener) {
        this.context = context;
        this.mylist = mylist;
        this.listener = listener;
        this.Layout = Layout;
    }

    @Override
    public CustomView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(Layout, parent, false);
        CustomView viewholder = new CustomView(view);
        return viewholder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final CustomView holder, final int position) {
        holder.row_name.setText(mylist.get(position).getItemName() + "");
        holder.row_text_price.setText(mylist.get(position).getPrice() + " " + context.getResources().getString(R.string.SAR));
        holder.text_count_cart.setText("" + mylist.get(position).getQty() + " " + context.getResources().getString(R.string.Quantity));
        holder.row_text_tax.setText(mylist.get(position).getTax() + " " + context.getResources().getString(R.string.Tax));
        holder.row_text_service.setVisibility(View.GONE);

        Log.e("lisrtservice", mylist.get(position).getItemServices().size() + "f");
        if (mylist.get(position).getItemServices() != null) {
            if (mylist.get(position).getItemServices().size() > 0)
                holder.row_text_service.setVisibility(View.VISIBLE);

        }

        holder.row_text_service.setOnClickListener(view -> {
            try {
                listener.onItemClick(view,position,"service");
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        });

        String total = mylist.get(position).getTotal();
        if (!TextUtils.isEmpty(total)) {
          //  String totalAsString = String.format(Locale.ENGLISH, "%.2f",total  );
            holder.row_text_total.setText(context.getResources().getString(R.string.total)+ " " +total  );

        }
        if (mylist.get(position).getImage() != null) {
            Glide.with(context).load(mylist.get(position).getImage()).into(holder.row_img_item);
        }


    }


    @Override
    public int getItemCount() {
        return mylist.size();
    }


}

