package com.app.hotfresh.Ui.Activites;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Layout;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.hotfresh.CallBack.InstallCallback;
import com.app.hotfresh.CallBack.InternetAvailableCallback;
import com.app.hotfresh.Manager.AppErrorsManager;
import com.app.hotfresh.Manager.AppLanguage;
import com.app.hotfresh.Manager.AppPreferences;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Manager.InternetConnectionUtils;
import com.app.hotfresh.Manager.RootManager;
import com.app.hotfresh.Manager.getUserDetials;
import com.app.hotfresh.Medoles.Install.Branch;
import com.app.hotfresh.Medoles.Install.Definition;
import com.app.hotfresh.Medoles.User;
import com.app.hotfresh.R;
import com.app.hotfresh.WebService.RetrofitWebService;
import com.app.hotfresh.WebService.model.response.BranchsResponse;
import com.app.hotfresh.WebService.model.response.InstallResponse;
import com.app.hotfresh.WebService.model.response.LoginResponse;
import com.app.hotfresh.WebService.model.response.TaxValueResponse;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {
    /**
     * Duration of wait
     **/
    private final int SPLASH_DISPLAY_LENGTH = 500;
    private Handler handler;
    private CardView card_view;
    private TextView text_message;
    private Button btn_close;
    private static String BillCode = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, Window.FEATURE_NO_TITLE);
        AppLanguage.setContentLang(this);
        setContentView(R.layout.activity_splash);
        FontManager.applyFont(this, findViewById(R.id.layout));
        handler = new Handler(Looper.getMainLooper());
        BillCode = "";
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            //  Log.e("ref", bundle.toString() + " bundle");
            if (!TextUtils.isEmpty(bundle.getString("BillCode"))) {
                BillCode = bundle.getString("BillCode");
                //    Log.e("ref", BillCode + " Top");

            }
        }
        initSetUp();
    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (overrideConfiguration != null) {
            int uiMode = overrideConfiguration.uiMode;
            overrideConfiguration.setTo(getBaseContext().getResources().getConfiguration());
            overrideConfiguration.uiMode = uiMode;
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    boolean isGuideShow = false;

    private void initSetUp() {
        card_view = findViewById(R.id.card_view);
        text_message = findViewById(R.id.text_message);
        card_view.setVisibility(View.GONE);
        btn_close = findViewById(R.id.btn_close);
        btn_close.setOnClickListener(view -> {
            onBackPressed();
        });
        isGuideShow = AppPreferences.getBoolean(this, "isGuideShow", false);
        /**     new Handler().postDelayed(new Runnable() {
        @Override public void run() {
        /* Create an Intent that will start the Menu-Activity. */
        /**   GetInstalDataWebService();

         }
         }, SPLASH_DISPLAY_LENGTH);*/
        setSplash();
    }

    private void setSplash() {
        Animation fade = AnimationUtils.loadAnimation(this, R.anim.alpha);
        View relSplash = findViewById(R.id.layout);
        relSplash.clearAnimation();
        relSplash.startAnimation(fade);

        fade.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                GetInstalDataWebService();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }


    private void GetInstalDataWebService() {
        String TaxValue = "5";
        AppPreferences.saveString(SplashActivity.this, "taxJson", TaxValue + "");
        RetrofitWebService.getService(this).GetInstall().enqueue(new Callback<InstallResponse>() {
            @Override
            public void onResponse(Call<InstallResponse> call, Response<InstallResponse> response) {
                Log.e("GetInstall", response.toString());
                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (response.body() != null) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (response.body().getDefinition() != null) {
                                    if (TextUtils.equals("False", response.body().getDefinition().getCloseAppStatus())) {

                                        List<Branch> branches = response.body().getBranches();
                                        if (branches != null) {
                                            String branchesJson = new Gson().toJson(branches);
                                            AppPreferences.saveString(SplashActivity.this, "branchesJson", branchesJson + "");
                                        }

                                        Definition definitions = response.body().getDefinition();
                                        if (definitions != null) {
                                            String definitionsJson = new Gson().toJson(definitions);
                                            AppPreferences.saveString(SplashActivity.this, "definitionsJson", definitionsJson + "");
                                        }

                                        User user = getUserDetials.User(getApplicationContext());

                                        if (user != null) {
                                            GetMyProfile(user);
                                        } else {
                                            if (isGuideShow == false) {
                                                Intent mainIntent = new Intent(SplashActivity.this, GuideActivity.class);
                                                mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                startActivity(mainIntent);
                                                finish();
                                            } else {
                                                Intent mainIntent = new Intent(SplashActivity.this, UserActionsActivity.class);
                                                mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                startActivity(mainIntent);
                                                finish();
                                            }
                                        }
                                    } else {
                                        card_view.setVisibility(View.VISIBLE);
                                        text_message.setText("" + response.body().getDefinition().getCloseMsg());
                                    }
                                } else {
                                    card_view.setVisibility(View.VISIBLE);
                                    text_message.setText("" + getResources().getString(R.string.InternalServerError));
                                }

                            }
                        });


                        /**    } else if (TextUtils.equals("False", response.body().getStatus())) {
                         AppErrorsManager.showCustomErrorDialog(SplashActivity.this, getResources().getString(R.string.error),
                         response.body().getMessage());
                         }*/
                    } else {
                        AppErrorsManager.showCustomErrorDialog(SplashActivity.this, getResources().getString(R.string.Failure),
                                getResources().getString(R.string.InternalServerError));
                    }
                } else {
                    AppErrorsManager.showCustomErrorDialog(SplashActivity.this, getResources().getString(R.string.Failure),
                            getResources().getString(R.string.InternalServerError));
                }
            }

            @Override
            public void onFailure(Call<InstallResponse> call, Throwable t) {

                InternetConnectionUtils.isInternetAvailable(getApplicationContext(), new InternetAvailableCallback() {
                    @Override
                    public void onInternetAvailable(boolean isAvailable) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (isAvailable) {
                                    AppErrorsManager.showCustomErrorDialog(SplashActivity.this, getResources().getString(R.string.Failure),
                                            t.getMessage() + "");
                                } else {
                                    AppErrorsManager.InternetUnAvailableDialog(SplashActivity.this, getResources().getString(R.string.Failure),
                                            getResources().getString(R.string.nointernetconnection), new InstallCallback() {
                                                @Override
                                                public void onStatusDone(String status) {
                                                    if (TextUtils.equals("retry", status)) {
                                                        GetInstalDataWebService();
                                                    } else if (TextUtils.equals("close", status)) {
                                                        onBackPressed();
                                                    }
                                                }
                                            });
                                }
                            }
                        });

                    }
                });
            }
        });

    }

    private void GetMyProfile(User user) {
        if (user != null) {
            String old_token = user.getAccessToken();
            RetrofitWebService.getService(this).GetMyProfile(user.getAccessToken()).enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    if (RootManager.RESPONSE_CODE_OK == response.code()) {
                        if (response.body() != null) {
                            if (TextUtils.equals("True", response.body().getStatus())) {

                                User user = response.body().getUser();
                                user.setAccessToken(old_token);
                                if (user != null) {
                                    if (!TextUtils.isEmpty(BillCode)) {
                                        // Log.e("ref", BillCode + " splash");
                                        Intent intent = new Intent(SplashActivity.this, DetailsOrderActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        intent.putExtra("BillCode", BillCode);
                                        PendingIntent pendingIntent = PendingIntent.getActivity(SplashActivity.this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
                                        startActivity(intent);
                                        finish();
                                    } else {
                                        Gson json = new Gson();
                                        String userJson = json.toJson(user);
                                        AppPreferences.saveString(SplashActivity.this, "userJson", userJson);
                                        if (isGuideShow == false) {
                                            Intent mainIntent = new Intent(SplashActivity.this, GuideActivity.class);
                                            mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(mainIntent);
                                            finish();
                                        } else {
                                            Intent mainIntent = new Intent(SplashActivity.this, HomeActivity.class);
                                            mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                            startActivity(mainIntent);
                                            finish();
                                        }
                                    }
                                } else {
                                    if (isGuideShow == false) {
                                        Intent mainIntent = new Intent(SplashActivity.this, GuideActivity.class);
                                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(mainIntent);
                                        finish();
                                    } else {
                                        Intent mainIntent = new Intent(SplashActivity.this, UserActionsActivity.class);
                                        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(mainIntent);
                                        finish();
                                    }
                                }
                            } else if (TextUtils.equals("False", response.body().getStatus())) {
                                if (isGuideShow == false) {
                                    Intent mainIntent = new Intent(SplashActivity.this, GuideActivity.class);
                                    mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(mainIntent);
                                    finish();
                                } else {
                                    Intent mainIntent = new Intent(SplashActivity.this, UserActionsActivity.class);
                                    mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(mainIntent);
                                    finish();
                                }
                            }
                        } else {
                            if (isGuideShow == false) {
                                Intent mainIntent = new Intent(SplashActivity.this, GuideActivity.class);
                                mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(mainIntent);
                                finish();
                            } else {
                                Intent mainIntent = new Intent(SplashActivity.this, UserActionsActivity.class);
                                mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(mainIntent);
                                finish();
                            }
                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(SplashActivity.this, getResources().getString(R.string.Failure),
                                getResources().getString(R.string.InternalServerError));
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Log.e("GetMyProfile", t.toString());
                    AppErrorsManager.showCustomErrorDialog(SplashActivity.this, getResources().getString(R.string.Failure),
                            t.getMessage() + "");
                }
            });
        }
    }
}
