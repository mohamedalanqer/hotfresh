package com.app.hotfresh.Ui.Fragments;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.hotfresh.CallBack.InstallCallback;
import com.app.hotfresh.Manager.AppErrorsManager;
import com.app.hotfresh.Manager.AppPreferences;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Manager.RootManager;
import com.app.hotfresh.Medoles.User;
import com.app.hotfresh.R;
import com.app.hotfresh.Ui.Activites.HomeActivity;
import com.app.hotfresh.Ui.Activites.UserPasswordsActivity;
import com.app.hotfresh.WebService.RetrofitWebService;
import com.app.hotfresh.WebService.model.response.LoginResponse;
import com.app.hotfresh.WebService.model.response.RootResponse;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginFragment extends Fragment implements View.OnClickListener {


    FrameLayout layout_loading;
    private EditText phoneEditText, passwordEditText;
    private TextView text_forget_pass;
    private Button btn_action;
    private String newToken = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        FontManager.applyFont(getActivity(), view);
        initSetUp(view);
        return view;
    }

    private void initSetUp(View view) {
        layout_loading = getActivity().findViewById(R.id.layout_loading);
        btn_action = getActivity().findViewById(R.id.btn_action);
        phoneEditText = view.findViewById(R.id.phoneEditText);
        passwordEditText = view.findViewById(R.id.passwordEditText);
        text_forget_pass = view.findViewById(R.id.text_forget_pass);
        text_forget_pass.setOnClickListener(this::onClick);
        btn_action.setOnClickListener(this::onClick);
        layout_loading.setOnClickListener(v -> {
            return;
        });

        FirebaseApp.initializeApp(getActivity());
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(getActivity(), new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                Log.e("newToken", newToken);

            }
        });
    }


    private void CreateLoginWebService() {
        String mobile = phoneEditText.getText().toString().trim();
        String password = passwordEditText.getText().toString().trim();
        passwordEditText.clearFocus();
        phoneEditText.clearFocus();
        if (TextUtils.isEmpty(mobile)) {
            phoneEditText.setError(getResources().getString(R.string.field_required));
            phoneEditText.requestFocus();
        }else if (mobile.length() == 10 &&  !mobile.startsWith("05" )){
            AppErrorsManager.showCustomErrorDialog(getActivity(),getResources().getString(R.string.info),
                        getResources().getString(R.string.mobilenumberdigits)  );
        }else if (mobile.length() > 9 && mobile.length() != 10) {
            if(mobile.startsWith("00966")){
                AppErrorsManager.showCustomErrorDialog(getActivity(),getResources().getString(R.string.info),
                        getResources().getString(R.string.Dont_put_countrycode_mobile) +"\n"+"00966");
            }else if(mobile.startsWith("966")){
                AppErrorsManager.showCustomErrorDialog(getActivity(),getResources().getString(R.string.info),
                        getResources().getString(R.string.Dont_put_countrycode_mobile) +"\n"+"966");

            }else if(mobile.startsWith("+966")){
                AppErrorsManager.showCustomErrorDialog(getActivity(),getResources().getString(R.string.info),
                        getResources().getString(R.string.Dont_put_countrycode_mobile) +"\n"+"+966");

            }else{
                AppErrorsManager.showCustomErrorDialog(getActivity(),getResources().getString(R.string.info),
                        getResources().getString(R.string.mobilenumberdigits)  );

            }
            phoneEditText.setError(getResources().getString(R.string.field_required));
            phoneEditText.requestFocus();
        } else if (mobile.length() < 9) {
            AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                    getResources().getString(R.string.mobilenumberdigits) +"");
            phoneEditText.setError(getResources().getString(R.string.field_required));
            phoneEditText.requestFocus();

        }else if (TextUtils.isEmpty(password)) {
            passwordEditText.setError(getResources().getString(R.string.field_required));
            passwordEditText.requestFocus();
        } else {
            layout_loading.setVisibility(View.VISIBLE);
            if (TextUtils.isEmpty(newToken)) {
                FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(getActivity(), new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {
                        newToken = instanceIdResult.getToken();
                        Log.e("newToken", newToken);

                    }
                });
            }

            if(mobile.length() == 10 && mobile.startsWith("05")){
                mobile = mobile.substring(1);
            }
            mobile = "966"+mobile;
       //     Log.e("mobile",mobile);

        //    mobile = "05971946070";
        //    password = "000000";
            RetrofitWebService.getService(getActivity()).createLogin(mobile, password, newToken, "android").enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    Log.e("createLogin", response.toString());
                    if (RootManager.RESPONSE_CODE_OK == response.code()) {
                        if (TextUtils.equals("True", response.body().getStatus())) {
                            if (response.body() != null) {
                                User user = (User) response.body().getUser();
                                if (user != null) {
                                    Gson json = new Gson();
                                    String userJson = json.toJson(user);
                                    AppPreferences.saveString(getActivity(), "userJson", userJson);
                                    if (!userJson.isEmpty()) {

                                        Intent intent = new Intent(getActivity(), HomeActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                        getActivity().finish();
                                    }


                                } else {
                                    Toast.makeText(getActivity(), "" + false, Toast.LENGTH_SHORT).show();
                                    AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.error),
                                            response.body().getMessage());
                                }
                            } else {
                                AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.error),
                                        response.body().getMessage());
                            }
                        } else if (TextUtils.equals("False", response.body().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.error),
                                    response.body().getMessage());
                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.Failure),
                                getResources().getString(R.string.InternalServerError));
                    }
                    layout_loading.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Log.e("createLogin", t.toString());
                    layout_loading.setVisibility(View.GONE);
                    AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.Failure),
                            t.getMessage() + "");
                }
            });
        }

    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_action:
                CreateLoginWebService();
                break;
            case R.id.text_forget_pass:
                OpenDialogForgetPassword();
                break;
        }
    }

    private void OpenDialogForgetPassword() {
     /**   AppErrorsManager.showResetPasswordDialog(getActivity(), new InstallCallback() {
            @Override
            public void onStatusDone(String mobile) {
                if (TextUtils.isEmpty(mobile)) {
                    phoneEditText.setError(getResources().getString(R.string.field_required));
                    phoneEditText.requestFocus();
                }else if (mobile.length() == 10 &&  !mobile.startsWith("05" )){
                    AppErrorsManager.showCustomErrorDialog(getActivity(),getResources().getString(R.string.info),
                            getResources().getString(R.string.mobilenumberdigits)  );
                }else if (mobile.length() > 9 && mobile.length() != 10) {
                    if(mobile.startsWith("00966")){
                        AppErrorsManager.showCustomErrorDialog(getActivity(),getResources().getString(R.string.info),
                                getResources().getString(R.string.Dont_put_countrycode_mobile) +"\n"+"00966");
                    }else if(mobile.startsWith("966")){
                        AppErrorsManager.showCustomErrorDialog(getActivity(),getResources().getString(R.string.info),
                                getResources().getString(R.string.Dont_put_countrycode_mobile) +"\n"+"966");

                    }else if(mobile.startsWith("+966")){
                        AppErrorsManager.showCustomErrorDialog(getActivity(),getResources().getString(R.string.info),
                                getResources().getString(R.string.Dont_put_countrycode_mobile) +"\n"+"+966");

                    }else{
                        AppErrorsManager.showCustomErrorDialog(getActivity(),getResources().getString(R.string.info),
                                getResources().getString(R.string.mobilenumberdigits)  );

                    }
                    phoneEditText.setError(getResources().getString(R.string.field_required));
                    phoneEditText.requestFocus();
                }else{
                    if(mobile.length() == 10 && mobile.startsWith("05")){
                        mobile = mobile.substring(1);
                    }
                    mobile = "966"+mobile;
                    Log.e("mobile",mobile);
                    ResetMyPassWordWebServices(mobile);
                }
            }
        });*/
     Intent intent =new Intent(getActivity() , UserPasswordsActivity.class);
     intent.putExtra("action",getResources().getString(R.string.ForgetPassword));
     startActivity(intent);
    }

    private void ResetMyPassWordWebServices(String mobile) {
        layout_loading.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(getActivity()).ResetMyPassWord(mobile).enqueue(new Callback<RootResponse>() {
            @Override
            public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                Log.e("ResetMyPassWord", response.toString());
                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (TextUtils.equals("True", response.body().getStatus())) {
                        if (response.body() != null) {
                            AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                                    response.body().getMessage());
                        } else {
                            AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.error),
                                    response.body().getMessage());
                        }
                    } else if (TextUtils.equals("False", response.body().getStatus())) {
                        AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.error),
                                response.body().getMessage());
                    }
                } else {
                    AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.Failure),
                            getResources().getString(R.string.InternalServerError));
                }
                layout_loading.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<RootResponse> call, Throwable t) {
                Log.e("ResetMyPassWord", t.toString());
                layout_loading.setVisibility(View.GONE);
                AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.Failure),
                        t.getMessage() + "");
            }
        });
    }
}
