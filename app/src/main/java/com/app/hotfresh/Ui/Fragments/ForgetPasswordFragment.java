package com.app.hotfresh.Ui.Fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.app.hotfresh.Manager.AppErrorsManager;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Manager.RootManager;
import com.app.hotfresh.R;
import com.app.hotfresh.WebService.RetrofitWebService;
import com.app.hotfresh.WebService.model.response.RootResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ForgetPasswordFragment extends Fragment {

    private TextView text_name_fragment;
    private ImageView img_logo;
    private EditText phoneEditText;
    private Button btn_action, btn_skip;
    private View layout_loading ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_forget_password, container, false);
        FontManager.applyFont(getActivity() ,view);
        initSetUp(view);
        return view;
    }

    private void initSetUp(View view) {
        text_name_fragment = view.findViewById(R.id.text_name_fragment);
        img_logo = getActivity().findViewById(R.id.img_logo);
        img_logo.setImageResource(R.drawable.img_forget);
        text_name_fragment.setText("" + getResources().getString(R.string.ForgetPassword));
        phoneEditText = view.findViewById(R.id.phoneEditText);
        btn_action = view.findViewById(R.id.btn_action);
        layout_loading = getActivity().findViewById(R.id.layout_loading);
        btn_skip = view.findViewById(R.id.btn_skip);
        btn_skip.setOnClickListener(view1 -> {
            ToggleForgetChickUp("");

        });
        //   ((UserActionActivity) getActivity()).SetStyleWhite(false);

        btn_action.setOnClickListener(view1 -> {
            String mobile = phoneEditText.getText().toString().trim();
            phoneEditText.setError(null);
            phoneEditText.clearFocus();
            if (mobile.length() == 10 && !mobile.startsWith("05")) {
                AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                        getResources().getString(R.string.mobilenumberdigits));
            } else if (mobile.length() > 9 && mobile.length() != 10) {
                if (mobile.startsWith("00966")) {
                    AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                            getResources().getString(R.string.Dont_put_countrycode_mobile) + "\n" + "00966");
                } else if (mobile.startsWith("966")) {
                    AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                            getResources().getString(R.string.Dont_put_countrycode_mobile) + "\n" + "966");

                } else if (mobile.startsWith("+966")) {
                    AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                            getResources().getString(R.string.Dont_put_countrycode_mobile) + "\n" + "+966");

                } else {
                    AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                            getResources().getString(R.string.mobilenumberdigits));

                }
                phoneEditText.setError(getResources().getString(R.string.field_required));
                phoneEditText.requestFocus();
            } else {
                if (mobile.length() == 10 && mobile.startsWith("05")) {
                    mobile = mobile.substring(1);
                }
                mobile = "966" + mobile;
                Log.e("mobile", mobile);
                String finalMobile = mobile;
                layout_loading.setVisibility(View.VISIBLE);
                RetrofitWebService.getService(getActivity()).SendMobileCodeAndPassWord(finalMobile).enqueue(new Callback<RootResponse>() {
                    @Override
                    public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                        Log.e("ResetMyPassWord", response.toString());
                        if (RootManager.RESPONSE_CODE_OK == response.code()) {
                            if (TextUtils.equals("True", response.body().getStatus())) {
                                if (response.body() != null) {
                                    ToggleForgetChickUp(finalMobile);
                                } else {
                                    AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.error),
                                            response.body().getMessage());
                                }
                            } else if (TextUtils.equals("False", response.body().getStatus())) {
                                AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.error),
                                        response.body().getMessage());
                            }
                        } else {
                            AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.Failure),
                                    getResources().getString(R.string.InternalServerError));
                        }
                        layout_loading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onFailure(Call<RootResponse> call, Throwable t) {
                        Log.e("ResetMyPassWord", t.toString());
                        layout_loading.setVisibility(View.GONE);
                        AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.Failure),
                                t.getMessage() + "");
                    }
                });


            }
        });

    }

    private void ToggleForgetChickUp(String email) {
        Fragment fragment = new ResetPasswrodFragment();
        FragmentManager manager = getActivity().getSupportFragmentManager();
        Bundle bundle = new Bundle();
        bundle.putString("mobile", email);
        fragment.setArguments(bundle);
        manager.beginTransaction().replace(R.id.fragment, fragment).commit();
    }
}