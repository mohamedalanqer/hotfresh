package com.app.hotfresh.Ui.Adapters;

import android.content.Context;
import android.content.res.ColorStateList;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.hotfresh.CallBack.OnItemClickTagListener;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Medoles.Category;
import com.app.hotfresh.R;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.List;


public class RecyclerCategoryHome extends RecyclerView.Adapter<RecyclerCategoryHome.CustomView> {
    Context context;
    List<Category> mylist;
    private OnItemClickTagListener listener;
    int Layout;
    ColorGenerator colorGenerator  ;


    public class CustomView extends RecyclerView.ViewHolder {
        TextView row_name;
        ImageView row_img ;

        public CustomView(View v) {
            super(v);
            FontManager.applyFont(context, v);
            row_name = v.findViewById(R.id.row_name);
            row_img = v.findViewById(R.id.row_img);
        }

    }

    public RecyclerCategoryHome() {
    }

    public RecyclerCategoryHome(Context context, List<Category> mylist, int Layout, OnItemClickTagListener listener) {
        this.context = context;
        this.mylist = mylist;
        this.listener = listener;
        this.Layout = Layout;
        this.colorGenerator =   ColorGenerator.MATERIAL;
    }

    @Override
    public CustomView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(Layout, parent, false);
        CustomView viewholder = new CustomView(view);
        return viewholder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final CustomView holder, final int position) {
        holder.row_name.setText(mylist.get(position).getCategoryName() + "");
        int randromColor = colorGenerator.getRandomColor();
       // holder.row_img.setColorFilter(randromColor, android.graphics.PorterDuff.Mode.MULTIPLY);

        ViewCompat.setBackgroundTintList(
                holder.row_img,
                ColorStateList.valueOf(randromColor));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    listener.onItemClick(view, position, "view");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });


    }
    public void filterList(List<Category> spinnerItemList) {
        this.mylist = spinnerItemList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mylist.size();
    }


}

