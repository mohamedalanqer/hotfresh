package com.app.hotfresh.Ui.Activites;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.hotfresh.CallBack.InstallCallback;
import com.app.hotfresh.CallBack.InternetAvailableCallback;
import com.app.hotfresh.CallBack.OnItemClickTagListener;
import com.app.hotfresh.DataBase.AppDatabase;
import com.app.hotfresh.DataBase.InsertUpdateDeleteItem;
import com.app.hotfresh.Manager.AppErrorsManager;
import com.app.hotfresh.Manager.AppLanguage;
import com.app.hotfresh.Manager.AppMp3Manager;
import com.app.hotfresh.Manager.DrawableRound;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Manager.InternetConnectionUtils;
import com.app.hotfresh.Manager.RootManager;
import com.app.hotfresh.Manager.getUserDetials;
import com.app.hotfresh.Medoles.Category;
import com.app.hotfresh.Medoles.Charities.CharitiesItem;
import com.app.hotfresh.Medoles.Item;
import com.app.hotfresh.Medoles.ItemService;
import com.app.hotfresh.Medoles.User;
import com.app.hotfresh.R;
import com.app.hotfresh.Ui.Adapters.RecyclerCategory;
import com.app.hotfresh.Ui.Adapters.RecyclerCharitiesItems;
import com.app.hotfresh.Ui.Adapters.RecyclerItems;
import com.app.hotfresh.Ui.Adapters.RecyclerServiceDialog;
import com.app.hotfresh.WebService.RetrofitWebService;
import com.app.hotfresh.WebService.model.response.CategoryResponse;
import com.app.hotfresh.WebService.model.response.CharitiesItemsResponse;
import com.app.hotfresh.WebService.model.response.ItemsResponse;
import com.app.hotfresh.WebService.model.response.ItemsServiceResponse;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.hotfresh.Manager.RootManager.ActionKeyLocalBroadcastManager;
import static com.app.hotfresh.Manager.RootManager.DataBaseName;

public class CharitiesItemsActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    private FrameLayout layout_loading;

    RecyclerView recycler_view;
    RecyclerCharitiesItems adapter;
    List<CharitiesItem> items_list = new ArrayList<>();
    private String CategoryName = "";
    private TextView toolbarNameTxt;
    AppDatabase db;
    List<CharitiesItem> itemListDataBase = new ArrayList<>();
    List<ItemService> itemServicesListDataBase = new ArrayList<>();
    SwipeRefreshLayout swipeRefreshLayout;


    RecyclerView recycler_view_category;
    RecyclerCategory adapter_category;
    List<Category> categories_list = new ArrayList<>();
    private LinearLayout layout_empty;
    private TextView text_empty;
    private Handler handler;
    private String CharitiesName = "";
    private RelativeLayout end_main;
    private RelativeLayout Layout_notification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppLanguage.setContentLang(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_list_offers);
        FontManager.applyFont(this, findViewById(R.id.layout));
        handler = new Handler(Looper.getMainLooper());


        initSetup();
        setUpDataBase();
    }

    private int countitem = 0;

    private void setUpDataBase() {
        db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, DataBaseName).allowMainThreadQueries().build();

        ImageView count_cart_img = findViewById(R.id.image_count_cart);
        TextView text_count_cart = findViewById(R.id.text_count_cart);

        db.charitiesItemDao().getCount().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                countitem = integer;
                DrawableRound.TextToDrawableRoundGreen(getApplicationContext(), count_cart_img, String.valueOf(integer), text_count_cart);
            }
        });
        itemListDataBase = db.charitiesItemDao().getAll();
    }


    private void initSetup() {
        recycler_view = findViewById(R.id.recycler_view);
        recycler_view_category = findViewById(R.id.recycler_view_category);
        layout_loading = findViewById(R.id.layout_loading);
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        layout_empty = findViewById(R.id.layout_empty);
        text_empty = findViewById(R.id.text_empty);
        end_main = findViewById(R.id.end_main);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this::onRefresh);
        end_main.setOnClickListener(view -> {
            onBackPressed();
        });
        layout_loading.setOnClickListener(view -> {
            return;
        });
        CharitiesName = getIntent().getStringExtra("CharitiesName");
        GetListCategoriesWebService();
        toolbarNameTxt.setText("" + CharitiesName);
        Layout_notification = findViewById(R.id.Layout_notification);
        Layout_notification.setOnClickListener(view -> {
            GoToCartActvity();
        });
    }

    private void GoToCartActvity() {
        if (countitem > 0) {
            Intent intent = new Intent(getApplicationContext(), ListCartCharitiesActivity.class);
            startActivity(intent);
        } else {
            AppErrorsManager.showCustomErrorDialog(CharitiesItemsActivity.this, getResources().getString(R.string.info),
                    getResources().getString(R.string.emptyCartEpty));
        }

    }

    private void GetListItemsWebService(String CategoryID ,String CategoryName, String x) {
        layout_loading.setVisibility(View.VISIBLE);
        User user = getUserDetials.User(this);
        String AccessToken = "";
        if(user != null){
            AccessToken =user.getAccessToken();
        }
        RetrofitWebService.getService(this).GetItemsCharitiesItems(CategoryID, x , AccessToken).enqueue(new Callback<CharitiesItemsResponse>() {
            @Override
            public void onResponse(Call<CharitiesItemsResponse> call, Response<CharitiesItemsResponse> response) {
                Log.e("GetItemByCategory", response.toString());
                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (response.body() != null) {
                        if (TextUtils.equals("True", response.body().getStatus())) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {


                                    items_list = response.body().getCharitiesItems();
                                    if (items_list.size() > 0) {
                                        for (int i = 0; i < items_list.size(); i++) {
                                            for (int x = 0; x < itemListDataBase.size(); x++) {
                                                if (TextUtils.equals(items_list.get(i).getCode(), itemListDataBase.get(x).getCode())) {
                                                    items_list.get(i).setQuantity(itemListDataBase.get(x).getQuantity());
                                                }
                                            }
                                        }
                                        layout_empty.setVisibility(View.GONE);
                                        recycler_view.setVisibility(View.VISIBLE);
                                        setupRecycler();
                                    } else {
                                        recycler_view.setVisibility(View.GONE);
                                        layout_empty.setVisibility(View.VISIBLE);
                                        text_empty.setText(Html.fromHtml("<font color='#FDC723'>" + CategoryName + "</font>" + "<br/>" + response.body().getMessage()));
                                    }
                                }
                            });

                        } else if (TextUtils.equals("False", response.body().getStatus())) {
                            /**  AppErrorsManager.showCustomErrorDialog(CharitiesItemsActivity.this, getResources().getString(R.string.error),
                             response.body().getMessage());*/
                            recycler_view.setVisibility(View.GONE);
                            layout_empty.setVisibility(View.VISIBLE);
                            // text_empty.setText("" + CategoryName + " \n" + response.body().getMessage());
                            text_empty.setText(Html.fromHtml("<font color='#FDC723'>" + CategoryName + "</font>" + "<br/>" + response.body().getMessage()));
                        }
                    }
                } else {
                    AppErrorsManager.showCustomErrorDialog(CharitiesItemsActivity.this, getResources().getString(R.string.Failure),
                            getResources().getString(R.string.InternalServerError));
                }


                layout_loading.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<CharitiesItemsResponse> call, Throwable t) {

                layout_loading.setVisibility(View.GONE);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        InternetConnectionUtils.isInternetAvailable(getApplicationContext(), new InternetAvailableCallback() {
                            @Override
                            public void onInternetAvailable(boolean isAvailable) {
                                if (isAvailable) {

                                    AppErrorsManager.showCustomErrorDialog(CharitiesItemsActivity.this, getResources().getString(R.string.Failure),
                                            t.getMessage() + "");
                                } else {
                                    AppErrorsManager.InternetUnAvailableDialog(CharitiesItemsActivity.this, getResources().getString(R.string.Failure),
                                            getResources().getString(R.string.nointernetconnection), new InstallCallback() {
                                                @Override
                                                public void onStatusDone(String status) {
                                                    if (TextUtils.equals("retry", status)) {
                                                        GetListItemsWebService(CategoryID ,CategoryName, x);
                                                    }
                                                }
                                            });
                                }
                            }
                        });
                    }
                });
            }
        });


    }


    private void GetListCategoriesWebService() {
        layout_loading.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(this).GetCategory().enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                Log.e("createLogin", response.toString());
                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (TextUtils.equals("True", response.body().getStatus())) {
                        categories_list = response.body().category;
                        if (categories_list.size() > 0) {
                            GetListItemsWebService(categories_list.get(0).getCategoryID() ,categories_list.get(0).getCategoryName(), CharitiesName);
                            setupRecyclerCategory();
                        }

                    } else if (TextUtils.equals("False", response.body().getStatus())) {
                        AppErrorsManager.showCustomErrorDialog(CharitiesItemsActivity.this, getResources().getString(R.string.error),
                                response.body().getMessage());
                    }
                } else {
                    AppErrorsManager.showCustomErrorDialog(CharitiesItemsActivity.this, getResources().getString(R.string.Failure),
                            getResources().getString(R.string.InternalServerError));
                }
                layout_loading.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
                AppErrorsManager.showCustomErrorDialog(CharitiesItemsActivity.this, getResources().getString(R.string.error),
                        t.getMessage() + "");
                layout_loading.setVisibility(View.GONE);

            }
        });
    }


    public void setupRecyclerCategory() {
        adapter_category = new RecyclerCategory(getApplicationContext(), categories_list, 0, R.layout.row_item_category_horizontal, new OnItemClickTagListener() {
            @Override
            public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                adapter_category.ItemSelect = position;
                adapter_category.notifyDataSetChanged();
                GoToItemActivity(categories_list.get(position));
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.HORIZONTAL, false);
        recycler_view_category.setLayoutManager(linearLayoutManager);
        recycler_view_category.setAdapter(adapter_category);

    }

    private void GoToItemActivity(Category category) {
        GetListItemsWebService(category.getCategoryID() ,category.getCategoryName(), CharitiesName);
    }

    public void setupRecycler() {
        adapter = new RecyclerCharitiesItems(getApplicationContext(), "ListItem", items_list, R.layout.row_item_items, new OnItemClickTagListener() {
            @Override
            public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                CharitiesItem item = items_list.get(position);
                if (TextUtils.equals("plus", tag)) {
                    if (item.getQuantity() < 20) {
                        item.setQuantity(item.getQuantity() + 1);
                        InsertUpdateDeleteItem.upsert_CharitiesItem(db, item);
                    }

                } else if (TextUtils.equals("add", tag)) {
                    if (item.getQuantity() == 0) {
                        item.setQuantity(item.getQuantity() + 1);
                        InsertUpdateDeleteItem.upsert_CharitiesItem(db, item);
                    }

                } else if (TextUtils.equals("minus", tag)) {
                    if (item.getQuantity() > 0) {
                        item.setQuantity(item.getQuantity() - 1);

                        InsertUpdateDeleteItem.upsert_CharitiesItem(db, item);
                    }
                }
                adapter.notifyDataSetChanged();
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setAdapter(adapter);

    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                GetListCategoriesWebService();
            }
        }, 2000);
    }

    private AppMp3Manager appMp3Manager;
    private BroadcastReceiver mHander = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String title = intent.getStringExtra("title");
            String body = intent.getStringExtra("body");
            appMp3Manager = new AppMp3Manager(CharitiesItemsActivity.this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    AppErrorsManager.showCustomErrorDialogTop(CharitiesItemsActivity.this, title, body, new InstallCallback() {
                        @Override
                        public void onStatusDone(String status) {

                        }
                    });
                    appMp3Manager.palyNotificationMusic(R.raw.bell_sound);
                }
            }, 1000);
        }
    };


    @Override
    protected void onRestart() {
        setUpDataBase();
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        super.onRestart();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHander);
        super.onPause();
    }
}
