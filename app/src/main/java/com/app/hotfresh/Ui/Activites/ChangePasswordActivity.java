package com.app.hotfresh.Ui.Activites;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.hotfresh.CallBack.InstallCallback;
import com.app.hotfresh.CallBack.InternetAvailableCallback;
import com.app.hotfresh.Manager.AppErrorsManager;
import com.app.hotfresh.Manager.AppLanguage;
import com.app.hotfresh.Manager.AppMp3Manager;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Manager.InternetConnectionUtils;
import com.app.hotfresh.Manager.RootManager;
import com.app.hotfresh.Manager.getUserDetials;
import com.app.hotfresh.Medoles.User;
import com.app.hotfresh.R;
import com.app.hotfresh.WebService.RetrofitWebService;
import com.app.hotfresh.WebService.model.response.RootResponse;
import com.app.hotfresh.WebService.model.response.checkoutsResponse;
import com.oppwa.mobile.connect.checkout.dialog.CheckoutActivity;
import com.oppwa.mobile.connect.checkout.meta.CheckoutSettings;
import com.oppwa.mobile.connect.exception.PaymentError;
import com.oppwa.mobile.connect.provider.Connect;
import com.oppwa.mobile.connect.provider.Transaction;
import com.oppwa.mobile.connect.provider.TransactionType;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;

import io.card.payment.CardIOActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.hotfresh.Manager.RootManager.ActionKeyLocalBroadcastManager;

public class ChangePasswordActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    private FrameLayout layout_loading;


    private TextView toolbarNameTxt;
    private RelativeLayout Layout_notification;
    private RelativeLayout end_main;
    private Button btn_action;
    private EditText passwordEditText, new_passwordEditText, re_passwordEditText;
    SwipeRefreshLayout swipeRefreshLayout;


    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppLanguage.setContentLang(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_change_password);
        FontManager.applyFont(this, findViewById(R.id.layout));
        handler = new Handler(Looper.getMainLooper());


        initSetup();

    }

    User user = new User();

    private void initSetup() {
        layout_loading = findViewById(R.id.layout_loading);
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        Layout_notification = findViewById(R.id.Layout_notification);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);

        passwordEditText = findViewById(R.id.passwordEditText);
        new_passwordEditText = findViewById(R.id.new_passwordEditText);
        re_passwordEditText = findViewById(R.id.re_passwordEditText);
        btn_action = findViewById(R.id.btn_action);

        btn_action.setOnClickListener(view -> {
            ChangePasswordWebService();
        });


        end_main = findViewById(R.id.end_main);
        end_main.setOnClickListener(view -> {
            onBackPressed();
        });


        Layout_notification.setVisibility(View.GONE);
        layout_loading.setOnClickListener(view -> {
            return;
        });

        toolbarNameTxt.setText("" + getResources().getString(R.string.ChangePassword));


    }

    private void ChangePasswordWebService() {
        String password = passwordEditText.getText().toString().trim();
        String new_password = new_passwordEditText.getText().toString().trim();
        String password_confirmation = re_passwordEditText.getText().toString().trim();
        if (TextUtils.isEmpty(password)) {
            passwordEditText.setError(getString(R.string.field_required));
            passwordEditText.requestFocus();
        } else if (TextUtils.isEmpty(new_password)) {
            new_passwordEditText.setError(getString(R.string.field_required));
            new_passwordEditText.requestFocus();
        } else if (TextUtils.isEmpty(password_confirmation)) {
            re_passwordEditText.setError(getString(R.string.field_required));
            re_passwordEditText.requestFocus();
        } else if (!TextUtils.equals(new_password, password_confirmation)) {
            re_passwordEditText.setError(getString(R.string.Passworddoesnotmatch));
            re_passwordEditText.requestFocus();
        } else {
            layout_loading.setVisibility(View.VISIBLE);
            user = getUserDetials.User(this);
            if (user != null) {
                RetrofitWebService.getService(this).UpdateUserPassword(user.getAccessToken(), password, new_password).enqueue(new Callback<RootResponse>() {
                    @Override
                    public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                        Log.e("UpdateUserPassword", response.toString());
                        if (RootManager.RESPONSE_CODE_OK == response.code()) {
                            if (TextUtils.equals("True", response.body().getStatus())) {
                                if (response.body() != null) {
                                    AppErrorsManager.showCustomErrorDialogNotCancel(ChangePasswordActivity.this, getResources().getString(R.string.info),
                                            response.body().getMessage(), new InstallCallback() {
                                                @Override
                                                public void onStatusDone(String status) {
                                                    onBackPressed();
                                                }
                                            });
                                } else {
                                    AppErrorsManager.showCustomErrorDialog(ChangePasswordActivity.this, getResources().getString(R.string.error),
                                            response.body().getMessage());
                                }
                            } else if (TextUtils.equals("False", response.body().getStatus())) {
                                AppErrorsManager.showCustomErrorDialog(ChangePasswordActivity.this, getResources().getString(R.string.error),
                                        response.body().getMessage());
                            }
                        } else {
                            AppErrorsManager.showCustomErrorDialog(ChangePasswordActivity.this, getResources().getString(R.string.Failure),
                                    getResources().getString(R.string.InternalServerError));
                        }
                        layout_loading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onFailure(Call<RootResponse> call, Throwable t) {
                        layout_loading.setVisibility(View.GONE);
                        InternetConnectionUtils.isInternetAvailable(getApplicationContext(), new InternetAvailableCallback() {
                            @Override
                            public void onInternetAvailable(boolean isAvailable) {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (isAvailable) {

                                            AppErrorsManager.showCustomErrorDialog(ChangePasswordActivity.this, getResources().getString(R.string.Failure),
                                                    t.getMessage() + "");
                                        } else {
                                            AppErrorsManager.InternetUnAvailableDialog(ChangePasswordActivity.this, getResources().getString(R.string.Failure),
                                                    getResources().getString(R.string.nointernetconnection), new InstallCallback() {
                                                        @Override
                                                        public void onStatusDone(String status) {
                                                            if (TextUtils.equals("retry", status)) {
                                                                ChangePasswordWebService();
                                                            }
                                                        }
                                                    });
                                        }
                                    }
                                });
                            }
                        });
                    }
                });
            }
        }

    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                initSetup();
            }
        }, 2000);
    }

    private AppMp3Manager appMp3Manager;
    private BroadcastReceiver mHander = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String title = intent.getStringExtra("title");
            String body = intent.getStringExtra("body");
            appMp3Manager = new AppMp3Manager(ChangePasswordActivity.this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    AppErrorsManager.showCustomErrorDialogTop(ChangePasswordActivity.this, title, body, new InstallCallback() {
                        @Override
                        public void onStatusDone(String status) {

                        }
                    });
                    appMp3Manager.palyNotificationMusic(R.raw.bell_sound);
                }
            }, 1000);
        }
    };


    @Override
    protected void onRestart() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        super.onRestart();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHander);
        super.onPause();
    }
}
