package com.app.hotfresh.Ui.Activites;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.hotfresh.CallBack.InstallCallback;
import com.app.hotfresh.CallBack.OnItemClickTagListener;
import com.app.hotfresh.DataBase.AppDatabase;
import com.app.hotfresh.Manager.AppErrorsManager;
import com.app.hotfresh.Manager.AppLanguage;
import com.app.hotfresh.Manager.AppMp3Manager;
import com.app.hotfresh.Manager.AppPreferences;
import com.app.hotfresh.Manager.DrawableRound;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Manager.RootManager;
import com.app.hotfresh.Manager.getUserDetials;
import com.app.hotfresh.Medoles.Charities.Charitie;
import com.app.hotfresh.Medoles.Item;
import com.app.hotfresh.Medoles.ItemService;
import com.app.hotfresh.Medoles.SettingItem;
import com.app.hotfresh.Medoles.User;
import com.app.hotfresh.R;
import com.app.hotfresh.Ui.Adapters.RecyclerCharitie;
import com.app.hotfresh.Ui.Adapters.RecyclerSetting;
import com.app.hotfresh.WebService.RetrofitWebService;
import com.app.hotfresh.WebService.model.response.CharitiesResponse;
import com.google.gson.Gson;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.hotfresh.Manager.RootManager.ActionKeyLocalBroadcastManager;
import static com.app.hotfresh.Manager.RootManager.DataBaseName;

public class SettingListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener{


    private FrameLayout layout_loading;

    RecyclerView recycler_view;
    RecyclerSetting adapter;
    List<SettingItem> settingItems = new ArrayList<>();
    private TextView toolbarNameTxt;
    private RelativeLayout Layout_notification;
    private RelativeLayout end_main;


    private Handler handler;
    private User user = new User();
    SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppLanguage.setContentLang(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_list_item_id);
        FontManager.applyFont(this, findViewById(R.id.layout));
        handler = new Handler(Looper.getMainLooper());


        initSetup();

    }


    private void initSetup() {
        recycler_view = findViewById(R.id.recycler_view);
        layout_loading = findViewById(R.id.layout_loading);
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        Layout_notification = findViewById(R.id.Layout_notification);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);

        Layout_notification.setVisibility(View.GONE);
        end_main = findViewById(R.id.end_main);
        end_main.setOnClickListener(view -> {
            onBackPressed();
        });
        layout_loading.setOnClickListener(view -> {
            return;
        });

        toolbarNameTxt.setText("" + getResources().getString(R.string.settings));
        setupRecycler();
    }


    public void setupRecycler() {
        user = getUserDetials.User(this);
        settingItems =new ArrayList<>();
        if (user != null) {
            settingItems.add(new SettingItem(1, getResources().getString(R.string.myAccount), R.drawable.ic_person_black_24dp));
            settingItems.add(new SettingItem(3, getResources().getString(R.string.Editmyaccount), R.drawable.ic_edit_user));
            settingItems.add(new SettingItem(5, getResources().getString(R.string.Notifications), R.drawable.ic_notifications_black_24dp));
            settingItems.add(new SettingItem(2, getResources().getString(R.string.ChangePassword), R.drawable.ic_lock_black_24dp));
            settingItems.add(new SettingItem(6, getResources().getString(R.string.paymentlist), R.drawable.ic_pay));
            settingItems.add(new SettingItem(7, getResources().getString(R.string.Privacy), R.drawable.ic_public_black_24dp));
            settingItems.add(new SettingItem(8, getResources().getString(R.string.aboutus), R.drawable.ic_error_black_24dp));
            settingItems.add(new SettingItem(9, getResources().getString(R.string.Branchs), R.drawable.ic_location_on_black_24dp));
            settingItems.add(new SettingItem(4, getResources().getString(R.string.logout), R.drawable.ic_logout));
        } else {
            settingItems.add(new SettingItem(7, getResources().getString(R.string.Privacy), R.drawable.ic_public_black_24dp));
            settingItems.add(new SettingItem(8, getResources().getString(R.string.aboutus), R.drawable.ic_error_black_24dp));
            settingItems.add(new SettingItem(9, getResources().getString(R.string.Branchs), R.drawable.ic_location_on_black_24dp));
            settingItems.add(new SettingItem(10, getResources().getString(R.string.login), R.drawable.ic_logout));
        }


        adapter = new RecyclerSetting(getApplicationContext(), settingItems, -1, R.layout.row_item_setting, new OnItemClickTagListener() {
            @Override
            public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                SettingItem settingItem = settingItems.get(position);
                if (TextUtils.equals("view", tag)) {
                    adapter.ItemSelect = position;
                    if (settingItem.getId() == 4) {
                        logOut();
                    } else if (settingItem.getId() == 2) {
                        GoToActivityChangePassword();
                    } else if (settingItem.getId() == 3) {
                        GoToActivityEditPrfile();
                    } else if (settingItem.getId() == 5) {
                        GoToActivityNotifications();
                    } else if (settingItem.getId() == 6) {
                        GoToActivityPayment();
                    } else if (settingItem.getId() == 7) {
                        GoToActivityinfo(RootManager.Info_Tag_Privacy);
                    } else if (settingItem.getId() == 8) {
                        GoToActivityinfo(RootManager.Info_Tag_About);
                    } else if (settingItem.getId() == 9) {
                        GoToActivityinfo(RootManager.Info_Tag_Branch);
                    } else if (settingItem.getId() == 10) {
                        GoToActivityLogin();
                    } else {
                        GoToActivity();
                    }
                }
                adapter.notifyDataSetChanged();
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setAdapter(adapter);

    }

    private void GoToActivityLogin() {
        Intent mainIntent = new Intent(SettingListActivity.this, UserActionsActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
        finish();
    }

    private void GoToActivityinfo(String info_tag_about) {
        Intent intent = new Intent(getApplicationContext(), InfoActivity.class);
        intent.putExtra("TagAction", info_tag_about);
        startActivity(intent);
    }

    private void GoToActivityPayment() {
        Intent intent = new Intent(getApplicationContext(), PayametActivity.class);
        startActivity(intent);
    }

    private void GoToActivityNotifications() {
        Intent intent = new Intent(getApplicationContext(), NofiticationsActivity.class);
        startActivity(intent);
    }

    private void GoToActivityEditPrfile() {
        Intent intent = new Intent(getApplicationContext(), UpdateProfileActivity.class);
        startActivity(intent);
    }

    private void GoToActivityChangePassword() {
        Intent intent = new Intent(getApplicationContext(), ChangePasswordActivity.class);
        startActivity(intent);
    }

    public void logOut() {
        User user = getUserDetials.User(this);
        if (user != null) {
            AppErrorsManager.showSuccessDialog(SettingListActivity.this, getString(R.string.logout), getString(R.string.do_you_want_logout), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Gson json = new Gson();
                    AppPreferences.saveString(SettingListActivity.this, "userJson", "0");
                    Intent intent = new Intent(SettingListActivity.this, SplashActivity.class);
                    startActivity(intent);
                    finish();

                }
            }, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
        } else {
            Intent intent = new Intent(SettingListActivity.this, SplashActivity.class);
            startActivity(intent);
        }


    }

    private void GoToActivity() {
        Intent intent = new Intent(getApplicationContext(), MyProfileActivity.class);
        startActivity(intent);
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                initSetup();
            }
        }, 2000);
    }
    private AppMp3Manager appMp3Manager;
    private BroadcastReceiver mHander = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String title = intent.getStringExtra("title");
            String body = intent.getStringExtra("body");
            appMp3Manager = new AppMp3Manager(SettingListActivity.this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    AppErrorsManager.showCustomErrorDialogTop(SettingListActivity.this, title, body, new InstallCallback() {
                        @Override
                        public void onStatusDone(String status) {

                        }
                    });
                    appMp3Manager.palyNotificationMusic(R.raw.bell_sound);
                }
            }, 1000);
        }
    };


    @Override
    protected void onRestart() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        super.onRestart();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHander);
        super.onPause();
    }
}
