package com.app.hotfresh.Ui.Activites;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.hotfresh.CallBack.InstallCallback;
import com.app.hotfresh.CallBack.InternetAvailableCallback;
import com.app.hotfresh.Manager.AppErrorsManager;
import com.app.hotfresh.Manager.AppLanguage;
import com.app.hotfresh.Manager.AppMp3Manager;
import com.app.hotfresh.Manager.AppPreferences;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Manager.InternetConnectionUtils;
import com.app.hotfresh.Manager.RootManager;
import com.app.hotfresh.Manager.getUserDetials;
import com.app.hotfresh.Medoles.User;
import com.app.hotfresh.R;
import com.app.hotfresh.WebService.RetrofitWebService;
import com.app.hotfresh.WebService.model.response.LoginResponse;
import com.app.hotfresh.WebService.model.response.RootResponse;
import com.app.hotfresh.WebService.model.response.checkoutsResponse;
import com.google.gson.Gson;
import com.oppwa.mobile.connect.checkout.dialog.CheckoutActivity;
import com.oppwa.mobile.connect.checkout.meta.CheckoutSettings;
import com.oppwa.mobile.connect.exception.PaymentError;
import com.oppwa.mobile.connect.provider.Connect;
import com.oppwa.mobile.connect.provider.Transaction;
import com.oppwa.mobile.connect.provider.TransactionType;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;

import io.card.payment.CardIOActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.hotfresh.Manager.RootManager.ActionKeyLocalBroadcastManager;

public class UpdateProfileActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    private FrameLayout layout_loading;


    private TextView toolbarNameTxt;
    private RelativeLayout Layout_notification;
    private RelativeLayout end_main;
    private Button btn_action;
    private EditText nameEditText, phoneEditText, addressEditText;
    SwipeRefreshLayout swipeRefreshLayout;


    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppLanguage.setContentLang(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_update_profile);
        FontManager.applyFont(this, findViewById(R.id.layout));
        handler = new Handler(Looper.getMainLooper());


        initSetup();

    }


    User user = new User();

    private void initSetup() {
        layout_loading = findViewById(R.id.layout_loading);
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        Layout_notification = findViewById(R.id.Layout_notification);
        btn_action = findViewById(R.id.btn_action);
        phoneEditText = findViewById(R.id.phoneEditText);
        addressEditText = findViewById(R.id.addressEditText);
        nameEditText = findViewById(R.id.nameEditText);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);
        end_main = findViewById(R.id.end_main);
        end_main.setOnClickListener(view -> {
            onBackPressed();
        });
        user = getUserDetials.User(this);
        if (user != null) {
            nameEditText.setText(user.getName());
            phoneEditText.setText(user.getMobileString());
            if (user.getAddress() != null) {
                addressEditText.setText("" + user.getAddress());
            }

            if (user.getMobileActivate() != null) {
                phoneEditText.setEnabled(false);
                if (TextUtils.equals("False", user.getMobileActivate())) {
                    phoneEditText.setEnabled(true);
                }
            } else {
                phoneEditText.setEnabled(true);
            }

        }

        btn_action.setOnClickListener(view -> {
            UpdateProfileWebServices();
        });

        Layout_notification.setVisibility(View.GONE);
        layout_loading.setOnClickListener(view -> {
            return;
        });

        toolbarNameTxt.setText("" + getResources().getString(R.string.Editmyaccount));

    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                initSetup();
            }
        }, 2000);
    }

    private void UpdateProfileWebServices() {
        if (user != null) {
            String mobile = phoneEditText.getText().toString().trim();
            String address = addressEditText.getText().toString().trim();
            String name = nameEditText.getText().toString().trim();

            nameEditText.clearFocus();
            phoneEditText.clearFocus();
            addressEditText.clearFocus();
            if (TextUtils.isEmpty(name)) {
                nameEditText.setError(getResources().getString(R.string.field_required));
                nameEditText.requestFocus();
            } else if (TextUtils.isEmpty(mobile)) {
                phoneEditText.setError(getResources().getString(R.string.field_required));
                phoneEditText.requestFocus();
            } else if (mobile.length() == 10 && !mobile.startsWith("05")) {
                AppErrorsManager.showCustomErrorDialog(UpdateProfileActivity.this, getResources().getString(R.string.info),
                        getResources().getString(R.string.mobilenumberdigits));
            } else if (mobile.length() > 9 && mobile.length() != 10) {
                if (mobile.startsWith("00966")) {
                    AppErrorsManager.showCustomErrorDialog(UpdateProfileActivity.this, getResources().getString(R.string.info),
                            getResources().getString(R.string.Dont_put_countrycode_mobile) + "\n" + "00966");
                } else if (mobile.startsWith("966")) {
                    AppErrorsManager.showCustomErrorDialog(UpdateProfileActivity.this, getResources().getString(R.string.info),
                            getResources().getString(R.string.Dont_put_countrycode_mobile) + "\n" + "966");

                } else if (mobile.startsWith("+966")) {
                    AppErrorsManager.showCustomErrorDialog(UpdateProfileActivity.this, getResources().getString(R.string.info),
                            getResources().getString(R.string.Dont_put_countrycode_mobile) + "\n" + "+966");

                } else {
                    AppErrorsManager.showCustomErrorDialog(UpdateProfileActivity.this, getResources().getString(R.string.info),
                            getResources().getString(R.string.mobilenumberdigits));

                }
                phoneEditText.setError(getResources().getString(R.string.field_required));
                phoneEditText.requestFocus();
            } else if (mobile.length() < 9) {
                AppErrorsManager.showCustomErrorDialog(UpdateProfileActivity.this, getResources().getString(R.string.info),
                        getResources().getString(R.string.mobilenumberdigits) + "");
                phoneEditText.setError(getResources().getString(R.string.field_required));
                phoneEditText.requestFocus();

            } else if (TextUtils.isEmpty(address)) {
                addressEditText.setError(getResources().getString(R.string.field_required));
                addressEditText.requestFocus();
            } else {

                if (mobile.length() == 10 && mobile.startsWith("05")) {
                    mobile = mobile.substring(1);
                }
                mobile = "966" + mobile;

                layout_loading.setVisibility(View.VISIBLE);
                RetrofitWebService.getService(this).UpdateProfile(user.getAccessToken(), name, mobile, address).enqueue(new Callback<RootResponse>() {
                    @Override
                    public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                        Log.e("UpdateProfile", response.toString());
                        if (RootManager.RESPONSE_CODE_OK == response.code()) {
                            if (TextUtils.equals("True", response.body().getStatus())) {
                                if (response.body() != null) {
                                    GetMyProfile();
                                } else {
                                    AppErrorsManager.showCustomErrorDialog(UpdateProfileActivity.this, getResources().getString(R.string.error),
                                            response.body().getMessage());
                                }
                            } else if (TextUtils.equals("False", response.body().getStatus())) {
                                AppErrorsManager.showCustomErrorDialog(UpdateProfileActivity.this, getResources().getString(R.string.error),
                                        response.body().getMessage());
                            }
                        } else {
                            AppErrorsManager.showCustomErrorDialog(UpdateProfileActivity.this, getResources().getString(R.string.Failure),
                                    getResources().getString(R.string.InternalServerError));
                        }
                        layout_loading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onFailure(Call<RootResponse> call, Throwable t) {
                        Log.e("UpdateProfile", t.toString());


                        layout_loading.setVisibility(View.GONE);
                        InternetConnectionUtils.isInternetAvailable(getApplicationContext(), new InternetAvailableCallback() {
                            @Override
                            public void onInternetAvailable(boolean isAvailable) {
                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (isAvailable) {

                                            AppErrorsManager.showCustomErrorDialog(UpdateProfileActivity.this, getResources().getString(R.string.Failure),
                                                    t.getMessage() + "");

                                        } else {
                                            AppErrorsManager.InternetUnAvailableDialog(UpdateProfileActivity.this, getResources().getString(R.string.Failure),
                                                    getResources().getString(R.string.nointernetconnection), new InstallCallback() {
                                                        @Override
                                                        public void onStatusDone(String status) {
                                                            if (TextUtils.equals("retry", status)) {
                                                                UpdateProfileWebServices();
                                                            }
                                                        }
                                                    });
                                        }
                                    }
                                });
                            }
                        });
                    }
                });
            }
        }
    }

    private void GetMyProfile() {
        if (user != null) {
            layout_loading.setVisibility(View.VISIBLE);
            String old_token = user.getAccessToken();
            RetrofitWebService.getService(this).GetMyProfile(user.getAccessToken()).enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    Log.e("GetMyProfile", response.toString());
                    if (RootManager.RESPONSE_CODE_OK == response.code()) {
                        if (TextUtils.equals("True", response.body().getStatus())) {
                            if (response.body() != null) {
                                User user = (User) response.body().getUser();
                                user.setAccessToken(old_token);
                                if (user != null) {
                                    Gson json = new Gson();
                                    String userJson = json.toJson(user);
                                    AppPreferences.saveString(UpdateProfileActivity.this, "userJson", userJson);
                                    AppErrorsManager.showCustomErrorDialogNotCancel(UpdateProfileActivity.this, getResources().getString(R.string.info),
                                            response.body().getMessage(), new InstallCallback() {
                                                @Override
                                                public void onStatusDone(String status) {
                                                    onBackPressed();
                                                }
                                            });


                                } else {

                                    AppErrorsManager.showCustomErrorDialog(UpdateProfileActivity.this, getResources().getString(R.string.error),
                                            response.body().getMessage());
                                }
                            } else {
                                AppErrorsManager.showCustomErrorDialog(UpdateProfileActivity.this, getResources().getString(R.string.error),
                                        response.body().getMessage());
                            }
                        } else if (TextUtils.equals("False", response.body().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(UpdateProfileActivity.this, getResources().getString(R.string.error),
                                    response.body().getMessage());
                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(UpdateProfileActivity.this, getResources().getString(R.string.Failure),
                                getResources().getString(R.string.InternalServerError));
                    }
                    layout_loading.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Log.e("GetMyProfile", t.toString());
                    layout_loading.setVisibility(View.GONE);
                    InternetConnectionUtils.isInternetAvailable(getApplicationContext(), new InternetAvailableCallback() {
                        @Override
                        public void onInternetAvailable(boolean isAvailable) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (isAvailable) {

                                        AppErrorsManager.showCustomErrorDialog(UpdateProfileActivity.this, getResources().getString(R.string.Failure),
                                                t.getMessage() + "");

                                    } else {
                                        AppErrorsManager.InternetUnAvailableDialog(UpdateProfileActivity.this, getResources().getString(R.string.Failure),
                                                getResources().getString(R.string.nointernetconnection), new InstallCallback() {
                                                    @Override
                                                    public void onStatusDone(String status) {
                                                        if (TextUtils.equals("retry", status)) {
                                                            GetMyProfile();
                                                        }
                                                    }
                                                });
                                    }
                                }
                            });
                        }
                    });

                }
            });
        }
    }

    private AppMp3Manager appMp3Manager;
    private BroadcastReceiver mHander = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String title = intent.getStringExtra("title");
            String body = intent.getStringExtra("body");
            appMp3Manager = new AppMp3Manager(UpdateProfileActivity.this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    AppErrorsManager.showCustomErrorDialogTop(UpdateProfileActivity.this, title, body, new InstallCallback() {
                        @Override
                        public void onStatusDone(String status) {

                        }
                    });
                    appMp3Manager.palyNotificationMusic(R.raw.bell_sound);
                }
            }, 1000);
        }
    };


    @Override
    protected void onRestart() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        super.onRestart();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHander);
        super.onPause();
    }
}
