package com.app.hotfresh.Ui.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.app.hotfresh.Manager.AppErrorsManager;
import com.app.hotfresh.Manager.AppPreferences;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Manager.RootManager;
import com.app.hotfresh.Medoles.User;
import com.app.hotfresh.R;
import com.app.hotfresh.Ui.Activites.HomeActivity;
import com.app.hotfresh.WebService.RetrofitWebService;
import com.app.hotfresh.WebService.model.response.LoginResponse;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RegisterFragment extends Fragment implements View.OnClickListener {
    FrameLayout layout_loading;
    private EditText phoneEditText, passwordEditText, re_passwordEditText, nameEditText;
    private Button btn_action;
    private String newToken = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        FontManager.applyFont(getActivity(), view);
        initSetUp(view);
        return view;
    }

    private void initSetUp(View view) {
        layout_loading = getActivity().findViewById(R.id.layout_loading);
        btn_action = getActivity().findViewById(R.id.btn_action);
        phoneEditText = view.findViewById(R.id.phoneEditText);
        passwordEditText = view.findViewById(R.id.passwordEditText);
        nameEditText = view.findViewById(R.id.nameEditText);
        re_passwordEditText = view.findViewById(R.id.re_passwordEditText);
        btn_action.setOnClickListener(this::onClick);
        layout_loading.setOnClickListener(v -> {
            return;
        });

        FirebaseApp.initializeApp(getActivity());
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(getActivity(), new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                newToken = instanceIdResult.getToken();
                Log.e("newToken", newToken);

            }
        });
    }

    private void CreateRegisterWebService() {
        String mobile = phoneEditText.getText().toString().trim();
        String password = passwordEditText.getText().toString().trim();
        String re_password = re_passwordEditText.getText().toString().trim();
        String name = nameEditText.getText().toString().trim();

        passwordEditText.clearFocus();
        phoneEditText.clearFocus();
        re_passwordEditText.clearFocus();
        nameEditText.clearFocus();
        if (TextUtils.isEmpty(name)) {
            nameEditText.setError(getResources().getString(R.string.field_required));
            nameEditText.requestFocus();
        } else if (TextUtils.isEmpty(mobile)) {
            phoneEditText.setError(getResources().getString(R.string.field_required));
            phoneEditText.requestFocus();
        } else if (mobile.length() == 10 && !mobile.startsWith("05")) {
            AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                    getResources().getString(R.string.mobilenumberdigits));
        } else if (mobile.length() > 9 && mobile.length() != 10) {
            if (mobile.startsWith("00966")) {
                AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                        getResources().getString(R.string.Dont_put_countrycode_mobile) + "\n" + "00966");
            } else if (mobile.startsWith("966")) {
                AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                        getResources().getString(R.string.Dont_put_countrycode_mobile) + "\n" + "966");

            } else if (mobile.startsWith("+966")) {
                AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                        getResources().getString(R.string.Dont_put_countrycode_mobile) + "\n" + "+966");

            } else {
                AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                        getResources().getString(R.string.mobilenumberdigits));

            }
            phoneEditText.setError(getResources().getString(R.string.field_required));
            phoneEditText.requestFocus();
        }else if (mobile.length() < 9) {
            AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                    getResources().getString(R.string.mobilenumberdigits) +"");
            phoneEditText.setError(getResources().getString(R.string.field_required));
            phoneEditText.requestFocus();

        } else if (TextUtils.isEmpty(password)) {
            passwordEditText.setError(getResources().getString(R.string.field_required));
            passwordEditText.requestFocus();
        } else if (TextUtils.isEmpty(re_password)) {
            re_passwordEditText.setError(getResources().getString(R.string.field_required));
            re_passwordEditText.requestFocus();
        } else if (!TextUtils.equals(re_password, password)) {
            re_passwordEditText.setError(getResources().getString(R.string.Password_not_match));
            re_passwordEditText.requestFocus();
            passwordEditText.setError(getResources().getString(R.string.Password_not_match));
            passwordEditText.requestFocus();
            AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.error),
                    getResources().getString(R.string.Password_not_match));
        } else {
            layout_loading.setVisibility(View.VISIBLE);
            if (TextUtils.isEmpty(newToken)) {
                FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(getActivity(), new OnSuccessListener<InstanceIdResult>() {
                    @Override
                    public void onSuccess(InstanceIdResult instanceIdResult) {
                        newToken = instanceIdResult.getToken();
                        Log.e("newToken", newToken);

                    }
                });
            }
            if (mobile.length() == 10 && mobile.startsWith("05")) {
                mobile = mobile.substring(1);
            }
            mobile = "966" + mobile;

            RetrofitWebService.getService(getActivity()).createRegister(mobile, password, name, newToken, "android").enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    Log.e("createLogin", response.toString());
                    if (RootManager.RESPONSE_CODE_OK == response.code()) {

                        if (TextUtils.equals("True", response.body().getStatus())) {
                            if (response.body() != null) {
                                User user = (User) response.body().getUser();
                                if (user != null) {
                                    AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                                            response.body().getMessage());
                                    Gson json = new Gson();
                                    String userJson = json.toJson(user);
                                    AppPreferences.saveString(getActivity(), "userJson", userJson);
                                    if (!userJson.isEmpty()) {
                                        Intent intent = new Intent(getActivity(), HomeActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);
                                        getActivity().finish();
                                    }
                                } else {
                                    Toast.makeText(getActivity(), "" + false, Toast.LENGTH_SHORT).show();
                                    AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.error),
                                            response.body().getMessage());
                                }
                            }
                        } else if (TextUtils.equals("False", response.body().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.error),
                                    response.body().getMessage());
                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.Failure),
                                getResources().getString(R.string.InternalServerError));
                    }
                    layout_loading.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Log.e("createLogin", t.toString());
                    layout_loading.setVisibility(View.GONE);
                    AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.Failure),
                            t.getMessage() + "");
                }
            });
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_action:
                CreateRegisterWebService();
                break;
        }
    }
}
