package com.app.hotfresh.Ui.Activites;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.Observer;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;

import androidx.appcompat.app.AlertDialog;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.hotfresh.CallBack.InstallCallback;
import com.app.hotfresh.CallBack.InternetAvailableCallback;
import com.app.hotfresh.CallBack.OnItemClickTagListener;
import com.app.hotfresh.CallBack.SmsBroadcastReceiverListener;
import com.app.hotfresh.DataBase.AppDatabase;
import com.app.hotfresh.DataBase.InsertUpdateDeleteItem;
import com.app.hotfresh.Manager.AppErrorsManager;
import com.app.hotfresh.Manager.AppLanguage;
import com.app.hotfresh.Manager.AppMp3Manager;
import com.app.hotfresh.Manager.AppPreferences;
import com.app.hotfresh.Manager.DrawableRound;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Manager.InternetConnectionUtils;
import com.app.hotfresh.Manager.RootManager;
import com.app.hotfresh.Manager.getUserDetials;
import com.app.hotfresh.Medoles.CreateOrder.CreateOrder;
import com.app.hotfresh.Medoles.Install.Branch;
import com.app.hotfresh.Medoles.Install.Definition;
import com.app.hotfresh.Medoles.Item;
import com.app.hotfresh.Medoles.ItemService;
import com.app.hotfresh.Medoles.CreateOrder.ItemsCreateOrder;
import com.app.hotfresh.Medoles.CreateOrder.ItemsServiceCreateOrder;
import com.app.hotfresh.Medoles.MapAddress.Address;
import com.app.hotfresh.Medoles.PaymentType;
import com.app.hotfresh.Medoles.User;
import com.app.hotfresh.R;
import com.app.hotfresh.Ui.Adapters.RecyclerBranch;
import com.app.hotfresh.Ui.Adapters.RecyclerItems;
import com.app.hotfresh.Ui.Adapters.RecyclerPayamentDialog;
import com.app.hotfresh.Ui.Adapters.RecyclerServiceDialog;
import com.app.hotfresh.Ui.Fragments.DatePickerFragment;
import com.app.hotfresh.Ui.Fragments.TimePickerFragment;
import com.app.hotfresh.WebService.RetrofitWebService;
import com.app.hotfresh.WebService.model.response.DResponse;
import com.app.hotfresh.WebService.model.response.ItemsServiceResponse;
import com.app.hotfresh.WebService.model.response.LoginResponse;
import com.app.hotfresh.WebService.model.response.MapAddressResponse;
import com.app.hotfresh.WebService.model.response.RootResponse;
import com.app.hotfresh.WebService.model.response.WalletResponse;
import com.app.hotfresh.WebService.model.response.checkoutsResponse;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.oppwa.mobile.connect.checkout.dialog.CheckoutActivity;
import com.oppwa.mobile.connect.checkout.meta.CheckoutSettings;
import com.oppwa.mobile.connect.exception.PaymentError;
import com.oppwa.mobile.connect.provider.Connect;
import com.oppwa.mobile.connect.provider.Transaction;
import com.oppwa.mobile.connect.provider.TransactionType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import es.dmoral.toasty.Toasty;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.hotfresh.Manager.RootManager.ActionKeyLocalBroadcastManager;
import static com.app.hotfresh.Manager.RootManager.Company_Select;
import static com.app.hotfresh.Manager.RootManager.DataBaseName;
import static com.app.hotfresh.Manager.RootManager.DeliveryType_Cash;
import static com.app.hotfresh.Manager.RootManager.DeliveryType_OnHand;
import static com.app.hotfresh.Manager.RootManager.PaymentType_cash;
import static com.app.hotfresh.Manager.RootManager.PaymentType_visa;
import static com.app.hotfresh.Manager.RootManager.PaymentType_wallet;
import static com.app.hotfresh.Manager.RootManager.SMS_PATTERN_NUMBER;
import static com.app.hotfresh.Manager.RootManager.SMS_PROVIDER_NAME;

public class ListCartActivity extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private static final int REQ_USER_CONSENT = 200;
    private FrameLayout layout_loading;

    RecyclerView recycler_view;
    RecyclerItems adapter;
    private TextView toolbarNameTxt;
    AppDatabase db;
    List<Item> itemListDataBase = new ArrayList<>();
    private Button btn_action;
    List<ItemService> itemServicesListDataBase = new ArrayList<>();
    private Handler handler;
    private TextView text_count_item, text_price_items, text_price_service, text_tax, text_total;
    private String taxValue = "0";
    private RelativeLayout end_main;
    SwipeRefreshLayout swipeRefreshLayout;
    EditText locationEditText;
    private TextView text_sheet_tottal;
    private Button btn_sheet_action;

    private LinearLayout layout_select_location;
    private TextView payment_methodEditText;


    private CardView card_location, card_shop;
    private ImageView img_shop, img_location;
    private TextView text_name_shop, text_name_location;
    private TextView deliveryDateTxt, deliveryTimeTxt, text_select_branch;
    private FrameLayout layout_my_location;
    private LinearLayout layout_Branch_address;
    Double latitude = 0.0, longitude = 0.0;
    private FusedLocationProviderClient fusedLocationClient;
    private EditText notes_edittext;
    User user = new User();

    private LinearLayout layout_gifts;
    private TextView text_total_gifts;

    private String DeliveryType = DeliveryType_Cash;
    AlertDialog alertDialogBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppLanguage.setContentLang(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_list_cart);
        FontManager.applyFont(this, findViewById(R.id.layout));
        handler = new Handler(Looper.getMainLooper());

        initSetup();

    }

    TextView text_price_items_2, text_price_delivery, text_total_;
    LinearLayout layout_price_delivery;

    private void SetUpViewSheet() {
        card_location = findViewById(R.id.card_location);
        card_shop = findViewById(R.id.card_shop);
        img_shop = findViewById(R.id.img_shop);
        img_location = findViewById(R.id.img_location);
        text_name_shop = findViewById(R.id.text_name_shop);
        text_name_location = findViewById(R.id.text_name_location);
        layout_my_location = findViewById(R.id.layout_my_location);
        layout_Branch_address = findViewById(R.id.layout_Branch_address);
        text_sheet_tottal = findViewById(R.id.text_sheet_tottal);
        btn_sheet_action = findViewById(R.id.btn_sheet_action);
        locationEditText = findViewById(R.id.locationEditText);
        layout_select_location = findViewById(R.id.layout_select_location);
        deliveryDateTxt = findViewById(R.id.deliveryDateTxt);
        deliveryTimeTxt = findViewById(R.id.deliveryTimeTxt);
        payment_methodEditText = findViewById(R.id.payment_methodEditText);
        notes_edittext = findViewById(R.id.notes_edittext);
        text_select_branch = findViewById(R.id.text_select_branch);
        text_select_branch.setOnClickListener(this::onClick);
        payment_methodEditText.setOnClickListener(this::onClick);
        layout_select_location.setOnClickListener(this);
        card_shop.setOnClickListener(this);
        card_location.setOnClickListener(this);
        btn_sheet_action.setOnClickListener(this::onClick);
        deliveryDateTxt.setOnClickListener(this::onClick);
        deliveryTimeTxt.setOnClickListener(this::onClick);
        text_sheet_tottal.setOnClickListener(this::onClick);


        text_price_items_2 = findViewById(R.id.text_price_items_2);
        text_price_delivery = findViewById(R.id.text_price_delivery);
        text_total_ = findViewById(R.id.text_total_);
        layout_price_delivery = findViewById(R.id.layout_price_delivery);
        layout_price_delivery.setVisibility(View.GONE);

        Locale loc = new Locale("en", "US");
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", loc);
        String currentDateandTime = format.format(new Date());
        deliveryDateTxt.setText(currentDateandTime.toString() + "");
        deliveryDateTxt.setTag(currentDateandTime.toString() + "");

        instalBranch();
    }


    BottomSheetBehavior bottomSheetBehavior;

    private void SetUpBottomSheet() {
        GetLocationUser();
        LinearLayout llBottomSheet = findViewById(R.id.bottom_sheet);
        bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HALF_EXPANDED);
        bottomSheetBehavior.setPeekHeight(0);
        bottomSheetBehavior.setHideable(true);
        bottomSheetBehavior.addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                Log.e("addBottomSheetCallback", newState + " ");
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                Log.e("addBottomSheetCallback", slideOffset + " ");

            }
        });
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

    }

    private void ToggleTypeDelivery(CardView selectCard, CardView UnselectCard) {
        selectCard.setTag("1");
        UnselectCard.setTag("0");

        String definitionsJson = AppPreferences.getString(ListCartActivity.this, "definitionsJson");
        if (definitionsJson != null) {
            Definition definition = new Gson().fromJson(definitionsJson, new Definition().getClass());


            if (TextUtils.equals("1", card_shop.getTag().toString())) {

                if (!TextUtils.equals("True", definition.getDeliveryTypeOnShop())) {
                    AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.info), getResources().getString(R.string.DeliveryTypeOnShopFalseMsg));
                } else {
                    DeliveryType = DeliveryType_Cash;
                    img_shop.setVisibility(View.VISIBLE);
                    img_location.setVisibility(View.GONE);

                    layout_my_location.setVisibility(View.GONE);
                    layout_Branch_address.setVisibility(View.VISIBLE);

                    text_name_shop.setTextColor(getResources().getColor(R.color.colorWhite));
                    text_name_location.setTextColor(getResources().getColor(R.color.colorBlack));

                    card_shop.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    card_location.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                    SetValueDeliveryNotMap();
                }
            } else {

                if (!TextUtils.equals("True", definition.getDeliveryTypeDelivery())) {
                    AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.info), getResources().getString(R.string.DeliveryTypeDeliveryFalseMsg));
                } else {
                    DeliveryType = DeliveryType_OnHand;
                    img_shop.setVisibility(View.GONE);
                    img_location.setVisibility(View.VISIBLE);

                    layout_my_location.setVisibility(View.VISIBLE);
                    layout_Branch_address.setVisibility(View.VISIBLE);
                    text_name_shop.setTextColor(getResources().getColor(R.color.colorBlack));
                    text_name_location.setTextColor(getResources().getColor(R.color.colorWhite));

                    card_shop.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                    card_location.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                }
            }
        }

    }

    private int countCart = 0;

    private void setUpDataBase() {
        db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, DataBaseName).allowMainThreadQueries().build();

        ImageView count_cart_img = findViewById(R.id.image_count_cart);
        TextView text_count_cart = findViewById(R.id.text_count_cart);
        db.itemDao().getCount().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                countCart = integer;
                CalculationOfCart();
                DrawableRound.TextToDrawableRound(getApplicationContext(), count_cart_img, String.valueOf(integer), text_count_cart);

            }
        });
        itemListDataBase = db.itemDao().getAll();
        itemServicesListDataBase = db.itemServiceDao().getAll();
        CalculationOfCart();
    }

    private float FinalTotal = 0f;

    private void CalculationOfCart() {
        itemListDataBase = db.itemDao().getAll();
        itemServicesListDataBase = db.itemServiceDao().getAll();

        float totalItems = 0;
        float totalItemsService = 0;
        float totalGifts = 0;
        int Quantity = 0;
        for (int i = 0; i < itemListDataBase.size(); i++) {
            String price_str = itemListDataBase.get(i).getTotal();
            if (!TextUtils.isEmpty(price_str)) {
                float price = Float.parseFloat(price_str);
                totalItems += itemListDataBase.get(i).getQuantity() * price;


                if (itemListDataBase.get(i).getOfferPoints() == null || TextUtils.equals(itemListDataBase.get(i).getOfferPoints(), "0") || TextUtils.isEmpty(itemListDataBase.get(i).getOfferPoints())) {
                    if (itemListDataBase.get(i).getPoints() != null) {
                        if (TextUtils.equals(itemListDataBase.get(i).getPoints(), "0") || TextUtils.isEmpty(itemListDataBase.get(i).getOfferPoints())) {

                        } else {
                            float Points = Float.parseFloat(itemListDataBase.get(i).getPoints());
                            totalGifts += Points * itemListDataBase.get(i).getQuantity();
                        }
                    }

                } else {
                    float Points = Float.parseFloat(itemListDataBase.get(i).getTotalPoints());
                    totalGifts += Points * itemListDataBase.get(i).getQuantity();
                }


                Quantity += itemListDataBase.get(i).getQuantity();
                for (int s = 0; s < itemServicesListDataBase.size(); s++) {
                    String price_service_str = itemServicesListDataBase.get(s).getServicePrice();
                    if (!TextUtils.isEmpty(price_service_str)) {
                        float price_servie = Float.parseFloat(price_service_str);
                        if (TextUtils.equals(itemServicesListDataBase.get(s).getItemCode(), itemListDataBase.get(i).getCode())) {
                            totalItemsService += itemListDataBase.get(i).getQuantity() * price_servie;
                        }
                    }
                }

            }
        }
        String totalItemsAsString = String.format(Locale.ENGLISH, "%.2f", totalItems);
        String totalItemsServiceAsString = String.format(Locale.ENGLISH, "%.2f", totalItemsService);

        text_price_items.setText("" + totalItemsAsString + " " + getResources().getString(R.string.SAR));
        text_count_item.setText(" " + itemListDataBase.size() + " " + getResources().getString(R.string.countItem)
                + " | " + Quantity + " " + getResources().getString(R.string.Quantity));
        text_price_service.setText("" + totalItemsServiceAsString + " " + getResources().getString(R.string.SAR));

        float tax = Float.parseFloat(taxValue);
        float res_tax = ((totalItems + totalItemsService) / 100.0f) * tax;
        float total = (totalItems + totalItemsService) + res_tax;
        String total_tax = String.format(Locale.ENGLISH, "%.2f", res_tax);
        text_tax.setText("" + taxValue + " %" + "   -  " + total_tax + " " + getResources().getString(R.string.SAR));


        String totalAsString = String.format(Locale.ENGLISH, "%.2f", total);
        text_total.setText("" + totalAsString + " " + getResources().getString(R.string.SAR));
        FinalTotal = total;
        String totaltotalGifts = String.format(Locale.ENGLISH, "%.0f", totalGifts);
        if (totalGifts > 0) {
            layout_gifts.setVisibility(View.VISIBLE);
            text_total_gifts.setText("" + totaltotalGifts + " " + getResources().getString(R.string.Point));
        } else {
            layout_gifts.setVisibility(View.GONE);
        }
        for (int x = 0; x < itemListDataBase.size(); x++) {
            if (itemServicesListDataBase.size() > 0) {
                List<ItemService> newitemService = new ArrayList<>();
                for (int s = 0; s < itemServicesListDataBase.size(); s++) {
                    if (TextUtils.equals(itemListDataBase.get(x).getCode() + "".trim(), itemServicesListDataBase.get(s).getItemCode() + "".trim())) {
                        newitemService.add(itemServicesListDataBase.get(s));
                    }
                }
                itemListDataBase.get(x).setItemServices(newitemService);

            }
        }
        text_sheet_tottal.setText("" + totalAsString + " " + getResources().getString(R.string.SAR));
        FinalTotal = total;

        if (PayamentItemSelect != -1) {
            PayamentItemSelect = -1;
            for (int i = 0; i < paymentTypeList.size(); i++) {
                if (paymentTypeList.get(i).isItemSelect()) {
                    if (TextUtils.equals(paymentTypeList.get(i).getTagName(), PaymentType_wallet)) {
                        payment_methodEditText.setText("");
                        paymentTypeList.get(i).setItemSelect(false);
                    }
                }

            }

        }

        setupRecycler();
    }


    private void initSetup() {
        recycler_view = findViewById(R.id.recycler_view);
        layout_loading = findViewById(R.id.layout_loading);
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        btn_action = findViewById(R.id.btn_action);
        btn_action.setOnClickListener(this::onClick);
        text_count_item = findViewById(R.id.text_count_item);
        text_price_items = findViewById(R.id.text_price_items);
        text_price_service = findViewById(R.id.text_price_service);
        text_tax = findViewById(R.id.text_tax);
        text_total = findViewById(R.id.text_total);

        layout_gifts = findViewById(R.id.layout_gifts);
        text_total_gifts = findViewById(R.id.text_total_gifts);
        layout_gifts.setVisibility(View.GONE);
        end_main = findViewById(R.id.end_main);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this::onRefresh);
        end_main.setOnClickListener(view -> {
            onBackPressed();
        });
        user = getUserDetials.User(this);
        if (user != null) {

        }
        layout_loading.setOnClickListener(view -> {
            return;
        });
        String definitionsJson = AppPreferences.getString(ListCartActivity.this, "definitionsJson");
        if (definitionsJson != null) {
            Definition definition = new Gson().fromJson(definitionsJson, new Definition().getClass());
            if (definition.getTax() != null) {
                AppPreferences.saveString(ListCartActivity.this, "taxJson", definition.getTax());

            }
        }
        taxValue = AppPreferences.getString(ListCartActivity.this, "taxJson", "0");
        text_tax.setText("" + taxValue + " %");
        toolbarNameTxt.setText("" + getResources().getString(R.string.ListCart));

        SetUpViewSheet();
        SetUpBottomSheet();
        ListOfPayament();
        setUpDataBase();


        PackageManager pm = getPackageManager();
        int hasPerm2 = pm.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, getPackageName());
        if (hasPerm2 == PackageManager.PERMISSION_GRANTED) {
            GetLocationUser();
        } else {
            checkAndRequestPermissionsLocation();
        }
    }

    public void setupRecycler() {
        adapter = new RecyclerItems(getApplicationContext(), "CartList", itemListDataBase, R.layout.row_cart_items, new OnItemClickTagListener() {
            @Override
            public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                Item item = itemListDataBase.get(position);
                if (TextUtils.equals("plus", tag)) {
                    if (item.getQuantity() < 20) {
                        item.setQuantity(item.getQuantity() + 1);
                        InsertUpdateDeleteItem.upsert_Item(db, item);
                    }

                } else if (TextUtils.equals("add", tag)) {
                    if (item.getQuantity() == 0) {
                        item.setQuantity(item.getQuantity() + 1);
                        InsertUpdateDeleteItem.upsert_Item(db, item);
                    }

                } else if (TextUtils.equals("minus", tag)) {
                    if (item.getQuantity() > 1) {
                        item.setQuantity(item.getQuantity() - 1);
                        if (item.getQuantity() == 0) {
                            if (item.getItemServices().size() > 0) {
                                item.setItemServices(new ArrayList<>());
                                InsertUpdateDeleteItem.deleteByItemService(db, item.getCode());
                            }
                        }
                        InsertUpdateDeleteItem.upsert_Item(db, item);
                    }
                } else if (TextUtils.equals("service", tag)) {
                    GetItemServiceByItemCodeWebService(item);

                } else if (TextUtils.equals("remove", tag)) {
                    OpenDialogRemoveItemCart(item);

                }
                adapter.notifyDataSetChanged();
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setAdapter(adapter);

    }

    private void OpenDialogRemoveItemCart(Item item) {
        AppErrorsManager.showRemoveItemDialog(ListCartActivity.this, getResources().getString(R.string.Remove)
                , getResources().getString(R.string.are_you_sure_remove_item_from_cart), new InstallCallback() {
                    @Override
                    public void onStatusDone(String status) {
                        if (TextUtils.equals("yes", status)) {
                            item.setQuantity(0);
                            if (item.getItemServices().size() > 0) {
                                item.setItemServices(new ArrayList<>());
                                InsertUpdateDeleteItem.deleteByItemService(db, item.getCode());
                            }

                            InsertUpdateDeleteItem.upsert_Item(db, item);
                            adapter.notifyDataSetChanged();
                            setUpDataBase();

                        }
                    }
                });
    }


    private void GetItemServiceByItemCodeWebService(Item item) {
        layout_loading.setVisibility(View.VISIBLE);

        RetrofitWebService.getService(this).GetItemServiceByCodeItem(item.getCode() + "").enqueue(new Callback<ItemsServiceResponse>() {
            @Override
            public void onResponse(Call<ItemsServiceResponse> call, Response<ItemsServiceResponse> response) {
                Log.e("GetItemServiceByCode", response.toString());
                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (response.body() != null) {
                        List<ItemService> itemServices = response.body().getItemServices();
                        if (itemServices != null) {
                            if (itemServices.size() > 0) {
                                itemServicesListDataBase = db.itemServiceDao().getAll();
                                for (int i = 0; i < itemServices.size(); i++) {
                                    for (int x = 0; x < itemServicesListDataBase.size(); x++) {
                                        Log.e("itemServiceataBase0", itemServicesListDataBase.get(x).getItemCode() + "==" + item.getCode());
                                        if (TextUtils.equals(itemServicesListDataBase.get(x).getItemCode(), item.getCode())) {
                                            if (TextUtils.equals(itemServicesListDataBase.get(x).getServiceCode(), itemServices.get(i).getServiceCode())) {
                                                itemServices.get(i).setIs_selected(true);
                                            }
                                        }
                                    }
                                }
                                ShowDialogServiceItem(itemServices, item);
                            } else {
                                AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.info),
                                        response.body().getMessage());
                            }
                        } else {
                            AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.error),
                                    getResources().getString(R.string.notHaveService));
                        }
                    }
                } else {
                    AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.Failure),
                            getResources().getString(R.string.InternalServerError));
                }


                if (response.errorBody() != null) {
                    try {
                        Log.e("GetItemServiceByCode", " error code: " + response.code() + " error content: " + response.errorBody().string().toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                layout_loading.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ItemsServiceResponse> call, Throwable t) {
                Log.e("GetItemServiceByCode", t.toString());
                AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.Failure),
                        t.getMessage() + "");
                layout_loading.setVisibility(View.GONE);
            }
        });

    }

    RecyclerServiceDialog adapter_service;


    private void ShowDialogServiceItem(List<ItemService> itemServices, Item item) {

        adapter_service = new RecyclerServiceDialog(this, itemServices, R.layout.row_item_service, new OnItemClickTagListener() {
            @Override
            public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (TextUtils.equals("select", tag)) {
                            ItemService itemService = itemServices.get(position);
                            if (itemService.isIs_selected()) {
                                itemService.setIs_selected(false);
                            } else {
                                itemService.setIs_selected(true);
                            }
                            List<ItemService> itemServicenewList = new ArrayList<>();

                            for (int i = 0; i < itemServices.size(); i++) {
                                if (itemServices.get(i).isIs_selected()) {
                                    itemServices.get(i).setItemCode(item.getCode());
                                    itemServicenewList.add(itemServices.get(i));
                                }
                            }

                            InsertUpdateDeleteItem.deleteByItemService(db, item.getCode());

                            for (int i = 0; i < itemListDataBase.size(); i++) {
                                if (itemListDataBase.get(i).equals(item)) {
                                    itemListDataBase.get(i).setItemServices(itemServicenewList);
                                    if (item.getQuantity() > 0) {
                                        InsertUpdateDeleteItem.upsert_ItemService(db, itemServicenewList);
                                    }
                                }
                            }

                            setUpDataBase();
                            adapter_service.notifyDataSetChanged();
                            adapter.notifyDataSetChanged();

                        }
                    }
                });


            }
        });

        AppErrorsManager.showAddressListDialog(ListCartActivity.this, adapter_service, new InstallCallback() {
            @Override
            public void onStatusDone(String status) {
                if (TextUtils.equals("yes", status)) {
                }

            }
        });

    }

    private void CreateNewOrder(List<Item> itemListDataBase) {
        text_select_branch.clearFocus();
        text_select_branch.setError(null);//removes error
        locationEditText.clearFocus();
        locationEditText.setError(null);//removes error
        payment_methodEditText.clearFocus();
        payment_methodEditText.setError(null);

        deliveryDateTxt.clearFocus();
        deliveryDateTxt.setError(null);
        deliveryTimeTxt.clearFocus();
        deliveryTimeTxt.setError(null);

        int error_count = 0;
        User user = getUserDetials.User(this);
        if (user != null) {

            String AccessToken = user.getAccessToken();
            CreateOrder ItemsListCreate = ItemsListCreate(itemListDataBase, AccessToken);
            ItemsListCreate.setLat("");
            ItemsListCreate.setaLong("");
            ItemsListCreate.setClientAddress("");
            ItemsListCreate.setCompany("");
            ItemsListCreate.setCheckID("");
            ItemsListCreate.setDepositamount("");
            ItemsListCreate.setNotes("");
            ItemsListCreate.setReceivedDate("");
            ItemsListCreate.setReceivedTime("");
            ItemsListCreate.setNotes("");


            //if (TextUtils.equals(DeliveryType, DeliveryType_Cash)) {
            if (branchItemSelect == -1) {
                text_select_branch.setError(getResources().getString(R.string.field_required));
                text_select_branch.requestFocus();
                error_count++;
            } else {
                ItemsListCreate.setCompany(branchList.get(branchItemSelect).getCompany());
                ItemsListCreate.setDeliveryType(DeliveryType);

            }
            if (TextUtils.equals(DeliveryType, DeliveryType_OnHand)) {
                if (latitude == 0.0 && longitude == 0.0) {
                    locationEditText.setError(getResources().getString(R.string.field_required));
                    locationEditText.requestFocus();
                    error_count++;
                } else {
                    ItemsListCreate.setLat(latitude + "");
                    ItemsListCreate.setaLong("" + longitude);
                    ItemsListCreate.setDeliveryType(DeliveryType);
                }
                if (TextUtils.isEmpty("" + locationEditText.getText().toString().trim())) {
                    locationEditText.setError(getResources().getString(R.string.field_required));
                    locationEditText.requestFocus();
                    error_count++;
                } else {
                    ItemsListCreate.setClientAddress(locationEditText.getText().toString().trim());
                }
            } /**else {
             error_count++;
             AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.error)
             , getResources().getString(R.string.SelectDeliveryType));
             }*/


            if (PayamentItemSelect == -1) {
                payment_methodEditText.setError(getResources().getString(R.string.field_required));
                payment_methodEditText.requestFocus();
                error_count++;
            } else {
                if (TextUtils.equals(paymentTypeList.get(PayamentItemSelect).getTagName(), PaymentType_visa)) {
                    ItemsListCreate.setCheckID("");
                    ItemsListCreate.setDepositamount(FinalTotal + "");
                }
                ItemsListCreate.setPaymentType(paymentTypeList.get(PayamentItemSelect).getTagName());
            }

            if (TextUtils.isEmpty("" + deliveryDateTxt.getText().toString().trim())) {
                deliveryDateTxt.setError(getResources().getString(R.string.field_required));
                deliveryDateTxt.requestFocus();
                error_count++;
            } else {
                ItemsListCreate.setReceivedDate(deliveryDateTxt.getTag().toString() + "");
            }


            if (TextUtils.isEmpty("" + deliveryTimeTxt.getText().toString().trim())) {
                /**     deliveryTimeTxt.setError(getResources().getString(R.string.field_required));
                 deliveryTimeTxt.requestFocus();
                 error_count++;*/
                ItemsListCreate.setReceivedTime("");
            } else {
                ItemsListCreate.setReceivedTime(deliveryTimeTxt.getTag().toString() + "");
            }

            if (!TextUtils.isEmpty("" + notes_edittext.getText().toString().trim())) {
                ItemsListCreate.setNotes(notes_edittext.getText().toString().trim());
            }
            if (error_count == 0) {
                if (TextUtils.equals(paymentTypeList.get(PayamentItemSelect).getTagName(), PaymentType_visa)) {
                    CreateNewOrderVisa(itemListDataBase);
                } else {
                    layout_loading.setVisibility(View.VISIBLE);
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                    RetrofitWebService.getService(this).CreateNewOrder(ItemsListCreate).enqueue(new Callback<DResponse>() {
                        @Override
                        public void onResponse(Call<DResponse> call, Response<DResponse> response) {
                            Log.e("CreateNewOrder", "onResponse  " + response.toString());
                            if (response.body() != null) {
                                AppPreferences.saveString(ListCartActivity.this, Company_Select, ItemsListCreate.getCompany());
                                Log.e("CreateNewOrder", "onResponse  " + response.body().toString());
                                if (response.body().getStatus().contains("{")) {
                                    RootResponse rootResponse = new Gson().fromJson(response.body().getStatus(), RootResponse.class);
                                    if (TextUtils.equals("True", rootResponse.getStatus())) {
                                        AppErrorsManager.showCreateSuccessDialogNotCancel(ListCartActivity.this, getResources().getString(R.string.info),
                                                rootResponse.getMessage(), new InstallCallback() {
                                                    @Override
                                                    public void onStatusDone(String status) {
                                                        Intent intent = new Intent(getApplicationContext(), OrdersActivity.class);
                                                        startActivity(intent);
                                                        finish();
                                                    }
                                                });
                                        db.itemServiceDao().deleteAll();
                                        db.itemDao().DeletgetAll();

                                    } else {
                                        AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.error),
                                                rootResponse.getMessage());
                                    }
                                }
                            } else {
                                AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.Failure),
                                        getResources().getString(R.string.InternalServerError));
                            }
                            if (response.errorBody() != null) {
                                try {
                                    Log.e("CreateNewOrder", " error code: " + response.code() + " error content: " + response.errorBody().string().toString());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                            layout_loading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onFailure(Call<DResponse> call, Throwable t) {
                            Log.e("CreateNewOrder", "onFailure  " + t.toString());
                            layout_loading.setVisibility(View.GONE);

                            t.printStackTrace();


                        }
                    });
                }
            }

        }
    }

    private void CreateNewOrderVisa(List<Item> itemListDataBase) {
        text_select_branch.clearFocus();
        text_select_branch.setError(null);//removes error
        locationEditText.clearFocus();
        locationEditText.setError(null);//removes error
        payment_methodEditText.clearFocus();
        payment_methodEditText.setError(null);

        deliveryDateTxt.clearFocus();
        deliveryDateTxt.setError(null);


        deliveryTimeTxt.clearFocus();
        deliveryTimeTxt.setError(null);

        int error_count = 0;
        User user = getUserDetials.User(this);
        if (user != null) {

            String AccessToken = user.getAccessToken();
            CreateOrder ItemsListCreate = ItemsListCreate(itemListDataBase, AccessToken);
            ItemsListCreate.setLat("");
            ItemsListCreate.setaLong("");
            ItemsListCreate.setClientAddress("");
            ItemsListCreate.setCompany("");
            ItemsListCreate.setCheckID("");
            ItemsListCreate.setDepositamount("");
            ItemsListCreate.setNotes("");
            ItemsListCreate.setReceivedDate("");
            ItemsListCreate.setReceivedTime("");
            ItemsListCreate.setNotes("");


            //   if (TextUtils.equals(DeliveryType, DeliveryType_Cash)) {
            if (branchItemSelect == -1) {
                text_select_branch.setError(getResources().getString(R.string.field_required));
                text_select_branch.requestFocus();
                error_count++;
            } else {
                ItemsListCreate.setCompany(branchList.get(branchItemSelect).getCompany());
                ItemsListCreate.setDeliveryType(DeliveryType);
                branchString = branchList.get(branchItemSelect).getCompany() + "";

            }

            //  }
            if (TextUtils.equals(DeliveryType, DeliveryType_OnHand)) {
                if (latitude == 0.0 && longitude == 0.0) {
                    locationEditText.setError(getResources().getString(R.string.field_required));
                    locationEditText.requestFocus();
                    error_count++;
                } else {
                    ItemsListCreate.setLat(latitude + "");
                    ItemsListCreate.setaLong("" + longitude);
                    ItemsListCreate.setDeliveryType(DeliveryType);
                }
                if (TextUtils.isEmpty("" + locationEditText.getText().toString().trim())) {
                    locationEditText.setError(getResources().getString(R.string.field_required));
                    locationEditText.requestFocus();
                    error_count++;
                } else {
                    ItemsListCreate.setClientAddress(locationEditText.getText().toString().trim());
                }
            }


            if (PayamentItemSelect == -1) {
                payment_methodEditText.setError(getResources().getString(R.string.field_required));
                payment_methodEditText.requestFocus();
                error_count++;
            } else {
                if (TextUtils.equals(paymentTypeList.get(PayamentItemSelect).getTagName(), PaymentType_visa)) {
                    ItemsListCreate.setCheckID("");
                    ItemsListCreate.setDepositamount(FinalTotal + "");

                }
                ItemsListCreate.setPaymentType(paymentTypeList.get(PayamentItemSelect).getTagName());
            }

            if (TextUtils.isEmpty("" + deliveryDateTxt.getText().toString().trim())) {
                deliveryDateTxt.setError(getResources().getString(R.string.field_required));
                deliveryDateTxt.requestFocus();
                error_count++;
            } else {
                ItemsListCreate.setReceivedDate(deliveryDateTxt.getTag().toString() + "");
            }


            if (TextUtils.isEmpty("" + deliveryTimeTxt.getText().toString().trim())) {
                /**  deliveryTimeTxt.setError(getResources().getString(R.string.field_required));
                 deliveryTimeTxt.requestFocus();
                 error_count++;*/
                ItemsListCreate.setReceivedTime("");
            } else {
                ItemsListCreate.setReceivedTime(deliveryTimeTxt.getTag().toString() + "");
            }

            if (!TextUtils.isEmpty("" + notes_edittext.getText().toString().trim())) {
                ItemsListCreate.setNotes(notes_edittext.getText().toString().trim());
            }
            if (error_count == 0) {
                layout_loading.setVisibility(View.VISIBLE);
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
                RetrofitWebService.getService(this).CreateNewOrder(ItemsListCreate).enqueue(new Callback<DResponse>() {
                    @Override
                    public void onResponse(Call<DResponse> call, Response<DResponse> response) {
                        Log.e("CreateNewOrder", "onResponse  " + response.toString());
                        if (response.body() != null) {
                            AppPreferences.saveString(ListCartActivity.this, Company_Select, ItemsListCreate.getCompany());
                            if (response.body().getStatus().contains("{")) {
                                RootResponse rootResponse = new Gson().fromJson(response.body().getStatus(), RootResponse.class);
                                if (TextUtils.equals("True", rootResponse.getStatus())) {
                                    AppErrorsManager.showCreateSuccessDialogNotCancel(ListCartActivity.this, getResources().getString(R.string.info),
                                            rootResponse.getMessage(), new InstallCallback() {
                                                @Override
                                                public void onStatusDone(String status) {
                                                    Intent intent = new Intent(getApplicationContext(), OrdersActivity.class);
                                                    startActivity(intent);
                                                    finish();
                                                }
                                            });
                                    db.itemServiceDao().deleteAll();
                                    db.itemDao().DeletgetAll();

                                } else {
                                    AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.error),
                                            rootResponse.getMessage());
                                }
                            }
                        } else {
                            AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.Failure),
                                    getResources().getString(R.string.InternalServerError));
                        }
                        if (response.errorBody() != null) {
                            try {
                                Log.e("CreateNewOrder", " error code: " + response.code() + " error content: " + response.errorBody().string().toString());
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        layout_loading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onFailure(Call<DResponse> call, Throwable t) {
                        Log.e("CreateNewOrder", "onFailure  " + t.toString());
                        layout_loading.setVisibility(View.GONE);

                        t.printStackTrace();


                    }
                });
            }

        }
    }

    private void GetMyProfileRefresh() {
        if (user != null) {
            layout_loading.setVisibility(View.VISIBLE);
            String old_token = user.getAccessToken();
            RetrofitWebService.getService(this).GetMyProfile(user.getAccessToken()).enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    Log.e("GetMyProfile", response.toString());
                    if (RootManager.RESPONSE_CODE_OK == response.code()) {
                        if (TextUtils.equals("True", response.body().getStatus())) {
                            if (response.body() != null) {
                                User user = (User) response.body().getUser();
                                user.setAccessToken(old_token);
                                if (user != null) {
                                    Gson json = new Gson();
                                    String userJson = json.toJson(user);
                                    AppPreferences.saveString(ListCartActivity.this, "userJson", userJson);
                                    AppErrorsManager.showCustomErrorDialogNotCancel(ListCartActivity.this, getResources().getString(R.string.info),
                                            response.body().getMessage(), new InstallCallback() {
                                                @Override
                                                public void onStatusDone(String status) {
                                                    recreate();
                                                }
                                            });
                                } else {

                                    AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.error),
                                            response.body().getMessage());
                                }
                            } else {
                                AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.error),
                                        response.body().getMessage());
                            }
                        } else if (TextUtils.equals("False", response.body().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.error),
                                    response.body().getMessage());
                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.Failure),
                                getResources().getString(R.string.InternalServerError));
                    }
                    layout_loading.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Log.e("GetMyProfile", t.toString());
                    layout_loading.setVisibility(View.GONE);
                    AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.Failure),
                            t.getMessage() + "");
                }
            });
        }
    }


    private String branchString = "hatt14";


    private CreateOrder ItemsListCreate(List<Item> itemListDataBase, String AccessToken) {
        List<MultipartBody.Part> items_services = new ArrayList<>();
        CreateOrder createOrder = new CreateOrder();
        List<ItemsCreateOrder> itemsCreateOrder = new ArrayList<>();
        for (int i = 0; i < itemListDataBase.size(); i++) {
            items_services.add(MultipartBody.Part.createFormData("Items[" + i + "][ItemCode]", String.valueOf(itemListDataBase.get(i).getCode())));
            items_services.add(MultipartBody.Part.createFormData("Items[" + i + "][Qty]", String.valueOf(itemListDataBase.get(i).getQuantity())));
            if (itemListDataBase.get(i).getItemServices().size() > 0) {
                List<ItemService> serviceList = itemListDataBase.get(i).getItemServices();
                List<ItemsServiceCreateOrder> itemsServiceCreateOrderList = new ArrayList<>();
                for (int x = 0; x < serviceList.size(); x++) {
                    items_services.add(MultipartBody.Part.createFormData("Items[" + i + "][ItemsService[" + x + "][ItemCode]]", String.valueOf(serviceList.get(x).getItemCode())));
                    items_services.add(MultipartBody.Part.createFormData("Items[" + i + "][ItemsService[" + x + "][ServiceName]]", String.valueOf(serviceList.get(x).getServiceNameAra())));
                    items_services.add(MultipartBody.Part.createFormData("Items[" + i + "][ItemsService[" + x + "][Qty]]", String.valueOf(itemListDataBase.get(i).getQuantity())));
                    itemsServiceCreateOrderList.add(new ItemsServiceCreateOrder(serviceList.get(x).getItemCode(), String.valueOf(serviceList.get(x).getServiceNameAra()), String.valueOf(itemListDataBase.get(i).getQuantity())));
                }
                itemsCreateOrder.add(new ItemsCreateOrder(itemListDataBase.get(i).getCode(), itemListDataBase.get(i).getQuantity() + "", itemsServiceCreateOrderList));

            } else {
                itemsCreateOrder.add(new ItemsCreateOrder(itemListDataBase.get(i).getCode(), itemListDataBase.get(i).getQuantity() + ""));
            }

        }
        createOrder = new CreateOrder(AccessToken, itemsCreateOrder);
        Gson gson = new Gson();
        String jso = gson.toJson(createOrder, CreateOrder.class);
        Log.e("CreateNewOrder", jso);
        return createOrder;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.payment_methodEditText:
                OpenDialogPaymentType();
                break;

            case R.id.btn_action:
                if (countCart > 0) {
                    if (user != null) {
                        //  bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                        if (user.getMobileActivate() != null) {
                            if (TextUtils.equals("False", user.getMobileActivate())) {
                                AppErrorsManager.showMobileNotActiveDialog(ListCartActivity.this, getResources().getString(R.string.active_mobile)
                                        , getResources().getString(R.string.active_mobile_message), new InstallCallback() {
                                            @Override
                                            public void onStatusDone(String status) {
                                                if (TextUtils.equals("active", status)) {
                                                    OpenDialogActiveMobile();

                                                }
                                            }
                                        });
                            } else {
                                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

                            }
                        } else {
                            AppErrorsManager.showMobileNotActiveDialog(ListCartActivity.this, getResources().getString(R.string.active_mobile)
                                    , getResources().getString(R.string.active_mobile_message), new InstallCallback() {
                                        @Override
                                        public void onStatusDone(String status) {
                                            if (TextUtils.equals("active", status)) {
                                                OpenDialogActiveMobile();

                                            }
                                        }
                                    });
                        }
                    } else {
                        AppErrorsManager.showLoginAfterDialogNotCancel(ListCartActivity.this, getResources().getString(R.string.login),
                                getResources().getString(R.string.processrequireslogin), new InstallCallback() {
                                    @Override
                                    public void onStatusDone(String status) {
                                        if (TextUtils.equals("login", status)) {
                                            Intent mainIntent = new Intent(ListCartActivity.this, UserActionsActivity.class);
                                            startActivity(mainIntent);
                                            finish();
                                        }
                                    }
                                });
                    }

                } else {
                    AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.info),
                            getResources().getString(R.string.emptyCartEpty));
                }
                break;

            case R.id.card_location:
                ToggleTypeDelivery(card_location, card_shop);
                break;

            case R.id.card_shop:
                ToggleTypeDelivery(card_shop, card_location);
                break;


            case R.id.layout_select_location:
                Dexter.withActivity(ListCartActivity.this)
                        .withPermissions(Manifest.permission.ACCESS_FINE_LOCATION)
                        .withListener(new MultiplePermissionsListener() {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport multiplePermissionsReport) {
                                getLocation();

                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> list, PermissionToken permissionToken) {

                            }
                        }).check();
                break;


            case R.id.btn_sheet_action:
                if (user != null) {
                    CreateNewOrder(itemListDataBase);
                } else {
                    AppErrorsManager.showLoginAfterDialogNotCancel(ListCartActivity.this, getResources().getString(R.string.login),
                            getResources().getString(R.string.processrequireslogin), new InstallCallback() {
                                @Override
                                public void onStatusDone(String status) {
                                    if (TextUtils.equals("login", status)) {
                                        Intent mainIntent = new Intent(ListCartActivity.this, UserActionsActivity.class);
                                        startActivity(mainIntent);
                                        finish();
                                    }
                                }
                            });
                }
                break;
            case R.id.deliveryDateTxt:
                DialogFragment newFragmentDate = new DatePickerFragment();
                newFragmentDate.show(getSupportFragmentManager(), "datePicker");
                break;

            case R.id.deliveryTimeTxt:
                DialogFragment newFragment = new TimePickerFragment(60);
                newFragment.show(getSupportFragmentManager(), "timePicker");
                break;


            case R.id.text_select_branch:
                OpenDialogBranch();
                break;
        }
    }

    List<PaymentType> paymentTypeList = new ArrayList<>();

    private void ListOfPayament() {
        paymentTypeList = new ArrayList<>();


        String definitionsJson = AppPreferences.getString(ListCartActivity.this, "definitionsJson");
        if (definitionsJson != null) {
            Definition definition = new Gson().fromJson(definitionsJson, new Definition().getClass());

            if (TextUtils.equals("True", definition.getPaymentTypeCash()))
                paymentTypeList.add(new PaymentType(1, PaymentType_cash, getResources().getString(R.string.payment_cash), R.drawable.ic_cash));
            if (TextUtils.equals("True", definition.getPaymentTypeVisa()))
                paymentTypeList.add(new PaymentType(2, PaymentType_visa, getResources().getString(R.string.payment_visa), R.drawable.visa));
            if (TextUtils.equals("True", definition.getPaymentTypeWallet()))
                paymentTypeList.add(new PaymentType(3, PaymentType_wallet, getResources().getString(R.string.payment_wallet), R.drawable.ic_cash));


            if (!TextUtils.equals("True", definition.getDeliveryTypeOnShop())) {
                //card_shop.setEnabled(false);
                ToggleTypeDelivery(card_location, card_shop);
            }
            if (!TextUtils.equals("True", definition.getDeliveryTypeDelivery())) {
                //card_location.setEnabled(false);
                ToggleTypeDelivery(card_shop, card_location);
            }

        }
    }

    RecyclerPayamentDialog adapter_payament;
    private int PayamentItemSelect = -1;
    private int PayamentItemVisa = -1;

    private void OpenDialogPaymentType() {
        adapter_payament = new RecyclerPayamentDialog(this, paymentTypeList, R.layout.row_item_payamet, new OnItemClickTagListener() {
            @Override
            public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                if (TextUtils.equals("select", tag)) {
                    if (TextUtils.equals(paymentTypeList.get(position).getTagName(), PaymentType_cash)) {
                        PayamentItemSelect = position;
                        for (int i = 0; i < paymentTypeList.size(); i++) {
                            if (position == i) {
                                paymentTypeList.get(position).setItemSelect(true);

                            } else {
                                paymentTypeList.get(i).setItemSelect(false);
                            }
                        }
                    } else if (TextUtils.equals(paymentTypeList.get(position).getTagName(), PaymentType_wallet)) {
                        CheckWalletBalanceAvailable(position);
                    } else if (TextUtils.equals(paymentTypeList.get(position).getTagName(), PaymentType_visa)) {

                        PayamentItemVisa = position;
                        if (PayamentItemVisa != -1) {
                            PayamentItemSelect = PayamentItemVisa;
                            for (int i = 0; i < paymentTypeList.size(); i++) {
                                if (PayamentItemSelect == i) {
                                    paymentTypeList.get(PayamentItemSelect).setItemSelect(true);
                                } else {
                                    paymentTypeList.get(i).setItemSelect(false);
                                }
                            }

                        }

                    }

                    adapter_payament.notifyDataSetChanged();
                    payment_methodEditText.clearFocus();
                    payment_methodEditText.setError(null);
                }
            }
        });

        AppErrorsManager.showPayamentListDialog(ListCartActivity.this, adapter_payament, new InstallCallback() {
            @Override
            public void onStatusDone(String status) {
                if (TextUtils.equals("yes", status)) {
                    if (PayamentItemSelect != -1)
                        payment_methodEditText.setText(paymentTypeList.get(PayamentItemSelect).getCategoryName());
                }

            }
        });
    }

    private void CheckWalletBalanceAvailable(int position) {
        GetWalletBalanceWebService(position);
    }

    private void GetWalletBalanceWebService(int pos) {
        layout_loading.setVisibility(View.VISIBLE);
        if (user != null) {
            RetrofitWebService.getService(this).GetWalletBalance(user.getAccessToken()).enqueue(new Callback<WalletResponse>() {
                @Override
                public void onResponse(Call<WalletResponse> call, Response<WalletResponse> response) {
                    Log.e("GetWalletBalance", response.toString());
                    if (RootManager.RESPONSE_CODE_OK == response.code()) {
                        if (TextUtils.equals("True", response.body().getStatus())) {
                            String MySaveBalance = response.body().getBalance();
                            if (!TextUtils.isEmpty(MySaveBalance)) {
                                float BalanceMy = Float.parseFloat(MySaveBalance);
                                if (BalanceMy == 0) {
                                    AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.error),
                                            getResources().getString(R.string.wallet_balance_insufficient));
                                } else if (BalanceMy > 0) {
                                    if (FinalTotal > BalanceMy) {
                                        AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.info),
                                                getResources().getString(R.string.wallet_balance_insufficient) + "\n" +
                                                        getResources().getString(R.string.Yourcurrentbalance) + BalanceMy + " " + getResources().getString(R.string.SAR) + "\n" +
                                                        getResources().getString(R.string.Totalorder) + FinalTotal + " " + getResources().getString(R.string.SAR));

                                    } else {
                                        PayamentItemSelect = pos;
                                        for (int i = 0; i < paymentTypeList.size(); i++) {
                                            if (pos == i) {
                                                paymentTypeList.get(pos).setItemSelect(true);
                                            } else {
                                                paymentTypeList.get(i).setItemSelect(false);
                                            }
                                        }
                                        adapter_payament.notifyDataSetChanged();

                                    }
                                }
                            }

                        } else if (TextUtils.equals("False", response.body().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.error),
                                    response.body().getMessage());
                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.Failure),
                                getResources().getString(R.string.InternalServerError));
                    }

                    if (response.errorBody() != null) {
                        try {
                            Log.e("GetWalletBalance", " error code: " + response.code() + " error content: " + response.errorBody().string().toString());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    layout_loading.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<WalletResponse> call, Throwable t) {
                    Log.e("GetWalletBalance", t.toString());
                    AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.Failure),
                            t.getMessage() + "");
                    layout_loading.setVisibility(View.GONE);
                }
            });
        }

    }

    public void getLocation() {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            assert lm != null;
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ignored) {
        }

        try {
            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ignored) {
        }

        if (!gps_enabled && !network_enabled) {
            // notify user
            AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            dialog.setMessage(getResources().getString(R.string.gps_network_not_enabled));
            dialog.setPositiveButton(getResources().getString(R.string.open_location_settings), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(myIntent);
                    //get gps
                }
            });
            dialog.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub

                }
            });
            dialog.show();
        } else {


            if (branchItemSelect == -1) {
                AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.info),
                        getResources().getString(R.string.SelectBranchFirest));
            } else {
                Intent intent = new Intent(this, MapsActivity.class);
                intent.putExtra("latitude", latitude);
                intent.putExtra("longitude", longitude);
                intent.putExtra("branchString", branchList.get(branchItemSelect).getCompany());
                startActivityForResult(intent, 2);

            }

        }
    }


    public void GetLocationUser() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        Dexter.withActivity(ListCartActivity.this)
                .withPermissions(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport multiplePermissionsReport) {
                        fusedLocationClient.getLastLocation()
                                .addOnSuccessListener(ListCartActivity.this, new OnSuccessListener<Location>() {
                                    @Override
                                    public void onSuccess(Location location) {
                                        // Got last known location. In some rare situations this can be null.
                                        if (location != null) {
                                            latitude = location.getLatitude();
                                            longitude = location.getLongitude();

                                        }
                                    }
                                });
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<com.karumi.dexter.listener.PermissionRequest> list, PermissionToken permissionToken) {

                    }
                }).check();


    }


    private void GetNameStreet(Double latitude, Double longitude) {
        layout_loading.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(this).GetNameStreet(latitude, longitude, 18, 1, "ar").enqueue(new Callback<MapAddressResponse>() {
            @Override
            public void onResponse(Call<MapAddressResponse> call, Response<MapAddressResponse> response) {
                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (response.body() != null) {
                        String address_str = response.body().getDisplay_name();
                        if (!TextUtils.isEmpty(address_str)) {
                            locationEditText.setText("" + address_str);
                            Address address_ = response.body().getAddress();
                            if (address_ != null) {
                                locationEditText.setTextSize(12);
                                String country = " ";
                                String city = " ";
                                if (address_.getCountry() != null)
                                    country = address_.getCountry() + " - ";
                                if (address_.getCity() != null)
                                    city = address_.getCity() + " - ";

                                locationEditText.setText(country + city + address_str);
                                locationEditText.clearFocus();
                                locationEditText.setError(null);

                            }
                        }
                    }
                }

                layout_loading.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<MapAddressResponse> call, Throwable t) {
                layout_loading.setVisibility(View.GONE);

            }
        });
    }

    String DeliveryAmount = "0.00";

    @SuppressLint("NewApi")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2 && data != null) {
            latitude = data.getDoubleExtra("latitude", 0.0);
            longitude = data.getDoubleExtra("longitude", 0.0);
            DeliveryAmount = data.getStringExtra("DeliveryAmount");
            SetValueDeliveryMap();
            locationEditText.setTextColor(getResources().getColor(R.color.colorPrimary));
            locationEditText.setText(R.string.donesuccessfully);
            GetNameStreet(latitude, longitude);
        } else if (requestCode == REQ_USER_CONSENT) {
            if ((resultCode == RESULT_OK) && (data != null)) {
                //That gives all message to us.
                // We need to get the code from inside with regex
                String message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE);
                getOtpFromMessage(message);
            }

        }
    }

    private void SetValueDeliveryNotMap() {
        latitude = 0.0;
        longitude = 0.0;
        DeliveryAmount = "0.00";
        layout_price_delivery.setVisibility(View.GONE);
        text_price_delivery.setText("" + DeliveryAmount + " " + getResources().getString(R.string.SAR));
        text_price_items_2.setText("" + String.format(Locale.ENGLISH, "%.2f", FinalTotal) + " " + getResources().getString(R.string.SAR));
        Double total = Double.parseDouble(DeliveryAmount) + FinalTotal;
        String totalFormat = String.format(Locale.ENGLISH, "%.2f", total);
        text_total_.setText("" + totalFormat + " " + getResources().getString(R.string.SAR));
        text_sheet_tottal.setText("" + totalFormat + " " + getResources().getString(R.string.SAR));
        locationEditText.setText("");
    }

    private void SetValueDeliveryMap() {
        layout_price_delivery.setVisibility(View.VISIBLE);
        text_price_delivery.setText("" + DeliveryAmount + " " + getResources().getString(R.string.SAR));
        text_price_items_2.setText("" + String.format(Locale.ENGLISH, "%.2f", FinalTotal) + " " + getResources().getString(R.string.SAR));
        Double total = Double.parseDouble(DeliveryAmount) + FinalTotal;
        String totalFormat = String.format(Locale.ENGLISH, "%.2f", total);
        text_total_.setText("" + totalFormat + " " + getResources().getString(R.string.SAR));
        text_sheet_tottal.setText("" + totalFormat + " " + getResources().getString(R.string.SAR));
    }


    RecyclerBranch adapter_branch;
    List<Branch> branchList = new ArrayList<>();
    private int branchItemSelect = -1;

    private void instalBranch() {
        String branchesJson = AppPreferences.getString(ListCartActivity.this, "branchesJson");
        if (branchList.size() < 1) {
            if (branchesJson != null) {
                branchList = new Gson().fromJson(branchesJson, new TypeToken<List<Branch>>() {
                }.getType());
            }
        }
        String save_branch = AppPreferences.getString(ListCartActivity.this, Company_Select);
        if (!TextUtils.isEmpty(save_branch)) {
            for (int i = 0; i < branchList.size(); i++) {
                if (TextUtils.equals(save_branch, branchList.get(i).getCompany())) {
                    branchItemSelect = i;
                    branchList.get(i).setSelect(true);
                    text_select_branch.setText(branchList.get(branchItemSelect).getAddress());
                }
            }
        }
    }

    private void OpenDialogBranch() {
        instalBranch();
        adapter_branch = new RecyclerBranch(this, branchList, -1, R.layout.row_item_branch, new OnItemClickTagListener() {
            @Override
            public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                if (TextUtils.equals("select", tag)) {
                    branchItemSelect = position;
                    adapter_branch.ItemSelect = position;
                    for (int i = 0; i < branchList.size(); i++) {
                        if (position == i) {
                            branchList.get(position).setSelect(true);
                        } else {
                            branchList.get(i).setSelect(false);
                        }
                    }
                    adapter_branch.notifyDataSetChanged();
                } else if (TextUtils.equals("map", tag)) {
                    GoToMapBranchActivity(branchList.get(position).getLat(), branchList.get(position).getLong());
                }

            }
        });

        AppErrorsManager.showBranchListDialog(ListCartActivity.this, adapter_branch, new InstallCallback() {
            @Override
            public void onStatusDone(String status) {
                if (TextUtils.equals("yes", status)) {
                    if (branchItemSelect != -1)
                        text_select_branch.setText(branchList.get(branchItemSelect).getAddress());
                }

            }
        });
    }

    private void GoToMapBranchActivity(String lat, String aLong) {
        Uri gmmIntentUri = Uri.parse("google.streetview:cbll=" + lat + "," + aLong);
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        startActivity(mapIntent);
    }


    private void OpenDialogActiveMobile() {
        alertDialogBuilder = new AlertDialog.Builder(ListCartActivity.this).create();
        if (user != null) {
            AppErrorsManager.showActiveMobileDialog(ListCartActivity.this, "", user.getMobileString(), new InstallCallback() {
                @Override
                public void onStatusDone(String code) {
                    if (!TextUtils.isEmpty(code)) {
                        if (TextUtils.equals(code, "NewCode")) {
                            startSmsUserConsent();
                            SendMobileActivateCodeWebServices();
                        } else {
                            CheckMobileActivateCodeWebServices(code);
                        }
                    }

                }
            }, alertDialogBuilder);
        }

    }

    private void CheckMobileActivateCodeWebServices(String code) {
        layout_loading.setVisibility(View.VISIBLE);
        if (user != null) {
            RetrofitWebService.getService(this).CheckMobileActivateCode(user.getAccessToken(), code).enqueue(new Callback<RootResponse>() {
                @Override
                public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                    Log.e("CheckMobileActivateCode", response.toString());
                    if (RootManager.RESPONSE_CODE_OK == response.code()) {
                        if (TextUtils.equals("True", response.body().getStatus())) {
                            if (response.body() != null) {
                                Toasty.success(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT, true).show();
                                GetMyProfileRefresh();
                            } else {
                                AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.error),
                                        response.body().getMessage());
                            }
                        } else if (TextUtils.equals("False", response.body().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.error),
                                    response.body().getMessage());
                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.Failure),
                                getResources().getString(R.string.InternalServerError));
                    }
                    layout_loading.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<RootResponse> call, Throwable t) {
                    Log.e("CheckMobileActivateCode", t.toString());
                    layout_loading.setVisibility(View.GONE);
                    AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.Failure),
                            t.getMessage() + "");
                }
            });
        }
    }

    private void CheckMobileActivateServices(String code) {
        if (user != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(getResources().getString(R.string.progress_activation));
            progressDialog.show();
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            if (alertDialogBuilder != null) {
                alertDialogBuilder.dismiss();
            }
            RetrofitWebService.getService(this).CheckMobileActivateCode(user.getAccessToken(), code).enqueue(new Callback<RootResponse>() {
                @Override
                public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                    Log.e("CheckMobileActivateCode", response.toString());
                    if (RootManager.RESPONSE_CODE_OK == response.code()) {
                        if (TextUtils.equals("True", response.body().getStatus())) {
                            if (response.body() != null) {
                                Toasty.success(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT, true).show();
                                GetMyProfileRefresh();
                            } else {
                                AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.error),
                                        response.body().getMessage());
                            }
                        } else if (TextUtils.equals("False", response.body().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.error),
                                    response.body().getMessage());
                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.Failure),
                                getResources().getString(R.string.InternalServerError));
                    }
                    progressDialog.hide();
                }

                @Override
                public void onFailure(Call<RootResponse> call, Throwable t) {
                    Log.e("CheckMobileActivateCode", t.toString());
                    progressDialog.hide();
                    AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.Failure),
                            t.getMessage() + "");
                }
            });
        }
    }


    private void SendMobileActivateCodeWebServices() {
        layout_loading.setVisibility(View.VISIBLE);
        if (user != null) {
            RetrofitWebService.getService(this).SendMobileActivateCode(user.getAccessToken()).enqueue(new Callback<RootResponse>() {
                @Override
                public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                    Log.e("SendMobileActivateCode", response.toString());
                    if (RootManager.RESPONSE_CODE_OK == response.code()) {
                        if (TextUtils.equals("True", response.body().getStatus())) {
                            if (response.body() != null) {
                                Toasty.success(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT, true).show();
                            } else {
                                Toasty.warning(getApplicationContext(), getResources().getString(R.string.error), Toast.LENGTH_SHORT, true).show();
                            }
                        } else if (TextUtils.equals("False", response.body().getStatus())) {
                            Toasty.error(getApplicationContext(), response.body().getMessage() + "", Toast.LENGTH_SHORT, true).show();
                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.Failure),
                                getResources().getString(R.string.InternalServerError));
                    }
                    layout_loading.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<RootResponse> call, Throwable t) {
                    layout_loading.setVisibility(View.GONE);
                    InternetConnectionUtils.isInternetAvailable(getApplicationContext(), new InternetAvailableCallback() {
                        @Override
                        public void onInternetAvailable(boolean isAvailable) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (isAvailable) {
                                        AppErrorsManager.showCustomErrorDialog(ListCartActivity.this, getResources().getString(R.string.Failure),
                                                t.getMessage() + "");

                                    } else {
                                        AppErrorsManager.InternetUnAvailableDialog(ListCartActivity.this, getResources().getString(R.string.Failure),
                                                getResources().getString(R.string.nointernetconnection), new InstallCallback() {
                                                    @Override
                                                    public void onStatusDone(String status) {
                                                        if (TextUtils.equals("retry", status)) {
                                                            SendMobileActivateCodeWebServices();
                                                        }
                                                    }
                                                });
                                    }
                                }
                            });
                        }
                    });
                }
            });
        }
    }

    static final int REQUST_CODE = 1000;

    private void checkAndRequestPermissionsLocation() {
        String[] permissions = new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
        };
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(permission);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        Log.e("onRequestPeresult", requestCode + "  || " + REQUST_CODE + " " + PackageManager.PERMISSION_GRANTED + "  ||  " + grantResults[0]);
        switch (requestCode) {
            case REQUST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    GetLocationUser();
                } else {

                    Toast.makeText(this, "" + getResources().getString(R.string.message_gps_not_enabled), Toast.LENGTH_SHORT).show();
                    onBackPressed();
                }
                break;


        }

    }

    private AppMp3Manager appMp3Manager;
    private BroadcastReceiver mHander = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
                String title = intent.getStringExtra("title");
                String body = intent.getStringExtra("body");
                appMp3Manager = new AppMp3Manager(ListCartActivity.this);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        AppErrorsManager.showCustomErrorDialogTop(ListCartActivity.this, title, body, new InstallCallback() {
                            @Override
                            public void onStatusDone(String status) {

                            }
                        });
                        appMp3Manager.palyNotificationMusic(R.raw.bell_sound);
                    }
                }, 1000);
            }
        }
    };


    @Override
    protected void onRestart() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        super.onRestart();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHander);
        super.onPause();
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                setUpDataBase();
            }
        }, 2000);
    }

    private void startSmsUserConsent() {
        SmsRetrieverClient client = SmsRetriever.getClient(this);
        client.startSmsUserConsent(SMS_PROVIDER_NAME).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });
    }


    private void getOtpFromMessage(String message) {
        // This will match any 6 digit number in the message
        Pattern pattern = Pattern.compile("(|^)\\d{" + SMS_PATTERN_NUMBER + "}");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            // Toasty.info(getApplicationContext(), "" + getResources().getString(R.string.progress_activation), Toast.LENGTH_SHORT, true).show();
            CheckMobileActivateServices(matcher.group(0));
        }
    }

    /** private BroadcastReceiver smsVerificationReceiver = new BroadcastReceiver() {
    @Override public void onReceive(Context context, Intent intent) {
    if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
    Bundle extras = intent.getExtras();
    Status smsRetrieverStatus = (Status) extras.get(SmsRetriever.EXTRA_STATUS);

    switch (smsRetrieverStatus.getStatusCode()) {
    case CommonStatusCodes.SUCCESS:
    // Get consent intent
    Intent consentIntent = extras.getParcelable(SmsRetriever.EXTRA_CONSENT_INTENT);
    try {
    // Start activity to show consent dialog to user, activity must be started in
    // 5 minutes, otherwise you'll receive another TIMEOUT intent
    startActivityForResult(consentIntent, REQ_USER_CONSENT);
    } catch (ActivityNotFoundException e) {
    // Handle the exception ...
    }
    break;
    case CommonStatusCodes.TIMEOUT:
    // Time out occurred, handle the error.
    break;
    }
    }
    }
    };*/

    /**
     * private void registerBroadcastReceiver() {
     * IntentFilter intentFilter = new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION);
     * registerReceiver(smsVerificationReceiver, intentFilter);
     * }
     */

    @Override
    public void onStart() {
        super.onStart();
        // registerBroadcastReceiver();
    }

    @Override
    public void onStop() {
        super.onStop();
        //unregisterReceiver(smsVerificationReceiver);
    }
}
