package com.app.hotfresh.Ui.Adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.hotfresh.CallBack.OnItemClickTagListener;
import com.app.hotfresh.Manager.DrawableRound;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Medoles.Category;
import com.app.hotfresh.Medoles.OrderCategory;
import com.app.hotfresh.R;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import org.json.JSONException;
import org.w3c.dom.Text;

import java.io.FileNotFoundException;
import java.util.List;


public class RecyclerCategoryOrders extends RecyclerView.Adapter<RecyclerCategoryOrders.CustomView> {
    Context context;
    List<OrderCategory> mylist;
    private OnItemClickTagListener listener;
    int Layout;
    public static int ItemSelect = -1;
    private boolean isAnimationTrueShow = false;


    public class CustomView extends RecyclerView.ViewHolder {
        TextView row_name;
        LinearLayout layout_line;
        private ImageView row_img;

        public CustomView(View v) {
            super(v);
            FontManager.applyFont(context, v);
            row_name = v.findViewById(R.id.row_name);
            row_img = v.findViewById(R.id.row_img);
            layout_line = v.findViewById(R.id.layout_line);
        }

    }

    public RecyclerCategoryOrders() {
    }

    public RecyclerCategoryOrders(Context context, List<OrderCategory> mylist, int ItemSelect, int Layout, OnItemClickTagListener listener) {
        this.context = context;
        this.mylist = mylist;
        this.listener = listener;
        this.Layout = Layout;
        this.ItemSelect = ItemSelect;
    }

    @Override
    public CustomView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(Layout, parent, false);
        CustomView viewholder = new CustomView(view);
        return viewholder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final CustomView holder, final int position) {
        holder.row_name.setText(mylist.get(position).getCategoryName() + "");
        if (mylist.get(position).getCount() != null) {
            if (!TextUtils.equals(mylist.get(position).getCount(), "0")) {
                holder.row_img.setVisibility(View.VISIBLE);
                DrawableRound.TextToDrawable(context, holder.row_img, mylist.get(position).getCount() + "");
            } else {
                holder.row_img.setVisibility(View.VISIBLE);
                DrawableRound.TextToDrawableEmpty(context, holder.row_img);
            }
        }

        if (isAnimationTrueShow == false) {
            if (mylist.get(position).isAnimationTrue()) {
                YoYo.with(Techniques.ZoomIn)
                        .duration(400)
                        .repeat(2)
                        .playOn(holder.row_img);
                isAnimationTrueShow = true;
            }
        }
        if (ItemSelect == position) {
            holder.layout_line.setVisibility(View.VISIBLE);
            //  holder.row_name.setTextColor(context.getResources().getColor(R.color.colorWhite));
        } else {
            holder.layout_line.setVisibility(View.INVISIBLE);
            // holder.row_name.setTextColor(context.getResources().getColor(R.color.colorBlack));
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    listener.onItemClick(view, position, "view");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return mylist.size();
    }


}

