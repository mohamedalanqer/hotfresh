package com.app.hotfresh.Ui.Activites;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.hotfresh.CallBack.InstallCallback;
import com.app.hotfresh.CallBack.InternetAvailableCallback;
import com.app.hotfresh.CallBack.OnItemClickListener;
import com.app.hotfresh.CallBack.OnItemClickTagListener;
import com.app.hotfresh.DataBase.AppDatabase;
import com.app.hotfresh.Manager.AppErrorsManager;
import com.app.hotfresh.Manager.AppLanguage;
import com.app.hotfresh.Manager.AppMp3Manager;
import com.app.hotfresh.Manager.AppPreferences;
import com.app.hotfresh.Manager.DrawableRound;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Manager.InternetConnectionUtils;
import com.app.hotfresh.Manager.RootManager;
import com.app.hotfresh.Manager.getUserDetials;
import com.app.hotfresh.Medoles.Category;
import com.app.hotfresh.Medoles.Install.Definition;
import com.app.hotfresh.Medoles.Item;
import com.app.hotfresh.Medoles.ItemService;
import com.app.hotfresh.Medoles.User;
import com.app.hotfresh.R;
import com.app.hotfresh.Ui.Adapters.AdapterOfferHome;
import com.app.hotfresh.Ui.Adapters.KKViewPager;
import com.app.hotfresh.Ui.Adapters.RecyclerCategoryHome;
import com.app.hotfresh.Ui.Adapters.RecyclerSearch;
import com.app.hotfresh.WebService.RetrofitWebService;
import com.app.hotfresh.WebService.model.response.CategoryResponse;
import com.app.hotfresh.WebService.model.response.ItemsOffersResponse;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.Observer;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.hotfresh.Manager.RootManager.ActionKeyLocalBroadcastManager;
import static com.app.hotfresh.Manager.RootManager.DataBaseName;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, SwipeRefreshLayout.OnRefreshListener {
    RecyclerView recycler_view_search;
    RecyclerSearch adapter_search;
    List<Category> categories_search = new ArrayList<>();

    private FrameLayout layout_loading;

    RecyclerView recycler_view_category;
    RecyclerCategoryHome adapter_category;
    List<Category> categories_list = new ArrayList<>();
    SwipeRefreshLayout swipeRefreshLayout;

    private KKViewPager view_pager;
    List<Item> items_list = new ArrayList<>();
    private RelativeLayout Layout_notification;
    private LinearLayout layout_pager;
    private Handler handler;

    AppDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppLanguage.setContentLang(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_home);
        FontManager.applyFont(this, findViewById(R.id.drawer_layout));
        handler = new Handler(Looper.getMainLooper());

        initSetup();
        setUpDataBase();
    }

    private int countitem = 0;

    private void setUpDataBase() {
        db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, DataBaseName).allowMainThreadQueries().build();
        ImageView count_cart_img = findViewById(R.id.image_count_cart);
        TextView text_count_cart = findViewById(R.id.text_count_cart);

        count_cart_img.setOnClickListener(view -> {
            GoToCartActvity();
        });
        db.itemDao().getCount().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                countitem = integer;
                DrawableRound.TextToDrawableRound(getApplicationContext(), count_cart_img, String.valueOf(integer), text_count_cart);
            }
        });
    }


    EditText searchEditText;

    private void initSetup() {
        initSetupDrawer();
        recycler_view_search = findViewById(R.id.recycler_view_search);
        recycler_view_category = findViewById(R.id.recycler_view_category);
        view_pager = findViewById(R.id.kk_pager);
        Layout_notification = findViewById(R.id.Layout_notification);
        layout_pager = findViewById(R.id.layout_pager);
        Layout_notification.setOnClickListener(view -> {
            GoToCartActvity();
        });
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this::onRefresh);
        searchEditText = findViewById(R.id.searchEditText);
        layout_loading = findViewById(R.id.layout_loading);
        layout_loading.setOnClickListener(view -> {
            return;
        });
        GetListCategoriesWebService();
        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //after the change calling the method and passing the search input
                if (editable.length() >= 1) {
                    recycler_view_search.setVisibility(View.VISIBLE);
                    filter(editable.toString());
                } else {
                    recycler_view_search.setVisibility(View.GONE);
                }
            }
        });

    }

    private void GetListCategoriesWebService() {
        layout_loading.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(this).GetCategory().enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                Log.e("createLogin", response.toString());
                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (TextUtils.equals("True", response.body().getStatus())) {
                        categories_search = response.body().category;
                        categories_list = response.body().category;
                        Log.e("createLogin", categories_search.size() + "F");
                        setupRecyclerSearch();
                        setupRecyclerCategory();
                        GetListOffersWebService();
                    } else if (TextUtils.equals("False", response.body().getStatus())) {
                        AppErrorsManager.showCustomErrorDialog(HomeActivity.this, getResources().getString(R.string.error),
                                response.body().getMessage());
                    }
                } else {
                    AppErrorsManager.showCustomErrorDialog(HomeActivity.this, getResources().getString(R.string.Failure),
                            getResources().getString(R.string.InternalServerError));
                }
                layout_loading.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
                layout_loading.setVisibility(View.GONE);

                InternetConnectionUtils.isInternetAvailable(getApplicationContext(), new InternetAvailableCallback() {
                    @Override
                    public void onInternetAvailable(boolean isAvailable) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {

                                if (isAvailable) {
                                    AppErrorsManager.showCustomErrorDialog(HomeActivity.this, getResources().getString(R.string.error),
                                            t.getMessage() + "");
                                } else {
                                    AppErrorsManager.InternetUnAvailableDialog(HomeActivity.this, getResources().getString(R.string.Failure),
                                            getResources().getString(R.string.nointernetconnection), new InstallCallback() {
                                                @Override
                                                public void onStatusDone(String status) {
                                                    if (TextUtils.equals("retry", status)) {
                                                        GetListCategoriesWebService();
                                                    }
                                                }
                                            });
                                }
                            }
                        });
                    }
                });

            }
        });
    }

    private void GetListOffersWebService() {
        items_list.clear();
        layout_loading.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(this).GetLast5ItemsOffers().enqueue(new Callback<ItemsOffersResponse>() {
            @Override
            public void onResponse(Call<ItemsOffersResponse> call, Response<ItemsOffersResponse> response) {
                Log.e("GetItemByCategory", response.toString());
                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (response.body() != null) {
                        if (TextUtils.equals("True", response.body().getStatus())) {
                            items_list = response.body().getItems();
                            if (items_list != null) {
                                layout_pager.setVisibility(View.VISIBLE);
                                SetValueToViewPager(items_list);
                            }
                        } else if (TextUtils.equals("False", response.body().getStatus())) {

                            layout_pager.setVisibility(View.GONE);
                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(RecyclerView.LayoutParams.WRAP_CONTENT, RecyclerView.LayoutParams.WRAP_CONTENT);
                            params.setMargins(8,8,8,0);
                            recycler_view_category.setLayoutParams(params);

                        }
                    }
                } else {
                    AppErrorsManager.showCustomErrorDialog(HomeActivity.this, getResources().getString(R.string.Failure),
                            getResources().getString(R.string.InternalServerError));
                }
                if (response.errorBody() != null) {
                    try {
                        Log.e("GetItemByCategory2", " error code: " + response.code() + " error content: " + response.errorBody().string().toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                layout_loading.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ItemsOffersResponse> call, Throwable t) {
                Log.e("GetItemByCategory", t.toString());
                AppErrorsManager.showCustomErrorDialog(HomeActivity.this, getResources().getString(R.string.Failure),
                        t.getMessage() + "");
                layout_loading.setVisibility(View.GONE);
            }
        });


    }

    public void setupRecyclerSearch() {
        adapter_search = new RecyclerSearch(getApplicationContext(), categories_search, R.layout.row_item_search, new OnItemClickTagListener() {
            @Override
            public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                GoToItemActivity(filterdNames.get(position));
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recycler_view_search.setLayoutManager(linearLayoutManager);
        recycler_view_search.setAdapter(adapter_search);

    }

    public void setupRecyclerCategory() {
        adapter_category = new RecyclerCategoryHome(getApplicationContext(), categories_list, R.layout.row_item_category, new OnItemClickTagListener() {
            @Override
            public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                GoToItemActivity(categories_list.get(position));
            }
        });
        GridLayoutManager linearLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        recycler_view_category.setLayoutManager(linearLayoutManager);
        recycler_view_category.setAdapter(adapter_category);

    }

    private void GoToItemActivity(Category category) {
        Intent intent = new Intent(getApplicationContext(), ListItemByIdActivity.class);
        intent.putExtra("CategoryName", category.getCategoryName());
        intent.putExtra("CategoryID", category.getCategoryID());
        startActivity(intent);
    }

    List<Category> filterdNames = new ArrayList<>();

    private void filter(String text) {
        //new array list that will hold the filtered data
        filterdNames = new ArrayList<>();

        //looping through existing elements
        for (Category s : categories_search) {
            //if the existing elements contains the search input

            if (s.getCategoryName().toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterdNames.add(s);
            }
        }
        //calling a method of the adapter class and passing the filtered list
        adapter_search.filterList(filterdNames);
    }

    User user = new User();

    private int itemSelect = 0;

    private void SetValueToViewPager(List<Item> items_list) {
        String lang = AppLanguage.getLanguage(this);
        if (TextUtils.equals("ar", lang + "")) {
            if (items_list.size() > 0)
                itemSelect = items_list.size() - 1;
        } else {
            Collections.reverse(items_list);
        }
        final AdapterOfferHome adapterComment = new AdapterOfferHome(this, items_list, new OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) throws JSONException, FileNotFoundException {
                Intent intent = new Intent(getApplicationContext(), ListOffersActivity.class);
                intent.putExtra("Categ", items_list.get(position).getCateg());
                startActivity(intent);
            }
        });
        view_pager.setAnimationEnabled(true);
        view_pager.setFadeEnabled(true);
        view_pager.setFadeFactor(0.6f);
        view_pager.setAdapter(adapterComment);
        if (items_list.size() > 0)
            view_pager.setCurrentItem(itemSelect);

    }

    DrawerLayout drawer;
    ActionBarDrawerToggle toggle;

    private void initSetupDrawer() {
        MaterialToolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(
                this,
                drawer,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);

        toggle.setDrawerIndicatorEnabled(false);


        toggle.setToolbarNavigationClickListener(v -> {
            if (drawer.isDrawerVisible(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                drawer.openDrawer(GravityCompat.START);

            }
        });

        int resMenu = R.drawable.ic_menu;
        String lang = AppLanguage.getLanguage(this);
        if (TextUtils.equals("ar", lang)) {
            resMenu = R.drawable.ic_menu_ar;
        } else {
            resMenu = R.drawable.ic_menu;
        }
        drawer.addDrawerListener(toggle);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), resMenu, this.getTheme());
        toggle.setHomeAsUpIndicator(drawable);

        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);
        user = getUserDetials.User(this);
        if (user == null) {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.activity_visitor_drawer);
        }


        setUpSocialMediaView();
        FontManager.ChangeFontMenu(getApplicationContext(), navigationView);


    }

    private void setUpSocialMediaView() {
        ImageView img_instgram = findViewById(R.id.img_instgram);
        ImageView img_twitter = findViewById(R.id.img_twitter);
        ImageView img_face_book = findViewById(R.id.img_face_book);
        String definitionsJson = AppPreferences.getString(HomeActivity.this, "definitionsJson");
        if (definitionsJson != null) {
            Definition definition = new Gson().fromJson(definitionsJson, new Definition().getClass());

            img_instgram.setOnClickListener(view -> {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("" + definition.getInstgram()));
                this.startActivity(intent);
            });
            img_twitter.setOnClickListener(view -> {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("" + definition.getTwitter()));
                this.startActivity(intent);
            });
            img_face_book.setOnClickListener(view -> {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("" + definition.getFaceBook()));
                this.startActivity(intent);
            });
        }
    }


    @Override
    public void onBackPressed() {
        if (TextUtils.isEmpty(searchEditText.getText().toString().trim())) {
            AppErrorsManager.showLogOutsDialogNotCancel(HomeActivity.this, getResources().getString(R.string.exit),
                    getResources().getString(R.string.wantexitapplication), new InstallCallback() {
                        @Override
                        public void onStatusDone(String status) {
                            if (TextUtils.equals("Exit", status)) {
                                ExitApp();
                            }
                        }
                    });
        } else {
            searchEditText.setText("");
        }
    }

    private void ExitApp() {
        super.onBackPressed();

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.nav_home) {
            initSetup();
            //  logOut();
        } else if (item.getItemId() == R.id.nav_offers) {
            GoToOffersActvity();
        } else if (item.getItemId() == R.id.nav_cart_list) {
            GoToCartActvity();
        } else if (item.getItemId() == R.id.nav_orders) {
            GoToOrdersActvity();
        } else if (item.getItemId() == R.id.nav_package) {
            GoToPackagesActivity();
        } else if (item.getItemId() == R.id.nav_donation) {
            GoToCharitiesActvity();
        } else if (item.getItemId() == R.id.nav_wallet) {
            GoToWalletActvity();
        } else if (item.getItemId() == R.id.nav_setting) {
            GoToSettingActvity();
        }
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

    private void GoToPackagesActivity() {
        Intent intent =new Intent(getApplicationContext() ,PackagesActivity.class);
        startActivity(intent);

    }

    private void GoToWalletActvity() {
        Intent intent = new Intent(getApplicationContext(), MyWalletActivity.class);
        startActivity(intent);
    }

    private void GoToOrdersActvity() {
        Intent intent = new Intent(getApplicationContext(), OrdersActivity.class);
        startActivity(intent);
    }

    private void GoToSettingActvity() {
        Intent intent = new Intent(getApplicationContext(), SettingListActivity.class);
        startActivity(intent);
    }

    private void GoToCharitiesActvity() {
        Intent intent = new Intent(getApplicationContext(), CharitiesActivity.class);
        startActivity(intent);
    }

    private void GoToCartActvity() {
        if (countitem > 0) {
            Intent intent = new Intent(getApplicationContext(), ListCartActivity.class);
            startActivity(intent);
        } else {
            AppErrorsManager.showCustomErrorDialog(HomeActivity.this, getResources().getString(R.string.info),
                    getResources().getString(R.string.emptyCartEpty));
        }

    }

    private void GoToOffersActvity() {
        Intent intent = new Intent(getApplicationContext(), ListOffersActivity.class);
        startActivity(intent);
    }

    public void logOut() {
        User user = getUserDetials.User(this);
        if (user != null) {
            AppErrorsManager.showSuccessDialog(HomeActivity.this, getString(R.string.logout), getString(R.string.do_you_want_logout), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Gson json = new Gson();
                    AppPreferences.saveString(HomeActivity.this, "userJson", "0");
                    Intent intent = new Intent(HomeActivity.this, SplashActivity.class);
                    startActivity(intent);
                    finish();

                }
            }, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
        } else {
            Intent intent = new Intent(HomeActivity.this, SplashActivity.class);
            startActivity(intent);
        }


    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                initSetup();
            }
        }, 2000);
    }


    private AppMp3Manager appMp3Manager;

    private BroadcastReceiver mHander = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String title = intent.getStringExtra("title");
            String body = intent.getStringExtra("body");
            appMp3Manager = new AppMp3Manager(HomeActivity.this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    AppErrorsManager.showCustomErrorDialogTop(HomeActivity.this, title, body, new InstallCallback() {
                        @Override
                        public void onStatusDone(String status) {

                        }
                    });
                    appMp3Manager.palyNotificationMusic(R.raw.bell_sound);
                }
            }, 1000);
        }
    };


    @Override
    protected void onRestart() {
        setUpDataBase();
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        super.onRestart();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHander);
        super.onPause();
    }
}
