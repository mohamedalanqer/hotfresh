package com.app.hotfresh.Ui.Activites;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.app.hotfresh.Manager.AppLanguage;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.R;
import com.app.hotfresh.Ui.Fragments.ForgetPasswordFragment;
import com.app.hotfresh.Ui.Fragments.LoginFragment;
import com.app.hotfresh.Ui.Fragments.RegisterFragment;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.gson.Gson;

public class UserPasswordsActivity extends AppCompatActivity   {

    private CardView card_view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppLanguage.setContentLang(this);
        setContentView(R.layout.activity_user_password);
        FontManager.applyFont(this, findViewById(R.id.layout));

        initSetUp();

    }

    private void initSetUp() {
        card_view = findViewById(R.id.card_view);
        String action = getIntent().getStringExtra("action");
        if (!TextUtils.isEmpty(action)) {
            if (TextUtils.equals(action, getResources().getString(R.string.ForgetPassword))) {
                openFragment(new ForgetPasswordFragment());

            }
        }


    }

    public String toJson(Object object) {
        return new Gson().toJson(object);
    }





    public void openFragment(Fragment fragment) {
        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.fragment, fragment).commit();
        YoYo.with(Techniques.BounceInLeft)
                .duration(700)
                .playOn(card_view);
    }

    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (overrideConfiguration != null) {
            int uiMode = overrideConfiguration.uiMode;
            overrideConfiguration.setTo(getBaseContext().getResources().getConfiguration());
            overrideConfiguration.uiMode = uiMode;
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    @Override
    public void onBackPressed() {
        /** if (TextUtils.equals(tag, getResources().getString(R.string.create_subscription))) {
         Intent intent = new Intent(UserActionsActivity.this, HomeProviderActivity.class);

         intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
         startActivity(intent);
         ActivityCompat.finishAffinity(UserActionsActivity.this); //with v4 support library
         } else {*/
        super.onBackPressed();
        // }

    }
}
