package com.app.hotfresh.Ui.Activites;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.Observer;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.hotfresh.CallBack.InstallCallback;
import com.app.hotfresh.CallBack.InternetAvailableCallback;
import com.app.hotfresh.CallBack.OnItemClickTagListener;
import com.app.hotfresh.DataBase.AppDatabase;
import com.app.hotfresh.DataBase.InsertUpdateDeleteItem;
import com.app.hotfresh.Manager.AppErrorsManager;
import com.app.hotfresh.Manager.AppLanguage;
import com.app.hotfresh.Manager.AppMp3Manager;
import com.app.hotfresh.Manager.DrawableRound;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Manager.InternetConnectionUtils;
import com.app.hotfresh.Manager.RootManager;
import com.app.hotfresh.Medoles.Category;
import com.app.hotfresh.Medoles.Charities.Charitie;
import com.app.hotfresh.Medoles.Item;
import com.app.hotfresh.Medoles.ItemService;
import com.app.hotfresh.R;
import com.app.hotfresh.Ui.Adapters.RecyclerCharitie;
import com.app.hotfresh.Ui.Adapters.RecyclerItems;
import com.app.hotfresh.Ui.Adapters.RecyclerServiceDialog;
import com.app.hotfresh.WebService.RetrofitWebService;
import com.app.hotfresh.WebService.model.response.CharitiesResponse;
import com.app.hotfresh.WebService.model.response.ItemsResponse;
import com.app.hotfresh.WebService.model.response.ItemsServiceResponse;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.hotfresh.Manager.RootManager.ActionKeyLocalBroadcastManager;
import static com.app.hotfresh.Manager.RootManager.DataBaseName;

public class CharitiesActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    private FrameLayout layout_loading;

    RecyclerView recycler_view;
    RecyclerCharitie adapter;
    List<Charitie> charities = new ArrayList<>();
    private String CategoryName = "";
    private TextView toolbarNameTxt;
    AppDatabase db;
    List<Item> itemListDataBase = new ArrayList<>();
    List<ItemService> itemServicesListDataBase = new ArrayList<>();
    private Handler handler;
    private RelativeLayout end_main;
    SwipeRefreshLayout swipeRefreshLayout;
    private RelativeLayout Layout_notification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppLanguage.setContentLang(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_list_item_id);
        FontManager.applyFont(this, findViewById(R.id.layout));
        handler = new Handler(Looper.getMainLooper());


        initSetup();
        setUpDataBase();
    }

    private int countitem = 0;

    private void setUpDataBase() {
        db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, DataBaseName).allowMainThreadQueries().build();

        ImageView count_cart_img = findViewById(R.id.image_count_cart);
        TextView text_count_cart = findViewById(R.id.text_count_cart);


        db.charitiesItemDao().getCount().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                countitem = integer;
                DrawableRound.TextToDrawableRoundGreen(getApplicationContext(), count_cart_img, String.valueOf(integer), text_count_cart);
            }
        });
        itemListDataBase = db.itemDao().getAll();
        itemServicesListDataBase = db.itemServiceDao().getAll();
    }


    private void initSetup() {
        recycler_view = findViewById(R.id.recycler_view);
        layout_loading = findViewById(R.id.layout_loading);
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        end_main = findViewById(R.id.end_main);

        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this::onRefresh);

        end_main.setOnClickListener(view -> {
            onBackPressed();
        });
        layout_loading.setOnClickListener(view -> {
            return;
        });
        GetCharitiesWebService();
        toolbarNameTxt.setText("" + getResources().getString(R.string.TheCharities));

        Layout_notification = findViewById(R.id.Layout_notification);
        Layout_notification.setOnClickListener(view -> {
            GoToCartActvity();
        });
    }

    private void GoToCartActvity() {
        if (countitem > 0) {
            Intent intent = new Intent(getApplicationContext(), ListCartCharitiesActivity.class);
            startActivity(intent);
        } else {
            AppErrorsManager.showCustomErrorDialog(CharitiesActivity.this, getResources().getString(R.string.info),
                    getResources().getString(R.string.emptyCartEpty));
        }

    }

    private void GetCharitiesWebService() {
        layout_loading.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(this).GetCharities().enqueue(new Callback<CharitiesResponse>() {
            @Override
            public void onResponse(Call<CharitiesResponse> call, Response<CharitiesResponse> response) {
                Log.e("GetItemByCategory", response.toString());
                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (response.body() != null) {
                        if (TextUtils.equals("True", response.body().getStatus())) {
                            charities = response.body().getCharities();
                            if (charities.size() > 0) {
                                recycler_view.setVisibility(View.VISIBLE);
                                setupRecycler();
                            }
                        } else if (TextUtils.equals("False", response.body().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(CharitiesActivity.this, getResources().getString(R.string.error),
                                    response.body().getMessage());
                        }
                    }
                } else {
                    AppErrorsManager.showCustomErrorDialog(CharitiesActivity.this, getResources().getString(R.string.Failure),
                            getResources().getString(R.string.InternalServerError));
                }

                layout_loading.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<CharitiesResponse> call, Throwable t) {

                layout_loading.setVisibility(View.GONE);
                InternetConnectionUtils.isInternetAvailable(getApplicationContext(), new InternetAvailableCallback() {
                    @Override
                    public void onInternetAvailable(boolean isAvailable) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (isAvailable) {

                                    AppErrorsManager.showCustomErrorDialog(CharitiesActivity.this, getResources().getString(R.string.Failure),
                                            t.getMessage() + "");
                                } else {
                                    AppErrorsManager.InternetUnAvailableDialog(CharitiesActivity.this, getResources().getString(R.string.Failure),
                                            getResources().getString(R.string.nointernetconnection), new InstallCallback() {
                                                @Override
                                                public void onStatusDone(String status) {
                                                    if (TextUtils.equals("retry", status)) {
                                                        GetCharitiesWebService();
                                                    }
                                                }
                                            });
                                }
                            }
                        });
                    }
                });
            }
        });


    }


    public void setupRecycler() {
        adapter = new RecyclerCharitie(getApplicationContext(), charities, -1, R.layout.row_item_charitie, new OnItemClickTagListener() {
            @Override
            public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                Charitie charitie = charities.get(position);
                if (TextUtils.equals("view", tag)) {
                    adapter.ItemSelect = position;
                    GoToItemActivity(charitie);
                }
                adapter.notifyDataSetChanged();
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setAdapter(adapter);

    }

    private void GoToItemActivity(Charitie charitie) {
        Intent intent = new Intent(getApplicationContext(), CharitiesItemsActivity.class);
        intent.putExtra("CharitiesName", charitie.getName());
        startActivity(intent);
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                GetCharitiesWebService();
            }
        }, 2000);
    }


    private AppMp3Manager appMp3Manager;
    private BroadcastReceiver mHander = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String title = intent.getStringExtra("title");
            String body = intent.getStringExtra("body");
            appMp3Manager = new AppMp3Manager(CharitiesActivity.this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    AppErrorsManager.showCustomErrorDialogTop(CharitiesActivity.this, title, body, new InstallCallback() {
                        @Override
                        public void onStatusDone(String status) {

                        }
                    });
                    appMp3Manager.palyNotificationMusic(R.raw.bell_sound);
                }
            }, 1000);
        }
    };


    @Override
    protected void onRestart() {
        setUpDataBase();
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        super.onRestart();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHander);
        super.onPause();
    }
}
