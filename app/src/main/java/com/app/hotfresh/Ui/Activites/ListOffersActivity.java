package com.app.hotfresh.Ui.Activites;

import android.animation.ValueAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.airbnb.lottie.LottieAnimationView;
import com.app.hotfresh.CallBack.InstallCallback;
import com.app.hotfresh.CallBack.InternetAvailableCallback;
import com.app.hotfresh.CallBack.OnItemClickTagListener;
import com.app.hotfresh.DataBase.AppDatabase;
import com.app.hotfresh.DataBase.InsertUpdateDeleteItem;
import com.app.hotfresh.Manager.AppErrorsManager;
import com.app.hotfresh.Manager.AppLanguage;
import com.app.hotfresh.Manager.AppMp3Manager;
import com.app.hotfresh.Manager.DrawableRound;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Manager.InternetConnectionUtils;
import com.app.hotfresh.Manager.RootManager;
import com.app.hotfresh.Manager.getUserDetials;
import com.app.hotfresh.Medoles.Category;
import com.app.hotfresh.Medoles.Item;
import com.app.hotfresh.Medoles.ItemService;
import com.app.hotfresh.Medoles.User;
import com.app.hotfresh.R;
import com.app.hotfresh.Ui.Adapters.RecyclerCategory;
import com.app.hotfresh.Ui.Adapters.RecyclerItems;
import com.app.hotfresh.Ui.Adapters.RecyclerItemsOffers;
import com.app.hotfresh.Ui.Adapters.RecyclerSearch;
import com.app.hotfresh.Ui.Adapters.RecyclerServiceDialog;
import com.app.hotfresh.WebService.RetrofitWebService;
import com.app.hotfresh.WebService.model.response.CategoryResponse;
import com.app.hotfresh.WebService.model.response.ItemsOffersResponse;
import com.app.hotfresh.WebService.model.response.ItemsResponse;
import com.app.hotfresh.WebService.model.response.ItemsServiceResponse;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.hotfresh.Manager.RootManager.ActionKeyLocalBroadcastManager;
import static com.app.hotfresh.Manager.RootManager.DataBaseName;

public class ListOffersActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    private FrameLayout layout_loading;

    RecyclerView recycler_view;
    RecyclerItemsOffers adapter;
    List<Item> items_list = new ArrayList<>();
    private String CategoryName = "";
    private TextView toolbarNameTxt;
    AppDatabase db;
    List<Item> itemListDataBase = new ArrayList<>();
    List<ItemService> itemServicesListDataBase = new ArrayList<>();
    private String Categ = "";


    RecyclerView recycler_view_category;
    RecyclerCategory adapter_category;
    List<Category> categories_list = new ArrayList<>();
    private LinearLayout layout_empty;
    private TextView text_empty;
    private Handler handler;
    private RelativeLayout end_main;
    SwipeRefreshLayout swipeRefreshLayout;
    private RelativeLayout Layout_notification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppLanguage.setContentLang(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_list_offers);
        FontManager.applyFont(this, findViewById(R.id.layout));
        handler = new Handler(Looper.getMainLooper());
        Categ = getIntent().getStringExtra("Categ");

        initSetup();
        setUpDataBase();
    }

    private int countitem = 0;
    LottieAnimationView animationView;

    private void setUpDataBase() {
        db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, DataBaseName).allowMainThreadQueries().build();

        ImageView count_cart_img = findViewById(R.id.image_count_cart);
        TextView text_count_cart = findViewById(R.id.text_count_cart);
        db.itemDao().getCount().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                countitem = integer;
                DrawableRound.TextToDrawableRound(getApplicationContext(), count_cart_img, String.valueOf(integer), text_count_cart);
            }
        });
        itemListDataBase = db.itemDao().getAll();
        itemServicesListDataBase = db.itemServiceDao().getAll();
    }


    private void initSetup() {
        recycler_view = findViewById(R.id.recycler_view);
        recycler_view_category = findViewById(R.id.recycler_view_category);
        layout_loading = findViewById(R.id.layout_loading);
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        layout_empty = findViewById(R.id.layout_empty);
        end_main = findViewById(R.id.end_main);
        text_empty = findViewById(R.id.text_empty);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this::onRefresh);
        animationView = findViewById(R.id.lottie_layer_name);
        end_main.setOnClickListener(view -> {
            onBackPressed();
        });
        layout_loading.setOnClickListener(view -> {
            return;
        });
        GetListCategoriesWebService();
        toolbarNameTxt.setText("" + getResources().getString(R.string.offerList));

        Layout_notification = findViewById(R.id.Layout_notification);
        Layout_notification.setOnClickListener(view -> {
            GoToCartActvity();
        });
    }

    private void GoToCartActvity() {
        if (countitem > 0) {
            Intent intent = new Intent(getApplicationContext(), ListCartActivity.class);
            startActivity(intent);
        } else {
            AppErrorsManager.showCustomErrorDialog(ListOffersActivity.this, getResources().getString(R.string.info),
                    getResources().getString(R.string.emptyCartEpty));
        }

    }

    private void GetListItemsWebService(String CategoryID ,String CategoryName) {
        layout_loading.setVisibility(View.VISIBLE);
        recycler_view.setVisibility(View.GONE);
        layout_empty.setVisibility(View.GONE);
        items_list = new ArrayList<>();
        User user = getUserDetials.User(this);
        String AccessToken = "";
        if(user != null){
            AccessToken =user.getAccessToken();
        }
        RetrofitWebService.getService(this).GetItemsOffersByCategory(CategoryID , AccessToken).enqueue(new Callback<ItemsOffersResponse>() {
            @Override
            public void onResponse(Call<ItemsOffersResponse> call, Response<ItemsOffersResponse> response) {
                Log.e("GetItemByCategory", response.toString());
                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (response.body() != null) {
                        if (TextUtils.equals("True", response.body().getStatus())) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    items_list = response.body().getItems();
                                    if (items_list != null) {
                                        if (items_list.size() > 0) {
                                            for (int i = 0; i < items_list.size(); i++) {
                                                for (int x = 0; x < itemListDataBase.size(); x++) {
                                                    if (TextUtils.equals(items_list.get(i).getCode(), itemListDataBase.get(x).getCode())) {
                                                        items_list.get(i).setQuantity(itemListDataBase.get(x).getQuantity());
                                                        Log.e("itemServiceataBase", "== " + itemServicesListDataBase.size());
                                                        if (itemServicesListDataBase.size() > 0) {
                                                            List<ItemService> newitemService = new ArrayList<>();
                                                            for (int s = 0; s < itemServicesListDataBase.size(); s++) {
                                                                if (TextUtils.equals(items_list.get(i).getCode() + "".trim(), itemServicesListDataBase.get(s).getItemCode() + "".trim())) {
                                                                    newitemService.add(itemServicesListDataBase.get(s));
                                                                }

                                                            }
                                                            items_list.get(i).setItemServices(newitemService);
                                                        }
                                                    }
                                                }
                                            }
                                            layout_empty.setVisibility(View.GONE);
                                            recycler_view.setVisibility(View.VISIBLE);
                                            setupRecycler();
                                        } else {
                                            recycler_view.setVisibility(View.GONE);
                                            layout_empty.setVisibility(View.VISIBLE);
                                            text_empty.setText(Html.fromHtml("<font color='#FDC723'>" + CategoryName + "</font>" + "<br/>" + response.body().getMessage()));

                                        }
                                    } else {
                                        recycler_view.setVisibility(View.GONE);
                                        layout_empty.setVisibility(View.VISIBLE);
                                        //  text_empty.setText(CategoryName + "\n" + response.body().getMessage());
                                        text_empty.setText(Html.fromHtml("<font color='#FDC723'>" + CategoryName + "</font>" + "<br/>" + response.body().getMessage()));
                                    }

                                }
                            });

                        } else if (TextUtils.equals("False", response.body().getStatus())) {
                            recycler_view.setVisibility(View.GONE);
                            layout_empty.setVisibility(View.VISIBLE);
                            // text_empty.setText(CategoryName + "\n" + response.body().getMessage());
                            text_empty.setText(Html.fromHtml("<font color='#EFBF2F'>" + CategoryName + "</font>" + "<br/>" + response.body().getMessage()));
                        }
                    }
                } else {
                    AppErrorsManager.showCustomErrorDialog(ListOffersActivity.this, getResources().getString(R.string.Failure),
                            getResources().getString(R.string.InternalServerError));
                }


                if (response.errorBody() != null) {
                    try {
                        Log.e("GetItemByCategory2", " error code: " + response.code() + " error content: " + response.errorBody().string().toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                layout_loading.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ItemsOffersResponse> call, Throwable t) {


                layout_loading.setVisibility(View.GONE);
                InternetConnectionUtils.isInternetAvailable(getApplicationContext(), new InternetAvailableCallback() {
                    @Override
                    public void onInternetAvailable(boolean isAvailable) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (isAvailable) {
                                    AppErrorsManager.showCustomErrorDialog(ListOffersActivity.this, getResources().getString(R.string.Failure),
                                            t.getMessage() + "");
                                } else {
                                    AppErrorsManager.InternetUnAvailableDialog(ListOffersActivity.this, getResources().getString(R.string.Failure),
                                            getResources().getString(R.string.nointernetconnection), new InstallCallback() {
                                                @Override
                                                public void onStatusDone(String status) {
                                                    if (TextUtils.equals("retry", status)) {
                                                        GetListCategoriesWebService();
                                                    }
                                                }
                                            });
                                }
                            }
                        });
                    }
                });
            }
        });


    }

    private void GetListCategoriesWebService() {
        layout_loading.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(this).GetCategory().enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {

                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (TextUtils.equals("True", response.body().getStatus())) {
                        categories_list = response.body().category;
                        if (categories_list.size() > 0) {
                            if (TextUtils.isEmpty(Categ)) {
                                itemSelect = 0;
                                GetListItemsWebService(categories_list.get(0).getCategoryID() ,categories_list.get(0).getCategoryName());
                            } else {
                                for (int i = 0; i < categories_list.size(); i++) {
                                    if (TextUtils.equals(Categ, categories_list.get(i).getCategoryName())) {
                                        GetListItemsWebService(categories_list.get(i).getCategoryID() , categories_list.get(i).getCategoryName());
                                        itemSelect = i;
                                    }
                                }
                            }
                            setupRecyclerCategory();
                        }

                    } else if (TextUtils.equals("False", response.body().getStatus())) {
                        AppErrorsManager.showCustomErrorDialog(ListOffersActivity.this, getResources().getString(R.string.error),
                                response.body().getMessage());
                    }
                } else {
                    AppErrorsManager.showCustomErrorDialog(ListOffersActivity.this, getResources().getString(R.string.Failure),
                            getResources().getString(R.string.InternalServerError));
                }
                layout_loading.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
                AppErrorsManager.showCustomErrorDialog(ListOffersActivity.this, getResources().getString(R.string.error),
                        t.getMessage() + "");
                layout_loading.setVisibility(View.GONE);

            }
        });
    }

    private int itemSelect = 0;

    public void setupRecyclerCategory() {
        adapter_category = new RecyclerCategory(getApplicationContext(), categories_list, itemSelect, R.layout.row_item_category_horizontal, new OnItemClickTagListener() {
            @Override
            public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                adapter_category.ItemSelect = position;
                adapter_category.notifyDataSetChanged();
                GoToItemActivity(categories_list.get(position));
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.HORIZONTAL, false);
        recycler_view_category.setLayoutManager(linearLayoutManager);
        recycler_view_category.setAdapter(adapter_category);

    }

    private void GoToItemActivity(Category category) {
        GetListItemsWebService(category.getCategoryID() ,category.getCategoryName());
    }

    public void setupRecycler() {
        adapter = new RecyclerItemsOffers(getApplicationContext(), "ListItem", items_list, R.layout.row_item_items_offer, new OnItemClickTagListener() {
            @Override
            public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                Item item = items_list.get(position);
                if (TextUtils.equals("plus", tag)) {
                    if (item.getQuantity() < 20) {
                        item.setQuantity(item.getQuantity() + 1);
                        InsertUpdateDeleteItem.upsert_Item(db, item);
                    }

                } else if (TextUtils.equals("add", tag)) {
                    if (item.getQuantity() == 0) {
                        item.setQuantity(item.getQuantity() + 1);
                        InsertUpdateDeleteItem.upsert_Item(db, item);
                    }

                } else if (TextUtils.equals("minus", tag)) {
                    if (item.getQuantity() > 0) {
                        item.setQuantity(item.getQuantity() - 1);
                        if (item.getQuantity() == 0) {
                            if (item.getItemServices().size() > 0) {
                                item.setItemServices(new ArrayList<>());
                                InsertUpdateDeleteItem.deleteByItemService(db, item.getCode());
                            }
                        }
                        InsertUpdateDeleteItem.upsert_Item(db, item);
                    }
                } else if (TextUtils.equals("service", tag)) {
                    if (item.getQuantity() == 0) {
                        Toasty.warning(getApplicationContext(), getResources().getString(R.string.add_to_cart_after_service), Toast.LENGTH_SHORT, true).show();
                    } else {
                        GetItemServiceByItemCodeWebService(item);
                    }
                }
                adapter.notifyDataSetChanged();
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setAdapter(adapter);

    }

    private void GetItemServiceByItemCodeWebService(Item item) {
        layout_loading.setVisibility(View.VISIBLE);

        RetrofitWebService.getService(this).GetItemServiceByCodeItem(item.getCode() + "").enqueue(new Callback<ItemsServiceResponse>() {
            @Override
            public void onResponse(Call<ItemsServiceResponse> call, Response<ItemsServiceResponse> response) {
                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (response.body() != null) {
                        List<ItemService> itemServices = response.body().getItemServices();
                        if (itemServices != null) {
                            if (itemServices.size() > 0) {
                                itemServicesListDataBase = db.itemServiceDao().getAll();
                                for (int i = 0; i < itemServices.size(); i++) {
                                    Log.e("itemSerase", itemServices.get(i).getServiceCode());
                                    for (int x = 0; x < itemServicesListDataBase.size(); x++) {
                                        Log.e("itemServiceataBase0", itemServicesListDataBase.get(x).getItemCode() + "==" + item.getCode());
                                        if (TextUtils.equals(itemServicesListDataBase.get(x).getItemCode(), item.getCode())) {
                                            Log.e("itemSerase", itemServicesListDataBase.get(x).getServiceCode() + " == " + itemServices.get(i).getServiceCode());
                                            if (TextUtils.equals(itemServicesListDataBase.get(x).getServiceCode(), itemServices.get(i).getServiceCode()))
                                                if (TextUtils.equals(itemServicesListDataBase.get(x).getServiceCode(), itemServices.get(i).getServiceCode())) {
                                                    itemServices.get(i).setIs_selected(true);
                                                }
                                        }
                                    }
                                }
                                ShowDialogServiceItem(itemServices, item);
                            } else {
                                AppErrorsManager.showCustomErrorDialog(ListOffersActivity.this, getResources().getString(R.string.info),
                                        response.body().getMessage());
                            }
                        } else {
                            AppErrorsManager.showCustomErrorDialog(ListOffersActivity.this, getResources().getString(R.string.error),
                                    getResources().getString(R.string.notHaveService));
                        }
                    }
                } else {
                    AppErrorsManager.showCustomErrorDialog(ListOffersActivity.this, getResources().getString(R.string.Failure),
                            getResources().getString(R.string.InternalServerError));
                }


                if (response.errorBody() != null) {
                    try {
                        Log.e("GetItemServiceByCode", " error code: " + response.code() + " error content: " + response.errorBody().string().toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                layout_loading.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ItemsServiceResponse> call, Throwable t) {
                Log.e("GetItemServiceByCode", t.toString());
                AppErrorsManager.showCustomErrorDialog(ListOffersActivity.this, getResources().getString(R.string.Failure),
                        t.getMessage() + "");
                layout_loading.setVisibility(View.GONE);
            }
        });

    }

    RecyclerServiceDialog adapter_service;

    private void ShowDialogServiceItem(List<ItemService> itemServices, Item item) {

        adapter_service = new RecyclerServiceDialog(this, itemServices, R.layout.row_item_service, new OnItemClickTagListener() {
            @Override
            public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (TextUtils.equals("select", tag)) {
                            ItemService itemService = itemServices.get(position);
                            if (itemService.isIs_selected()) {
                                itemService.setIs_selected(false);
                            } else {
                                itemService.setIs_selected(true);
                            }
                            List<ItemService> itemServicenewList = new ArrayList<>();

                            for (int i = 0; i < itemServices.size(); i++) {
                                if (itemServices.get(i).isIs_selected()) {
                                    itemServices.get(i).setItemCode(item.getCode());
                                    itemServicenewList.add(itemServices.get(i));
                                }
                            }

                            InsertUpdateDeleteItem.deleteByItemService(db, item.getCode());

                            for (int i = 0; i < items_list.size(); i++) {
                                if (items_list.get(i).equals(item)) {
                                    items_list.get(i).setItemServices(itemServicenewList);
                                    if (item.getQuantity() > 0) {
                                        InsertUpdateDeleteItem.upsert_ItemService(db, itemServicenewList);
                                    }/** else {
                                     InsertUpdateDeleteItem.deleteByItemService(db, item.getCode());
                                     }*/
                                }
                            }

                            adapter_service.notifyDataSetChanged();
                            adapter.notifyDataSetChanged();
                        }
                    }
                });


            }
        });

        AppErrorsManager.showAddressListDialog(ListOffersActivity.this, adapter_service, new InstallCallback() {
            @Override
            public void onStatusDone(String status) {
                if (TextUtils.equals("yes", status)) {
                }

            }
        });

    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                initSetup();
            }
        }, 2000);
    }


    private AppMp3Manager appMp3Manager;
    private BroadcastReceiver mHander = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String title = intent.getStringExtra("title");
            String body = intent.getStringExtra("body");
            appMp3Manager = new AppMp3Manager(ListOffersActivity.this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    AppErrorsManager.showCustomErrorDialogTop(ListOffersActivity.this, title, body, new InstallCallback() {
                        @Override
                        public void onStatusDone(String status) {

                        }
                    });
                    appMp3Manager.palyNotificationMusic(R.raw.bell_sound);
                }
            }, 1000);
        }
    };


    @Override
    protected void onRestart() {
        setUpDataBase();
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        super.onRestart();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHander);
        super.onPause();
    }
}
