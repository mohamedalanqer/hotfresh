package com.app.hotfresh.Ui.Adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.hotfresh.CallBack.OnItemClickTagListener;
import com.app.hotfresh.Manager.DrawableRound;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Manager.RootManager;
import com.app.hotfresh.Medoles.Category;
import com.app.hotfresh.Medoles.Order;
import com.app.hotfresh.R;
import com.bumptech.glide.Glide;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.List;


public class RecyclerOrder extends RecyclerView.Adapter<RecyclerOrder.CustomView> {
    Context context;
    List<Order> mylist;
    private OnItemClickTagListener listener;
    int Layout;
    public static int ItemSelect = -1;


    public class CustomView extends RecyclerView.ViewHolder {
        TextView row_date, row_name, row_text_price, text_status;
        CardView card_view;
        LinearLayout layout_status;
        ImageView img_status;
        RelativeLayout layout_data;
        TextView text_details, text_invoicen_umber;
        TextView row_text_pay;

        public CustomView(View v) {
            super(v);
            FontManager.applyFont(context, v);
            row_name = v.findViewById(R.id.row_name);
            row_date = v.findViewById(R.id.row_date);
            row_text_price = v.findViewById(R.id.row_text_price);
            card_view = v.findViewById(R.id.card_view);
            layout_status = v.findViewById(R.id.layout_status);
            img_status = v.findViewById(R.id.img_status);
            text_status = v.findViewById(R.id.text_status);
            layout_data = v.findViewById(R.id.layout_data);
            text_details = v.findViewById(R.id.text_details);
            text_invoicen_umber = v.findViewById(R.id.text_invoicen_umber);
            row_text_pay = v.findViewById(R.id.row_text_pay);
        }

    }

    public RecyclerOrder() {
    }

    public RecyclerOrder(Context context, List<Order> mylist, int ItemSelect, int Layout, OnItemClickTagListener listener) {
        this.context = context;
        this.mylist = mylist;
        this.listener = listener;
        this.Layout = Layout;
        this.ItemSelect = ItemSelect;
    }

    @Override
    public CustomView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(Layout, parent, false);
        CustomView viewholder = new CustomView(view);
        return viewholder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final CustomView holder, final int position) {
        holder.row_name.setText(mylist.get(position).getBillCode() + "");
        holder.row_date.setText(mylist.get(position).getBillDate() + "");
        holder.row_text_price.setText(mylist.get(position).getTotal() + " " + context.getResources().getString(R.string.SAR));
        holder.text_invoicen_umber.setText("" + mylist.get(position).getBillCode());
        holder.row_text_pay.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(mylist.get(position).getPayStatus())) {
            if (TextUtils.equals(RootManager.Stats_certain, mylist.get(position).getStatus()) ||
                    TextUtils.equals(RootManager.Stats_updated, mylist.get(position).getStatus())) {
                if (!TextUtils.equals("True", mylist.get(position).getPayStatus())) {
                    holder.row_text_pay.setVisibility(View.VISIBLE);
                }
            }
        }
        holder.row_text_pay.setOnClickListener(view -> {
            try {
                listener.onItemClick(holder.img_status, 0, "view");
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        });
        if (ItemSelect == position) {
            holder.card_view.setCardBackgroundColor(context.getResources().getColor(R.color.colorWhite));
            //holder.layout_data.setBackgroundColor(context.getResources().getColor(R.color.colorGreyLight));
            holder.row_name.setTextColor(context.getResources().getColor(R.color.colorWhite));
            holder.layout_status.setVisibility(View.VISIBLE);
            SetStatusView(mylist.get(position).getStatus(), holder.img_status, holder.text_status);
        } else {
            // holder.layout_data.setBackgroundColor(context.getResources().getColor(R.color.colorWhite));
            holder.card_view.setCardBackgroundColor(context.getResources().getColor(R.color.colorWhite));
            holder.row_name.setTextColor(context.getResources().getColor(R.color.colorBlack));
            holder.layout_status.setVisibility(View.GONE);
        }

        if (TextUtils.equals(RootManager.Stats_OnWay, mylist.get(position).getStatus())) {
            holder.text_status.setTextColor(context.getResources().getColor(R.color.colorRed));
            int hight = DrawableRound.dpToPx(300);
            holder.img_status.setLayoutParams(new LinearLayout.LayoutParams(hight, hight));
            Glide.with(context).load(R.drawable.animation_delvey)
                    .into(holder.img_status);

            try {
                listener.onItemClick(holder.img_status, 0, "onway");
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    listener.onItemClick(view, position, "open");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

        holder.text_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    listener.onItemClick(view, position, "view");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void SetStatusView(String status, ImageView img_status, TextView text_status) {
        String message = RootManager.GetMessageStatusByTagName(status, context, img_status);
        text_status.setText("" + message);

        /** if (TextUtils.equals("Ready", status)) {
         img_status.setImageResource(R.drawable.img_status_1);
         } else {
         img_status.setImageResource(R.drawable.img_status_2);
         text_status.setText("" + message);
         }*/
    }


    @Override
    public int getItemCount() {
        return mylist.size();
    }


}

