package com.app.hotfresh.Ui.Activites;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.hotfresh.Manager.AppLanguage;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.R;
import com.app.hotfresh.Ui.Fragments.LoginFragment;
import com.app.hotfresh.Ui.Fragments.RegisterFragment;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.gson.Gson;

public class UserActionsActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView text_login, text_line_login, text_register, text_line_register;
    private CardView card_view ;
    private Button btn_action , btn_visitor ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppLanguage.setContentLang(this);
        setContentView(R.layout.activity_user_actions);
        FontManager.applyFont(this, findViewById(R.id.layout));

        initSetUp();

    }

    private void initSetUp() {
        text_login = findViewById(R.id.text_login);
        text_line_login = findViewById(R.id.text_line_login);
        text_register = findViewById(R.id.text_register);
        text_line_register = findViewById(R.id.text_line_register);
        btn_action = findViewById(R.id.btn_action);
        card_view = findViewById(R.id.card_view);
        btn_visitor = findViewById(R.id.btn_visitor);
        btn_visitor.setOnClickListener(this::onClick);
        text_login.setOnClickListener(this::onClick);
        text_register.setOnClickListener(this::onClick);
        btn_action.setOnClickListener(this::onClick);
        openFragment(new LoginFragment());


    }

    public String toJson(Object object) {
        return new Gson().toJson(object);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.text_login:
                toogleLoginRegister(text_login, text_register);
                break;
            case R.id.text_register:
                toogleLoginRegister(text_register, text_login);

                break;

            case R.id.btn_visitor:
                Intent intent =new Intent(getApplicationContext(),HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();

                break;


        }
    }

    private void toogleLoginRegister(TextView text_click, TextView text_unckick) {
        if (TextUtils.equals("0", text_click.getTag().toString())) {
            text_click.setTextColor(getResources().getColor(R.color.colorWhite));
            text_click.setTag("1");
            text_unckick.setTextColor(getResources().getColor(R.color.colorBlack));
            text_unckick.setTag("0");

            if (TextUtils.equals("1", text_login.getTag().toString())) {
                btn_action.setText(getResources().getString(R.string.login));
                openFragment(new LoginFragment());
                text_line_login.setVisibility(View.VISIBLE);
                btn_visitor.setVisibility(View.VISIBLE);
                text_line_register.setVisibility(View.INVISIBLE);
                YoYo.with(Techniques.BounceInLeft)
                        .duration(700)
                        .playOn(card_view);
            } else {
                btn_action.setText(getResources().getString(R.string.register));
                openFragment(new RegisterFragment());
                text_line_login.setVisibility(View.INVISIBLE);
                btn_visitor.setVisibility(View.GONE);
                text_line_register.setVisibility(View.VISIBLE);
                YoYo.with(Techniques.BounceInRight)
                        .duration(700)
                        .playOn(card_view);
            }


        }
    }

    public void openFragment(Fragment fragment) {
        FragmentManager manager = getSupportFragmentManager();
        manager.beginTransaction().replace(R.id.fragment, fragment).commit();

    }
    @Override
    public void applyOverrideConfiguration(Configuration overrideConfiguration) {
        if (overrideConfiguration != null) {
            int uiMode = overrideConfiguration.uiMode;
            overrideConfiguration.setTo(getBaseContext().getResources().getConfiguration());
            overrideConfiguration.uiMode = uiMode;
        }
        super.applyOverrideConfiguration(overrideConfiguration);
    }

    @Override
    public void onBackPressed() {
        /** if (TextUtils.equals(tag, getResources().getString(R.string.create_subscription))) {
         Intent intent = new Intent(UserActionsActivity.this, HomeProviderActivity.class);

         intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
         startActivity(intent);
         ActivityCompat.finishAffinity(UserActionsActivity.this); //with v4 support library
         } else {*/
        super.onBackPressed();
        // }

    }
}
