package com.app.hotfresh.Ui.Adapters;

import android.content.Context;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.PointerIcon;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.app.hotfresh.CallBack.OnItemClickListener;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Medoles.Item;
import com.app.hotfresh.Medoles.PointsGroups.PointsGroups;
import com.app.hotfresh.R;
import com.bumptech.glide.Glide;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.List;


public class AdapterPackages extends PagerAdapter {
    private List<PointsGroups> mylist;
    Context mContext;
    private LayoutInflater mInflater;
    private OnItemClickListener onItemClickListener;

    public AdapterPackages(Context context, List<PointsGroups> mylist, OnItemClickListener onItemClickListener) {
        this.mylist = mylist;
        mContext = context;
        this.onItemClickListener = onItemClickListener;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public void destroyItem(View container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }

    @Override
    public int getCount() {
        return mylist.size();
    }


    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View itemView = mInflater.inflate(R.layout.row_item_package, view, false);
        FontManager.applyFont(mContext, itemView);
        TextView row_name = itemView.findViewById(R.id.row_name);
        ImageView img_item = itemView.findViewById(R.id.img_item);
        TextView text_body = itemView.findViewById(R.id.text_body);
        TextView text_count = itemView.findViewById(R.id.text_count);
        TextView text_my_package = itemView.findViewById(R.id.text_my_package);
        CardView   card_view = itemView.findViewById(R.id.card_view);
        ImageView   img_drop = itemView.findViewById(R.id.img_drop);
        ImageView   img_drop_2 = itemView.findViewById(R.id.img_drop_2);
        ImageView   img_drop_3 = itemView.findViewById(R.id.img_drop_3);
        ImageView   img_drop_4 = itemView.findViewById(R.id.img_drop_4);



    text_my_package.setVisibility(View.GONE);
        if (mylist.get(position).isSelectGroup()) {
            text_my_package.setVisibility(View.VISIBLE);
            card_view.setCardBackgroundColor(mContext.getResources().getColor(R.color.colortest));
            img_drop.setColorFilter(mContext.getResources().getColor(R.color.colortest));
            img_drop_2.setColorFilter(mContext.getResources().getColor(R.color.colortest));
            img_drop_3.setColorFilter(mContext.getResources().getColor(R.color.colortest));
            img_drop_4.setColorFilter(mContext.getResources().getColor(R.color.colortest));
        }else {

            card_view.setCardBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
            img_drop.setColorFilter(mContext.getResources().getColor(R.color.colorPrimaryDark));
            img_drop_2.setColorFilter(mContext.getResources().getColor(R.color.colorPrimaryDark));
            img_drop_3.setColorFilter(mContext.getResources().getColor(R.color.colorPrimaryDark));
            img_drop_4.setColorFilter(mContext.getResources().getColor(R.color.colorPrimaryDark));
        }
        row_name.setText(mylist.get(position).getGroupName() + "");
        text_count.setText(" 1 " + mContext.getResources().getString(R.string.SAR) + " = " + mylist.get(position).getConvertPointsRate() +
                " " + mContext.getResources().getString(R.string.Point)
        );

        text_body.setText(mContext.getResources().getString(R.string.On_every_one) + "  " + mylist.get(position).getConvertPointsRate()
                + "  " + mContext.getResources().getString(R.string.On_every_one2));
        text_body.setVisibility(View.VISIBLE);


        String image = mylist.get(position).getGroupImage();
        if (image != null) {
            Glide.with(mContext).load("" + image).into(img_item);
        }

        view.addView(itemView);
        return itemView;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

}