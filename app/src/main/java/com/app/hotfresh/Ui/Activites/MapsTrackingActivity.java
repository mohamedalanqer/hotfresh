package com.app.hotfresh.Ui.Activites;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.app.hotfresh.DirectionHelpers.FetchURL;
import com.app.hotfresh.DirectionHelpers.LatLongHistory;
import com.app.hotfresh.DirectionHelpers.ModelDeleveryFB;
import com.app.hotfresh.DirectionHelpers.TaskLoadedCallback;
import com.app.hotfresh.Manager.AppPreferences;
import com.app.hotfresh.Medoles.Install.Branch;
import com.app.hotfresh.Medoles.Order;
import com.app.hotfresh.R;
import com.app.hotfresh.WebService.model.response.OrderResponse;
import com.bumptech.glide.Glide;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapsTrackingActivity extends FragmentActivity implements OnMapReadyCallback, TaskLoadedCallback {
    FirebaseFirestore db;
    CollectionReference ref;
    Map<String, String> mapHash = new HashMap<>();
    private GoogleMap mMap;
    private Location mLocation;
    private double latitudeKitchen, longitudeKitchen;
    private double longitudeOrder, latitudeOrder;
    private double Mylatitude, Mylongitude;
    private FusedLocationProviderClient fusedLocationProviderClient = null;
    private MarkerOptions Kitchen = null, Customer = null;
    private Polyline currentPolyline;
    static final int REQUST_CODE = 1000;
    int zoomMap = 17;
    OrderResponse order;

    String NameKichen = "Kitchen";
    String NameCustomer = "Customer";
    LocationManager locationManager;
    String provider;


    String orderid;
    TextView text_name_user, text_delivery_date, text_address, text_kitchenName;
    TextView text_price_total, text_mobile, text_status, text_duratoion, text_ID;
    ImageView image_delivery;
    Button btn_action;

    private String getCollectionName() {
        return orderid + ""; //1351663 +
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_map_content);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        registerForLocationUpdates();

        setUpView();
    }

    Marker mCurrLocationMarker;

    private void setOrderToView(OrderResponse order) {

        NameKichen = order.getCompany();
        NameCustomer = order.getCustName();
        text_name_user.setText(order.getCustName() + "");
        text_price_total.setText("(" + getResources().getString(R.string.total) + "  " + order.getTotal() + " " + getResources().getString(R.string.SAR) + ")");
        text_kitchenName.setText("" + NameKichen);
        text_delivery_date.setText("" + order.getReceivedDate());
        if (order.getReceivedTime() != null) {
            if (!TextUtils.isEmpty(order.getReceivedTime()))
                text_delivery_date.setText(order.getReceivedDate() + " " + order.getReceivedTime());
        }
        if (order.getReceivedTime() != null) {
            if (!TextUtils.isEmpty(order.getCustAddress()))
                text_address.setText(" " + order.getCustAddress());
        }

        if (order.getDeliveryStatus() != null) {
            if (!TextUtils.isEmpty(order.getDeliveryStatus())) {
                text_status.setText("" + order.getDeliveryStatus());
            }
        }

        text_ID.setText("#" + order.getBillCode());
        text_mobile.setText("");
        if (!TextUtils.isEmpty(order.getCompany())) {
            Branch branch = GetBranchByNameCompany(order.getCompany());
            if (branch != null) {
                if (branch.getLat() != null)
                    latitudeKitchen = Double.parseDouble(branch.getLat());
                if (branch.getLong() != null)
                    longitudeKitchen = Double.parseDouble(branch.getLong());
                if (branch.getName() != null) {
                    text_kitchenName.setText("" + branch.getName() + "  ");
                    NameKichen = branch.getName();
                }
            }

        }
        if (!TextUtils.isEmpty(order.getLat()))
            longitudeOrder = Double.parseDouble(order.getLong());

        if (!TextUtils.isEmpty(order.getLong()))
            latitudeOrder = Double.parseDouble(order.getLat());

        orderid = order.getBillCode();
        Mylatitude = latitudeKitchen;
        Mylongitude = longitudeKitchen;


        //Glide.with(this).load(order.getCustAddress()).into(image_delivery);
        text_mobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!TextUtils.isEmpty(text_mobile.getText().toString())) {
                    Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", text_mobile.getText().toString(), null));
                    startActivity(intent);

                }
            }
        });


        Kitchen = new MarkerOptions().position(new LatLng(latitudeKitchen, longitudeKitchen)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_kitchin_map)).title(NameKichen);
        Customer = new MarkerOptions().position(new LatLng(latitudeOrder, longitudeOrder)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_user_map)).title(NameCustomer);
        new FetchURL(MapsTrackingActivity.this).execute(getUrl(Kitchen.getPosition(), Customer.getPosition(), "driving"), "driving");
        if (mMap != null) {
            Marker marker = mMap.addMarker(Kitchen);
            Marker marker2 = mMap.addMarker(Customer);
            marker.setTag(order.getCompany());
            marker2.setTag(order.getCustName());
        }


        SetUpBottomSheet();
        db = FirebaseFirestore.getInstance();
        ref = db.collection("Delevery::").document(getCollectionName()).collection(getCollectionName());
        ref.whereGreaterThanOrEqualTo("OrderId", getCollectionName())
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                        if (documentSnapshots.size() == 0) {

                        } else {
                            List<ModelDeleveryFB> data = documentSnapshots.toObjects(ModelDeleveryFB.class);
                            for (int i = 0; i < data.size(); i++) {
                                if (data.get(i).getDrationAndDistance() != null) {
                                    text_duratoion.setText("" + data.get(i).getDrationAndDistance());
                                }

                                String DeliveryLate = data.get(i).getDeliveryLate();
                                String DeliveryLang = data.get(i).getDeliveryLang();
                                String userId = data.get(i).getUserId();
                                String userName = data.get(i).getUserName();
                                String DeliveryId = data.get(i).getDeliveryId();
                                String DeliveryName = data.get(i).getDeliveryName();
                                String DeliveryPic = data.get(i).getDeliveryPic();
                                String DeliveryMobile = data.get(i).getDeliveryMobile();
                                String OrderId = data.get(i).getOrderId();
                                String Status = data.get(i).getStatus();
                                String DrationAndDistance = data.get(i).getDrationAndDistance();
                                String Distance = data.get(i).getDistance();
                                String Dration = data.get(i).getDration();


                                if (!TextUtils.isEmpty(DeliveryName)) {
                                    text_name_user.setText("" + DeliveryName);
                                }
                                if (!TextUtils.isEmpty(DeliveryMobile)) {
                                    String numberMobile = DeliveryMobile;
                                    if (DeliveryMobile.length() > 9) {
                                        if (DeliveryMobile.startsWith("00966")) {
                                            numberMobile = DeliveryMobile.replace("00966", "");
                                        } else if (DeliveryMobile.startsWith("966")) {
                                            numberMobile = DeliveryMobile.replace("966", "");
                                        } else if (DeliveryMobile.startsWith("+966")) {
                                            numberMobile = DeliveryMobile.replace("+966", "");

                                        }
                                    }
                                    text_mobile.setText("" + numberMobile);
                                }

                                if (!TextUtils.isEmpty(DeliveryPic)) {
                                    Glide.with(MapsTrackingActivity.this).load(DeliveryPic).into(image_delivery);
                                }


                                if (data.get(i).getTrackingList() != null) {
                                    if (data.get(i).getTrackingList().size() > 0) {
                                        List<LatLongHistory> latLngListTraking = data.get(i).getTrackingList();
                                        PolyLineHistory(latLngListTraking);
                                    }
                                }


                                if (mMap != null) {
                                    Mylatitude = Double.parseDouble(DeliveryLang);
                                    Mylongitude = Double.parseDouble(DeliveryLate);
                                    if (mCurrLocationMarker != null) {
                                        mCurrLocationMarker.remove();
                                    }
                                    LatLng latLng = new LatLng(Mylatitude, Mylongitude);
                                    MarkerOptions markerOptions = new MarkerOptions();
                                    markerOptions.position(latLng);
                                    markerOptions.title(DeliveryName);
                                    markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_delivery));
                                    mCurrLocationMarker = mMap.addMarker(markerOptions);
                                    //move map camera
                                    //   mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomMap));
                                    CameraUpdate center =
                                            CameraUpdateFactory.newLatLng(latLng);
                                    CameraUpdate zoom = CameraUpdateFactory.zoomTo(zoomMap);
                                    mMap.moveCamera(center);
                                    mMap.animateCamera(zoom);

                                    if (Dration != null)
                                        text_duratoion.setText("" + Dration + " " + getResources().getString(R.string.min) + "| " + Distance + " " + getResources().getString(R.string.KM));

                                    text_status.setText("" + Status);
                                    if (TextUtils.equals(getResources().getString(R.string.status0), Status)) {
                                        text_status.setText("" + getResources().getString(R.string.status00));
                                    } else if (TextUtils.equals(getResources().getString(R.string.status1), Status)) {
                                        text_status.setText("" + getResources().getString(R.string.status11));
                                    } else if (TextUtils.equals(getResources().getString(R.string.status2), Status)) {
                                        text_status.setText("" + getResources().getString(R.string.status22));
                                    }

                                }
                            }
                        }


                        /**      for (DocumentChange documentChange : documentSnapshots.getDocumentChanges()) {
                         String DeliveryLate = documentChange.getDocument().getString("DeliveryLate");
                         String DeliveryLang = documentChange.getDocument().getString("DeliveryLang");
                         String userId = documentChange.getDocument().getString("userId");
                         String userName = documentChange.getDocument().getString("userName");
                         String DeliveryId = documentChange.getDocument().getString("DeliveryId");
                         String DeliveryName = documentChange.getDocument().getString("DeliveryName");
                         String DeliveryPic = documentChange.getDocument().getString("DeliveryPic");
                         String DeliveryMobile = documentChange.getDocument().getString("DeliveryMobile");
                         String OrderId = documentChange.getDocument().getString("OrderId");
                         String Status = documentChange.getDocument().getString("Status");
                         String DrationAndDistance = documentChange.getDocument().getString("DrationAndDistance");
                         String Distance = documentChange.getDocument().getString("Distance");
                         String Dration = documentChange.getDocument().getString("Dration");


                         if (!TextUtils.isEmpty(DeliveryName)) {
                         text_name_user.setText("" + DeliveryName);
                         }
                         if (!TextUtils.isEmpty(DeliveryMobile)) {
                         text_mobile.setText("" + DeliveryMobile);
                         }

                         if (!TextUtils.isEmpty(DeliveryPic)) {
                         Glide.with(MapsTrackingActivity.this).load(DeliveryPic).into(image_delivery);
                         }
                         /**     if(documentChange.getDocument().getString("TrackingList") != null) {
                         GeoPoint TrackingList = documentChange.getDocument().get("TrackingList");
                         if(TrackingList != null){
                         if(TrackingList.s)
                         }

                         }
                         */

/**
 if (mMap != null) {
 Mylatitude = Double.parseDouble(DeliveryLang);
 Mylongitude = Double.parseDouble(DeliveryLate);
 if (mCurrLocationMarker != null) {
 mCurrLocationMarker.remove();
 }
 LatLng latLng = new LatLng(Mylatitude, Mylongitude);
 MarkerOptions markerOptions = new MarkerOptions();
 markerOptions.position(latLng);
 markerOptions.title(DeliveryName);
 markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_delivery));
 mCurrLocationMarker = mMap.addMarker(markerOptions);

 //move map camera
 //   mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomMap));
 CameraUpdate center =
 CameraUpdateFactory.newLatLng(latLng);
 CameraUpdate zoom = CameraUpdateFactory.zoomTo(zoomMap);
 mMap.moveCamera(center);
 mMap.animateCamera(zoom);

 if (Dration != null)
 text_duratoion.setText("" + Dration + " " + getResources().getString(R.string.min) + "| " + Distance + " " + getResources().getString(R.string.KM));

 text_status.setText("" + Status);
 if (TextUtils.equals(getResources().getString(R.string.status0), Status)) {
 text_status.setText("" + getResources().getString(R.string.status00));
 } else if (TextUtils.equals(getResources().getString(R.string.status1), Status)) {
 text_status.setText("" + getResources().getString(R.string.status11));
 } else if (TextUtils.equals(getResources().getString(R.string.status2), Status)) {
 text_status.setText("" + getResources().getString(R.string.status22));
 }

 }

 }
 */


                    }
                });


    }

    private void PolyLineHistory(List<LatLongHistory> result) {
        ArrayList<LatLng> points;
        PolylineOptions lineOptions = null;
        for (int i = 0; i < result.size(); i++) {
            points = new ArrayList<>();
            lineOptions = new PolylineOptions();
            // Fetching all the points in i-th route
            for (int j = 0; j < result.size(); j++) {
                LatLongHistory point = result.get(j);
                double lat = point.getLatitude();
                double lng = point.getLongitude();
                LatLng position = new LatLng(lat, lng);
                points.add(position);
            }
            // Adding all the points in the route to LineOptions
            lineOptions.addAll(points);
            lineOptions.width(20);
            lineOptions.color(getResources().getColor(R.color.colortest));
        }

        // Drawing polyline in the Google Map for the i-th route
        if (lineOptions != null) {
            mMap.addPolyline(lineOptions);
        }
    }

    private Branch GetBranchByNameCompany(String company) {
        List<Branch> branchList = new ArrayList<>();
        Branch branch = null;
        String branchesJson = AppPreferences.getString(MapsTrackingActivity.this, "branchesJson");

        if (branchesJson != null) {
            branchList = new Gson().fromJson(branchesJson, new TypeToken<List<Branch>>() {
            }.getType());
        }

        for (int i = 0; i < branchList.size(); i++) {
            if (TextUtils.equals(company, branchList.get(i).getCompany())) {
                branch = branchList.get(i);
                return branch;
            }
        }
        return branch;
    }

    private void setUpView() {
        text_name_user = findViewById(R.id.text_name_user);
        text_delivery_date = findViewById(R.id.text_delivery_date);
        text_address = findViewById(R.id.text_address);
        text_kitchenName = findViewById(R.id.text_kitchenName);
        text_price_total = findViewById(R.id.text_price_total);
        text_mobile = findViewById(R.id.text_mobile);
        text_status = findViewById(R.id.text_status);
        image_delivery = findViewById(R.id.image_delivery);
        text_duratoion = findViewById(R.id.text_duratoion);
        btn_action = findViewById(R.id.btn_action);
        text_ID = findViewById(R.id.text_ID);
        String str_order = getIntent().getStringExtra("order_str");
        if (!TextUtils.isEmpty(str_order)) {
            order = new Gson().fromJson(str_order, OrderResponse.class);
            setOrderToView(order);

        }
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        /**   Kitchen = new MarkerOptions().position(new LatLng(latitudeKitchen, longitudeKitchen)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_kitchin_map)).title(NameKichen);
         Customer = new MarkerOptions().position(new LatLng(latitudeOrder, longitudeOrder)).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_user_map)).title(NameCustomer);
         new FetchURL(MapsTrackingActivity.this).execute(getUrl(Kitchen.getPosition(), Customer.getPosition(), "driving"), "driving");
         */


    }


    BottomSheetBehavior bottomSheetBehavior;

    private void SetUpBottomSheet() {
        LinearLayout llBottomSheet = findViewById(R.id.bottom_sheet);
        bottomSheetBehavior = BottomSheetBehavior.from(llBottomSheet);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        bottomSheetBehavior.setPeekHeight(60);
        bottomSheetBehavior.setHideable(true);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }


    @SuppressLint("MissingPermission")
    void registerForLocationUpdates() {
        FusedLocationProviderClient locationProviderClient = getFusedLocationProviderClient();
        LocationRequest locationRequest = LocationRequest.create();
        Looper looper = Looper.myLooper();
        locationProviderClient.requestLocationUpdates(locationRequest, locationCallback, looper);
    }

    @NonNull
    private FusedLocationProviderClient getFusedLocationProviderClient() {
        if (fusedLocationProviderClient == null) {
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        }
        return fusedLocationProviderClient;
    }

    private LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);
            Location lastLocation = locationResult.getLastLocation();

            Log.e("locationdata", lastLocation.getLatitude() + " // " + lastLocation.getLongitude());
        }
    };


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        mMap = googleMap;

        googleMap.getUiSettings().setMapToolbarEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);


        if (mMap != null) {
            LatLng latLng = new LatLng(Mylatitude, Mylongitude);
            //move map camera
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomMap));
            mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                @Override
                public void onCameraIdle() {
                    Log.e("zoom", mMap.getCameraPosition().zoom + "");
                }
            });
            Marker marker = mMap.addMarker(Kitchen);
            Marker marker2 = mMap.addMarker(Customer);
            marker.setTag("Kitchen");
            marker2.setTag("Customer");


            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                // Use default InfoWindow frame
                @Override
                public View getInfoWindow(Marker args) {
                    return null;
                }

                // Defines the contents of the InfoWindow
                @Override
                public View getInfoContents(Marker args) {
                    View v = getLayoutInflater().inflate(R.layout.info_window_layout, null);
                    TextView title = v.findViewById(R.id.tvTitle);
                    title.setText(args.getTitle());
                    mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                        public void onInfoWindowClick(Marker marker) {
                            OnclickMarkerIcon(marker);


                        }
                    });

                    return v;

                }
            });

        }
    }


    private String getUrl(LatLng origin, LatLng dest, String directionMode) {
        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        // Mode
        String mode = "mode=" + directionMode;
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + mode;
        // Output format
        String output = "json";
        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + getString(R.string.google_maps_key2);
        return url;
    }

    @Override
    public void onTaskDone(Object... values) {
        if (currentPolyline != null)
            currentPolyline.remove();
        currentPolyline = mMap.addPolyline((PolylineOptions) values[0]);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                }
                break;
        }
    }

    private void OnclickMarkerIcon(Marker marker) {

        if (marker.getTag() != null) {
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

        }
    }


}




