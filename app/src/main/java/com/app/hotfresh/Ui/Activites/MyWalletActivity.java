package com.app.hotfresh.Ui.Activites;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.app.hotfresh.CallBack.InstallCallback;
import com.app.hotfresh.CallBack.InstallTwoStringCallback;
import com.app.hotfresh.CallBack.InternetAvailableCallback;
import com.app.hotfresh.Manager.AppErrorsManager;
import com.app.hotfresh.Manager.AppLanguage;
import com.app.hotfresh.Manager.AppMp3Manager;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Manager.InternetConnectionUtils;
import com.app.hotfresh.Manager.RootManager;
import com.app.hotfresh.Manager.getUserDetials;
import com.app.hotfresh.Medoles.User;
import com.app.hotfresh.R;
import com.app.hotfresh.WebService.RetrofitWebService;
import com.app.hotfresh.WebService.model.response.RootResponse;
import com.app.hotfresh.WebService.model.response.WalletResponse;
import com.app.hotfresh.WebService.model.response.checkoutInternelResponse;
import com.app.hotfresh.WebService.model.response.checkoutsResponse;
import com.google.gson.Gson;
import com.oppwa.mobile.connect.checkout.dialog.CheckoutActivity;
import com.oppwa.mobile.connect.checkout.meta.CheckoutSettings;
import com.oppwa.mobile.connect.exception.PaymentError;
import com.oppwa.mobile.connect.provider.Connect;
import com.oppwa.mobile.connect.provider.Transaction;
import com.oppwa.mobile.connect.provider.TransactionType;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Set;

import javax.net.ssl.HttpsURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.hotfresh.Manager.RootManager.ActionKeyLocalBroadcastManager;

public class MyWalletActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    private FrameLayout layout_loading;


    private TextView toolbarNameTxt;
    private RelativeLayout Layout_notification;

    private TextView text_count_cart;
    private ImageView image_count_cart;
    private ImageView img_bar_cart;

    private TextView text_balance;
    private RelativeLayout end_main;
    SwipeRefreshLayout swipeRefreshLayout;


    private Handler handler;
    private String checkoutId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppLanguage.setContentLang(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_my_wallet);
        FontManager.applyFont(this, findViewById(R.id.layout));
        handler = new Handler(Looper.getMainLooper());


        initSetup();

    }

    public String requestCheckoutId() {
        URL url;
        String urlString;
        HttpURLConnection connection = null;
        String checkoutId = null;

        urlString = "https://test.oppwa.com/v1/checkouts" + "?amount=48.99&currency=SAR&paymentType=DB";

        try {
            url = new URL(urlString);
            connection = (HttpURLConnection) url.openConnection();

            JsonReader reader = new JsonReader(
                    new InputStreamReader(connection.getInputStream(), "UTF-8"));

            reader.beginObject();

            while (reader.hasNext()) {
                if (reader.nextName().equals("checkoutId")) {
                    checkoutId = reader.nextString();

                    break;
                }
            }

            reader.endObject();
            reader.close();
        } catch (Exception e) {
            /* error occurred */
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
        }

        return checkoutId;
    }

    private String request() throws IOException {


        URL url = new URL("https://test.oppwa.com/v1/checkouts");
        HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Authorization", "Bearer OGFjN2E0Yzk3MWFhYmYwZTAxNzFiM2QyZTQxOTE0MTl8UFFTZlk4NjN0Tg==");
        conn.setDoInput(true);
        conn.setDoOutput(true);

        String data = ""
                + "entityId=8ac7a4ca71aac89b0171b3d5027d1695"
                + "&amount=78.00"
                + "&currency=SAR"
                + "&paymentType=DB";
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
        wr.writeBytes(data);
        wr.flush();
        wr.close();
        int responseCode = conn.getResponseCode();
        InputStream is;

        if (responseCode >= 400) is = conn.getErrorStream();
        else is = conn.getInputStream();

        return is.toString();
    }

    /**
     * private void GetCheckOutID(String amount) {
     * ammount = amount;
     * layout_loading.setVisibility(View.VISIBLE);
     * RetrofitWebService.getServiceOut().requestCheckoutId(
     * "8ac7a4ca71aac89b0171b3d5027d1695"
     * , amount
     * , "SAR"
     * , "DB").enqueue(new Callback<checkoutsResponse>() {
     *
     * @Override public void onResponse(Call<checkoutsResponse> call, Response<checkoutsResponse> response) {
     * Log.e("requestCheckoutId", response.toString());
     * if (RootManager.RESPONSE_CODE_OK == response.code()) {
     * if (response.body() != null) {
     * checkoutId = response.body().getId();
     * Log.e("requestCheckoutId", response.body().getResult().getDescription() + "    " + checkoutId);
     * <p>
     * configCheckout(checkoutId);
     * }
     * } else {
     * AppErrorsManager.showCustomErrorDialog(MyWalletActivity.this, getResources().getString(R.string.Failure),
     * getResources().getString(R.string.InternalServerError));
     * }
     * <p>
     * <p>
     * if (response.errorBody() != null) {
     * try {
     * Log.e("requestCheckoutId", " error code: " + response.code() + " error content: " + response.errorBody().string().toString());
     * } catch (IOException e) {
     * e.printStackTrace();
     * }
     * }
     * <p>
     * layout_loading.setVisibility(View.GONE);
     * }
     * @Override public void onFailure(Call<checkoutsResponse> call, Throwable t) {
     * <p>
     * <p>
     * layout_loading.setVisibility(View.GONE);
     * InternetConnectionUtils.isInternetAvailable(getApplicationContext(), new InternetAvailableCallback() {
     * @Override public void onInternetAvailable(boolean isAvailable) {
     * handler.post(new Runnable() {
     * @Override public void run() {
     * if (isAvailable) {
     * AppErrorsManager.showCustomErrorDialog(MyWalletActivity.this, getResources().getString(R.string.Failure),
     * t.getMessage() + "");
     * <p>
     * } else {
     * AppErrorsManager.InternetUnAvailableDialog(MyWalletActivity.this, getResources().getString(R.string.Failure),
     * getResources().getString(R.string.nointernetconnection), new InstallCallback() {
     * @Override public void onStatusDone(String status) {
     * if (TextUtils.equals("retry", status)) {
     * GetCheckOutID(amount);
     * }
     * }
     * });
     * }
     * }
     * });
     * }
     * });
     * }
     * });
     * }
     */
    private void GetCheckOutID(String amount, String typePay) {
        ammount = amount;
        layout_loading.setVisibility(View.VISIBLE);
        RetrofitWebService.getServiceOut().requestCheckoutId(
                user.getAccessToken()
                , amount
                , "" ,typePay).enqueue(new Callback<checkoutInternelResponse>() {
            @Override
            public void onResponse(Call<checkoutInternelResponse> call, Response<checkoutInternelResponse> response) {
                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (response.body() != null) {
                        if (TextUtils.equals("True", response.body().getStatus())) {
                            checkoutId = response.body().getId();
                            Log.e("requestCheckoutId", response.body().getId() + "    " + checkoutId);
                            configCheckout(checkoutId, typePay);
                        } else if (TextUtils.equals("False", response.body().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(MyWalletActivity.this, getResources().getString(R.string.error),
                                    response.body().getMessage() + "");
                        }
                    }
                } else {
                    AppErrorsManager.showCustomErrorDialog(MyWalletActivity.this, getResources().getString(R.string.Failure),
                            getResources().getString(R.string.InternalServerError));
                }
                layout_loading.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<checkoutInternelResponse> call, Throwable t) {


                layout_loading.setVisibility(View.GONE);
                InternetConnectionUtils.isInternetAvailable(getApplicationContext(), new InternetAvailableCallback() {
                    @Override
                    public void onInternetAvailable(boolean isAvailable) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (isAvailable) {
                                    AppErrorsManager.showCustomErrorDialog(MyWalletActivity.this, getResources().getString(R.string.Failure),
                                            t.getMessage() + "");

                                } else {
                                    AppErrorsManager.InternetUnAvailableDialog(MyWalletActivity.this, getResources().getString(R.string.Failure),
                                            getResources().getString(R.string.nointernetconnection), new InstallCallback() {
                                                @Override
                                                public void onStatusDone(String status) {
                                                    if (TextUtils.equals("retry", status)) {
                                                        GetCheckOutID(amount, typePay);
                                                    }
                                                }
                                            });
                                }
                            }
                        });
                    }
                });
            }
        });
    }


    private boolean IsSendRequestCheckOut = false;

    private void checkoutsPayment(String trnid) {
        layout_loading.setVisibility(View.VISIBLE);
        IsSendRequestCheckOut = true;
        RetrofitWebService.getServiceOut().checkoutsPayment(trnid , SelectTypePay).enqueue(new Callback<RootResponse>() {
            @Override
            public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                Log.e("checkoutsPayment", response.toString() + "    " + checkoutId);
                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (response.body() != null) {
                        if (TextUtils.equals("True", response.body().getStatus())) {
                            String msg = response.body().getMessage();
                            checkoutsResponse result = new Gson().fromJson(msg, checkoutsResponse.class);
                            if (RootManager.IsResultCodesSuccessfullyPay(result.getResult().getCode() + "")) {
                                SaveTransactionWebService(ammount, checkoutId);
                            } else {
                                AppErrorsManager.showCustomErrorDialog(MyWalletActivity.this, getResources().getString(R.string.error),
                                        result.getResult().getDescription() + " \n  " + result.getResult().getCode() + " \n");
                            }
                            Log.e("checkoutsPayment", "result" + result.getResult().getDescription() + " || " + result.getResult().getCode());

                        } else if (TextUtils.equals("False", response.body().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(MyWalletActivity.this, getResources().getString(R.string.error),
                                    response.body().getMessage() + "");
                        }
                    }
                } else {
                    AppErrorsManager.showCustomErrorDialog(MyWalletActivity.this, getResources().getString(R.string.Failure),
                            getResources().getString(R.string.InternalServerError));
                }
                layout_loading.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<RootResponse> call, Throwable t) {
                layout_loading.setVisibility(View.GONE);
                AppErrorsManager.showCustomErrorDialog(MyWalletActivity.this, getResources().getString(R.string.Failure),
                        t.getMessage() + "");
            }
        });
    }

    private void configCheckout(String checkoutId, String typePay) {
        IsSendRequestCheckOut = false;
        Set<String> paymentBrands = new LinkedHashSet<String>();
        if (TextUtils.equals(RootManager.TypeSelectMada, typePay)) {
            paymentBrands.add("MADA");
        } else {
            paymentBrands.add("VISA");
            paymentBrands.add("MASTER");
          //  paymentBrands.add("DIRECTDEBIT_SEPA");

        }
        CheckoutSettings checkoutSettings = new CheckoutSettings(checkoutId, paymentBrands, Connect.ProviderMode.LIVE);

        // Set shopper result URL
        checkoutSettings.setShopperResultUrl("mywallet://result");
        checkoutSettings.setLocale(AppLanguage.getLanguage(this));
        Intent intent = checkoutSettings.createCheckoutActivityIntent(this);
        startActivityForResult(intent, CheckoutActivity.REQUEST_CODE_CHECKOUT);


        /**
         ComponentName receiverComponentName = new ComponentName("com.app.hotfresh", "com.app.hotfresh.BroadcastReceiver.CheckoutBroadcastReceiver");
         Intent intent = checkoutSettings.createCheckoutActivityIntent(this, receiverComponentName);
         startActivityForResult(intent, CheckoutActivity.REQUEST_CODE_CHECKOUT);*/
    }

    private String ammount = "";
    User user = new User();


    private String SelectTypePay = RootManager.TypeSelectVisa ;

    private void initSetup() {

        layout_loading = findViewById(R.id.layout_loading);
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        Layout_notification = findViewById(R.id.Layout_notification);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);
        text_count_cart = findViewById(R.id.text_count_cart);
        image_count_cart = findViewById(R.id.image_count_cart);
        img_bar_cart = findViewById(R.id.img_bar_cart);
        text_count_cart.setVisibility(View.VISIBLE);
        image_count_cart.setVisibility(View.GONE);
        img_bar_cart.setImageResource(R.drawable.ic_plus);
        text_balance = findViewById(R.id.text_balance);

        end_main = findViewById(R.id.end_main);
        end_main.setOnClickListener(view -> {
            onBackPressed();
        });

        user = getUserDetials.User(this);
        if (user != null) {
            text_balance.setText(getResources().getString(R.string.yourcurrentbalance) + " 00 " + getResources().getString(R.string.SAR));
        }

        text_count_cart.setOnClickListener(view -> {
            AppErrorsManager.showAddBalanceDialog(MyWalletActivity.this, new InstallTwoStringCallback() {
                @Override
                public void onStatusDone(String amount, String typePay) {
                        SelectTypePay =typePay ;
                    if (amount.contains(".")) {
                        if (amount.length() > 1) {
                            Double d = Double.parseDouble(amount);
                            String totalAsString = String.format(Locale.ENGLISH, "%.2f", d);
                            GetCheckOutID(totalAsString, typePay);
                            // SaveTransactionWebService("200", "text_checkid");
                        } else {
                            Double d = Double.parseDouble(amount);
                            String totalAsString = String.format(Locale.ENGLISH, "%.2f", d);
                            GetCheckOutID(totalAsString, typePay);
                        }
                    } else {
                        Double d = Double.parseDouble(amount);
                        String totalAsString = String.format(Locale.ENGLISH, "%.2f", d);
                        GetCheckOutID(totalAsString, typePay);
                    }
                }
            });


        });
        layout_loading.setOnClickListener(view -> {
            return;
        });

        toolbarNameTxt.setText("" + getResources().getString(R.string.MyWallet));
        GetWalletBalanceWebService();
    }

    String MySaveBalance = "0";

    private void GetWalletBalanceWebService() {
        layout_loading.setVisibility(View.VISIBLE);
        if (user != null) {
            RetrofitWebService.getService(this).GetWalletBalance(user.getAccessToken()).enqueue(new Callback<WalletResponse>() {
                @Override
                public void onResponse(Call<WalletResponse> call, Response<WalletResponse> response) {
                    if (RootManager.RESPONSE_CODE_OK == response.code()) {
                        if (TextUtils.equals("True", response.body().getStatus())) {
                            MySaveBalance = response.body().getBalance();
                            text_balance.setText(getResources().getString(R.string.yourcurrentbalance) + " " + MySaveBalance + " " + getResources().getString(R.string.SAR));
                        } else if (TextUtils.equals("False", response.body().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(MyWalletActivity.this, getResources().getString(R.string.error),
                                    response.body().getMessage());
                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(MyWalletActivity.this, getResources().getString(R.string.Failure),
                                getResources().getString(R.string.InternalServerError));
                    }

                    if (response.errorBody() != null) {
                        try {
                            Log.e("GetWalletBalance", " error code: " + response.code() + " error content: " + response.errorBody().string().toString());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    layout_loading.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<WalletResponse> call, Throwable t) {
                    Log.e("GetWalletBalance", t.toString());
                    AppErrorsManager.showCustomErrorDialog(MyWalletActivity.this, getResources().getString(R.string.Failure),
                            t.getMessage() + "");
                    layout_loading.setVisibility(View.GONE);
                }
            });
        }

    }

    private void SaveTransactionWebService(String amount, String checkoutId) {
        layout_loading.setVisibility(View.VISIBLE);
        if (user != null) {
            RetrofitWebService.getService(this).SaveTransaction(user.getMobile(), RootManager.Transaction_Deposit,
                    "hatt14", amount, checkoutId).enqueue(new Callback<RootResponse>() {
                @Override
                public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                    if (RootManager.RESPONSE_CODE_OK == response.code()) {
                        if (TextUtils.equals("True", response.body().getStatus())) {
                            AppErrorsManager.showWalletSuccessDialogNotCancel(MyWalletActivity.this, getResources().getString(R.string.info),
                                    response.body().getMessage(), new InstallCallback() {
                                        @Override
                                        public void onStatusDone(String status) {
                                        }
                                    });

                        } else if (TextUtils.equals("False", response.body().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(MyWalletActivity.this, getResources().getString(R.string.error),
                                    response.body().getMessage());
                        }
                        GetWalletBalanceWebService();

                    } else {
                        AppErrorsManager.showCustomErrorDialog(MyWalletActivity.this, getResources().getString(R.string.Failure),
                                getResources().getString(R.string.InternalServerError));
                    }

                    if (response.errorBody() != null) {
                        try {
                            Log.e("SaveTransaction", " error code: " + response.code() + " error content: " + response.errorBody().string().toString());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    layout_loading.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<RootResponse> call, Throwable t) {


                    layout_loading.setVisibility(View.GONE);
                    InternetConnectionUtils.isInternetAvailable(getApplicationContext(), new InternetAvailableCallback() {
                        @Override
                        public void onInternetAvailable(boolean isAvailable) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (isAvailable) {
                                        AppErrorsManager.showCustomErrorDialog(MyWalletActivity.this, getResources().getString(R.string.Failure),
                                                t.getMessage() + "");

                                    } else {
                                        AppErrorsManager.InternetUnAvailableDialog(MyWalletActivity.this, getResources().getString(R.string.Failure),
                                                getResources().getString(R.string.nointernetconnection), new InstallCallback() {
                                                    @Override
                                                    public void onStatusDone(String status) {
                                                        if (TextUtils.equals("retry", status)) {
                                                            SaveTransactionWebService(amount, checkoutId);
                                                        }
                                                    }
                                                });
                                    }
                                }
                            });
                        }
                    });

                }
            });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("transaction", "resultCode " + resultCode);

        switch (resultCode) {
            case CheckoutActivity.RESULT_OK:
                /* transaction completed */
                Transaction transaction = data.getParcelableExtra(CheckoutActivity.CHECKOUT_RESULT_TRANSACTION);
                Log.e("transaction", "RESULT_OK " + transaction.toString());
                /* resource path if needed */
                String resourcePath = data.getStringExtra(CheckoutActivity.CHECKOUT_RESULT_RESOURCE_PATH);

                Log.e("onNewIntent", "onActivityResult " + checkoutId + "");

                if (transaction.getTransactionType() == TransactionType.SYNC) {
                    /* check the result of synchronous transaction */
                    Log.e("onNewIntent", "onActivityResult " + "==");
                    if (!IsSendRequestCheckOut)
                        checkoutsPayment(checkoutId);
                } else {
                    Log.e("onNewIntent", "onActivityResult " + "!=");
                    Log.e("transaction", "else " + transaction.getTransactionType() + " ==" + TransactionType.SYNC);
                    /* wait for the asynchronous transaction callback in the onNewIntent() */
                }

                break;
            case CheckoutActivity.RESULT_CANCELED:
                /* shopper canceled the checkout process */
                Log.e("transaction", "RESULT_CANCELED ");
                break;
            case CheckoutActivity.RESULT_ERROR:
                /* error occurred */
                PaymentError error = data.getParcelableExtra(CheckoutActivity.CHECKOUT_RESULT_ERROR);
                Log.e("transaction", error.getErrorInfo().toString() + " RESULT_ERROR");
        }
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.getScheme().equals("mywallet")) {
            String checkoutId = intent.getData().getQueryParameter("id");
            if (!IsSendRequestCheckOut)
                checkoutsPayment(checkoutId);
        }


    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                initSetup();
            }
        }, 2000);
    }

    private AppMp3Manager appMp3Manager;
    private BroadcastReceiver mHander = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String title = intent.getStringExtra("title");
            String body = intent.getStringExtra("body");
            appMp3Manager = new AppMp3Manager(MyWalletActivity.this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    AppErrorsManager.showCustomErrorDialogTop(MyWalletActivity.this, title, body, new InstallCallback() {
                        @Override
                        public void onStatusDone(String status) {

                        }
                    });
                    appMp3Manager.palyNotificationMusic(R.raw.bell_sound);
                }
            }, 1000);
        }
    };


    @Override
    protected void onRestart() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        super.onRestart();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHander);
        super.onPause();
    }
}
