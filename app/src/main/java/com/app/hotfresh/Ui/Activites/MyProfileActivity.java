package com.app.hotfresh.Ui.Activites;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.JsonReader;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import com.app.hotfresh.BuildConfig;
import com.app.hotfresh.CallBack.InstallCallback;
import com.app.hotfresh.CallBack.InternetAvailableCallback;
import com.app.hotfresh.CallBack.OnItemClickTagListener;
import com.app.hotfresh.CallBack.SmsBroadcastReceiverListener;
import com.app.hotfresh.Manager.AppErrorsManager;
import com.app.hotfresh.Manager.AppLanguage;
import com.app.hotfresh.Manager.AppMp3Manager;
import com.app.hotfresh.Manager.AppPreferences;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Manager.InternetConnectionUtils;
import com.app.hotfresh.Manager.RootManager;
import com.app.hotfresh.Manager.getUserDetials;
import com.app.hotfresh.Medoles.Charities.Charitie;
import com.app.hotfresh.Medoles.SettingItem;
import com.app.hotfresh.Medoles.User;
import com.app.hotfresh.R;
import com.app.hotfresh.WebService.RetrofitWebService;
import com.app.hotfresh.WebService.model.response.LoginResponse;
import com.app.hotfresh.WebService.model.response.RootResponse;
import com.app.hotfresh.WebService.model.response.checkoutsResponse;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;
import com.oppwa.mobile.connect.checkout.dialog.CheckoutActivity;
import com.oppwa.mobile.connect.checkout.meta.CheckoutSettings;
import com.oppwa.mobile.connect.exception.PaymentError;
import com.oppwa.mobile.connect.provider.Connect;
import com.oppwa.mobile.connect.provider.Transaction;
import com.oppwa.mobile.connect.provider.TransactionType;


import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.HttpsURLConnection;

import es.dmoral.toasty.Toasty;
import io.card.payment.CardIOActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.hotfresh.Manager.RootManager.ActionKeyLocalBroadcastManager;
import static com.app.hotfresh.Manager.RootManager.SMS_PATTERN_NUMBER;
import static com.app.hotfresh.Manager.RootManager.SMS_PROVIDER_NAME;

public class MyProfileActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    private FrameLayout layout_loading;

    private static final int REQ_USER_CONSENT = 200;

    private TextView toolbarNameTxt;
    private RelativeLayout Layout_notification;
    private TextView text_name, text_mobile, text_password;
    private RelativeLayout end_main;
    private TextView text_verified, text_not_verified;
    private TextView text_address;
    SwipeRefreshLayout swipeRefreshLayout;
    AlertDialog alertDialogBuilder;


    private Handler handler;
    private String checkoutId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppLanguage.setContentLang(this);
       // registerBroadcastReceiver();
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_my_profile);
        FontManager.applyFont(this, findViewById(R.id.layout));
        handler = new Handler(Looper.getMainLooper());


        initSetup();

    }


    User user = new User();

    private void initSetup() {
        layout_loading = findViewById(R.id.layout_loading);
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        Layout_notification = findViewById(R.id.Layout_notification);
        text_name = findViewById(R.id.text_name);
        text_mobile = findViewById(R.id.text_mobile);
        text_password = findViewById(R.id.text_password);
        text_address = findViewById(R.id.text_address);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(this);
        text_verified = findViewById(R.id.text_verified);
        text_not_verified = findViewById(R.id.text_not_verified);
        text_verified.setVisibility(View.GONE);
        text_not_verified.setVisibility(View.GONE);


        end_main = findViewById(R.id.end_main);
        end_main.setOnClickListener(view -> {
            onBackPressed();
        });
        user = getUserDetials.User(this);
        if (user != null) {
            text_name.setText(user.getName());
            text_mobile.setText(user.getMobile());
            text_mobile.setText(user.getMobileString());
            if (user.getMobileActivate() != null) {
                text_verified.setVisibility(View.VISIBLE);
                text_not_verified.setVisibility(View.GONE);
                if (TextUtils.equals("False", user.getMobileActivate())) {
                    text_verified.setVisibility(View.GONE);
                    text_not_verified.setVisibility(View.VISIBLE);
                }
            } else {
                text_verified.setVisibility(View.GONE);
                text_not_verified.setVisibility(View.VISIBLE);
            }
            if (user.getAddress() != null) {
                text_address.setText("" + user.getAddress());
            } else {
                text_address.setText("" + getResources().getString(R.string.Notadded));
            }
        }

        text_not_verified.setOnClickListener(view -> {
            OpenDialogActiveMobile();
        });

        Layout_notification.setVisibility(View.GONE);
        layout_loading.setOnClickListener(view -> {
            return;
        });

        toolbarNameTxt.setText("" + getResources().getString(R.string.myAccount));


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQ_USER_CONSENT) {
            if ((resultCode == RESULT_OK) && (data != null)) {
                String message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE);
                Log.e("sms", message + " message");
              //  getOtpFromMessage(message);
            }
        }
    }


    private void OpenDialogActiveMobile() {
        if (user != null) {
            alertDialogBuilder = new AlertDialog.Builder(MyProfileActivity.this).create();
            AppErrorsManager.showActiveMobileDialog(MyProfileActivity.this, "",user.getMobileString(), new InstallCallback() {
                @Override
                public void onStatusDone(String code) {
                    if (!TextUtils.isEmpty(code)) {
                        if (TextUtils.equals(code, "NewCode")) {
                            //  startSmsUserConsent();
                            SendMobileActivateCodeWebServices();
                        } else {
                            CheckMobileActivateCodeWebServices(code);
                        }
                    }

                }
            }, alertDialogBuilder);
        }
    }

    private void CheckMobileActivateCodeWebServices(String code) {
        layout_loading.setVisibility(View.VISIBLE);
        if (user != null) {
            RetrofitWebService.getService(this).CheckMobileActivateCode(user.getAccessToken(), code).enqueue(new Callback<RootResponse>() {
                @Override
                public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                    Log.e("CheckMobileActivateCode", response.toString());
                    if (RootManager.RESPONSE_CODE_OK == response.code()) {
                        if (TextUtils.equals("True", response.body().getStatus())) {
                            if (response.body() != null) {
                                GetMyProfile();
                            } else {
                                /** AppErrorsManager.showCustomErrorDialog(MyProfileActivity.this, getResources().getString(R.string.error),
                                 response.body().getMessage());*/
                                Toasty.error(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT, true).show();

                            }
                        } else if (TextUtils.equals("False", response.body().getStatus())) {
                            /**   AppErrorsManager.showCustomErrorDialog(MyProfileActivity.this, getResources().getString(R.string.error),
                             response.body().getMessage());*/
                            Toasty.error(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT, true).show();

                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(MyProfileActivity.this, getResources().getString(R.string.Failure),
                                getResources().getString(R.string.InternalServerError));
                    }
                    layout_loading.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<RootResponse> call, Throwable t) {
                    Log.e("CheckMobileActivateCode", t.toString());
                    layout_loading.setVisibility(View.GONE);
                    AppErrorsManager.showCustomErrorDialog(MyProfileActivity.this, getResources().getString(R.string.Failure),
                            t.getMessage() + "");
                }
            });
        }
    }

    private void CheckMobileActivateServices(String code) {
        if (user != null) {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(getResources().getString(R.string.progress_activation));
            progressDialog.show();
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);
            if (alertDialogBuilder != null) {
                alertDialogBuilder.dismiss();
            }
            RetrofitWebService.getService(this).CheckMobileActivateCode(user.getAccessToken(), code).enqueue(new Callback<RootResponse>() {
                @Override
                public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                    Log.e("CheckMobileActivateCode", response.toString());
                    if (RootManager.RESPONSE_CODE_OK == response.code()) {
                        if (TextUtils.equals("True", response.body().getStatus())) {
                            if (response.body() != null) {
                                Toasty.success(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT, true).show();
                                GetMyProfile();
                            } else {
                                Toasty.error(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT, true).show();

                            }
                        } else if (TextUtils.equals("False", response.body().getStatus())) {
                            Toasty.error(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT, true).show();

                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(MyProfileActivity.this, getResources().getString(R.string.Failure),
                                getResources().getString(R.string.InternalServerError));
                    }
                    progressDialog.hide();
                }

                @Override
                public void onFailure(Call<RootResponse> call, Throwable t) {
                    Log.e("CheckMobileActivateCode", t.toString());
                    progressDialog.hide();

                }
            });
        }
    }


    private void SendMobileActivateCodeWebServices() {
        layout_loading.setVisibility(View.VISIBLE);
        if (user != null) {
            RetrofitWebService.getService(this).SendMobileActivateCode(user.getAccessToken()).enqueue(new Callback<RootResponse>() {
                @Override
                public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                    Log.e("SendMobileActivateCode", response.toString());
                    if (RootManager.RESPONSE_CODE_OK == response.code()) {
                        if (TextUtils.equals("True", response.body().getStatus())) {
                            if (response.body() != null) {
                                Toasty.success(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT, true).show();
                            } else {
                                Toasty.warning(getApplicationContext(), getResources().getString(R.string.error), Toast.LENGTH_SHORT, true).show();
                            }
                        } else if (TextUtils.equals("False", response.body().getStatus())) {
                            Toasty.error(getApplicationContext(), response.body().getMessage() + "", Toast.LENGTH_SHORT, true).show();

                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(MyProfileActivity.this, getResources().getString(R.string.Failure),
                                getResources().getString(R.string.InternalServerError));
                    }
                    layout_loading.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<RootResponse> call, Throwable t) {
                    Log.e("SendMobileActivateCode", t.toString());
                    layout_loading.setVisibility(View.GONE);
                    AppErrorsManager.showCustomErrorDialog(MyProfileActivity.this, getResources().getString(R.string.Failure),
                            t.getMessage() + "");
                }
            });
        }
    }

    private void GetMyProfile() {
        if (user != null) {
            layout_loading.setVisibility(View.VISIBLE);
            String old_token = user.getAccessToken();
            RetrofitWebService.getService(this).GetMyProfile(user.getAccessToken()).enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    Log.e("GetMyProfile", response.toString());
                    if (RootManager.RESPONSE_CODE_OK == response.code()) {
                        if (TextUtils.equals("True", response.body().getStatus())) {
                            if (response.body() != null) {
                                User user = (User) response.body().getUser();
                                user.setAccessToken(old_token);
                                if (user != null) {
                                    Gson json = new Gson();
                                    String userJson = json.toJson(user);
                                    AppPreferences.saveString(MyProfileActivity.this, "userJson", userJson);
                                    AppErrorsManager.showCustomErrorDialogNotCancel(MyProfileActivity.this, getResources().getString(R.string.info),
                                            response.body().getMessage(), new InstallCallback() {
                                                @Override
                                                public void onStatusDone(String status) {
                                                    onBackPressed();
                                                }
                                            });


                                } else {

                                    AppErrorsManager.showCustomErrorDialog(MyProfileActivity.this, getResources().getString(R.string.error),
                                            response.body().getMessage());
                                }
                            } else {
                                AppErrorsManager.showCustomErrorDialog(MyProfileActivity.this, getResources().getString(R.string.error),
                                        response.body().getMessage());
                            }
                        } else if (TextUtils.equals("False", response.body().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(MyProfileActivity.this, getResources().getString(R.string.error),
                                    response.body().getMessage());
                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(MyProfileActivity.this, getResources().getString(R.string.Failure),
                                getResources().getString(R.string.InternalServerError));
                    }
                    layout_loading.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    Log.e("GetMyProfile", t.toString());


                    layout_loading.setVisibility(View.GONE);
                    InternetConnectionUtils.isInternetAvailable(getApplicationContext(), new InternetAvailableCallback() {
                        @Override
                        public void onInternetAvailable(boolean isAvailable) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (isAvailable) {
                                        AppErrorsManager.showCustomErrorDialog(MyProfileActivity.this, getResources().getString(R.string.Failure),
                                                t.getMessage() + "");

                                    } else {
                                        AppErrorsManager.InternetUnAvailableDialog(MyProfileActivity.this, getResources().getString(R.string.Failure),
                                                getResources().getString(R.string.nointernetconnection), new InstallCallback() {
                                                    @Override
                                                    public void onStatusDone(String status) {
                                                        if (TextUtils.equals("retry", status)) {
                                                            GetMyProfile();
                                                        }
                                                    }
                                                });
                                    }
                                }
                            });
                        }
                    });
                }
            });
        }
    }


    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                initSetup();
            }
        }, 2000);
    }

    private AppMp3Manager appMp3Manager;
    private BroadcastReceiver mHander = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
          //  if (!SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
                String title = intent.getStringExtra("title");

                String body = intent.getStringExtra("body");
                appMp3Manager = new AppMp3Manager(MyProfileActivity.this);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        AppErrorsManager.showCustomErrorDialogTop(MyProfileActivity.this, title, body, new InstallCallback() {
                            @Override
                            public void onStatusDone(String status) {

                            }
                        });
                        appMp3Manager.palyNotificationMusic(R.raw.bell_sound);
                    }
                }, 1000);
            }
       // }
    };


    @Override
    protected void onRestart() {
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        super.onRestart();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHander);
        super.onPause();
    }


   /** private void startSmsUserConsent() {
        SmsRetrieverClient client = SmsRetriever.getClient(this);
        client.startSmsUserConsent(SMS_PROVIDER_NAME).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
            }
        });
    }


    private void getOtpFromMessage(String message) {
        // This will match any 6 digit number in the message
        Pattern pattern = Pattern.compile("(|^)\\d{" + SMS_PATTERN_NUMBER + "}");
        Matcher matcher = pattern.matcher(message);
        if (matcher.find()) {
            // Toasty.info(getApplicationContext(), "" + getResources().getString(R.string.progress_activation), Toast.LENGTH_SHORT, true).show();
            CheckMobileActivateServices(matcher.group(0));
        }
    }

    private BroadcastReceiver smsVerificationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (SmsRetriever.SMS_RETRIEVED_ACTION.equals(intent.getAction())) {
                Bundle extras = intent.getExtras();
                Status smsRetrieverStatus = (Status) extras.get(SmsRetriever.EXTRA_STATUS);

                switch (smsRetrieverStatus.getStatusCode()) {
                    case CommonStatusCodes.SUCCESS:
                        // Get consent intent
                        Intent consentIntent = extras.getParcelable(SmsRetriever.EXTRA_CONSENT_INTENT);
                        try {
                            // Start activity to show consent dialog to user, activity must be started in
                            // 5 minutes, otherwise you'll receive another TIMEOUT intent
                            startActivityForResult(consentIntent, REQ_USER_CONSENT);
                        } catch (ActivityNotFoundException e) {
                            // Handle the exception ...
                        }
                        break;
                    case CommonStatusCodes.TIMEOUT:
                        // Time out occurred, handle the error.
                        break;
                }
            }
        }
    };

    private void registerBroadcastReceiver() {
        IntentFilter intentFilter = new IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION);
        registerReceiver(smsVerificationReceiver, intentFilter);
    }*/

    @Override
    public void onStart() {
        super.onStart();
       // registerBroadcastReceiver();
    }

    @Override
    public void onStop() {
        super.onStop();
        //unregisterReceiver(smsVerificationReceiver);
    }
}
