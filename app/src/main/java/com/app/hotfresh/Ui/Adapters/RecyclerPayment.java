package com.app.hotfresh.Ui.Adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.hotfresh.CallBack.OnItemClickTagListener;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Manager.RootManager;
import com.app.hotfresh.Medoles.Checkouts.HatUserWallet;
import com.app.hotfresh.Medoles.Nofitication;
import com.app.hotfresh.R;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.List;


public class RecyclerPayment extends RecyclerView.Adapter<RecyclerPayment.CustomView> {
    Context context;
    List<HatUserWallet> mylist;
    private OnItemClickTagListener listener;
    int Layout;
    public static int ItemSelect = -1;


    public class CustomView extends RecyclerView.ViewHolder {
        TextView row_amount, row_date, row_type_text, row_CheckID;
        ImageView row_img;


        public CustomView(View v) {
            super(v);
            FontManager.applyFont(context, v);
            row_date = v.findViewById(R.id.row_date);
            row_amount = v.findViewById(R.id.row_amount);
            row_CheckID = v.findViewById(R.id.row_CheckID);
            row_img = v.findViewById(R.id.row_img);
            row_type_text = v.findViewById(R.id.row_type_text);
        }

    }

    public RecyclerPayment() {
    }

    public RecyclerPayment(Context context, List<HatUserWallet> mylist, int ItemSelect, int Layout, OnItemClickTagListener listener) {
        this.context = context;
        this.mylist = mylist;
        this.listener = listener;
        this.Layout = Layout;
        this.ItemSelect = ItemSelect;
    }

    @Override
    public CustomView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(Layout, parent, false);
        CustomView viewholder = new CustomView(view);
        return viewholder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final CustomView holder, final int position) {
        holder.row_CheckID.setText(mylist.get(position).getCheckID() + "");
        holder.row_amount.setText(mylist.get(position).getAmount() + ".00 " + context.getResources().getString(R.string.SAR));
        if (mylist.get(position).getDate() != null) {
            holder.row_date.setText(mylist.get(position).getDate() + " " + mylist.get(position).getTime());
        } else {
            holder.row_date.setText("");
        }
        if (!TextUtils.isEmpty(mylist.get(position).getAmount() + "")) {
            float amount =(float) Float.parseFloat(mylist.get(position).getAmount() + "")  ;
            int value = (int) Math.ceil(amount);
            BigDecimal result;
            result=round(value,2);
            holder.row_amount.setText(result + " " + context.getResources().getString(R.string.SAR));
        }

        if (mylist.get(position).getCheckID() != null) {
            String checkId = mylist.get(position).getCheckID();
            String[] checkList = checkId.split("\\.");
            Log.e("checkList", checkList.length + " checkList");
            Log.e("checkList", checkId + " checkId");

            if (checkList.length > 1) {
                Log.e("checkList", checkList[0] + " checkList[0]");
                holder.row_CheckID.setText(checkList[0] + "");
            }

        }


        if (TextUtils.equals(RootManager.Transaction_Deposit, mylist.get(position).getPaymentType())) {
            holder.row_img.setImageResource(R.drawable.ic_arrow_downward_black_24dp);
            holder.row_amount.setTextColor(context.getResources().getColor(R.color.colorGreen));
            holder.row_img.getBackground().setColorFilter(new PorterDuffColorFilter(context.getResources().getColor(R.color.colorGreen), PorterDuff.Mode.MULTIPLY));
            holder.row_CheckID.setVisibility(View.VISIBLE);
            holder.row_type_text.setVisibility(View.GONE);
        } else if (TextUtils.equals(RootManager.Transaction_Withdraw, mylist.get(position).getPaymentType())) {
            holder.row_img.setImageResource(R.drawable.ic_arrow_upward_black_24dp);
            holder.row_img.getBackground().setColorFilter(new PorterDuffColorFilter(context.getResources().getColor(R.color.colorPrimaryDark2), PorterDuff.Mode.MULTIPLY));
            holder.row_amount.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark2));
            holder.row_CheckID.setVisibility(View.INVISIBLE);
            holder.row_type_text.setVisibility(View.VISIBLE);
        } else if (TextUtils.equals(RootManager.Transaction_Archives, mylist.get(position).getPaymentType())) {
            holder.row_img.setImageResource(R.drawable.ic_compare_arrows_black_24dp);
            holder.row_img.getBackground().setColorFilter(new PorterDuffColorFilter(context.getResources().getColor(R.color.colorBlue), PorterDuff.Mode.MULTIPLY));
            holder.row_amount.setTextColor(context.getResources().getColor(R.color.colorBlue));
            holder.row_CheckID.setVisibility(View.VISIBLE);
            holder.row_type_text.setVisibility(View.GONE);

        }

        holder.row_CheckID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    listener.onItemClick(view, position, "view");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });


    }


    public static BigDecimal round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_UP);
        return bd;
    }
    @Override
    public int getItemCount() {
        return mylist.size();
    }


}

