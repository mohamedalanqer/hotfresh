package com.app.hotfresh.Ui.Activites;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.hotfresh.CallBack.InstallCallback;
import com.app.hotfresh.CallBack.InternetAvailableCallback;
import com.app.hotfresh.CallBack.OnItemClickListener;
import com.app.hotfresh.CallBack.OnItemClickTagListener;
import com.app.hotfresh.DataBase.AppDatabase;
import com.app.hotfresh.DataBase.InsertUpdateDeleteItem;
import com.app.hotfresh.DataBase.ItemDao;
import com.app.hotfresh.Manager.AppErrorsManager;
import com.app.hotfresh.Manager.AppLanguage;
import com.app.hotfresh.Manager.AppMp3Manager;
import com.app.hotfresh.Manager.AppPreferences;
import com.app.hotfresh.Manager.DrawableRound;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Manager.InternetConnectionUtils;
import com.app.hotfresh.Manager.RootManager;
import com.app.hotfresh.Manager.getUserDetials;
import com.app.hotfresh.Medoles.Category;
import com.app.hotfresh.Medoles.Item;
import com.app.hotfresh.Medoles.ItemService;
import com.app.hotfresh.Medoles.User;
import com.app.hotfresh.R;
import com.app.hotfresh.Ui.Adapters.AdapterOfferHome;
import com.app.hotfresh.Ui.Adapters.KKViewPager;
import com.app.hotfresh.Ui.Adapters.RecyclerItems;
import com.app.hotfresh.Ui.Adapters.RecyclerSearch;
import com.app.hotfresh.Ui.Adapters.RecyclerServiceDialog;
import com.app.hotfresh.WebService.RetrofitWebService;
import com.app.hotfresh.WebService.model.response.CategoryResponse;
import com.app.hotfresh.WebService.model.response.ItemsResponse;
import com.app.hotfresh.WebService.model.response.ItemsServiceResponse;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.android.material.appbar.MaterialToolbar;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.app.hotfresh.Manager.RootManager.ActionKeyLocalBroadcastManager;
import static com.app.hotfresh.Manager.RootManager.DataBaseName;

public class ListItemByIdActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {


    private FrameLayout layout_loading;

    RecyclerView recycler_view;
    RecyclerItems adapter;
    List<Item> items_list = new ArrayList<>();
    private String CategoryName = "";
    private String CategoryID = "";
    private TextView toolbarNameTxt, text_empty;
    AppDatabase db;
    List<Item> itemListDataBase = new ArrayList<>();
    List<ItemService> itemServicesListDataBase = new ArrayList<>();
    private Handler handler;
    private RelativeLayout end_main;
    SwipeRefreshLayout swipeRefreshLayout;
    private RelativeLayout Layout_notification;
    private LinearLayout layout_empty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppLanguage.setContentLang(this);
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
        setContentView(R.layout.activity_list_item_id);
        FontManager.applyFont(this, findViewById(R.id.layout));
        handler = new Handler(Looper.getMainLooper());


        initSetup();
        setUpDataBase();
    }

    private int countitem = 0;

    private void setUpDataBase() {
        db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, DataBaseName).allowMainThreadQueries().build();

        ImageView count_cart_img = findViewById(R.id.image_count_cart);
        TextView text_count_cart = findViewById(R.id.text_count_cart);


        db.itemDao().getCount().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(Integer integer) {
                countitem = integer;
                DrawableRound.TextToDrawableRound(getApplicationContext(), count_cart_img, String.valueOf(integer), text_count_cart);
            }
        });
        itemListDataBase = db.itemDao().getAll();
        itemServicesListDataBase = db.itemServiceDao().getAll();
    }

    private void GoToCartActvity() {
        if (countitem > 0) {
            Intent intent = new Intent(getApplicationContext(), ListCartActivity.class);
            startActivity(intent);
        } else {
            AppErrorsManager.showCustomErrorDialog(ListItemByIdActivity.this, getResources().getString(R.string.info),
                    getResources().getString(R.string.emptyCartEpty));
        }

    }

    private void initSetup() {
        recycler_view = findViewById(R.id.recycler_view);
        layout_loading = findViewById(R.id.layout_loading);
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        end_main = findViewById(R.id.end_main);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        layout_empty = findViewById(R.id.layout_empty);
        text_empty = findViewById(R.id.text_empty);
        swipeRefreshLayout.setOnRefreshListener(this::onRefresh);
        end_main.setOnClickListener(view -> {
            onBackPressed();
        });
        layout_loading.setOnClickListener(view -> {
            return;
        });
        CategoryName = getIntent().getStringExtra("CategoryName");
        CategoryID = getIntent().getStringExtra("CategoryID");
        if (!TextUtils.isEmpty(CategoryName)) {
            if (!TextUtils.isEmpty(CategoryID)) {
                GetListItemsWebService(CategoryID);
            }
            toolbarNameTxt.setText("" + CategoryName);
        }
        Layout_notification = findViewById(R.id.Layout_notification);
        Layout_notification.setOnClickListener(view -> {
            GoToCartActvity();
        });
    }

    private void GetListItemsWebService(String CategoryID) {
        layout_loading.setVisibility(View.VISIBLE);
        layout_empty.setVisibility(View.GONE);
        recycler_view.setVisibility(View.VISIBLE);
        User user = getUserDetials.User(this);
        String AccessToken = "";
        if(user != null){
            AccessToken =user.getAccessToken();
        }
        Log.e("CategoryName",CategoryName);
        RetrofitWebService.getService(this).GetItemByCategory(CategoryID ,AccessToken).enqueue(new Callback<ItemsResponse>() {
            @Override
            public void onResponse(Call<ItemsResponse> call, Response<ItemsResponse> response) {
                Log.e("GetItemByCategory", response.toString());
                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (response.body() != null) {
                        if (TextUtils.equals("True", response.body().getStatus())) {
                            items_list = response.body().getItems();
                            Log.e("itemServiceataBase", "== " + itemServicesListDataBase.size());
                            if (items_list.size() > 0) {
                                for (int i = 0; i < items_list.size(); i++) {
                                    for (int x = 0; x < itemListDataBase.size(); x++) {
                                        if (TextUtils.equals(items_list.get(i).getCode(), itemListDataBase.get(x).getCode())) {
                                            items_list.get(i).setQuantity(itemListDataBase.get(x).getQuantity());
                                            Log.e("itemServiceataBase", "== " + itemServicesListDataBase.size());
                                            if (itemServicesListDataBase.size() > 0) {
                                                List<ItemService> newitemService = new ArrayList<>();
                                                for (int s = 0; s < itemServicesListDataBase.size(); s++) {
                                                    if (TextUtils.equals(items_list.get(i).getCode() + "".trim(), itemServicesListDataBase.get(s).getItemCode() + "".trim())) {
                                                        newitemService.add(itemServicesListDataBase.get(s));
                                                    }

                                                }
                                                items_list.get(i).setItemServices(newitemService);
                                            }
                                        }
                                    }
                                }
                                setupRecycler();
                            } else {
                                recycler_view.setVisibility(View.GONE);
                                layout_empty.setVisibility(View.VISIBLE);
                                text_empty.setText(Html.fromHtml("<font color='#EFBF2F'>" + CategoryName + "</font>" + "<br/>" + response.body().getMessage()));

                            }

                        } else if (TextUtils.equals("False", response.body().getStatus())) {
                            recycler_view.setVisibility(View.GONE);
                            layout_empty.setVisibility(View.VISIBLE);
                            text_empty.setText(Html.fromHtml("<font color='#EFBF2F'>" + CategoryName + "</font>" + "<br/>" + response.body().getMessage()));

                        }
                    }
                } else {
                    AppErrorsManager.showCustomErrorDialog(ListItemByIdActivity.this, getResources().getString(R.string.Failure),
                            getResources().getString(R.string.InternalServerError));
                }


                if (response.errorBody() != null) {
                    try {
                        Log.e("GetItemByCategory2", " error code: " + response.code() + " error content: " + response.errorBody().string().toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                layout_loading.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ItemsResponse> call, Throwable t) {
                Log.e("GetItemByCategory", t.toString());

                layout_loading.setVisibility(View.GONE);
                InternetConnectionUtils.isInternetAvailable(getApplicationContext(), new InternetAvailableCallback() {
                    @Override
                    public void onInternetAvailable(boolean isAvailable) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (isAvailable) {
                                    AppErrorsManager.showCustomErrorDialog(ListItemByIdActivity.this, getResources().getString(R.string.Failure),
                                            t.getMessage() + "");
                                } else {
                                    AppErrorsManager.InternetUnAvailableDialog(ListItemByIdActivity.this, getResources().getString(R.string.Failure),
                                            getResources().getString(R.string.nointernetconnection), new InstallCallback() {
                                                @Override
                                                public void onStatusDone(String status) {
                                                    if (TextUtils.equals("retry", status)) {
                                                        GetListItemsWebService(CategoryID);
                                                    }
                                                }
                                            });
                                }
                            }
                        });
                    }
                });
            }
        });


    }


    public void setupRecycler() {
        adapter = new RecyclerItems(getApplicationContext(), "ListItem", items_list, R.layout.row_item_items, new OnItemClickTagListener() {
            @Override
            public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {
                Item item = items_list.get(position);
                if (TextUtils.equals("plus", tag)) {
                    if (item.getQuantity() < 20) {
                        item.setQuantity(item.getQuantity() + 1);
                        InsertUpdateDeleteItem.upsert_Item(db, item);
                    }

                } else if (TextUtils.equals("add", tag)) {
                    if (item.getQuantity() == 0) {
                        item.setQuantity(item.getQuantity() + 1);
                        InsertUpdateDeleteItem.upsert_Item(db, item);
                    }

                } else if (TextUtils.equals("minus", tag)) {
                    if (item.getQuantity() > 0) {
                        item.setQuantity(item.getQuantity() - 1);
                        if (item.getQuantity() == 0) {
                            if (item.getItemServices().size() > 0) {
                                item.setItemServices(new ArrayList<>());
                                InsertUpdateDeleteItem.deleteByItemService(db, item.getCode());
                            }
                        }
                        InsertUpdateDeleteItem.upsert_Item(db, item);
                    }
                } else if (TextUtils.equals("service", tag)) {
                    if (item.getQuantity() == 0) {
                        Toasty.warning(getApplicationContext(), getResources().getString(R.string.add_to_cart_after_service), Toast.LENGTH_SHORT, true).show();

                    } else {
                        GetItemServiceByItemCodeWebService(item);
                    }
                }
                adapter.notifyDataSetChanged();
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recycler_view.setLayoutManager(linearLayoutManager);
        recycler_view.setAdapter(adapter);

    }

    private void GetItemServiceByItemCodeWebService(Item item) {
        layout_loading.setVisibility(View.VISIBLE);

        RetrofitWebService.getService(this).GetItemServiceByCodeItem(item.getCode() + "").enqueue(new Callback<ItemsServiceResponse>() {
            @Override
            public void onResponse(Call<ItemsServiceResponse> call, Response<ItemsServiceResponse> response) {
                Log.e("GetItemServiceByCode", response.toString());
                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (response.body() != null) {
                        List<ItemService> itemServices = response.body().getItemServices();
                        if (itemServices != null) {
                            if (itemServices.size() > 0) {
                                itemServicesListDataBase = db.itemServiceDao().getAll();
                                for (int i = 0; i < itemServices.size(); i++) {
                                    for (int x = 0; x < itemServicesListDataBase.size(); x++) {
                                        Log.e("itemServiceataBase0", itemServicesListDataBase.get(x).getItemCode() + "==" + item.getCode());
                                        if (TextUtils.equals(itemServicesListDataBase.get(x).getItemCode(), item.getCode())) {
                                            if (TextUtils.equals(itemServicesListDataBase.get(x).getServiceCode(), itemServices.get(i).getServiceCode())) {
                                                itemServices.get(i).setIs_selected(true);
                                            }
                                        }
                                    }
                                }
                                ShowDialogServiceItem(itemServices, item);
                            } else {
                                AppErrorsManager.showCustomErrorDialog(ListItemByIdActivity.this, getResources().getString(R.string.info),
                                        response.body().getMessage());
                            }
                        } else {
                            AppErrorsManager.showCustomErrorDialog(ListItemByIdActivity.this, getResources().getString(R.string.error),
                                    getResources().getString(R.string.notHaveService));
                        }
                    }
                } else {
                    AppErrorsManager.showCustomErrorDialog(ListItemByIdActivity.this, getResources().getString(R.string.Failure),
                            getResources().getString(R.string.InternalServerError));
                }


                if (response.errorBody() != null) {
                    try {
                        Log.e("GetItemServiceByCode", " error code: " + response.code() + " error content: " + response.errorBody().string().toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                layout_loading.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ItemsServiceResponse> call, Throwable t) {
                AppErrorsManager.showCustomErrorDialog(ListItemByIdActivity.this, getResources().getString(R.string.Failure),
                        t.getMessage() + "");
                layout_loading.setVisibility(View.GONE);
            }
        });

    }

    RecyclerServiceDialog adapter_service;


    private void ShowDialogServiceItem(List<ItemService> itemServices, Item item) {

        adapter_service = new RecyclerServiceDialog(this, itemServices, R.layout.row_item_service, new OnItemClickTagListener() {
            @Override
            public void onItemClick(View view, int position, String tag) throws JSONException, FileNotFoundException {

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (TextUtils.equals("select", tag)) {
                            ItemService itemService = itemServices.get(position);
                            if (itemService.isIs_selected()) {
                                itemService.setIs_selected(false);
                            } else {
                                itemService.setIs_selected(true);
                            }
                            List<ItemService> itemServicenewList = new ArrayList<>();

                            for (int i = 0; i < itemServices.size(); i++) {
                                if (itemServices.get(i).isIs_selected()) {
                                    itemServices.get(i).setItemCode(item.getCode());
                                    itemServicenewList.add(itemServices.get(i));
                                }
                            }

                            InsertUpdateDeleteItem.deleteByItemService(db, item.getCode());

                            for (int i = 0; i < items_list.size(); i++) {
                                if (items_list.get(i).equals(item)) {
                                    items_list.get(i).setItemServices(itemServicenewList);
                                    if (item.getQuantity() > 0) {
                                        InsertUpdateDeleteItem.upsert_ItemService(db, itemServicenewList);
                                    }/** else {
                                     InsertUpdateDeleteItem.deleteByItemService(db, item.getCode());
                                     }*/
                                }
                            }

                            adapter_service.notifyDataSetChanged();
                            adapter.notifyDataSetChanged();
                        }
                    }
                });


            }
        });

        AppErrorsManager.showAddressListDialog(ListItemByIdActivity.this, adapter_service, new InstallCallback() {
            @Override
            public void onStatusDone(String status) {
                if (TextUtils.equals("yes", status)) {
                }

            }
        });

    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
                initSetup();
            }
        }, 2000);
    }


    private AppMp3Manager appMp3Manager;
    private BroadcastReceiver mHander = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String title = intent.getStringExtra("title");
            String body = intent.getStringExtra("body");
            appMp3Manager = new AppMp3Manager(ListItemByIdActivity.this);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    AppErrorsManager.showCustomErrorDialogTop(ListItemByIdActivity.this, title, body, new InstallCallback() {
                        @Override
                        public void onStatusDone(String status) {

                        }
                    });
                    appMp3Manager.palyNotificationMusic(R.raw.bell_sound);
                }
            }, 1000);
        }
    };


    @Override
    protected void onRestart() {
        setUpDataBase();
        LocalBroadcastManager.getInstance(this).registerReceiver(mHander, new IntentFilter(ActionKeyLocalBroadcastManager));
        super.onRestart();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mHander);
        super.onPause();
    }
}
