package com.app.hotfresh.Ui.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.app.hotfresh.CallBack.OnItemClickListener;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Medoles.Category;
import com.app.hotfresh.Medoles.Item;
import com.app.hotfresh.R;
import com.bumptech.glide.Glide;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.List;


public class AdapterOfferHome extends PagerAdapter {
    private List<Item> mylist;
    Context mContext;
    private LayoutInflater mInflater;
    private OnItemClickListener onItemClickListener;

    public AdapterOfferHome(Context context, List<Item> mylist, OnItemClickListener onItemClickListener) {
        this.mylist = mylist;
        mContext = context;
        this.onItemClickListener = onItemClickListener;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public void destroyItem(View container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }

    @Override
    public int getCount() {
        return mylist.size();
    }


    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View itemView = mInflater.inflate(R.layout.row_item_offer, view, false);
        FontManager.applyFont(mContext, itemView);
        TextView row_name = itemView.findViewById(R.id.row_name);
        ImageView img_item = itemView.findViewById(R.id.img_item);

        row_name.setText(mylist.get(position).getName() + "");
        row_name.setVisibility(View.GONE);
        String image = mylist.get(position).getImgPath();
        if (image != null) {
            Glide.with(mContext).load("" + image).into(img_item);
        }
        /**  if(position ==1){
         img_item.setImageResource(R.drawable.img_2);
         }else if(position ==2){
         img_item.setImageResource(R.drawable.img_3);
         }else if(position ==3){
         img_item.setImageResource(R.drawable.img_4);
         }else if(position ==4){
         img_item.setImageResource(R.drawable.img_5);
         }*/


        itemView.setOnClickListener(v -> {
            try {
                onItemClickListener.onItemClick(v, position);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        });
        view.addView(itemView);
        return itemView;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

}