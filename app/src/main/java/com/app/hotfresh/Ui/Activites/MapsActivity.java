package com.app.hotfresh.Ui.Activites;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import com.app.hotfresh.Manager.AppErrorsManager;
import com.app.hotfresh.Manager.AppLanguage;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Manager.RootManager;
import com.app.hotfresh.R;
import com.app.hotfresh.WebService.RetrofitWebService;
import com.app.hotfresh.WebService.model.response.DistanceResponse;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    FrameLayout layout_back;
    TextView toolbarNameTxt;
    private GoogleMap mMap;
    private RelativeLayout Layout_notification;


    Double latitude = 0.0, longitude = 0.0;
    private FusedLocationProviderClient fusedLocationProviderClient = null;
    RequestManager requestManager;
    boolean IsLodeMore = false;
    int page = 1;
    private Handler handler;
    private Button btn_save;
    private FrameLayout layout_loading;


    private LinearLayout layout_action, layout_data;
    private TextView   TextFinalAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppLanguage.setContentLang(this);
        setContentView(R.layout.activity_maps);
        View layout = findViewById(R.id.layout);
        FontManager.applyFont(this, layout);
        layout_back = findViewById(R.id.layout_toolbar_menu);
        layout_back.setOnClickListener(v -> {
            onBackPressed();
        });
        handler = new Handler(Looper.getMainLooper());
        Layout_notification = findViewById(R.id.Layout_notification);
        Layout_notification.setVisibility(View.GONE);
        toolbarNameTxt = findViewById(R.id.toolbarNameTxt);
        if (!TextUtils.isEmpty(getIntent().getStringExtra("branchString")))
            branchString = getIntent().getStringExtra("branchString");

        layout_action = findViewById(R.id.layout_action);
        layout_data = findViewById(R.id.layout_data);
        layout_action.setVisibility(View.GONE);
        layout_data.setVisibility(View.GONE);


        TextFinalAmount = findViewById(R.id.TextFinalAmount);


        layout_loading = findViewById(R.id.layout_loading);
        layout_loading.setVisibility(View.GONE);
        btn_save = findViewById(R.id.btn_save);
        btn_save.setText("" + getResources().getString(R.string.Review));
        btn_save.setOnClickListener(v -> {
            if (TextUtils.equals(getResources().getString(R.string.save), btn_save.getText().toString())) {
                getIntent().putExtra("latitude", latitude);
                getIntent().putExtra("longitude", longitude);
                getIntent().putExtra("DeliveryAmount", AmountFormat);
                setResult(2, getIntent());
                finish();
            } else if (TextUtils.equals(getResources().getString(R.string.Review), btn_save.getText().toString())) {
                GetDistanceAndPrice(latitude.toString(), longitude.toString());
            }
        });

        toolbarNameTxt.setText("" + getResources().getString(R.string.Placesmap));
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        requestManager = Glide.with(this);


        registerForLocationUpdates();

    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    Activity#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for Activity#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(true);

        googleMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                addMarker(latLng);
            }
        });
        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                addMarker(latLng);
            }
        });

        mMap = googleMap;
        googleMap.getUiSettings().setMapToolbarEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        //  mMap.getUiSettings().setMyLocationButtonEnabled(true);

        GetLocationUser();
    }

    private LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            super.onLocationResult(locationResult);
            Location lastLocation = locationResult.getLastLocation();

            Log.e("locationdata", lastLocation.getLatitude() + " // " + lastLocation.getLongitude());
            longitude = lastLocation.getLongitude();
            latitude = lastLocation.getLatitude();

        }
    };

    private Marker mMarker;

    private void addMarker(final LatLng latLng) {
        mMap.clear();
        latitude = latLng.latitude;
        longitude = latLng.longitude;
        mMarker = mMap.addMarker(new MarkerOptions().position(latLng).title(getString(R.string.mylocation))
                .icon(BitmapDescriptorFactory.defaultMarker()).draggable(true));
        mMarker.showInfoWindow();
        btn_save.setVisibility(View.VISIBLE);
        btn_save.setText("" + getResources().getString(R.string.Review));
        layout_action.setVisibility(View.VISIBLE);
        layout_data.setVisibility(View.GONE);
    }

    @NonNull
    private FusedLocationProviderClient getFusedLocationProviderClient() {
        if (fusedLocationProviderClient == null) {
            fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        }
        return fusedLocationProviderClient;
    }

    @SuppressLint("MissingPermission")
    void registerForLocationUpdates() {
        FusedLocationProviderClient locationProviderClient = getFusedLocationProviderClient();
        LocationRequest locationRequest = LocationRequest.create();
        Looper looper = Looper.myLooper();
        locationProviderClient.requestLocationUpdates(locationRequest, locationCallback, looper);
    }


    public void GetLocationUser() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        PackageManager pm2 = getPackageManager();
        int hasPerm4 = pm2.checkPermission(Manifest.permission.ACCESS_FINE_LOCATION, getPackageName());
        if (hasPerm4 == PackageManager.PERMISSION_GRANTED) {
            fusedLocationProviderClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();

                                if (mMap != null) {
                                    LatLng sydney = new LatLng(latitude, longitude);
                                    mMap.addMarker(new MarkerOptions()
                                            .position(sydney)
                                            .icon(BitmapDescriptorFactory
                                                    .defaultMarker(BitmapDescriptorFactory.HUE_RED)));
                                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 12));
                                    btn_save.setVisibility(View.VISIBLE);
                                    layout_action.setVisibility(View.VISIBLE);
                                    layout_data.setVisibility(View.GONE);
                                }
                            }
                        }
                    });

        } else {
            checkAndRequestPermissionsLocation();
        }

    }

    private void checkAndRequestPermissionsLocation() {
        String[] permissions = new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
        };
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(permission);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUST_CODE);
        }
    }

    static final int REQUST_CODE = 1000;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    GetLocationUser();
                }
                break;
        }
    }


    private String branchString = "hatt14";
    String AmountFormat = "0.0";

    private void GetDistanceAndPrice(String lat, String lng) {
        Log.e("GetUserDistanceMap", lat + " " + lng + " " + branchString);
        layout_loading.setVisibility(View.VISIBLE);
        RetrofitWebService.getService(this).GetUserDistanceMap(lat, lng, branchString).enqueue(new Callback<DistanceResponse>() {
            @Override
            public void onResponse(Call<DistanceResponse> call, Response<DistanceResponse> response) {
                Log.e("GetUserDistanceMap", response.toString());
                if (RootManager.RESPONSE_CODE_OK == response.code()) {
                    if (TextUtils.equals("True", response.body().getStatus())) {

                        layout_data.setVisibility(View.VISIBLE);
                        layout_action.setVisibility(View.VISIBLE);
                        btn_save.setVisibility(View.VISIBLE);
                        btn_save.setText("" + getResources().getString(R.string.save));
                      /*  String AmountPeerKilo = response.body().getAmountPeerKilo();
                        String Distance = response.body().getDistance();*/
                        String Amount = response.body().getFinalAmount();


                        /**  String AmountPeerKiloFormat = String.format(Locale.ENGLISH, "%.2f", Double.parseDouble(AmountPeerKilo));
                         String DistanceFormat = String.format(Locale.ENGLISH, "%.2f", Double.parseDouble(Distance));
                         */
                        AmountFormat = String.format(Locale.ENGLISH, "%.2f", Double.parseDouble(Amount));

                        /**    TextAmountPeerKilo.setText("" + AmountPeerKiloFormat + " " + getResources().getString(R.string.SAR));
                         TextDistance.setText("" + DistanceFormat + " " + getResources().getString(R.string.Kilo));
                         */
                        TextFinalAmount.setText("" + AmountFormat + " " + getResources().getString(R.string.SAR));


                    } else if (TextUtils.equals("False", response.body().getStatus())) {
                        AppErrorsManager.showCustomErrorDialog(MapsActivity.this, getResources().getString(R.string.error),
                                response.body().getMessage() + "");
                    }
                } else {
                    AppErrorsManager.showCustomErrorDialog(MapsActivity.this, getResources().getString(R.string.Failure),
                            getResources().getString(R.string.InternalServerError));
                }

                if (response.errorBody() != null) {
                    try {
                        Log.e("GetWalletBalance", " error code: " + response.code() + " error content: " + response.errorBody().string().toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                layout_loading.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<DistanceResponse> call, Throwable t) {
                AppErrorsManager.showCustomErrorDialog(MapsActivity.this, getResources().getString(R.string.Failure),
                        t.getMessage() + "");
                layout_loading.setVisibility(View.GONE);
            }
        });
    }

}
