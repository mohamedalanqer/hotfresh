package com.app.hotfresh.Ui.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.app.hotfresh.CallBack.OnItemClickTagListener;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Medoles.Category;
import com.app.hotfresh.Medoles.Item;
import com.app.hotfresh.R;
import com.app.hotfresh.Ui.Activites.ListCartActivity;
import com.bumptech.glide.Glide;

import org.json.JSONException;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Locale;


public class RecyclerItems extends RecyclerView.Adapter<RecyclerItems.CustomView> {
    Context context;
    String nameView;
    List<Item> mylist;
    private OnItemClickTagListener listener;
    int Layout;


    public class CustomView extends RecyclerView.ViewHolder {
        TextView row_name, row_text_price, row_text_service, row_text_add_cart, text_count_cart, row_text_total;
        ImageView row_img_item, img_plus, img_minus, img_gift;
        ImageView img_item_remove;
        private FrameLayout layout_points;
        private TextView text_points;
        private LinearLayoutCompat layout_offer_gift;

        public CustomView(View v) {
            super(v);
            FontManager.applyFont(context, v);
            row_name = v.findViewById(R.id.row_name);
            row_text_price = v.findViewById(R.id.row_text_price);
            row_text_service = v.findViewById(R.id.row_text_service);
            text_count_cart = v.findViewById(R.id.text_count_cart);
            row_text_add_cart = v.findViewById(R.id.row_text_add_cart);
            row_img_item = v.findViewById(R.id.row_img_item);
            img_plus = v.findViewById(R.id.img_plus);
            img_minus = v.findViewById(R.id.img_minus);
            row_text_total = v.findViewById(R.id.row_text_total);

            img_item_remove = v.findViewById(R.id.img_item_remove);

            layout_points = v.findViewById(R.id.layout_points);
            text_points = v.findViewById(R.id.text_points);
            img_gift = v.findViewById(R.id.img_gift);
            layout_offer_gift = v.findViewById(R.id.layout_offer_gift);

        }

    }

    public RecyclerItems() {
    }

    public RecyclerItems(Context context, String nameView, List<Item> mylist, int Layout, OnItemClickTagListener listener) {
        this.context = context;
        this.mylist = mylist;
        this.listener = listener;
        this.Layout = Layout;
        this.nameView = nameView;
    }

    @Override
    public CustomView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(Layout, parent, false);
        CustomView viewholder = new CustomView(view);
        return viewholder;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public void onBindViewHolder(final CustomView holder, final int position) {
        holder.row_name.setText(mylist.get(position).getName() + "");
        holder.row_text_price.setText(mylist.get(position).getTotal() + " " + context.getResources().getString(R.string.SAR));
        holder.text_count_cart.setText("" + mylist.get(position).getQuantity());

        holder.layout_offer_gift.setVisibility(View.GONE);
        holder.layout_points.setVisibility(View.GONE);
        if (mylist.get(position).getOfferPoints() == null || TextUtils.equals(mylist.get(position).getOfferPoints(), "0") || TextUtils.isEmpty(mylist.get(position).getOfferPoints())) {
            if (mylist.get(position).getPoints() != null) {
                if (TextUtils.equals(mylist.get(position).getPoints(), "0") || TextUtils.isEmpty(mylist.get(position).getPoints())) {
                    holder.layout_points.setVisibility(View.GONE);
                } else {
                    holder.layout_points.setVisibility(View.VISIBLE);
                    holder.text_points.setText(mylist.get(position).getPoints() + "");
                }
            } else {
                holder.layout_points.setVisibility(View.GONE);

            }

        } else {
            holder.layout_offer_gift.setVisibility(View.VISIBLE);
            holder.img_gift.getBackground().setColorFilter(context.getResources().getColor(R.color.colorGiftOffer), PorterDuff.Mode.MULTIPLY);
            holder.text_points.getBackground().setColorFilter(context.getResources().getColor(R.color.colorGiftOffer), PorterDuff.Mode.SRC_IN);
            holder.layout_points.setVisibility(View.VISIBLE);
            holder.img_gift.setImageResource(R.drawable.ic_gift);
            String msgHide = "";
            if (!TextUtils.equals(mylist.get(position).getPoints(), "0") || !TextUtils.isEmpty(mylist.get(position).getPoints())) {
                msgHide = " "+context.getResources().getString(R.string.MsgHidden) +"<s>"+mylist.get(position).getPoints()+"</s> " ;
                  holder.text_points.setText(Html.fromHtml("<b>"+mylist.get(position).getTotalPoints() + "</b><span>"+msgHide +"</span>"));
            }else {
                holder.text_points.setText(mylist.get(position).getTotalPoints() + "" );
            }
         }


        if (TextUtils.equals("CartList", nameView)) {
            holder.row_text_add_cart.getBackground().setColorFilter(context.getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
            holder.img_plus.getBackground().setColorFilter(context.getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
            holder.img_minus.getBackground().setColorFilter(context.getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);

            if (mylist.get(position).getItemServices().size() > 0) {
                holder.row_text_service.setText("" + context.getResources().getString(R.string.ListService));
                holder.row_text_service.setTextColor(context.getResources().getColor(R.color.colorWhite));
                holder.row_text_service.getBackground().setColorFilter(context.getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
            } else {
                holder.row_text_service.setText("" + context.getResources().getString(R.string.OtherService));
                holder.row_text_service.setTextColor(context.getResources().getColor(R.color.colorBlack));
                holder.row_text_service.getBackground().setColorFilter(context.getResources().getColor(R.color.colorGreyLight), PorterDuff.Mode.SRC_ATOP);

            }
            holder.img_item_remove.setOnClickListener(view -> {
                try {
                    listener.onItemClick(view, position, "remove");
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            });
        } else {
            if (mylist.get(position).getQuantity() > 0) {
                holder.row_text_add_cart.getBackground().setColorFilter(context.getResources().getColor(R.color.colorGreen), PorterDuff.Mode.SRC_ATOP);
                holder.img_plus.getBackground().setColorFilter(context.getResources().getColor(R.color.colorGreen), PorterDuff.Mode.SRC_ATOP);
                holder.img_minus.getBackground().setColorFilter(context.getResources().getColor(R.color.colorGreen), PorterDuff.Mode.SRC_ATOP);
            } else {
                holder.row_text_add_cart.getBackground().setColorFilter(context.getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
                holder.img_plus.getBackground().setColorFilter(context.getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
                holder.img_minus.getBackground().setColorFilter(context.getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
            }

            if (mylist.get(position).getItemServices().size() > 0) {
                holder.row_text_service.setText("" + context.getResources().getString(R.string.ListService));
                holder.row_text_service.setTextColor(context.getResources().getColor(R.color.colorWhite));
                holder.row_text_service.getBackground().setColorFilter(context.getResources().getColor(R.color.colorGreen), PorterDuff.Mode.SRC_ATOP);
            } else {
                holder.row_text_service.setText("" + context.getResources().getString(R.string.OtherService));
                holder.row_text_service.setTextColor(context.getResources().getColor(R.color.colorBlack));
                holder.row_text_service.getBackground().setColorFilter(context.getResources().getColor(R.color.colorGreyLight), PorterDuff.Mode.SRC_ATOP);

            }
        }


        String price = mylist.get(position).getTotal();
        if (!TextUtils.isEmpty(price)) {
            Float price_int = Float.parseFloat("" + price);
            if (price_int > 0) {
                if (mylist.get(position).getQuantity() > 0) {
                    String totalAsString = String.format(Locale.ENGLISH, "%.2f", price_int * mylist.get(position).getQuantity());
                    //  holder.row_text_total.setText("" + totalAsString + " " + context.getResources().getString(R.string.total));
                    holder.row_text_total.setText(context.getResources().getString(R.string.total) + " " + totalAsString);
                } else {
                    holder.row_text_total.setText("" + context.getResources().getString(R.string.total));
                }
            }
        }
        if (mylist.get(position).getImgPath() != null) {
            Glide.with(context).load(mylist.get(position).getImgPath()).into(holder.row_img_item);
        }
        holder.row_text_add_cart.setOnClickListener(view -> {
            try {
                listener.onItemClick(view, position, "add");
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        });
        holder.img_plus.setOnClickListener(view -> {
            try {
                listener.onItemClick(view, position, "plus");
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        });
        holder.row_text_service.setOnClickListener(view -> {
            try {
                listener.onItemClick(view, position, "service");
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        });
        holder.img_minus.setOnClickListener(view -> {
            try {
                listener.onItemClick(view, position, "minus");
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        });


    }


    @Override
    public int getItemCount() {
        return mylist.size();
    }


}

