package com.app.hotfresh.Ui.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.app.hotfresh.CallBack.InstallCallback;
import com.app.hotfresh.CallBack.InternetAvailableCallback;
import com.app.hotfresh.Manager.AppErrorsManager;
import com.app.hotfresh.Manager.FontManager;
import com.app.hotfresh.Manager.InternetConnectionUtils;
import com.app.hotfresh.Manager.PinEditText;
import com.app.hotfresh.Manager.RootManager;
import com.app.hotfresh.R;
import com.app.hotfresh.Ui.Activites.ChangePasswordActivity;
import com.app.hotfresh.Ui.Activites.SplashActivity;
import com.app.hotfresh.WebService.RetrofitWebService;
import com.app.hotfresh.WebService.model.response.RootResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPasswrodFragment extends Fragment {
    private TextView text_name_fragment;
    private ImageView img_logo;
    private FrameLayout layout_loading;

    private String mobile_intent = "";
    private EditText editTextMobile, edittext_new_password, edittext_re_password;
    private PinEditText pinEntry;
    private Button btn_action;
    private Handler handler;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_reset_passwrod, container, false);
        handler = new Handler(Looper.getMainLooper());
        FontManager.applyFont(getActivity(), view);
        initSetUp(view);
        return view;
    }

    private void initSetUp(View view) {

        text_name_fragment = view.findViewById(R.id.text_name_fragment);
        img_logo = getActivity().findViewById(R.id.img_logo);
        img_logo.setImageResource(R.drawable.ic_reset_pass);
        text_name_fragment.setText("" + getResources().getString(R.string.Restore_password));
        pinEntry = view.findViewById(R.id.txt_pin_entry);
        editTextMobile = view.findViewById(R.id.editTextMobile);
        layout_loading = getActivity().findViewById(R.id.layout_loading);
        btn_action = view.findViewById(R.id.btn_action);
        edittext_new_password = view.findViewById(R.id.edittext_new_password);
        edittext_re_password = view.findViewById(R.id.edittext_re_password);
        layout_loading.setOnClickListener(view1 -> {
            return;
        });
        if (getArguments() != null)
            mobile_intent = getArguments().getString("mobile");

        if (!TextUtils.isEmpty(mobile_intent)) {
            editTextMobile.setVisibility(View.VISIBLE);
            editTextMobile.setText(mobile_intent);
            AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                    "تم إرسال رمز التحقق الي رقم الموبايل : " + mobile_intent);
        }

        if (pinEntry != null) {
            pinEntry.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
                @Override
                public void onPinEntered(CharSequence str) {

                }
            });
        }
        pinEntry.setOnClickListener(view1 -> {
            pinEntry.setText(null);
        });

        btn_action.setOnClickListener(view1 -> {
            ResetPasswordWebService();
        });
    }


    private void ResetPasswordWebService() {
        String password = edittext_new_password.getText().toString().trim();
        String password_confirmation = edittext_re_password.getText().toString().trim();
        String code = pinEntry.getText().toString().trim();
        String mobile = editTextMobile.getText().toString().trim();


        if (mobile.length() == 10 && !mobile.startsWith("05")) {
            AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                    getResources().getString(R.string.mobilenumberdigits));
        } else if (mobile.length() > 9 && mobile.length() != 10) {
            if (mobile.startsWith("00966")) {
                AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                        getResources().getString(R.string.Dont_put_countrycode_mobile) + "\n" + "00966");
            } else if (mobile.startsWith("966")) {
                AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                        getResources().getString(R.string.Dont_put_countrycode_mobile) + "\n" + "966");

            } else if (mobile.startsWith("+966")) {
                AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                        getResources().getString(R.string.Dont_put_countrycode_mobile) + "\n" + "+966");

            } else {
                AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.info),
                        getResources().getString(R.string.mobilenumberdigits));

            }
            editTextMobile.setError(getResources().getString(R.string.field_required));
            editTextMobile.requestFocus();
        } else if (TextUtils.isEmpty(password)) {
            edittext_new_password.setError(getString(R.string.field_required));
            edittext_new_password.requestFocus();
        } else if (TextUtils.isEmpty(password_confirmation)) {
            edittext_re_password.setError(getString(R.string.field_required));
            edittext_re_password.requestFocus();
        } else if (TextUtils.isEmpty(code)) {
            pinEntry.setError(getString(R.string.field_required));
            pinEntry.requestFocus();
        } else {
            if (mobile.length() == 10 && mobile.startsWith("05")) {
                mobile = mobile.substring(1);
            }
            mobile = "966" + mobile;
            Log.e("mobile", mobile);
            String finalMobile = mobile;
            layout_loading.setVisibility(View.VISIBLE);
            RetrofitWebService.getService(getActivity()).ResetPassword(finalMobile, password, password_confirmation, code).enqueue(new Callback<RootResponse>() {
                @Override
                public void onResponse(Call<RootResponse> call, Response<RootResponse> response) {
                    Log.e("eeeeee", response.toString() + " " + finalMobile + " " + password + " " + " " + code);
                    if (RootManager.RESPONSE_CODE_OK == response.code()) {
                        if (TextUtils.equals("True", response.body().getStatus())) {
                            if (response.body() != null) {
                                AppErrorsManager.showCustomErrorDialogNotCancel(getActivity(), getResources().getString(R.string.info),
                                        response.body().getMessage(), new InstallCallback() {
                                            @Override
                                            public void onStatusDone(String status) {
                                                GoToLoginActivity();
                                            }
                                        });
                            } else {
                                AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.error),
                                        response.body().getMessage());
                            }
                        } else if (TextUtils.equals("False", response.body().getStatus())) {
                            AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.error),
                                    response.body().getMessage());
                        }
                    } else {
                        AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.Failure),
                                getResources().getString(R.string.InternalServerError));
                    }
                    layout_loading.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<RootResponse> call, Throwable t) {
                    layout_loading.setVisibility(View.GONE);
                    InternetConnectionUtils.isInternetAvailable(getActivity(), new InternetAvailableCallback() {
                        @Override
                        public void onInternetAvailable(boolean isAvailable) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (isAvailable) {

                                        AppErrorsManager.showCustomErrorDialog(getActivity(), getResources().getString(R.string.Failure),
                                                t.getMessage() + "");
                                    } else {
                                        AppErrorsManager.InternetUnAvailableDialog(getActivity(), getResources().getString(R.string.Failure),
                                                getResources().getString(R.string.nointernetconnection), new InstallCallback() {
                                                    @Override
                                                    public void onStatusDone(String status) {
                                                        if (TextUtils.equals("retry", status)) {
                                                            ResetPasswordWebService();
                                                        }
                                                    }
                                                });
                                    }
                                }
                            });
                        }
                    });
                }
            });
        }


    }

    private void GoToLoginActivity() {
        Intent intent = new Intent(getActivity(), SplashActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

}