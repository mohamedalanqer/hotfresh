package com.app.hotfresh.CallBack;

import android.content.Intent;

public interface SmsBroadcastReceiverListener {
    void onSuccess(Intent intent);
    void onFailure();
}
