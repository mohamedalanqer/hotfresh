package com.app.hotfresh.CallBack;

public interface InternetAvailableCallback {

	void onInternetAvailable(boolean isAvailable);
}
