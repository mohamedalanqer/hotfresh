package com.app.hotfresh.DataBase;

import android.database.sqlite.SQLiteConstraintException;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.app.hotfresh.Medoles.Item;
import com.app.hotfresh.Medoles.User;

import java.util.List;

@Dao
public interface ItemDao {
    @Query("SELECT * FROM Item")
    List<Item> getAll();

    @Query("SELECT COUNT(*) FROM Item")
    LiveData<Integer> getCount();

    @Query("SELECT * FROM Item WHERE Code IN (:itemIds)")
    List<Item> loadAllByIds(int[] itemIds);


    //AND " +
    //     "last_name LIKE :last
    @Query("SELECT * FROM Item WHERE Code LIKE :code  LIMIT 1")
    Item findByCode(String code);


    @Query("DELETE FROM Item WHERE Code = :code")
    void deleteById(String code);

    @Insert
    void insertAll(Item... items);

    @Update
    void updateAll(Item... items);

    @Delete
    void deleteAll(Item... items);
    @Query("DELETE From Item")
    void DeletgetAll();
    @Delete
    void delete(Item item);

}
