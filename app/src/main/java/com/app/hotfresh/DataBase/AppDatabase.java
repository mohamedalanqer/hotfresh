package com.app.hotfresh.DataBase;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.app.hotfresh.Medoles.Charities.CharitiesItem;
import com.app.hotfresh.Medoles.Item;
import com.app.hotfresh.Medoles.ItemService;

@Database(entities = {Item.class, ItemService.class , CharitiesItem.class}, version = 3)
public abstract class AppDatabase extends RoomDatabase {
    public abstract ItemDao itemDao();

    public abstract ItemServiceDao itemServiceDao();
    public abstract CharitiesItemDao charitiesItemDao();
}