package com.app.hotfresh.DataBase;

import android.util.Log;

import com.app.hotfresh.Medoles.Charities.CharitiesItem;
import com.app.hotfresh.Medoles.Item;
import com.app.hotfresh.Medoles.ItemService;

import java.util.List;

public class InsertUpdateDeleteItem {
    public static void upsert_Item(AppDatabase db, Item item) {
        if (db.itemDao().findByCode(item.getCode()) != null) {
            Log.e("getQuantity", item.getQuantity() + " F");
            if (item.getQuantity() == 0) {
                db.itemDao().deleteById(item.getCode());
            } else {
                db.itemDao().updateAll(item);
            }
        } else {
            db.itemDao().insertAll(item);
        }
    }

    public static void upsert_ItemService(AppDatabase db, List<ItemService> itemServices) {
        for (int i = 0; i < itemServices.size(); i++) {
            Log.e("itemServices", itemServices.get(i).getServiceCode() + " F");
            if (db.itemServiceDao().findByCode(itemServices.get(i).getItemCode()) != null) {
               // if (!itemServices.get(i).isIs_selected()) {
                    db.itemServiceDao().insertAll(itemServices.get(i));
             //   } else {
                //    db.itemServiceDao().delete(itemServices.get(i));
               // }
            } else {
                db.itemServiceDao().insertAll(itemServices.get(i));
            }

        }
    }

    public static void deleteAll_ItemService(AppDatabase db) {
        db.itemServiceDao().deleteAll();
    }

    public static void deleteByItemService(AppDatabase db, String itemCode) {
        if (db.itemServiceDao().findByCode(itemCode) != null) {
            db.itemServiceDao().deleteById(itemCode);

        }
    }

    public static void upsert_CharitiesItem(AppDatabase db, CharitiesItem item) {
        if (db.charitiesItemDao().findByCode(item.getCode()) != null) {
            Log.e("getQuantity", item.getQuantity() + " F");
            if (item.getQuantity() == 0) {
                db.charitiesItemDao().deleteById(item.getCode());
            } else {
                db.charitiesItemDao().updateAll(item);
            }
        } else {
            db.charitiesItemDao().insertAll(item);
        }
    }

}
