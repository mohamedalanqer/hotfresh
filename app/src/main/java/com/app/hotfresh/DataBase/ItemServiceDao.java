package com.app.hotfresh.DataBase;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.app.hotfresh.Medoles.Item;
import com.app.hotfresh.Medoles.ItemService;

import java.util.List;

@Dao
public interface ItemServiceDao {
    @Query("SELECT * FROM ItemService")
    List<ItemService> getAll();

    @Query("SELECT COUNT(*) FROM ItemService")
    LiveData<Integer> getCount();

    @Query("SELECT * FROM ItemService WHERE ItemCode IN (:itemIds)")
    List<ItemService> loadAllByIds(int[] itemIds);
    //AND " +
    //     "last_name LIKE :last
    @Query("SELECT * FROM ItemService WHERE ItemCode LIKE :code")
    List<ItemService> findByCode(String code);


    @Query("DELETE FROM ItemService WHERE ItemCode = :code ")
    void deleteById(String code);


    @Query("DELETE FROM ItemService")
    void deleteAll();

    @Insert
    void insertAll(ItemService... items);

    @Update
    void updateAll(ItemService... items);

    @Delete
    void deleteAll(ItemService... items);


    @Delete
    void delete(ItemService item);

}
