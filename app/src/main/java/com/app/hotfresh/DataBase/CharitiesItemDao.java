package com.app.hotfresh.DataBase;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.app.hotfresh.Medoles.Charities.CharitiesItem;
import com.app.hotfresh.Medoles.Item;

import java.util.List;

@Dao
public interface CharitiesItemDao {
    @Query("SELECT * FROM CharitiesItem")
    List<CharitiesItem> getAll();

    @Query("SELECT COUNT(*) FROM CharitiesItem")
    LiveData<Integer> getCount();

    @Query("SELECT * FROM CharitiesItem WHERE Code IN (:itemIds)")
    List<CharitiesItem> loadAllByIds(int[] itemIds);


    //AND " +
    //     "last_name LIKE :last
    @Query("SELECT * FROM CharitiesItem WHERE Code LIKE :code  LIMIT 1")
    CharitiesItem findByCode(String code);


    @Query("DELETE FROM CharitiesItem WHERE Code = :code")
    void deleteById(String code);

    @Insert
    void insertAll(CharitiesItem... items);

    @Update
    void updateAll(CharitiesItem... items);

    @Delete
    void deleteAll(CharitiesItem... items);

    @Query("DELETE From CharitiesItem")
    void DeletgetAll();

    @Delete
    void delete(CharitiesItem item);

}
